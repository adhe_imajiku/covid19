<?php
$column_breadcrumb   = $breadcrumb;
$column_banner_title = $result_banner['post_title'];
$column_banner_image = "{$result_banner['file_location']}{$result_banner['file_original']}";
if (file_exists($column_banner_image)) {
    $column_banner_image = base_url($column_banner_image);
} else {
    /** SET IMAGE DEFAULT IF NOT FOUND */
    $column_banner_image = '';
}
?>
<section class="banner-top">
    <div class="banner-top-title">
        <div class="container">
            <div class="breadcrumb-box">
                <h1><?php echo $column_banner_title; ?></h1>
                <?php if (!empty($column_breadcrumb)) { ?>
                    <ol class="breadcrumb">
                        <?php foreach ($column_breadcrumb as $index => $row) { ?>
                            <?php
                            $column_breadcrumb_title = $row['title'];
                            $column_breadcrumb_link  = $row['link'];
                            ?>
                            <?php if (!empty($column_breadcrumb_title)) { ?>
                                <?php if (empty($column_breadcrumb_link)) { ?>
                                    <li class="breadcrumb-item active"><?php echo $column_breadcrumb_title; ?></li>
                                <?php } else { ?>
                                    <li class="breadcrumb-item"><a href="<?php echo $column_breadcrumb_link; ?>"><?php echo $column_breadcrumb_title; ?></a></li>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    </ol>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-5 col-lg-5 col-xl-5 no-padding">
                <div class="banner-top-cover-left"></div>
            </div>
            <div class="col-sm-12 col-md-7 col-lg-7 col-xl-7 no-padding">
                <div class="banner-top-cover-right">
                    <figure>
                        <img src="<?php echo $column_banner_image; ?>" alt="<?php echo $column_banner_title; ?>"/>
                    </figure>
                </div>
                <div class="banner-top-cover-slogan-border"></div>
                <?php if (!empty($column_banner_title_sub)) { ?>
                    <div class="banner-top-cover-slogan-text">
                        <div class="banner-top-cover-slogan">
                            <h3><?php echo $column_banner_title_sub; ?></h3>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
