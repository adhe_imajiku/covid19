<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_front extends MX_Controller
{
    protected $title              = '';
    protected $meta               = [
        'meta_author'      => '',
        'meta_keywords'    => '',
        'meta_description' => '',
        'meta_title'       => '',
        'meta_image'       => '',
        'meta_image_alt'   => '',
        'meta_url'         => '',
    ];
    protected $sess_user          = [];
    protected $cart               = 0;
    protected $base_config        = [];
    protected $file_css           = '';
    protected $file_js            = '';
    protected $base_model         = 'front_model';
    protected $table_lang         = 'acs_lang';
    protected $table_lang_pk      = 'id';
    protected $table_lang_id      = 'acs_lang_id';
    protected $lang_active        = [];
    protected $lang_default       = [];
    protected $module_config      = [];
    protected $menu_nav           = [];
    protected $content_nav        = [];
    protected $content_news_event = [];
    protected $content_check      = [];
    protected $content_menu_news  = [];
    protected $user_group         = [1];
    protected $limit_default      = 9;
    protected $post_model         = 'Post_model';
    protected $cookie_id          = '';
    protected $breadcrumb         = array(
        0 => array(
            'title' => '',
            'link'  => '',
        ),
        1 => array(
            'title' => '',
            'link'  => '',
        ),
        2 => array(
            'title' => '',
            'link'  => '',
        ),
        3 => array(
            'title' => '',
            'link'  => '',
        )
    );


    /* VARIABLE USE ON CREATE MENU RECURSIVE ON ADMINS */
    private $config_menu_pages_id = '1911135DCBA3076C21B';

    /* VARIABLE USE ON CREATE MENU RECURSIVE ON ADMINS */
    protected $config_menu = [
        'table'    => 'menu',
        'table_id' => 'id',

        'table_related'    => 'menu_lang',
        'table_related_id' => 'id',
        'table_related_fk' => 'menu_id',

        'table_lang'    => 'acs_lang',
        'table_lang_id' => 'id',
        'table_lang_fk' => 'acs_lang_id',

        'table_post'    => 'post',
        'table_post_id' => 'id',
        'table_post_fk' => 'menu_id',

        'table_post_lang'    => 'post_lang',
        'table_post_lang_id' => 'id',
        'table_post_lang_fk' => 'post_id',

        'table_column_status_featured' => 'is_featured',
        'table_column_status_default'  => 'status_default',
        'table_column_status_mode'     => 'status_mode',
        'table_column_parent'          => 'parent',
        'table_column_title'           => 'title',
        'table_column_slug'            => 'slug',
        'table_column_ordering'        => 'ordering',
        'table_column_link'            => 'menu_site_link',

        'table_filter_ordering'    => 'filter_ordering',
        'table_filter_parent'      => 'filter_parent',
        'table_filter_shortdesc'   => 'filter_shortdesc',
        'table_filter_description' => 'filter_description',
        'table_filter_image'       => 'filter_image',
        'table_filter_tags'        => 'filter_tags',

        'table_init'           => 'a',
        'table_related_init'   => 'b',
        'table_lang_init'      => 'c',
        'table_post_init'      => 'd',
        'table_post_lang_init' => 'e',

    ];

    protected $csrf    = [
        'name' => '',
        'hash' => '',
    ];
    protected $csrf_js = [];
    protected $size    = [
        'original'  => 1000,
        'thumbnail' => 220
    ];

    public function __construct()
    {
        parent::__construct();
        $this->db->query("SET SESSION sql_mode = ''");
        $this->load->helper('cookie');
        $this->load->library('pagination');
        $this->load->model($this->base_model);
        $this->load->model($this->post_model);

        $this->set_language();
        $this->is_logged_in();
        $this->set_config();

        $this->meta['meta_url'] = current_url();

        $this->breadcrumb[0]['title'] = 'Home';
        $this->breadcrumb[0]['link']  = base_url();


        $this->csrf    = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        ];
        $this->csrf_js = json_encode((object)[$this->security->get_csrf_token_name() => $this->security->get_csrf_hash()]);

    }

    private function is_logged_in()
    {
        if ($this->ion_auth->logged_in()){
            if(in_array($this->ion_auth->get_users_groups()->row()->id,$this->user_group)){
                $this->ion_auth->logout();
            }else{
                $newdata=[
                    'user_data'=>$this->ion_auth->user()->row()
                ];
                $this->session->set_userdata($newdata);
                $parameter_result       = [$this->lang_default['id'],'1912185DF982AF5AE59',$this->ion_auth->user()->row_array()['id']];
                $this->cart                   = $this->{$this->base_model}->get_cart($parameter_result);
            }
        }
    }

    public function set_pagination($parameter_total_rows = null, $parameter_url_config = null, $parameter_limit_perpage = null)
    {
        $config['base_url']   = $parameter_url_config;
        $config['total_rows'] = $parameter_total_rows;
        $config['per_page']   = $parameter_limit_perpage;
        $config['suffix']     = '?' . http_build_query($_GET, '', "&");

//        echo '<pre>';
//        var_dump($config);
//        die();
        $config['use_page_numbers'] = TRUE;
        $config['attributes']       = array('class' => 'page-link');

        $config['full_tag_open']  = '<nav aria-label="pagination" class="mt-4">' .
                                    '<ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</nav>' .
                                    '</ul>';

        $config['first_link']      = 'First Page';
        $config['first_tag_open']  = '<li class="page-item">';
        $config['first_tag_close'] = '</li>';

        $config['last_link']      = 'Last Page';
        $config['last_tag_open']  = '<li class="page-item">';
        $config['last_tag_close'] = '</li>';

        $config['next_link']      = 'Next Page';
        $config['next_tag_open']  = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['prev_link']      = 'Prev Page';
        $config['prev_tag_open']  = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['cur_tag_open']  = '<li class="page-item active">' .
                                   '<span class="page-link">';
        $config['cur_tag_close'] = '</span>' .
                                   '</li>';

        $config['num_tag_open']  = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';

        $this->pagination->initialize($config);
    }
    private function set_config()
    {
        $get_config = $this->{$this->base_model}->get_group_config();
        foreach ($get_config as $row_config) {
            if (!empty($this->lang_default)) {
                if ($this->lang_default["id"] == 'LGEN') {
                    $this->base_config[$row_config['config_name']] = array(
                        'value'         => $row_config['config_value'],
                        'desc'          => $row_config['config_desc'],
                        'file_location' => $row_config['file_location'],
                        'file_thumb'    => $row_config['file_thumb'],
                        'file_original' => $row_config['file_original']
                    );
                } else {
                    $this->base_config[$row_config['config_name']] = array(
                        'value'         => $row_config['config_value_id'],
                        'desc'          => $row_config['config_desc'],
                        'file_location' => $row_config['file_location'],
                        'file_thumb'    => $row_config['file_thumb'],
                        'file_original' => $row_config['file_original']
                    );
                }
            } else {
                $this->base_config[$row_config['config_name']] = array(
                    'value'         => $row_config['config_value'],
                    'desc'          => $row_config['config_desc'],
                    'file_location' => $row_config['file_location'],
                    'file_thumb'    => $row_config['file_thumb'],
                    'file_original' => $row_config['file_original']
                );
            }
        }
    }

    private function set_language()
    {
        $lang_force='';
        $uri = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $explode_uri=explode('/',str_replace(base_url(),'',$uri));
        if(!empty($explode_uri[0])){
            $lang_force=str_replace('.html','',strtolower($explode_uri[0]));
        }else{
            $get_default = $this->{$this->base_model}->get_language_default();
            $lang_force=str_replace('.html','',strtolower($get_default['lang']));
            redirect(site_url($lang_force));
        }
        $get = $this->{$this->base_model}->get_language_active(1);
        foreach ($get as $index => $row) {
            $this->lang_active[] = $row;
            if (strtolower($row['lang']) == strtolower($lang_force)) {
                $this->lang_default = $row;
            }
        }
    }

    protected function set_output($output_mode = 'json', $output, $id = null)
    {
        if ($output_mode == 'json') {
            return $this->output->set_output(json_encode($output));
        } else {
            $this->session->set_flashdata('output', $output);
            return $id;
        }
    }

    protected function set_form_validaton_error()
    {
        if ($this->session->flashdata('output') == '') {
            $output = $this->output_message('alert', 'error', [
                'status'  => false,
                'message' => validation_errors()
            ]);
            $this->session->set_flashdata('output', $output);
        }
    }
    
    protected function generate_new_id_string()
    {
        return strtoupper(date('ymd') . uniqid());
    }

    protected function is_ajax()
    {
        if (!$this->input->is_ajax_request()) {
            $output = [
                'status'  => false,
                'message' => 'No access allowed'
            ];
            return $this->output->set_output(json_encode($output));
        }
    }

    protected function load_css($address = null, $external = false)
    {
        if ($external) {
            $this->file_css .= '<link rel="stylesheet" type="text/css" href="' . $address . '"/>';
        } else {
            $this->file_css .= '<link rel="stylesheet" type="text/css" href="' . ($address) . '"/>';
        }
    }

    protected function load_js($address = null, $external = false)
    {
        if ($external) {
            $this->file_js .= '<script type="text/javascript" src="' . $address . '"></script>';
        } else {
            $this->file_js .= '<script type="text/javascript" src="' . ($address) . '"></script>';
        }
    }

    protected function output_message($mode = 'alert', $mode_output = 'success', $message = null, $message_header = 'Success', $action = null)
    {
        if ($message == '') {
            return '';
        }

        $string = [
            'status'  => isset($message['status']) ? $message['status'] : false,
            'message' => isset($message['message']) ? $message['message'] : ''
        ];
        if ($action != '') {
            $message_header    = ucwords($message_header);
            $message_output    = ($string['status']) ? 'successfully' : 'failed to';
            $string['message'] = "<b>{$message_header}</b> {$message_output}";
            switch ($action) {
                case 'add':
                    $string['message'] .= ' added';
                    break;
                case 'edit':
                    $string['message'] .= ' edited';
                    break;
                case 'delete':
                    $string['message'] .= ' deleted';
                    break;
                case 'status':
                    $string['message'] = ' update status';
                    break;
                case 'check':
                    $message_output    = (!$string['status']) ? 'still used' : 'not used';
                    $string['message'] = "<b>{$message_header}</b> {$message_output} in other data";
                    break;
            }
        }

        if ($string['message'] == '') {
            return '';
        }

        if ($mode == 'alert') {
            $string = "<div class='alert alert-{$mode_output}'><h4>{$message_header}</h4>{$string['message']}</div>";
        } else if ($mode == 'text') {
            $string = "<p class='text-{$mode_output}'><b>{$message_header}</b>, {$string['message']}</p>";
        } else if ($mode == 'json') {
            /* No format for output json */
        }
        return $string;
    }

    /* CREATE MENU POST BY STATUS_MODE */
    protected function set_menu_navbar_by_mode($parent = 0, $result = '', $params = null, $status_featured = null)
    {
        $html = '';
        $icon = '';

        $result = $this->menu_by_parent_and_mode($parent, 1, $params, $status_featured, 'result');


        if (count($result) > 0) {
            foreach ($result as $index => $row) {
                $url_params = ($params == 'other') ? $row[$this->config_menu['table_column_slug']] : $params;
                $url_link   = site_url($row[$this->config_menu['table_column_link']]);
                $url_title  = $row[$this->config_menu['table_column_title']];

                $check = $this->menu_by_parent_and_mode($row[$this->config_menu['table_id']], 1, $params, $status_featured, 'result');
                $title = $row[$this->config_menu['table_column_title']];
                $slug  = $row[$this->config_menu['table_column_slug']];

                if (count($check) != 0) {
                    $html .= "<li class='nav-item dropdown'>";
                    $html .= "<a href='#' class='nav-link dropdown-toggle' id='dropdown_{$slug}' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'><i class='{$icon}'></i> {$title}</a>";
                    $html .= "<ul class='dropdown-menu' role='menu' aria-labelledby='dropdown_{$slug}'>";

                    $html .= $this->set_menu_navbar_by_mode($row[$this->config_menu['table_id']], $html, $params, $status_featured);

                    $html .= "</ul>";
                    $html .= "</li>";
                } else {
                    $html .= "<li class='nav-item'><a href='{$url_link}' class='dropdown-item' title='{$url_title}'>{$url_title}</a></li>";
                }
            }
        }
        return $html;
    }

    /* CREATE MENU POST BY STATUS_MODE */
    protected function set_menu_footer_by_mode($parent = 0, $result = '', $params = null, $status_featured = null)
    {
        $html = '';
        $icon = '';

        $result = $this->menu_by_parent_and_mode($parent, 1, $params, $status_featured, 'result');


        if (count($result) > 0) {
            foreach ($result as $index => $row) {
                $url_params = ($params == 'other') ? $row[$this->config_menu['table_column_slug']] : $params;
                $url_link   = site_url($row[$this->config_menu['table_column_link']]);
                $url_title  = $row[$this->config_menu['table_column_title']];

                $check = $this->menu_by_parent_and_mode($row[$this->config_menu['table_id']], 1, $params, $status_featured, 'result');
                $title = $row[$this->config_menu['table_column_title']];
                $slug  = $row[$this->config_menu['table_column_slug']];

                if ($parent == $this->config_menu_pages_id) {
                    if ($title != 'Careers') {
                        $html .= "<div class=\"col-12 col-sm-12 col-md-2 col-lg-2 col-xl-2\">";
                        $html .= "<ul class='foo-list-menu '>";
                    }
                }

                if (count($check) != 0) {
                    $html .= "<li>";
                    $html .= "<a href='#' title='{$title}'>{$title}</a>";
                    $html .= "<ul class='foo-list-submenu'>";

                    $html .= $this->set_menu_footer_by_mode($row[$this->config_menu['table_id']], $html, $params, $status_featured);

                    $html .= "</ul>";
                    $html .= "</li>";
                } else {
                    $html .= "<li><a href='{$url_link}' title='{$url_title}'>{$url_title}</a></li>";
                }

                if ($parent == $this->config_menu_pages_id) {
                    if ($title != 'FAQ') {
                        $html .= '</ul>';
                        $html .= "</div>";
                    }
                }


            }
        }

        return $html;
    }

    protected function menu_by_parent_and_mode($parent, $status_default, $status_mode, $status_featured, $output = 'result', $output_result = 'array')
    {
        $result = $this->{$this->base_model}->query_builder(
            "{$this->config_menu['table']} {$this->config_menu['table_init']}",
            "
            {$this->config_menu['table_init']}.{$this->config_menu['table_id']},
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']},
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_link']},
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_title']},
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']}
            ",
            [
                "{$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']}"              => $parent . '',
                "{$this->config_menu['table_lang_init']}.{$this->config_menu['table_column_status_default']}" => $status_default, /* DEFAULT STATUS ACTIVE */
                "{$this->config_menu['table_init']}.{$this->config_menu['table_column_status_mode']}"         => $status_mode,
                "{$this->config_menu['table_init']}.{$this->config_menu['table_column_status_featured']}"     => "{$status_featured}",
            ],
            [
                "{$this->config_menu['table_related']} {$this->config_menu['table_related_init']}" => [
                    "{$this->config_menu['table_init']}.{$this->config_menu['table_id']} = {$this->config_menu['table_related_init']}.{$this->config_menu['table_related_fk']}",
                ],
                "{$this->config_menu['table_lang']} {$this->config_menu['table_lang_init']}"       => [
                    "{$this->config_menu['table_related_init']}.{$this->config_menu['table_lang_fk']} = {$this->config_menu['table_lang_init']}.{$this->config_menu['table_lang_id']}",
                ]
            ],
            "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
            "{$this->config_menu['table_init']}.{$this->config_menu['table_column_ordering']} ASC",
            ['output' => $output, 'result' => $output_result]
        );
        return $result;
    }

    protected function display($tpl_content = 'partials/default.php', $data = [], $tpl_footer = 'partials/script_page')
    {
        $config_file_js  = $this->file_js;
        $config_file_css = $this->file_css;

        $config['user_group']         = $this->user_group;
        $config['menu_navbar']        = $this->set_menu_navbar_by_mode($this->config_menu_pages_id, '', 'post', 1);
        $config['menu_footer']        = $this->set_menu_footer_by_mode($this->config_menu_pages_id, '', '', 0);
        $config['content_news_event'] = $this->content_news_event;
        $config['content_nav']        = $this->content_nav;
        $config['content_check']      = $this->content_check;
        $config['content_menu_news']  = $this->content_menu_news;
        $config['variable']           = $this->base_config;
        $config['SET_DEFAULT_DATA']   = $this->module_config;
        $config_csrf['SESS_USER']     = $this->sess_user;
        $config_csrf['CSRF']          = $this->csrf;
        $config_csrf['CSRF_JSON']     = "&{$this->csrf['name'] }={$this->csrf['hash']}";
        $config_csrf['CSRF_JS']       = $this->csrf_js;

        $data['lang_set']               = $this->lang_active;
        $data['lang_active']            = $this->lang_default;
        $data['category_product']       = $this->{$this->base_model}->get_category();
        
        $data['cart']                   = $this->cart;
        $newdata = array(
                'lang_setting'  => strtolower($data['lang_active']['lang'])
        );

        $this->session->set_userdata($newdata);
        $parameter_result                = [$this->lang_default['id'],'1912185DF9844950D3A', 2];
        $data['result_branch']           = $this->{$this->post_model}->get_article_by_menu_id($parameter_result, 'array',null," a.is_featured='1'");
        $data['breadcrumb'] = $this->breadcrumb;
        $data               += $this->meta;

        $this->template_front
            ->title($this->title, (empty($this->base_config['web_title'])) ? '' : strip_tags($this->base_config['web_title']['value']))
            ->set('meta_author', $this->meta['meta_author'])
            ->set('meta_keywords', $this->meta['meta_keywords'])
            ->set('meta_description', $this->meta['meta_description'])
            ->set('FILE_CSS', $config_file_css)
            ->set('FILE_JS', $config_file_js)
            ->set('CONFIG', $config)
            ->set('SESS_USER', $config_csrf['SESS_USER'])
            ->set('CSRF', $config_csrf['CSRF'])
            ->set('CSRF_JSON', $config_csrf['CSRF_JSON'])
            ->set('CSRF_JS', $config_csrf['CSRF_JS'])
            ->set_partial('content', $tpl_content, $data)
            ->set_partial('footer', $tpl_footer, $data)
            ->build('themes');
    }

    protected function generate_array_from_table_column($table = null, $data = null, $mode = null)
    {
        $output           = [];
        $get_column_table = $this->{$this->base_model}->show_table_column($table);

        /* VALIDATE TABLE HAVE COLUMN */
        if (!empty($get_column_table) && is_array($output)) {
            foreach ($get_column_table as $index => $row) {
                if ($row['Key'] != 'PRI') {
                    $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : null;

                    if ($row['Default'] != '') {
                        $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : $row['Default'];
                    }

                    /* SET DEFAULT FOR BASED ON CASE COLUMN */
                    if ($row['Field'] == 'parent') {
                        $output['parent'] = (isset($data['parent'])) ? $data['parent'] : 0;
                    }

                    if ($row['Field'] == 'ordering') {
                        $output['ordering'] = (isset($data['ordering'])) ? $data['ordering'] : 0;
                    }

                    if ($row['Field'] == 'slug') {
                        $output['slug'] = (isset($data['title'])) ? strtolower(url_title($data['title'])) : null;
                    }

                    if ($row['Field'] == 'published') {
                        if ($mode == '') {
                            $output['published'] = (isset($data['published'])) ? date('d/m/Y', strtotime($data['published'])) : null;
                        } else {
                            $output['published'] = (isset($data['published'])) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['published']))) : date('Y-m-d');
                        }
                    }

                    /* ON MODE ADD - CTB & CTD SET */
                    if ($mode != '' || $mode != null) {
                        if ($mode == 'add') {
                            $output['ctb'] = !empty($this->sess_user) ? $this->sess_user['id'] : 0;
                            $output['ctd'] = date('Y-m-d H:i:s');
                        }
                        /* ON ALL MODE - MDB & MDD SET */
                        $output['mdb'] = !empty($this->sess_user) ? $this->sess_user['id'] : 0;
                        $output['mdd'] = date('Y-m-d H:i:s');
                    }
                } else {
                    /* COLUMN PRIMARY CAN SET IF MODE = EDIT */
                    if (($mode != '' || $mode != null) && $mode == 'edit') {
                        $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : null;
                    }
                }
            }
        }
        /* ON MODE ADD - CTB & CTD not SET by DEFAULT */
        if ($mode != 'add') {
            unset($output['ctb']);
            unset($output['ctd']);
        }
        return $output;
    }

    protected function set_banner_page_by_parameter_slug($parameter_slug = null, $parameter_breadcrumb = null)
    {
        if (empty($parameter_breadcrumb)) {
            $parameter_breadcrumb = $this->breadcrumb;
        }

        $output = array(
            'result_banner' => array()
        );

        /** QUERY FOR GET DATA BANNER BY PARAMETER SLUG*/
        $parameter_result_banner = array(
            'banner',           /* MENU SLUG FOR BANNER PAGE */
            $parameter_slug     /* POST SLUG FOR BANNER CONTENT */
        );
        $result_get_data         = $this->{$this->post_model}->get_article_by_post_slug($parameter_result_banner, 'array');
        if (!empty($result_get_data)) {
            $output['result_banner'] = $result_get_data;
        }

        return $output;
    }

    protected function set_meta_tags($parameter = null, $status = '')
    {
        if (empty($parameter['menu_title'])) {
            $parameter['menu_title'] = $this->title;
        }

        if (empty($parameter['menu_title'])) {
            $parameter['menu_title'] = '';
        }

        if ($status == '') {
            if (empty($parameter['meta_title'])) {
                $parameter['meta_title'] = $parameter['post_title'];
            }

            if (empty($parameter['meta_description'])) {
                $parameter['meta_description'] = $parameter['post_shortdesc'];
                if (empty($parameter['meta_description'])) {
                    $parameter['meta_description'] = word_limiter(strip_tags($parameter['post_description']), 100);
                }
            }

            $this->title                    = "{$parameter['post_title']} - {$parameter['menu_title']}";
            $this->meta['meta_url']         = current_url();
            $this->meta['meta_title']       = $this->title;
            $this->meta['meta_keywords']    = (!empty($parameter['meta_keywords'])) ? $parameter['meta_keywords'] : '';
            $this->meta['meta_description'] = (!empty($parameter['meta_description'])) ? $parameter['meta_description'] : '';
        } else {
            $this->meta['meta_url']         = current_url();
            $this->meta['meta_title']       = $this->title;
            $this->meta['meta_keywords']    = (!empty($parameter['meta_keywords'])) ? $parameter['meta_keywords'] : '';
            $this->meta['meta_description'] = (!empty($parameter['meta_description'])) ? $parameter['meta_description'] : '';
        }

        if (!empty($parameter['file_original'])) {
            if (file_exists("{$parameter['file_location']}{$parameter['file_original']}")) {
                $check = explode('.', $parameter['file_original']);
                if (is_array($check)) {
                    $check = $check[count($check) - 1];
                    if ($check != 'pdf') {
                        $this->meta['meta_image']     = base_url() . "{$parameter['file_location']}{$parameter['file_original']}";
                        $this->meta['meta_image_alt'] = $parameter['meta_title'];
                    }
                } else {
                    $this->meta['meta_image']     = base_url() . "{$parameter['file_location']}{$parameter['file_original']}";
                    $this->meta['meta_image_alt'] = $parameter['meta_title'];
                }
            }
        }
    }


}
