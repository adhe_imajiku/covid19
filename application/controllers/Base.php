<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base extends MX_Controller
{
    protected $title              = '';
    protected $title_page         = '';
    protected $base_meta          = array(
        'meta_author'      => '',
        'meta_keywords'    => '',
        'meta_description' => '',
        'meta_title'       => '',
        'meta_image'       => '',
        'meta_image_alt'   => '',
        'meta_url'         => '',
    );
    protected $base_session       = array();
    protected $base_config        = array();
    protected $base_config_module = array();
    protected $base_model         = 'front_model';
    protected $table_lang         = 'acs_lang';
    protected $table_lang_pk      = 'id';
    protected $table_lang_id      = 'acs_lang_id';
    protected $lang_active        = array();
    protected $lang_default       = array();
    protected $csrf_js            = array();
    protected $csrf               = array(
        'name' => '',
        'hash' => ''
    );
    protected $image_size         = array(
        'original'  => 1000,
        'thumbnail' => 220
    );
    protected $file_css           = '';
    protected $file_js            = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->model);
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : is_logged_in
     * FUNCTION     : melakukan validasi apakah user yang saat ini sedang mengakses page sudah login atau belum sesuai session pada base module yang di declare
     * PARAMETER    :
     * @parameter_level_user    : id level user yang tidak di izinkan mengakses module / page yang menjadi anak pada base module yang dipilih
     * ================================================================================================================================================================================================
     */

    protected function is_logged_in($parameter_level_user = array())
    {
        $status_check = false;
        if ((!$this->ion_auth->logged_in())) {
            $this->ion_auth->logout();
            redirect(PAGE_CMS_AUTH);
        } else {
            if (!empty($parameter_level_user)) {
                if ($this->ion_auth->in_group($parameter_level_user)) {
                    redirect(PAGE_CMS_AUTH);
                } else {
                    $status_check = true;
                }
            } else {
                $status_check = true;
            }

            if ($status_check == TRUE) {
                $this->base_session = array(
                    'identity' => $_SESSION['identity'],
                    'email'    => $_SESSION['email'],
                    'user_id'  => $_SESSION['user_id'],
                );
            }
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : set_config
     * FUNCTION     : membuat variable dengan nilai dari tabel configuration config untuk keperluan pada semua bagian
     * PARAMETER    : -
     * ================================================================================================================================================================================================
     */
    private function set_config()
    {
        $get_config = $this->{$this->base_model}->get_group_config();
        if (!empty($get_config)) {
            foreach ($get_config as $row_config) {
                $this->base_config[$row_config['config_name']] = $row_config['config_value'];
            }
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : set_language
     * FUNCTION     : mendapatkan data language yang diaktifkan untuk digunakan pada module atau page yang sedang diakses saat ini
     * PARAMETER    : -
     * ================================================================================================================================================================================================
     */
    private function set_language()
    {
        $get = $this->{$this->base_model}->get_language_active(1);
        foreach ($get as $index => $row) {
            $this->lang_active[] = $row;
            if ($row['status_default'] == 1) {
                $this->lang_default = $row;
            }
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : set_output
     * FUNCTION     :
     * PARAMETER    :
     * @parameter_output_mode   :
     * @parameter_output        :
     * @parameter_id            :
     * ================================================================================================================================================================================================
     */
    protected function set_output($parameter_output_mode = 'json', $parameter_output, $parameter_id = null)
    {
        $parameter_output_mode = strtolower($parameter_output_mode);
        if ($parameter_output_mode == 'json') {
            $output = $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($parameter_output));
        } else {
            $this->session->set_flashdata('output', $parameter_output);
            $output = $parameter_id;
        }
        return $output;
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : set_form_validation_error_message
     * FUNCTION     : menampilkan pesan default dari aksi form validation ketika aturan atau "set_rules" nya gagal tervalidasi dalam sebuah aksi
     * PARAMETER    : -
     * ================================================================================================================================================================================================
     */
    protected function set_form_validation_message_error()
    {
        if ($this->session->flashdata('output') == '') {
            $parameter_message = array(
                'status'  => false,
                'message' => validation_errors()
            );
            $output            = $this->set_output_message('alert', 'warning', $parameter_message, 'Error');
            $this->session->set_flashdata('output', $output);
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : set_output_message
     * FUNCTION     : membuat output string/html dari setelah aksi pada suatu module dijalankan.
     * PARAMETER    :
     * @parameter_mode              : mode dari pesan berupa "alert" atau "text" atau "json"
     * @parameter_mode_output       : output aksi berhasil atau gagal dengan nilai "success" atau "danger" atau "warning"
     * @parameter_message           : pesan yang ditampilkan, dalam bentuk array berisi status dan message
     * @parameter_message_header    : judul dari pesan yang akan ditampilkan, bernilai "Success" atau "Warning" atau "Failed" atau bebas
     * @parameter_action            : aksi yang telah dilakukan ketika modul ini akan dipanggil, apakah aksi tambah, ubah, ubah status, atau hapus data
     * ================================================================================================================================================================================================
     */
    protected function set_output_message($parameter_mode = 'alert', $parameter_mode_output = 'success', $parameter_message = null, $parameter_message_header = 'Success', $parameter_action = null)
    {
        if (empty($parameter_message)) {
            return '';
        } else {
            $string = array(
                'status'  => !empty($parameter_message['status']) ? $parameter_message['status'] : false,
                'message' => !empty($parameter_message['message']) ? $parameter_message['message'] : ''
            );

            if (empty($string['message'])) {
                return '';
            } else {
                if (!empty($parameter_action)) {
                    $message_header    = ucwords($parameter_message_header);
                    $message_output    = ($string['status']) ? 'successfully' : 'failed to';
                    $string['message'] = "<b>{$message_header}</b> {$message_output}";
                    switch ($parameter_action) {
                        case 'add':
                            $string['message'] .= ' added';
                            break;
                        case 'edit':
                            $string['message'] .= ' edited';
                            break;
                        case 'delete':
                            $string['message'] .= ' deleted';
                            break;
                        case 'status':
                            $string['message'] = ' update status';
                            break;
                        case 'check':
                            $message_output    = ($string['status'] == FALSE) ? 'still used' : 'not used';
                            $string['message'] = "<b>{$message_header}</b> {$message_output} in other data";
                            break;
                    }
                }
                if ($parameter_mode == 'alert') {
                    $string = "<div class='alert alert-{$parameter_mode_output} fade in'><h4>{$message_header}</h4>{$string['message']}</div>";
                } else if ($parameter_mode == 'text') {
                    $string = "<p class='text-{$parameter_mode_output}'><b>{$message_header}</b>, {$string['message']}</p>";
                } else if ($parameter_mode == 'json') {
                    /** NO FORMAT HTML FOR MODE OUTPUT JSON */
                }
                return $string;
            }
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : is_ajax
     * FUNCTION     : melakukan validasi ketika fungsi yang diakses "HARUS" dengan metode AJAX. Apabila bukan, maka akan ditampilkan pesan/message default error ajax required
     * PARAMETER    : -
     * ================================================================================================================================================================================================
     */
    protected function is_ajax()
    {
        if (!$this->input->is_ajax_request()) {
            $output = array(
                'status'  => FALSE,
                'message' => ERROR_NOT_AJAX
            );
            return $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($output));
        }
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : load_css
     * FUNCTION     : untuk me-load css ketika pada page yang sedang di akses/membutuhkan css tersebut.
     * PARAMETER    :
     * @parameter_address : terisi alamat dimana file javascript/jquery tersebut
     * ================================================================================================================================================================================================
     */
    protected function load_css($parameter_address = null)
    {
        $this->file_css .= '<link rel="stylesheet" type="text/css" href="' . $parameter_address . '"/>';
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : load_js
     * FUNCTION     : untuk me-load javascript/jquery ketika pada page yang sedang di akses/membutuhkan javascript/jquery tersebut.
     * PARAMETER    :
     * @parameter_address : terisi alamat dimana file javascript/jquery tersebut
     * ================================================================================================================================================================================================
     */
    protected function load_js($parameter_address = null)
    {
        $this->file_js .= '<script type="text/javascript" src="' . $parameter_address . '"></script>';
    }


    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : get_generate_new_id_string
     * FUNCTION     : membuat string uniq berdasarkan waktu ketika fungsi diakses. digunakan untuk membuat ID Primary Key pada suatu tabel.
     * PARAMETER    :
     * @parameter_set_random : apabila memerlukan tambahan nilai random, maka di isi sesuai kebutuhan tambahan nilai random numeric yang dibutuhkan
     * ================================================================================================================================================================================================
     */
    protected function get_generate_new_id_string($parameter_set_random = null)
    {
        $generate_new_id = strtoupper(date('ymd') . uniqid());
        if (!empty($parameter_set_random) && is_numeric($parameter_set_random)) {
            $generate_new_id .= get_random_numeric($parameter_set_random);
        }
        return $generate_new_id;
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : get_generate_array_from_table_column
     * FUNCTION     : digunakan untuk membuat sebuah array dengan isi sesuai kolom pada tabel dan diberikan nilai dari parameter data yang di gunakan (data set optional).
     * PARAMETER    :
     * @parameter_table : nama tabel yang akan lakukan aksi query mendapatkan kolom pada tabel
     * @parameter_data  : data set yang di lempar dan di set value nya sesuai kolom pada tabel
     * @parameter_mode  : mode atau aksi yang sedang dilakukan , nilai yang di isi adalah "add" atau "edit"
     * ================================================================================================================================================================================================
     */
    protected function get_generate_array_from_table_column($parameter_table = null, $parameter_data = null, $parameter_mode = null)
    {
        $output           = array();
        $get_column_table = $this->{$this->base_model}->show_table_column($parameter_table);

        /* VALIDATE TABLE HAVE COLUMN */
        if (!empty($get_column_table) && is_array($output)) {
            foreach ($get_column_table as $index => $row) {
                if ($row['Key'] != 'PRI') {
                    $output[$row['Field']] = (!empty($parameter_data[$row['Field']])) ? $parameter_data[$row['Field']] : null;

                    if ($row['Default'] != '') {
                        $output[$row['Field']] = (!empty($parameter_data[$row['Field']])) ? $parameter_data[$row['Field']] : $row['Default'];
                    }

                    /* SET DEFAULT FOR BASED ON CASE COLUMN */
                    if ($row['Field'] == 'parent') {
                        $output['parent'] = (!empty($parameter_data['parent'])) ? $parameter_data['parent'] : 0;
                    }

                    if ($row['Field'] == 'ordering') {
                        $output['ordering'] = (!empty($parameter_data['ordering'])) ? $parameter_data['ordering'] : 0;
                    }

                    if ($row['Field'] == 'slug') {
                        $output['slug'] = (!empty($parameter_data['title'])) ? strtolower(url_title($parameter_data['title'])) : null;
                    }

                    if ($row['Field'] == 'published') {
                        if ($parameter_mode == '') {
                            $output['published'] = (!empty($parameter_data['published'])) ? date('d/m/Y', strtotime($parameter_data['published'])) : null;
                        } else {
                            $output['published'] = (!empty($parameter_data['published'])) ? date('Y-m-d', strtotime(str_replace('/', '-', $parameter_data['published']))) : date('Y-m-d');
                        }
                    }

                    /* ON MODE ADD - CTB & CTD SET */
                    if ($parameter_mode != '' || $parameter_mode != null) {
                        if ($parameter_mode == 'add') {
                            $output['ctb'] = $this->sess_user['user_id'];
                            $output['ctd'] = date('Y-m-d H:i:s');
                        }
                        /* ON ALL MODE - MDB & MDD SET */
                        $output['mdb'] = $this->sess_user['user_id'];
                        $output['mdd'] = date('Y-m-d H:i:s');
                    }
                } else {
                    /* COLUMN PRIMARY CAN SET IF MODE = EDIT */
                    if (($parameter_mode != '' || $parameter_mode != null) && $parameter_mode == 'edit') {
                        $output[$row['Field']] = (!empty($parameter_data[$row['Field']])) ? $parameter_data[$row['Field']] : null;
                    }
                }
            }
        }
        /* ON MODE ADD - CTB & CTD not SET by DEFAULT */
        if ($parameter_mode != 'add') {
            unset($output['ctb']);
            unset($output['ctd']);
        }
        return $output;
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : get_generate_array_lang_from_table_column
     * FUNCTION     : digunakan untuk membuat sebuah array multi language (sesuai data language yang aktif) dengan isi sesuai kolom pada tabel dan diberikan nilai dari parameter data yang di gunakan (data set optional).
     * PARAMETER    :
     * @parameter_table : nama tabel yang akan lakukan aksi query mendapatkan kolom pada tabel
     * @parameter_data  : data set yang di lempar dan di set value nya sesuai kolom pada tabel
     * @parameter_mode  : mode atau aksi yang sedang dilakukan , nilai yang di isi adalah "add" atau "edit"
     * ================================================================================================================================================================================================
     */
    protected function get_generate_array_lang_from_table_column($parameter_table = null, $parameter_data = null, $parameter_mode = null, $parameter_related = null)
    {
        $output           = array();
        $get_column_table = $this->{$this->base_model}->show_table_column($parameter_table);
        $get_lang         = $this->lang_active;

        /* VALIDATE TABLE HAVE COLUMN */
        if (!empty($get_column_table) && !empty($get_lang) && is_array($output)) {
            foreach ($get_lang as $index_lang => $row_lang) {
                foreach ($get_column_table as $index => $row) {
                    $lang_id = $row_lang[$this->table_lang_pk];

                    if ($row['Key'] != 'PRI') {
                        $output[$lang_id][$row['Field']] = (!empty($parameter_data[$row['Field']][$lang_id])) ? $parameter_data[$row['Field']][$lang_id] : '';

                        if ($row['Default'] != '') {
                            $output[$lang_id][$row['Field']] = (!empty($parameter_data[$row['Field']][$lang_id])) ? $parameter_data[$row['Field']][$lang_id] : $row['Default'];
                        }

                        if ($row['Field'] == $this->table_lang_id) {
                            //$output[$lang_id][$this->table_lang_id] = (!empty($parameter_data[$this->table_lang_id][$lang_id])) ? (($parameter_data[$this->table_lang_id][$lang_id])) : $row_lang[$this->table_lang_pk];
                            $output[$lang_id][$this->table_lang_id] = $lang_id;
                            if ((!empty($parameter_data[$this->table_lang_id][$lang_id]))) {
                                $search = array_search($lang_id, array_keys($parameter_data[$this->table_lang_id]));
                                if ($lang_id === $search) {
                                    $output[$lang_id][$this->table_lang_id] = $lang_id;
                                    $output[$lang_id][$this->table_lang_id] = $lang_id;
                                }
                            }
                        }

                        if ($row['Field'] == 'slug') {
                            $output[$lang_id]['slug'] = (!empty($parameter_data['title'][$lang_id])) ? strtolower(url_title($parameter_data['title'][$lang_id])) : '';
                        }

                        if ($parameter_mode != '' || $parameter_mode != null) {
                            if ($parameter_mode == 'add') {
                                $output[$lang_id]['ctb'] = $this->sess_user['user_id'];
                                $output[$lang_id]['ctd'] = date('Y-m-d H:i:s');
                            }
                            $output[$lang_id]['mdb'] = 1;
                            $output[$lang_id]['mdd'] = date('Y-m-d H:i:s');
                        }
                    } else {
                        if (($parameter_mode != '' || $parameter_mode != null) && $parameter_mode == 'edit') {
                            $output[$lang_id][$row['Field']] = (!empty($parameter_data[$this->table_lang_id][$lang_id])) ? $parameter_data[$this->table_lang_id][$lang_id] : '';
                            if ($output[$lang_id][$row['Field']] == '') {
                                $output[$lang_id][$row['Field']] = $this->generate_new_id_string();
                            }
                        }
                    }

                    if ($parameter_mode == 'add' && $row['Key'] == 'PRI') {
                        $output[$lang_id][$row['Field']] = $this->generate_new_id_string();
                    }

                    if (!empty($parameter_related)) {
                        foreach ($parameter_related as $index_related => $row_related) {
                            $output[$lang_id][$index_related] = $row_related;
                        }
                    }

                    if ($parameter_mode != 'add') {
                        unset($output[$lang_id]['ctb']);
                        unset($output[$lang_id]['ctd']);
                    }
                }
            }
        }
        return $output;
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : get_generate_array_lang_from_table_column
     * FUNCTION     : digunakan untuk membuat sebuah array multi language (sesuai data language yang aktif) dengan isi sesuai kolom pada tabel dan diberikan nilai dari parameter data yang di gunakan (data set optional).
     * PARAMETER    :
     * @parameter_table : nama tabel yang akan lakukan aksi query mendapatkan kolom pada tabel
     * @parameter_data  : data set yang di lempar dan di set value nya sesuai kolom pada tabel
     * @parameter_mode  : mode atau aksi yang sedang dilakukan , nilai yang di isi adalah "add" atau "edit"
     * ================================================================================================================================================================================================
     */
    protected function get_single_data_from_table_with_parameter(
        $parameter_table = null,
        $parameter_column = null,
        $parameter_id = null,
        $parameter_select = null,
        $parameter_join = null,
        $parameter_output = 'array'
    )
    {

    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : func_upload_file_or_document
     * FUNCTION     : digunakan untuk melakukan proses upload file atau document yang diperlukan pada module
     * PARAMETER    :
     * @parameter_upload_name           : nama input FILES yang digunakan ketika upload
     * @parameter_upload_path           : path folder atau lokasi hasil upload akan disimpan
     * @parameter_upload_max_size       : batas maksimal upload yang dilakukan
     * @parameter_upload_allowed_types  : batas eksentensi file yang diperbolehkan ketika upload
     * ================================================================================================================================================================================================
     */
    protected function func_upload_file_or_document(
        $parameter_upload_name = null,
        $parameter_upload_path = null,
        $parameter_upload_max_size = null,
        $parameter_upload_allowed_types = null
    )
    {
        $default_path          = 'uploads/';
        $default_max_size      = 100000;
        $default_allowed_types = 'gif|jpg|png|pdf|jpeg';

        if (!empty($parameter_upload_path)) {
            $default_path = $parameter_upload_path;
        }

        if (!empty($parameter_upload_allowed_types)) {
            $default_allowed_types = $parameter_upload_allowed_types;
        }

        if (!empty($parameter_upload_max_size)) {
            $default_max_size = $parameter_upload_max_size;
        }

        $config['upload_path']      = $default_path;
        $config['allowed_types']    = $default_allowed_types;
        $config['max_size']         = $default_max_size;
        $config['encrypt_name']     = true;
        $config['remove_spaces']    = true;
        $config['file_ext_tolower'] = true;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if (!$this->upload->do_upload($parameter_upload_name)) {
            $output = array(
                'status'  => 'error',
                'message' => $this->upload->display_errors(),
                'data'    => null
            );
        } else {
            $output = array(
                'status'  => 'success',
                'message' => 'success',
                'data'    => $this->upload->data()
            );
        }
        return $output;
    }

    /**
     * ================================================================================================================================================================================================
     * FUNCTION / METHOD
     * NAME         : func_image_upload
     * FUNCTION     : digunakan untuk melakukan proses upload file atau document yang diperlukan pada module
     * PARAMETER    :
     * @parameter_destination_url       :
     * @parameter_table_related         :
     * @parameter_table_file            :
     * @parameter_disable_edit          :
     * @$parameter_size                 :
     * ================================================================================================================================================================================================
     */
    protected function func_image_upload(
        $parameter_destination_url = null,
        $parameter_table_related = array(),
        $parameter_table_file = array(),
        $parameter_disable_edit = true,
        $parameter_size = array(
            'original'  => MAX_SIZE_IMAGE_ORIGINAL,
            'thumbnail' => MAX_SIZE_IMAGE_THUMBNAIL
        )
    )
    {
        $output_editable = array();
        $url_original    = $parameter_destination_url;
        $url_thumb       = "{$parameter_destination_url}/" . FOLDER_IMAGE_THUMBNAIL;
        $output          = array(
            'status'  => false,
            'message' => '',
            'data'    => null
        );

        /**
         * ------------------------------------------------------------
         * SETTING CONFIGURATION PLUGIN IMAGE UPLOAD RESIZER
         * ------------------------------------------------------------
         */
        $config["generate_image_file"]          = true;
        $config["generate_thumbnails"]          = true;
        $config["image_max_size"]               = $parameter_size['original'];                    // MAXIMUM IMAGE SIZE (HEIGHT AND WIDTH)
        $config["thumbnail_size"]               = $parameter_size['thumbnail'];                   // THUMBNAILS WILL BE CROPPED TO 200X200 PIXELS
        $config["thumbnail_prefix"]             = "thumb_";                             // NORMAL THUMB PREFIX
        $config["destination_folder"]           = "{$url_original}/";                   // UPLOAD DIRECTORY ENDS WITH / (SLASH)
        $config["thumbnail_destination_folder"] = "{$url_thumb}/";                      // UPLOAD DIRECTORY ENDS WITH / (SLASH)
        $config["quality"]                      = 90;                                   // JPEG QUALITY
        $config["random_file_name"]             = TRUE;                                 // RANDOMIZE EACH FILE NAME
        $config["upload_url"]                   = base_url($url_original) . '/';
        $config["upload_url_thumbs"]            = base_url($url_thumb) . '/';

        $this->load->library('image_upload_resize');
        $config["file_data"] = !empty($_FILES["__files"]) ? $_FILES["__files"] : null;

        $data_file = $config["file_data"]['type'][0];
        $type_file = explode('/', $data_file);

        if ($parameter_disable_edit == true && !empty($parameter_table_file)) {
            $parameter_editable_file_input = $_FILES['__files'];
            $parameter_editable_parameter  = array(
                'table'         => $parameter_table_file['table'],
                'table_id'      => $parameter_table_file['id'],
                'table_related' => $parameter_table_related,
                'table_join'    => null,
                'table_groupby' => null,
            );

            $output_editable = $this->func_image_update($parameter_editable_file_input, $parameter_editable_parameter);
        }

        $process_upload = $this->image_upload_resize->resize($config);
        try {
            $variable_process = array('add' => null, 'edit' => null);
            if (!empty($process_upload['images'])) {
                $variable_names = array();
                if (!empty($this->input->post('__files_name'))) {
                    $variable_names = $this->input->post('__files_name');
                }

                $index_numbering = 0;
                foreach ($process_upload["images"] as $index => $response) {
                    $config["destination_folder"] = str_replace('//', '/', $config["destination_folder"]);
                    $file_name                    = '';
                    if (!empty($variable_names[$index])) {
                        $file_name = $variable_names[$index];
                    }

                    if (!empty($response)) {
                        $variable_default = array(
                            $parameter_table_related['id'] => $parameter_table_related['value'],
                            $parameter_table_file['id']    => $this->get_generate_new_id_string(5),
                            'file_thumb'                   => "thumb_{$response}",
                            'file_name'                    => $file_name,
                            'file_original'                => $response,
                            'file_location'                => $config["destination_folder"],
                            'file_type'                    => $type_file[0],
                            'ctb'                          => 1,
                            'ctd'                          => date('Y-m-d H:i:s'),
                            'mdb'                          => 1,
                            'mdd'                          => date('Y-m-d H:i:s'),
                        );

                        if (empty($output_editable[$index])) {
                            $variable_process['add'][$index] = $variable_default;
                            if ($parameter_table_related['id'] == $parameter_table_file['id']) {
                                unset($variable_process['add'][$index][$parameter_table_file['id']]);
                            }
                        } else {
                            unset($variable_default['ctd']);
                            unset($variable_default['ctb']);

                            $variable_process['edit'][$index]                              = $variable_default;
                            $variable_process['edit'][$index][$parameter_table_file['id']] = $output_editable[$index];
                        }
                    }
                    $index_numbering++;
                }

            }
            $output = array(
                'status'  => true,
                'message' => 'success',
                'data'    => $variable_process
            );
        } catch (Exception $e) {
            $output['message'] = $e->getMessage();
        }
        return $output;

    }

    protected function func_image_update($parameter_files = null, $parameter = array())
    {
        /**
         * --------------------------------------------------------------------------------------------------
         * DOCUMENTATION
         * $parameter = array(
         *   'table'                => 'Name table file',
         *   'table_id'             => 'Field column on table file',
         *   'table_related'        => 'Condition where, use name table relation from table file',
         *   'table_join'           => 'Join another table (optional)'
         *   'table_groupby'        => 'Condition groupby  (optional)'
         * );
         * --------------------------------------------------------------------------------------------------
         */

        $image_folder_thumbnail = FOLDER_IMAGE_THUMBNAIL;
        $field_image_location   = 'file_location';
        $field_image_thumbnail  = 'file_thumb';
        $field_image_original   = 'file_original';

        $output    = array();
        $parameter = (object)$parameter;
        if (!empty($parameter)) {
            $file_type = $parameter->table_related['file_type'];
            if (!empty($parameter->table_related)) {
                $parameter->table_related = array(
                    $parameter->table_related['id'] => $parameter->table_related['value']
                );
            }
            if (!empty($file_type)) {
                $parameter->table_related[$file_type] = '';
            }

            $result = $this->{$this->base_model}->query_builder(
                $parameter->table,
                "{$parameter->table_id}, {$field_image_location}, {$field_image_thumbnail}, {$field_image_original}",
                (empty($parameter->table_related)) ? array() : $parameter->table_related,
                (empty($parameter->table_join)) ? array() : $parameter->table_join,
                (empty($parameter->table_groupby)) ? '' : $parameter->table_groupby,
                null,
                array('output' => 'result', 'result' => 'array'));

            if (count($result) > 0) {
                foreach ($result as $index => $row) {
                    if (!empty($parameter_files['name'][$index]) && $parameter_files['name'][$index] != '') {
                        $file_thumbnail = "{$row[$field_image_location]}{$image_folder_thumbnail}{$row[$field_image_thumbnail]}";
                        $file_original  = "{$row[$field_image_location]}/{$row[$field_image_original]}";

                        if (file_exists($file_thumbnail)) {
                            unlink($file_thumbnail);
                        }
                        if (file_exists($file_original)) {
                            unlink($file_original);
                        }
                        $output[$index] = $row[$parameter->table_id];
                    }
                }
            }
        }
        return $output;
    }

    protected function func_image_delete($parameter = null, $parameter_output_action = 'delete', $parameter_output_mode = 'json')
    {
        /**
         * --------------------------------------------------------------------------------------------------
         * DOCUMENTATION
         * $parameter = [
         *   'table'                => 'Name table file',
         *   'table_id'             => 'Field column on table file',
         *   'table_related'        => 'Condition where, use name table relation from table file',
         *   'table_join'           => 'Join another table (optional)'
         *   'table_groupby'        => 'Condition groupby  (optional)'
         * ];
         * --------------------------------------------------------------------------------------------------
         */


        $output        = array();
        $output_unlink = array();
        $parameter     = (object)$parameter;

        $this->db->trans_start();

        if (!empty($parameter)) {
            $result = $this->{$this->base_model}->query_builder(
                $parameter->table,
                "{$parameter->table_id}, file_location, file_thumb, file_original",
                (empty($parameter->table_related)) ? [] : $parameter->table_related,
                (empty($parameter->table_join)) ? [] : $parameter->table_join,
                (empty($parameter->table_groupby)) ? '' : $parameter->table_groupby,
                null,
                array(
                    'output' => 'result',
                    'result' => 'array'
                )
            );
            if (count($result) > 0) {
                foreach ($result as $index => $row) {
                    $file_thumbnail = "{$row['file_location']}thumb/{$row['file_thumb']}";
                    $file_original  = "{$row['file_location']}/{$row['file_original']}";

                    $output_unlink[$index] = array(
                        'original'  => is_file($file_original) ? $file_original : null,
                        'thumbnail' => is_file($file_thumbnail) ? $file_thumbnail : null,
                    );

                    if ($parameter_output_action == 'delete') {
                        $output[$index] = $row[$parameter->table_id];
                    } else {
                        $output[$index] = [
                            $parameter->table_id => $row[$parameter->table_id],
                            'file_thumb'         => null,
                            'file_original'      => null,
                            'file_location'      => null,
                        ];
                    }
                }
            }

        }
        if ($parameter_output_action == 'delete') {
            if (!empty($output)) {
                $this->{$this->base_model}->delete_data_batch($parameter->table, $parameter->table_id, $output);
            }
        } else if ($parameter_output_action == 'update') {
            if (!empty($output)) {
                $this->{$this->base_model}->update_data_batch($parameter->table, $output, $parameter->table_id);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if (count($output_unlink) > 0) {
                foreach ($output_unlink as $index => $row) {
                    $file_thumbnail = $row['thumbnail'];
                    $file_original  = $row['original'];

                    if ($file_thumbnail != '') {
                        if (file_exists($file_thumbnail)) {
                            unlink($file_thumbnail);
                        }
                    }
                    if ($file_original != '') {
                        if (file_exists($file_original)) {
                            unlink($file_original);
                        }
                    }
                }
            }
            $message = array(
                'status'  => true,
                'message' => "Picture {$parameter->title} deleted"
            );
        } else {
            $this->db->trans_rollback();
            $message = array(
                'status'  => false,
                'message' => "Picture {$parameter->title} can't delete"
            );
        }
        $output_status = ($message['status'] == TRUE) ? 'success' : 'error';
        $output        = $this->output_message($parameter_output_mode, $output_status, $message, $parameter->title);

        return $this->set_output($parameter_output_mode, $output);
    }
}
