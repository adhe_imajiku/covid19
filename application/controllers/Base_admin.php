<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Base_admin extends MX_Controller
{
    protected $title           = '';
    protected $meta            = [
        'author'      => '',
        'keywords'    => '',
        'description' => '',
    ];
    protected $sess_user       = [];
    protected $config          = [];
    protected $file_css        = '';
    protected $file_js         = '';
    protected $base_model      = 'admin_model';
    protected $table_lang      = 'acs_lang';
    protected $table_lang_pk   = 'id';
    protected $table_lang_id   = 'acs_lang_id';
    protected $lang_active     = [];
    protected $lang_default    = [];
    protected $module_config   = [];
    protected $menu_previleges = [];
    protected $user_group      = 1;
    protected $dashboard_group = 4;

    /* VARIABLE USE ON CREATE MENU RECURSIVE ON ADMINS */
    protected $config_menu = [
        'table'    => 'menu',
        'table_id' => 'id',

        'table_related'    => 'menu_lang',
        'table_related_id' => 'id',
        'table_related_fk' => 'menu_id',

        'table_lang'    => 'acs_lang',
        'table_lang_id' => 'id',
        'table_lang_fk' => 'acs_lang_id',

        'table_post'    => 'post',
        'table_post_id' => 'id',
        'table_post_fk' => 'menu_id',

        'table_post_lang'    => 'post_lang',
        'table_post_lang_id' => 'id',
        'table_post_lang_fk' => 'post_id',

        'table_column_status_default' => 'status_default',
        'table_column_status_mode'    => 'status_mode',
        'table_column_parent'         => 'parent',
        'table_column_title'          => 'title',
        'table_column_slug'           => 'slug',
        'table_column_ordering'       => 'ordering',
        'table_column_link'           => 'menu_cms_link',

        'table_filter_ordering'    => 'filter_ordering',
        'table_filter_parent'      => 'filter_parent',
        'table_filter_shortdesc'   => 'filter_shortdesc',
        'table_filter_description' => 'filter_description',
        'table_filter_image'       => 'filter_image',
        'table_filter_tags'        => 'filter_tags',

        'table_init'           => 'a',
        'table_related_init'   => 'b',
        'table_lang_init'      => 'c',
        'table_post_init'      => 'd',
        'table_post_lang_init' => 'e',

    ];
    protected $csrf        = [
        'name' => '',
        'hash' => '',
    ];
    protected $csrf_js     = [];
    protected $size        = [
        'original'  => 1000,
        'thumbnail' => 330
    ];

    public function __construct()
    {
        parent::__construct();
        $this->load->model($this->base_model);
        $this->db->query("SET SESSION sql_mode = ''");

//        $this->is_logged_in();
        $this->set_config();
        $this->set_language();
//        $this->check_previleges();
        $this->csrf    = [
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash(),
        ];
        $this->csrf_js = json_encode((object)[$this->security->get_csrf_token_name() => $this->security->get_csrf_hash()]);
    }


    public function check_previleges()
    {
        $group  = $this->ion_auth->get_users_groups()->row()->id;
        $uri[0] = (empty($this->uri->segment(1)) ? '' : $this->uri->segment(1));
        $uri[1] = (empty($this->uri->segment(2)) ? '' : $this->uri->segment(2));
        $uri[2] = (empty($this->uri->segment(3)) ? '' : $this->uri->segment(3));
        if (intval(substr($uri[2], 0, 6)) != 0) {
            unset($uri[2]);
        }
        if (empty($uri[2])) {
            unset($uri[2]);
        }
        if (empty($uri[1])) {
            $uri[1]='index';
        }
        $array    = [
            'add',
            'edit',
            'delete',
            'checkbox_status',
            'checkbox_delete',
            'index',
        ];
        
        if ($uri[0] != 'mod_dashboard' && $uri[0] != 'mod_menu' && $uri[0] != 'mod_tags_kt') {
            if(count($uri)<3){
                $query="select * from acs_portal_previleges join menu on menu.id=acs_portal_previleges.menu_id where menu_cms_link='{$uri[0]}' and group_id='{$group}'";
                $result=$this->db->query($query)->row_array();
                if(in_array($uri[1],$array)){
                    if(!empty($result)){
                        if(empty($uri[1])){
                            if ($result['list'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                            }else{
                                if($this->input->is_ajax_request()){
                                    return true;
                                }
                            }
                        }else if($uri[1]=='index'){
                            if ($result['list'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                            }else{
                                if($this->input->is_ajax_request()){
                                    return true;
                                }
                            }
                        }else if($uri[1]=='add'){
                            if ($result['add'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                            }else{
                                if($this->input->is_ajax_request()){
                                    return true;
                                }
                            }
                        }else if($uri[1]=='edit' || $uri[1]=='checkbox_status'){
                            if ($result['edit'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                            }else{
                                if($this->input->is_ajax_request()){
                                    return true;
                                }
                            }
                        }else if($uri[1]=='delete' || $uri[1]=='checkbox_delete'){
                            if ($result['delete'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                            }else{
                                if($this->input->is_ajax_request()){
                                    return true;
                                }
                            }
                        }
                    }else{
                        if (!$this->input->is_ajax_request()) {
                            $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                            $this->set_output('alert', $output);
                            redirect('mod_dashboard');
                        }else{
                            return false;
                        }
                    }
                }
            }else{
                if(in_array($uri[1],$array)){
                    $consoling[0]=$uri[0];
                    $consoling[1]=$uri[1];
                    $consoling[2]=$uri[2];
                    if($uri[1]!='index'){
                        $consoling[1]='index';
                    }
                    $full_url_console=implode('/',$consoling);
                    $query="select * from acs_portal_previleges join menu on menu.id=acs_portal_previleges.menu_id where menu_cms_link='{$full_url_console}' and group_id='{$group}'";
                    $result=$this->db->query($query)->row_array();
                    if(empty($uri[1])){
                        if ($result['list'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                        }else{
                            if($this->input->is_ajax_request()){
                                return true;
                            }
                        }
                    }else if($uri[1]=='add'){
                        if ($result['add'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                        }else{
                            if($this->input->is_ajax_request()){
                                return true;
                            }
                        }
                    }else if($uri[1]=='edit' || $uri[1]=='checkbox_status'){
                        if ($result['edit'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                        }else{
                            if($this->input->is_ajax_request()){
                                return true;
                            }
                        }
                    }else if($uri[1]=='delete' || $uri[1]=='checkbox_delete'){
                        if ($result['delete'] == '0') {
                                if (!$this->input->is_ajax_request()) {
                                    $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                    $this->set_output('alert', $output);
                                    redirect('mod_dashboard');
                                }else{
                                    return false;
                                }
                        }else{
                            if($this->input->is_ajax_request()){
                                return true;
                            }
                        }
                    }else if($uri[1]=='index'){
                        if ($result['list'] == '0') {
                            if (!$this->input->is_ajax_request()) {
                                $output = "<div class='alert alert-danger fade in'><h4>Access Denied</h4>You're not allowed</div>";
                                $this->set_output('alert', $output);
                                redirect('mod_dashboard');
                            }else{
                                return false;
                            }
                        }else{
                            if($this->input->is_ajax_request()){
                                return true;
                            }
                        }
                    }
                }
            }
        }else{
            if($this->input->is_ajax_request()){
                return true;
            }
        }
    }


    private function is_logged_in()
    {
        if ((!$this->ion_auth->logged_in())) {
            $this->ion_auth->logout();
            redirect('mod_authentication/login');
        } else {
            if ($this->ion_auth->in_group(2)) {
                $this->ion_auth->logout();
                redirect('mod_authentication/login');
            }
            $this->sess_user = [
                'identity' => $_SESSION['identity'],
                'email'    => $_SESSION['email'],
                'user_id'  => $_SESSION['user_id'],
            ];
        }
    }

    private function set_config()
    {
        $get_config = $this->{$this->base_model}->get_group_config();
        foreach ($get_config as $row_config) {
            $this->config[$row_config['config_name']] = $row_config['config_value'];
        }
    }

    private function set_language()
    {
        $get = $this->{$this->base_model}->get_language_active(1);
        foreach ($get as $index => $row) {
            $this->lang_active[] = $row;
            if ($row['status_default'] == 1) {
                $this->lang_default = $row;
            }
        }
    }

    protected function set_output($output_mode = 'json', $output, $id = null)
    {
        if ($output_mode == 'json') {
            return $this->output->set_output(json_encode($output));
        } else {
            $this->session->set_flashdata('output', $output);
            return $id;
        }
    }

    protected function set_form_validaton_error()
    {
        if ($this->session->flashdata('output') == '') {
            $output = $this->output_message('alert', 'warning', [
                'status'  => false,
                'message' => validation_errors()
            ], 'Error');
            $this->session->set_flashdata('output', $output);
        }
    }

    /* CREATE MENU POST BY STATUS_MODE */
    protected function set_menu_by_mode($parent = 0, $result = '', $params = null)
    {
        $html = '';
        $icon = '';
        $result = $this->menu_by_parent_and_mode($parent, 1, $params, 'result');
        if (count($result) > 0) {
            foreach ($result as $index => $row) {
                $query="select * from acs_portal_previleges where menu_id='{$row['id']}' ";
                $result=$this->db->query($query)->row_array();
                if(!empty($result)){
                    if($result['list']=='1'){
                        $url_params = ($params == 'other') ? $row[$this->config_menu['table_column_slug']] : $params;
                        $url_link   = site_url($row[$this->config_menu['table_column_link']]);
                        $url_title  = $row[$this->config_menu['table_column_title']];

                        $check = $this->menu_by_parent_and_mode($row[$this->config_menu['table_id']], 1, $params, '');
                        $title = $row[$this->config_menu['table_column_title']];
                        $slug  = $row[$this->config_menu['table_column_slug']];

                        $caret = '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';

                        $active='';
                        $active_parent='';
                        $actual_link = str_replace('.html','',str_replace(base_url(),'',(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ));
                        if($actual_link==$row[$this->config_menu['table_column_link']]){
                            $active='active';
                        }
                        $sql_active="select * from menu where menu.parent='{$row[$this->config_menu['table_id']]}' and menu_cms_link='{$actual_link}'";
                        if($this->db->query($sql_active)->num_rows() > 0) {
                            $active_parent = 'active';
                        }
                        if ($parent == 0) {
                            if (count($check) != 0) {
                                $html .= '<li class="treeview '.$active_parent.'"">';
                                $html .= "<a href='#'>{$title} {$caret}</a>";
                                $html .= '<ul class="treeview-menu" role="menu">';
                                $html .= $this->set_menu_by_mode($row[$this->config_menu['table_id']], $html, $params);
                                $html .= '</ul>';
                                $html .= '</li>';
                            } else {
                                $html .= "<li class='".$active."'><a href='{$url_link}'><i class='{$icon}'></i> {$url_title}</a></li>";
                            }
                        } else {
                            if (count($check) != 0) {
                                $html .= "<li class='treeview ".$active_parent." '>";
                                $html .= "<a href='#'>{$title} {$caret}</a>";
                                $html .= "<ul class='treeview-menu' role='menu'>";

                                $html .= $this->set_menu_by_mode($row[$this->config_menu['table_id']], $html, $params);

                                $html .= "</ul>";
                                $html .= "</li>";
                            } else {
                                $html .= "<li class='".$active."'><a href='{$url_link}'><i class='{$icon}'></i> {$url_title}</a></li>";
                            }
                        }    
                    }
                }else{
                    
                        $url_params = ($params == 'other') ? $row[$this->config_menu['table_column_slug']] : $params;
                        $url_link   = site_url($row[$this->config_menu['table_column_link']]);
                        $url_title  = $row[$this->config_menu['table_column_title']];

                        $check = $this->menu_by_parent_and_mode($row[$this->config_menu['table_id']], 1, $params, '');
                        $title = $row[$this->config_menu['table_column_title']];
                        $slug  = $row[$this->config_menu['table_column_slug']];

                        $caret = '<span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>';

                        $active='';
                        $active_parent='';
                        $actual_link = str_replace('.html','',str_replace(base_url(),'',(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" ));
                        if($actual_link==$row[$this->config_menu['table_column_link']]){
                            $active='active';
                        }
                        $sql_active="select * from menu where menu.parent='{$row[$this->config_menu['table_id']]}' and menu_cms_link='{$actual_link}'";
                        if($this->db->query($sql_active)->num_rows() > 0) {
                            $active_parent = 'active';
                        }
                        if ($parent == 0) {
                            if (count($check) != 0) {
                                $html .= '<li class="treeview '.$active_parent.'"">';
                                $html .= "<a href='#'>{$title} {$caret}</a>";
                                $html .= '<ul class="treeview-menu" role="menu">';
                                $html .= $this->set_menu_by_mode($row[$this->config_menu['table_id']], $html, $params);
                                $html .= '</ul>';
                                $html .= '</li>';
                            } else {
                                $html .= "<li class='".$active."'><a href='{$url_link}'><i class='{$icon}'></i> {$url_title}</a></li>";
                            }
                        } else {
                            if (count($check) != 0) {
                                $html .= "<li class='treeview ".$active_parent." '>";
                                $html .= "<a href='#'>{$title} {$caret}</a>";
                                $html .= "<ul class='treeview-menu' role='menu'>";

                                $html .= $this->set_menu_by_mode($row[$this->config_menu['table_id']], $html, $params);

                                $html .= "</ul>";
                                $html .= "</li>";
                            } else {
                                $html .= "<li class='".$active."'><a href='{$url_link}'><i class='{$icon}'></i> {$url_title}</a></li>";
                            }
                        }    
                }
            }
        }
        return $html;
    }

    protected function menu_by_parent_and_mode($parent, $status_default, $status_mode, $output = 'result', $output_result = 'array')
    {
        $result = $this->{$this->base_model}->query_builder(
            "{$this->config_menu['table']} {$this->config_menu['table_init']}",
            "
            {$this->config_menu['table_init']}.{$this->config_menu['table_id']},
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']},
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_link']},
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_title']},
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']}
            ",
            [
                "{$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']}"              => $parent . '',
                "{$this->config_menu['table_lang_init']}.{$this->config_menu['table_column_status_default']}" => $status_default
            ],
            [
                "{$this->config_menu['table_related']} {$this->config_menu['table_related_init']}" => [
                    "{$this->config_menu['table_init']}.{$this->config_menu['table_id']} = {$this->config_menu['table_related_init']}.{$this->config_menu['table_related_fk']}",
                ],
                "{$this->config_menu['table_lang']} {$this->config_menu['table_lang_init']}"       => [
                    "{$this->config_menu['table_related_init']}.{$this->config_menu['table_lang_fk']} = {$this->config_menu['table_lang_init']}.{$this->config_menu['table_lang_id']}",
                ]
            ],
            "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
            "{$this->config_menu['table_init']}.{$this->config_menu['table_column_ordering']} ASC",
            ['output' => $output, 'result' => $output_result]
        );
        return $result;
    }

    protected function menu_by_slug($slug, $status_default, $output = 'result', $output_result = 'array')
    {
        $result = $this->{$this->base_model}->query_builder(
            "{$this->config_menu['table']} {$this->config_menu['table_init']}",
            "
            {$this->config_menu['table_init']}.{$this->config_menu['table_id']} as menu_id,
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']} as menu_parent,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_title']} as menu_title,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']} as menu_slug,
           
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_ordering']} as menu_filter_ordering,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_parent']} as menu_filter_parent,
            {$this->config_menu['table_init']}.filter_featured as menu_filter_featured,
            {$this->config_menu['table_init']}.filter_icon as menu_filter_icon,
            {$this->config_menu['table_init']}.filter_promo as menu_filter_promo,
            {$this->config_menu['table_init']}.filter_author as menu_filter_author,
            {$this->config_menu['table_init']}.filter_event_start as menu_filter_event_start,
            {$this->config_menu['table_init']}.filter_event_end as menu_filter_event_end,
            {$this->config_menu['table_init']}.filter_brand as menu_filter_brand,
            {$this->config_menu['table_init']}.filter_product_tags as menu_filter_product_tags,
            {$this->config_menu['table_init']}.filter_title_sub as menu_filter_title_sub,
            {$this->config_menu['table_init']}.filter_location as menu_filter_location,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_shortdesc']} as menu_filter_shortdesc,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_description']} as menu_filter_description,
            {$this->config_menu['table_init']}.filter_note as menu_filter_note,
            {$this->config_menu['table_init']}.required_title_sub as required_title_sub,
            {$this->config_menu['table_init']}.required_shortdesc as required_shortdesc,
            {$this->config_menu['table_init']}.required_description as required_description,
            {$this->config_menu['table_init']}.required_note as required_note,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_image']} as menu_filter_image,
            {$this->config_menu['table_init']}.text_ordering as text_filter_ordering,
            {$this->config_menu['table_init']}.text_parent as text_filter_parent,
            {$this->config_menu['table_init']}.text_title as text_filter_title,
            {$this->config_menu['table_init']}.text_location as text_filter_location,
            {$this->config_menu['table_init']}.text_title_sub as text_filter_title_sub,
            {$this->config_menu['table_init']}.text_shortdesc as text_filter_shortdesc,
            {$this->config_menu['table_init']}.text_description as text_filter_description,
            {$this->config_menu['table_init']}.text_featured as text_filter_featured,
            {$this->config_menu['table_init']}.text_image as text_filter_image,
            {$this->config_menu['table_init']}.text_tags as text_filter_tags,
            {$this->config_menu['table_init']}.text_note as text_filter_note,
            {$this->config_menu['table_init']}.text_icon as text_filter_icon,
            {$this->config_menu['table_init']}.text_author as text_filter_author,
            {$this->config_menu['table_init']}.text_promo as text_filter_promo,
            {$this->config_menu['table_init']}.text_event_start as text_filter_event_start,
            {$this->config_menu['table_init']}.text_event_end as text_filter_event_end,
            {$this->config_menu['table_init']}.text_brand as text_filter_brand,
            {$this->config_menu['table_init']}.text_product_tags as text_filter_product_tags,
            {$this->config_menu['table_init']}.mode_title_sub as mode_title_sub,
            {$this->config_menu['table_init']}.mode_shortdesc as mode_shortdesc,
            {$this->config_menu['table_init']}.mode_tags as mode_tags,
            {$this->config_menu['table_init']}.status_mode as status_mode,
            {$this->config_menu['table_init']}.mode_note as mode_note,
            {$this->config_menu['table_init']}.mode_image as mode_image,
            {$this->config_menu['table_init']}.image_qty as image_qty,
            {$this->config_menu['table_init']}.tags_kt_id as tags_kt_id,
            {$this->config_menu['table_init']}.tags_brand_id as tags_brand_id,
            {$this->config_menu['table_init']}.tags_product_id as tags_product_id,
            {$this->config_menu['table_init']}.mode_description as mode_description,
            {$this->config_menu['table_init']}.recomended_image_size as image_size,
            {$this->config_menu['table_init']}.disable_add,
            {$this->config_menu['table_init']}.disable_delete,
            {$this->config_menu['table_init']}.disable_status,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_tags']} as menu_filter_tags
            ",
            [
                "{$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']}"        => $slug,
                "{$this->config_menu['table_lang_init']}.{$this->config_menu['table_column_status_default']}" => $status_default,
            ],
            [
                "{$this->config_menu['table_related']} {$this->config_menu['table_related_init']}" => [
                    "{$this->config_menu['table_init']}.{$this->config_menu['table_id']} = {$this->config_menu['table_related_init']}.{$this->config_menu['table_related_fk']}",
                ],
                "{$this->config_menu['table_lang']} {$this->config_menu['table_lang_init']}"       => [
                    "{$this->config_menu['table_related_init']}.{$this->config_menu['table_lang_fk']} = {$this->config_menu['table_lang_init']}.{$this->config_menu['table_lang_id']}",
                ]
            ],
            "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
            "{$this->config_menu['table_init']}.{$this->config_menu['table_column_ordering']} ASC",
            ['output' => $output, 'result' => $output_result]
        );
        return $result;
    }

    protected function menu_by_slug_content($slug, $status_default, $output = 'result', $output_result = 'array', $parent)
    {
        $result = $this->{$this->base_model}->query_builder(
            "{$this->config_menu['table']} {$this->config_menu['table_init']}",
            "
            {$this->config_menu['table_init']}.{$this->config_menu['table_id']} as menu_id,
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']} as menu_parent,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_title']} as menu_title,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']} as menu_slug,
           
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_ordering']} as menu_filter_ordering,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_parent']} as menu_filter_parent,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_shortdesc']} as menu_filter_shortdesc,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_description']} as menu_filter_description,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_image']} as menu_filter_image,
            {$this->config_menu['table_init']}.recomended_image_size as image_size,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_tags']} as menu_filter_tags,
            {$this->config_menu['table_post_lang_init']}.title
            ",
            [
                "{$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']}"        => $slug,
                "{$this->config_menu['table_lang_init']}.{$this->config_menu['table_column_status_default']}" => $status_default,
            ],
            [
                "{$this->config_menu['table_related']} {$this->config_menu['table_related_init']}"     => [
                    "{$this->config_menu['table_init']}.{$this->config_menu['table_id']} = {$this->config_menu['table_related_init']}.{$this->config_menu['table_related_fk']}",
                ],
                "{$this->config_menu['table_lang']} {$this->config_menu['table_lang_init']}"           => [
                    "{$this->config_menu['table_related_init']}.{$this->config_menu['table_lang_fk']} = {$this->config_menu['table_lang_init']}.{$this->config_menu['table_lang_id']}",
                ],
                "{$this->config_menu['table_post']} {$this->config_menu['table_post_init']}"           => [
                    "{$this->config_menu['table_post_init']}.{$this->config_menu['table_post_fk']} = {$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
                ],
                "{$this->config_menu['table_post_lang']} {$this->config_menu['table_post_lang_init']}" => [
                    "{$this->config_menu['table_post_lang_init']}.{$this->config_menu['table_post_lang_fk']} = {$this->config_menu['table_post_init']}.{$this->config_menu['table_post_id']}",
                ]
            ],
            "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
            "{$this->config_menu['table_init']}.{$this->config_menu['table_column_ordering']} ASC",
            ['output' => $output, 'result' => $output_result]
        );
        return $result;
    }

    protected function menu_by_id($id, $status_default, $output = 'result', $output_result = 'array')
    {
        $result = $this->{$this->base_model}->query_builder(
            "{$this->config_menu['table']} {$this->config_menu['table_init']}",
            "
            {$this->config_menu['table_init']}.{$this->config_menu['table_id']} as menu_id,
            {$this->config_menu['table_init']}.{$this->config_menu['table_column_parent']} as menu_parent,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_title']} as menu_title,
            {$this->config_menu['table_related_init']}.{$this->config_menu['table_column_slug']} as menu_slug,
           
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_ordering']} as menu_filter_ordering,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_parent']} as menu_filter_parent,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_shortdesc']} as menu_filter_shortdesc,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_description']} as menu_filter_description,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_image']} as menu_filter_image,
            {$this->config_menu['table_init']}.{$this->config_menu['table_filter_tags']} as menu_filter_tags
            ",
            [
                "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}"                         => $id,
                "{$this->config_menu['table_lang_init']}.{$this->config_menu['table_column_status_default']}" => $status_default,
            ],
            [
                "{$this->config_menu['table_related']} {$this->config_menu['table_related_init']}" => [
                    "{$this->config_menu['table_init']}.{$this->config_menu['table_id']} = {$this->config_menu['table_related_init']}.{$this->config_menu['table_related_fk']}",
                ],
                "{$this->config_menu['table_lang']} {$this->config_menu['table_lang_init']}"       => [
                    "{$this->config_menu['table_related_init']}.{$this->config_menu['table_lang_fk']} = {$this->config_menu['table_lang_init']}.{$this->config_menu['table_lang_id']}",
                ]
            ],
            "{$this->config_menu['table_init']}.{$this->config_menu['table_id']}",
            "{$this->config_menu['table_init']}.{$this->config_menu['table_column_ordering']} ASC",
            ['output' => $output, 'result' => $output_result]
        );
//        var_dump($this->config_menu);
//        die();
        return $result;
    }

    protected function generate_new_id_string()
    {
        return strtoupper(date('ymd') . uniqid());
    }

    protected function is_ajax()
    {
        if (!$this->input->is_ajax_request()) {
            $output = [
                'status'  => false,
                'message' => 'No access allowed'
            ];
            return $this->output->set_output(json_encode($output));
        }
    }

    protected function load_css($address = null, $external = false)
    {
        if ($external) {
            $this->file_css .= '<link rel="stylesheet" type="text/css" href="' . $address . '"/>';
        } else {
            $this->file_css .= '<link rel="stylesheet" type="text/css" href="' . ($address) . '"/>';
//            if (is_file($address)) {
//            } else {
//                $this->file_css .= 'CSS File' . $address . ' is not found ! <br/>';
//            }
        }
    }

    protected function load_js($address = null, $external = false)
    {
        if ($external) {
            $this->file_js .= '<script type="text/javascript" src="' . $address . '"></script>';
        } else {
            $this->file_js .= '<script type="text/javascript" src="' . ($address) . '"></script>';
//            if (is_file($address)) {
//            } else {
//                $this->file_js .= 'JS File' . $address . ' is not found ! <br/>';
//            }
        }
    }

    protected function output_message($mode = 'alert', $mode_output = 'success', $message = null, $message_header = 'Success', $action = null)
    {
        if ($message == '') {
            return '';
        }

        $string = [
            'status'  => isset($message['status']) ? $message['status'] : false,
            'message' => isset($message['message']) ? $message['message'] : ''
        ];
        if ($action != '') {
            $message_header    = ucwords($message_header);
            $message_output    = ($string['status']) ? 'successfully' : 'failed to';
            $string['message'] = "<b>{$message_header}</b> {$message_output}";
            switch ($action) {
                case 'add':
                    $string['message'] .= ' added';
                    break;
                case 'edit':
                    $string['message'] .= ' edited';
                    break;
                case 'delete':
                    $string['message'] .= ' deleted';
                    break;
                case 'status':
                    $string['message'] = ' update status';
                    break;
                case 'check':
                    $message_output    = (!$string['status']) ? 'still used' : 'not used';
                    $string['message'] = "<b>{$message_header}</b> {$message_output} in other data";
                    break;
            }
        }

        if ($string['message'] == '') {
            return '';
        }

        if ($mode == 'alert') {
            $string = "<div class='alert alert-{$mode_output} fade in'><h4>{$message_header}</h4>{$string['message']}</div>";
        } else if ($mode == 'text') {
            $string = "<p class='text-{$mode_output}'><b>{$message_header}</b>, {$string['message']}</p>";
        } else if ($mode == 'json') {
            /* No format for output json */
        }
        return $string;
    }

    private function generate_menu_navbar($parent = 0, $result = [], $prefix = '', $parenting_level = 0)
    {
        $output = $result;
        $result = $this->{$this->base_model}->get_menu_by_parent_menu_id($parent, $prefix);
        if (count($result) > 0) {
            foreach ($result as $index => $row) {
                $output[$index] = $row;
                if ($this->{$this->base_model}->get_menu_by_parent_menu_id($row['menu_id'], '', true) > 0) {
                    $output[$index]['menu_child'] = $this->generate_menu_navbar($row['menu_id'], $output, $prefix, $parenting_level);
                }
            }
        }
        return $output;
    }

    private function get_menu_navbar($parent = 0, $params = null)
    {
    }

    protected function display($tpl_content = 'partials/default.php', $data = [], $tpl_footer = 'partials/script_page')
    {
        $config          = $this->config;
        $config_file_js  = $this->file_js;
        $config_file_css = $this->file_css;
        $config['SET_DEFAULT_DATA'] = $this->module_config;
        
        $config_csrf['SESS_USER']   = $this->sess_user;
        $config_csrf['CSRF']        = $this->csrf;
        $config_csrf['CSRF_JSON']   = "&{$this->csrf['name'] }={$this->csrf['hash']}";
        $config_csrf['CSRF_JS']     = $this->csrf_js;

        $page_partials           = [
            'meta_css'           => 'partials/meta_css',
            'header'             => 'partials/header',
            'content'            => $tpl_content,
            'footer'             => 'partials/footer',
            'footer_script'      => 'partials/script',
            'footer_script_page' => $tpl_footer
        ];
        $data['user']            = $this->ion_auth->user()->result_array();
        $data['user_group']      = $this->user_group;
        $data['lang_set']        = $this->lang_active;
        $data['lang_default']    = $this->lang_default;
        $data['menu_previleges'] = $this->menu_previleges;
        
        $data['MENU_POST']          = $this->set_menu_by_mode(0, '', 'post');
        $data['menu_research_list'] = $this->get_menu_reseach_list('1810045BB51A8CA13DA');
        $data['menu_products_list'] = $this->get_menu_reseach_list('1810045BB5234E1E6D2');
        $this->template_admin
            ->title($this->title, (empty($this->config['web_title'])) ? '' : strip_tags($this->config['web_title']))
            ->set('meta_author', $this->meta['author'])
            ->set('meta_keywords', $this->meta['keywords'])
            ->set('meta_description', $this->meta['description'])
            ->set('FILE_CSS', $config_file_css)
            ->set('FILE_JS', $config_file_js)
            ->set('CONFIG', $config)
            ->set('SESS_USER', $config_file_css)
            ->set('CSRF', $config_csrf['CSRF'])
            ->set('CSRF_JSON', $config_csrf['CSRF_JSON'])
            ->set('CSRF_JS', $config_csrf['CSRF_JS'])
            ->set_partial('meta_css', $page_partials['meta_css'])
            // ->set_partial('header', $page_partials['header'])
            ->set_partial('content', $page_partials['content'], $data)
            ->set_partial('footer', $page_partials['footer'])
            ->set_partial('script', $page_partials['footer_script'])
            ->set_partial('script_page', $page_partials['footer_script_page'])
            ->build('themes');
    }

    /* FUNCTION IS CHECK RELATED ON DELETE ACTION */
    protected function is_check_related($table_related = null, $id = null)
    {
        $output          = false;
        $parameter       = [];
        $parameter_join  = [];
        $parameter_where = '';

        /* Check related Required ID from module table to check table related with field column foreign key */
        if ($id != '') {
            if (count($table_related) > 0) {
                /* Function if table related more than one table */
                foreach ($table_related as $index => $row) {
                    $parameter       = [
                        'table'          => $row['table'],
                        'table_fk_id'    => $row['table_fk_id'],
                        'table_fk_value' => $id
                    ];
                    $parameter_join  = (!empty($row['table_join'])) ? $row['table_join'] : '';
                    $parameter_where = (!empty($row['table_where'])) ? $row['table_where'] : '';
                    $output          = $this->{$this->base_model}->is_check_table_related_value($parameter, $parameter_join, $parameter_where);
                }
            } else {
                /* Function if table related is one table */
                $parameter       = [
                    'table'          => $table_related['table'],
                    'table_fk_id'    => $table_related['table_fk_id'],
                    'table_fk_value' => $id
                ];
                $parameter_join  = (!empty($table_related['table_join'])) ? $table_related['table_join'] : '';
                $parameter_where = (!empty($table_related['table_where'])) ? $table_related['table_where'] : '';

                unset($parameter['table_join']);
                $output = $this->{$this->base_model}->is_check_table_related_value((array)$parameter, $parameter_join, $parameter_where);
            }
        }
        return $output;
    }

    protected function generate_array_from_table_column($table = null, $data = null, $mode = null)
    {
        $output           = [];
        $get_column_table = $this->{$this->base_model}->show_table_column($table);

        /* VALIDATE TABLE HAVE COLUMN */
        if (!empty($get_column_table) && is_array($output)) {
            foreach ($get_column_table as $index => $row) {
                if ($row['Key'] != 'PRI') {
                    $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : null;

                    if ($row['Default'] != '') {
                        $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : $row['Default'];
                    }

                    /* SET DEFAULT FOR BASED ON CASE COLUMN */
                    if ($row['Field'] == 'parent') {
                        $output['parent'] = (isset($data['parent'])) ? $data['parent'] : 0;
                    }

                    if ($row['Field'] == 'ordering') {
                        $output['ordering'] = (isset($data['ordering'])) ? $data['ordering'] : 0;
                    }

                    if ($row['Field'] == 'slug') {
                        $output['slug'] = (isset($data['title'])) ? strtolower(url_title($data['title'])) : null;
                    }

                    if ($row['Field'] == 'published') {
                        if ($mode == '') {
                            $output['published'] = (isset($data['published'])) ? date('d/m/Y', strtotime($data['published'])) : null;
                        } else {
                            $output['published'] = (isset($data['published'])) ? date('Y-m-d', strtotime(str_replace('/', '-', $data['published']))) : date('Y-m-d');
                        }
                    }

                    /* ON MODE ADD - CTB & CTD SET */
                    if ($mode != '' || $mode != null) {
                        if ($mode == 'add') {
                            $output['ctb'] = $this->sess_user['user_id'];
                            $output['ctd'] = date('Y-m-d H:i:s');
                        }
                        /* ON ALL MODE - MDB & MDD SET */
                        $output['mdb'] = $this->sess_user['user_id'];
                        $output['mdd'] = date('Y-m-d H:i:s');
                    }
                } else {
                    /* COLUMN PRIMARY CAN SET IF MODE = EDIT */
                    if (($mode != '' || $mode != null) && $mode == 'edit') {
                        $output[$row['Field']] = (isset($data[$row['Field']])) ? $data[$row['Field']] : null;
                    }
                }
            }
        }
        /* ON MODE ADD - CTB & CTD not SET by DEFAULT */
        if ($mode != 'add') {
            unset($output['ctb']);
            unset($output['ctd']);
        }
        return $output;
    }

    protected function generate_array_lang_from_table_column($table = null, $data = null, $mode = null, $related = null)
    {
        $output           = [];
        $get_column_table = $this->{$this->base_model}->show_table_column($table);
        $get_lang         = $this->lang_active;

        $validating=[
            'title' => '',
            'title_sub' => '',
            'short_description' => '',
            'description' => '',
            'note' => '',
        ];
        $key_lang=[
            'title' ,
            'title_sub' ,
            'short_description' ,
            'description' ,
            'note' ,
        ];
        /* VALIDATE TABLE HAVE COLUMN */
        if (!empty($get_column_table) && !empty($get_lang) && is_array($output)) {
            foreach ($get_lang as $index_lang => $row_lang) {
                foreach ($get_column_table as $index => $row) {
                    $lang_id = $row_lang[$this->table_lang_pk];
                    if(in_array($row['Field'],$key_lang)){
                        if(!empty($data[$row['Field']][$lang_id])){
                            $validating[$row['Field']]=$data[$row['Field']][$lang_id];
                        }
                        if(empty($data[$row['Field']][$lang_id])){
                            $data[$row['Field']][$lang_id]=$validating[$row['Field']];
                        }
                    }

                    if ($row['Key'] != 'PRI') {
                        $output[$lang_id][$row['Field']] = (isset($data[$row['Field']][$lang_id])) ? $data[$row['Field']][$lang_id] : '';

                        if ($row['Default'] != '') {
                            $output[$lang_id][$row['Field']] = (isset($data[$row['Field']][$lang_id])) ? $data[$row['Field']][$lang_id] : $row['Default'];
                        }

                        if ($row['Field'] == $this->table_lang_id) {
                            //$output[$lang_id][$this->table_lang_id] = (isset($data[$this->table_lang_id][$lang_id])) ? (($data[$this->table_lang_id][$lang_id])) : $row_lang[$this->table_lang_pk];
                            $output[$lang_id][$this->table_lang_id] = $lang_id;
                            if ((isset($data[$this->table_lang_id][$lang_id]))) {
                                $search = array_search($lang_id, array_keys($data[$this->table_lang_id]));
                                if ($lang_id === $search) {
                                    $output[$lang_id][$this->table_lang_id] = $lang_id;
                                    $output[$lang_id][$this->table_lang_id] = $lang_id;
                                }
                            }
                        }

                        if ($row['Field'] == 'slug') {
                            $output[$lang_id]['slug'] = (isset($data['title'][$lang_id])) ? strtolower(url_title($data['title'][$lang_id])) : '';
                        }

                        if ($mode != '' || $mode != null) {
                            if ($mode == 'add') {
                                $output[$lang_id]['ctb'] = $this->sess_user['user_id'];
                                $output[$lang_id]['ctd'] = date('Y-m-d H:i:s');
                            }
                            $output[$lang_id]['mdb'] = 1;
                            $output[$lang_id]['mdd'] = date('Y-m-d H:i:s');
                        }
                    } else {
                        if (($mode != '' || $mode != null) && $mode == 'edit') {
                            $output[$lang_id][$row['Field']] = (isset($data[$this->table_lang_id][$lang_id])) ? $data[$this->table_lang_id][$lang_id] : '';
                            if ($output[$lang_id][$row['Field']] == '') {
                                $output[$lang_id][$row['Field']] = $this->generate_new_id_string();
                            }
                        }
                    }

                    if ($mode == 'add' && $row['Key'] == 'PRI') {
                        $output[$lang_id][$row['Field']] = $this->generate_new_id_string();
                    }

                    if (!empty($related)) {
                        foreach ($related as $index_related => $row_related) {
                            $output[$lang_id][$index_related] = $row_related;
                        }
                    }

                    if ($mode != 'add') {
                        unset($output[$lang_id]['ctb']);
                        unset($output[$lang_id]['ctd']);
                    }
                    
                }
            }
        }
//        echo '<pre>';
//        var_dump($output);
//        die();
        return $output;
    }

    protected function generate_dropdown_without_language($table = null)
    {

    }

    protected function generate_dropdown_with_language($table = null, $where = null, $group = null, $order = null, $output = 'result', $output_result = 'array')
    {
        $table              = $table;
        $table_init         = 'a';
        $table_id           = 'id';
        $table_related      = "{$table}_lang";
        $table_related_init = 'b';
        $table_related_id   = 'id';
        $table_related_fk   = "{$table}_id";
        $table_lang         = "{$this->table_lang}";
        $table_lang_init    = 'c';
        $table_lang_id      = 'id';
        $table_lang_fk      = $this->table_lang_id;
        $table_title        = 'title';
        $table_slug         = 'slug';

        $result = $this->{$this->base_model}->query_builder(
            "{$table} {$table_init}",
            "
            {$table_init}.{$table_id} as id,
            {$table_related_init}.{$table_title} as title,
            {$table_related_init}.{$table_slug} as slug
            ",
            $where,
            [
                "{$table_related} {$table_related_init}" => [
                    "{$table_init}.{$table_id} = {$table_related_init}.{$table_related_fk}",
                ],
                "{$table_lang} {$table_lang_init}"       => [
                    "{$table_related_init}.{$table_lang_fk} = {$table_lang_init}.{$table_lang_id}",
                ]
            ],
            "{$table_init}.{$table_id}",
            $order,
            ['output' => $output, 'result' => $output_result]
        );
//        echo '<pre>';
//        var_dump($this->db->last_query());
//        die();
        return $result;
    }

    /* SELECT DATA FROM TABLE DATABASE WITH OUTPUT SINGLE DATA - ROW ARRAY */
    protected function table_single_data($table = null, $column = null, $id = null, $select = null, $join = null, $output = 'array')
    {
        $query = $this->{$this->base_model}->show_table_by_column($table, $column, $id, $select, $join);
        if (!empty($query)) {
            if ($output == 'array') {
                $result = $query->row_array();
            } else {
                $result = $query->row();
            }
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* SELECT DATA FROM TABLE DATABASE WITH OUTPUT SINGLE DATA - ROW ARRAY */
    protected function table_multiple_data($table = null, $column = null, $id = null, $select = null, $join = null, $output = 'array')
    {
        $query = $this->{$this->base_model}->show_table_by_column($table, $column, $id, $select, $join);
        if (!empty($query)) {
            if ($output == 'array') {
                $result = $query->result_array();
            } else {
                $result = $query->result();
            }
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* FUNCTION ACTION ADD */
    protected function action_add_process($parameter = [], $id = [], $output_mode = 'json')
    {
        $this->db->trans_start();
        $parameter = (object)$parameter;

        /* 1. VALIDATE WITH FORM VALIDATION */
        if ($this->form_validation->run() === false) {
            $output = $this->output_message($output_mode, 'success', ['status' => false, 'message' => validation_errors()], $parameter->title);
            return $this->output->set_output(json_encode($output));
        }

        $var_add = $this->generate_array_from_table_column($parameter->table, $this->input->post(), 'add');

        /* 2. VALIDATE VARIABLE NOT EMPTY */
        if (empty($var_add)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 3. VALIDATE CHECK DUPLICATE */
        $output_batch = '';
        if (!empty($parameter->duplicate_check)) {
            foreach ($parameter->duplicate_check as $index => $row) {
                /* ACCESS MODEL CHECK TABLE COLUMN DUPLICATE */
                $output_status = $this->{$this->base_model}->is_check_table_column_duplicate(
                    [
                        'table'             => $parameter->table,
                        'table_check_id'    => $index,
                        'table_check_value' => ($index == 'slug') ? $this->input->post('title') : $this->input->post($index)
                    ],
                    (empty($parameter->duplicate_check_join)) ? [] : $parameter->duplicate_check_join,
                    (empty($parameter->duplicate_check_where)) ? [] : $parameter->duplicate_check_where
                );
                if ($output_batch != '') {
                    $output_batch .= '<br/>';
                }
                if (!$output_status) {
                    $output_batch .= "<span class='text-danger text-bold'>ERROR</span> Duplicate <b>{$row}</b>";
                }
            }
            if ($output_batch != '') {
                $output = $this->output_message($output_mode, 'error', ['status' => true, 'message' => $output_batch], $parameter->title);
                $this->set_output($output_mode, $output);
            }
        }

        /* SET PRIMARY KEY */
        $var_add += [
            $parameter->table_id => $this->generate_new_id_string()
        ];

//        echo '<pre>';
//        var_dump($parameter);
//        die();
        $output_status = $this->{$this->base_model}->add_data($parameter->table, $var_add);
        if ($parameter->table == 'product_finger_size') {
            $var_add_measurement = [
                'id'             => $this->generate_new_id_string(),
                'finger_size_id' => $var_add['id'],
                'status'         => '1',
            ];
            $this->{$this->base_model}->add_data('product_measurement', $var_add_measurement);
        }


        /* 5.4 ACTION POST TAGS (INSERT/DELETE) */
        if (!empty($parameter->table_tags)) {
            $parameter_tags = [
                'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                'table_id_value'        => $var_add[$parameter->table_id],                                                          //Post ID value
                'table_tags'            => (!empty($parameter->table_tags)) ? $parameter->table_tags : '',                          //Table tags
                'table_tags_title'      => (!empty($parameter->table_tags_title)) ? $parameter->table_tags_title : '',              //Field Title table tags
                'table_tags_slug'       => (!empty($parameter->table_tags_slug)) ? $parameter->table_tags_slug : '',                //Field Slug table tags
                'table_tags_id'         => (!empty($parameter->table_tags_id)) ? $parameter->table_tags_id : '',                    //Field ID Primary Key table tags
                'table_tags_related'    => (!empty($parameter->table_tags_related)) ? $parameter->table_tags_related : '',          //Table tags detail (related table with post)
                'table_tags_related_id' => (!empty($parameter->table_tags_related_id)) ? $parameter->table_tags_related_id : '',    //Field ID Primary Key table tags detail
                'table_tags_fk'         => (!empty($parameter->table_tags_fk)) ? $parameter->table_tags_fk : '',                    //Field ID Foreign Key table post on table tags detail
                'table_tags_tg_id'      => (!empty($parameter->table_tags_tg_id)) ? $parameter->table_tags_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
            ];
            $this->action_tags_process($parameter_tags, $this->input->post($parameter_tags['table_tags_tg_id']));
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_status) {
                if (!empty($parameter->table_file) && !empty($parameter->table_file_id)) {
                    $this->action_image_upload(
                        [
                            'table'           => $parameter->table_file,
                            'table_id'        => $parameter->table_file_id,
                            'table_file'      => $parameter->table_file_related,
                            'table_related'   => [
                                'id'    => $parameter->table_file_related_id,
                                'value' => $var_add[$parameter->table_id]
                            ],
                            'destination_url' => (empty($parameter->destination_url)) ? '' : $parameter->destination_url,
                            'disabled_edit'   => (empty($parameter->disabled_edit)) ? true : $parameter->disabled_edit,
                            'size'            => [
                                'original'  => (empty($parameter->size_original)) ? $this->size['original'] : $parameter->size_original,
                                'thumbnail' => (empty($parameter->size_thumbnail)) ? $this->size['thumbnail'] : $parameter->size_thumbnail
                            ]
                        ]
                    );
                }
                $output = $this->output_message($output_mode, 'success', ['status' => true], $parameter->title, 'add');
            } else {
                $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'add');
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message($output_mode, 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        $this->set_output($output_mode, $output);
    }

    /* FUNCTION ACTION ADD LANG */
    protected function action_add_lang_process($parameter = [], $id = [], $output_mode = 'json')
    {
        $this->db->trans_start();
        $parameter = (object)$parameter;

        /* 1. VALIDATE WITH FORM VALIDATION */
        if ($this->form_validation->run() === false) {
            $output = $this->output_message($output_mode, 'success', ['status' => false, 'message' => validation_errors()], $parameter->title);
            $this->set_output($output_mode, $output, null);
        }

        /* CREATE ARRAY FROM POST TO VAR ADD CHOOSEN BY COLUMN TABLE */
        $var_add = $this->generate_array_from_table_column($parameter->table, $this->input->post(), 'add');
        /* 2.1. VALIDATE VARIABLE NOT EMPTY */
        if (empty($var_add)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output, null);
        }

        /* SET PRIMARY KEY */
        $var_add += [
            $parameter->table_id => $this->generate_new_id_string()
        ];
        if (!empty($_FILES['document']['name'])) {
            $array                    = explode('.', $_FILES['document']['name']);
            $extension                = end($array);
            $var_add['file_location'] = 'userfiles/post/';
            $var_add['file']          = $var_add['id'] . '.' . $extension;

            $config['file_name']     = $var_add['file'];
            $config['upload_path']   = './userfiles/post/';
            $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg|ogg|mp4|webm';
            $this->load->library('upload', $config);
            $this->upload->do_upload('document');
        }
        /* INIT FOREIGN KEY VALUE TO VARIABLE LANG CHOOSEN BY COLUMN TABLE */
        $var_related = [
            $parameter->table_lang_fk => $var_add[$parameter->table_id]
        ];

        /* CREATE ARRAY FROM POST TO VAR ADD LANG */
        $var_add_lang = $this->generate_array_lang_from_table_column($parameter->table_lang, $this->input->post(), 'add', $var_related);
        if(!empty($parameter->table_product)){
            $data_batch_temporary=[];
            $data_count=0;
            if(!empty($this->input->post($parameter->table_product_key))){
                foreach($this->input->post($parameter->table_product_key) as $index => $key){
                    $data_temporary=[];
                    $data_batch_variant=[];
                    $data_temporary=[
                        $parameter->table_lang_fk               => $var_add[$parameter->table_id],
                        $parameter->table_product_key           => $key,
                        $parameter->table_product_price         => $this->input->post($parameter->table_product_price)[$index],
                        $parameter->table_product_price_promo   => $this->input->post($parameter->table_product_price_promo)[$index],
                        $parameter->table_product_stock         => $this->input->post($parameter->table_product_stock)[$index],
                        'weight'         => $this->input->post('weight')[$index],
                        'color_id'         => $this->input->post('color_id')[$index],
                    ];
                    $data_batch_temporary[$index]=$this->generate_array_from_table_column($parameter->table_product, $data_temporary, 'add');
                    $data_batch_temporary[$index][$parameter->table_id]=$index;
                    foreach($this->input->post('variant')[$index] as $index_variant => $row_variant){
                        $data_batch_variant=[
                            'post_product_id' =>  $index,
                            'variant_type_id' =>  $index_variant,
                            'variant'         =>  $row_variant,
                        ];
                        $data_batch_temporary_variant[$data_count]=$this->generate_array_from_table_column($parameter->table_product_variant, $data_batch_variant, 'add');
                        $data_batch_temporary_variant[$data_count]['id']=$this->generate_new_id_string();
                        $data_count++;
                    }
                }
            }
        }
        /* 2.2. VALIDATE VARIABLE LANG NOT EMPTY */
        if (empty($var_add_lang)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output, null);
        }

        /* 3. VALIDATE CHECK DUPLICATE */
        $output_batch = '';
        if (!empty($parameter->duplicate_check) && !empty($var_add_lang)) {
            foreach ($var_add_lang as $indet => $rowdet) {
                foreach ($parameter->duplicate_check as $index => $row) {
                    /* VALIDATE VALUE POST IS NOT EMPTY. IF EMPTY IGNORE DUPLICATE VALIDATION */
                    $index_value = ($index == 'slug') ? $this->input->post('title') : $this->input->post($index);
                    if ($index_value != '') {
                        /* ACCESS MODEL CHECK TABLE COLUMN DUPLICATE */
                        $output_status = $this->{$this->base_model}->is_check_table_column_duplicate(
                            [
                                'table'             => $parameter->table_lang,
                                'table_check_id'    => $index,
                                'table_check_value' => $index_value[$rowdet[$this->table_lang_id]]
                            ],
                            (empty($parameter->duplicate_check_join)) ? [] : $parameter->duplicate_check_join,
                            (empty($parameter->duplicate_check_where)) ? [] : $parameter->duplicate_check_where
                        );
                        if ($output_batch != '') {
                            $output_batch .= '<br/>';
                        }
                        if (!$output_status) {
                            $output_batch .= "<span class='text-danger text-bold'>ERROR</span> Duplicate <b>{$row}</b>";
                        }
                    }
                }
                if ($output_batch != '') {
                    $output = $this->output_message($output_mode, 'error', ['status' => true, 'message' => $output_batch], $parameter->title);
                    $this->set_output($output_mode, $output, null);
                }
            }
        }
        $output_status = $this->{$this->base_model}->add_data($parameter->table, $var_add);

        $output_status_2 = false;
        if(!empty($data_batch_temporary)){
            $output_status = $this->{$this->base_model}->add_data_batch($parameter->table_product, $data_batch_temporary);
        }
        if(!empty($data_batch_temporary_variant)){
            $output_status = $this->{$this->base_model}->add_data_batch($parameter->table_product_variant, $data_batch_temporary_variant);
        }
        if (!empty($var_add_lang)) {
            $var_batch_add  = [];
            $var_batch_edit = [];

            /* CHECK ID EXIST ON DATABASE */
            foreach ($var_add_lang as $index => $row) {

                $check = $this->{$this->base_model}->query_builder(
                    $parameter->table_lang,
                    "{$parameter->table_lang_id}",
                    [
                        $parameter->table_lang_id => $row[$parameter->table_lang_id]
                    ]
                );

                if (count($check) == 0) {
                    $var_batch_add[$index] = $row;
                } else {
                    $var_batch_edit[$index] = $row;
                }
            }

            if (!empty($var_batch_add)) {


                $output_status_2 = $this->{$this->base_model}->add_data_batch($parameter->table_lang, $var_batch_add);
                $status          = FALSE;
//                if(!$this->ion_auth->in_group(1) && ($parameter->slug_compare!='news' && $parameter->slug_compare!='event')){
////                if(!$this->ion_auth->in_group(1) && ($parameter->slug_compare='news' || $parameter->slug_compare='event')){
//                }else{
//                }
            }
            if (!empty($var_batch_edit)) {
                $output_status_2 = $this->{$this->base_model}->update_data_batch($parameter->table_lang, $var_batch_edit, $parameter->table_lang_id);
            }
        }


        /** Get Data tags from post */
        $post = $this->input->post();



        /* 5.4 ACTION POST TAGS FAQ (INSERT/DELETE) */


         if (!empty($parameter->table_tags)) {
             $parameter_tags = [
                 'table_id_ref_value'    => !empty($parameter->table_id_ref_tags_value) ? $parameter->table_id_ref_tags_value : '',
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_add[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_tags)) ? $parameter->table_tags : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_tags_title)) ? $parameter->table_tags_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_tags_slug)) ? $parameter->table_tags_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_tags_id)) ? $parameter->table_tags_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_tags_related)) ? $parameter->table_tags_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_tags_related_id)) ? $parameter->table_tags_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_tags_fk)) ? $parameter->table_tags_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_tags_tg_id)) ? $parameter->table_tags_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_tags_kt_id)) ? $parameter->table_tags_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post($parameter_tags['table_tags_tg_id']));
         }

         if (!empty($parameter->table_brand)) {
             $parameter_tags = [
                 'table_id_ref_value'    => !empty($parameter->table_id_ref_brand_value) ? $parameter->table_id_ref_brand_value : '',
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_add[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_brand)) ? $parameter->table_brand : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_brand_title)) ? $parameter->table_brand_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_brand_slug)) ? $parameter->table_brand_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_brand_id)) ? $parameter->table_brand_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_brand_related)) ? $parameter->table_brand_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_brand_related_id)) ? $parameter->table_brand_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_brand_fk)) ? $parameter->table_brand_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_brand_tg_id)) ? $parameter->table_brand_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_brand_kt_id)) ? $parameter->table_brand_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post('brand_id'));
         }

         if (!empty($parameter->table_product_tags)) {
             $parameter_tags = [
                 'table_id_ref_value'    => !empty($parameter->table_id_ref_product_tags_value) ? $parameter->table_id_ref_product_tags_value : '',
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_add[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_product_tags)) ? $parameter->table_product_tags : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_product_tags_title)) ? $parameter->table_product_tags_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_product_tags_slug)) ? $parameter->table_product_tags_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_product_tags_id)) ? $parameter->table_product_tags_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_product_tags_related)) ? $parameter->table_product_tags_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_product_tags_related_id)) ? $parameter->table_product_tags_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_product_tags_fk)) ? $parameter->table_product_tags_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_product_tags_tg_id)) ? $parameter->table_product_tags_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_product_tags_kt_id)) ? $parameter->table_product_tags_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post('product_tags_id'));
         }
        /* 5.4 ACTION POST TAGS NEWS (INSERT/DELETE) */


        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_status == TRUE) {
                if (!empty($parameter->table_file) && !empty($parameter->table_file_id)) {
                    $this->action_image_upload(
                        [
                            'table'           => $parameter->table_file,
                            'table_id'        => $parameter->table_file_id,
                            'table_file'      => $parameter->table_file_related,
                            'table_related'   => [
                                'id'        => $parameter->table_file_related_id,
                                'value'     => $var_add[$parameter->table_id],
                                'file_type' => !empty($parameter->table_file_type) ? $parameter->table_file_type : null,
                            ],
                            'destination_url' => (empty($parameter->destination_url)) ? '' : $parameter->destination_url,
                            'disabled_edit'   => (empty($parameter->disabled_edit)) ? true : $parameter->disabled_edit,
                            'size'            => [
                                'original'  => (empty($parameter->size_original)) ? $this->size['original'] : $parameter->size_original,
                                'thumbnail' => (empty($parameter->size_thumbnail)) ? $this->size['thumbnail'] : $parameter->size_thumbnail
                            ]
                        ]
                    );
                }
                $output = $this->output_message($output_mode, 'success', ['status' => true], $parameter->title, 'add');
            } else {
                $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'add');
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message($output_mode, 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        return $this->set_output($output_mode, $output, $var_add[$parameter->table_id]);
    }


    public function mailing($template, $subject)
    {
//        die();
        $transport = Swift_SmtpTransport::newInstance('smtp.mailgun.org', 587, 'tls')
                                        ->setUsername('imajiku.testing@sendermail.utomos.com')
                                        ->setPassword('51110f03a4f9986bc2e6bf6d6713aa8f-fd0269a6-3b02e3a5');
        $message   = Swift_Message::newInstance();

        //Give the message a subject
        $get_admin_mail       = $this->db->query("select config_value,config_desc from acs_config where config_name='admin_mail'")->row();
        $get_account_admin    = $this->db->query("select email,group_id from acs_users join acs_users_groups on user_id=acs_users.id where group_id='1'")->row();
        $get_account_admin_cc = $this->db->query("select email,group_id from acs_users join acs_users_groups on user_id=acs_users.id where group_id='1'")->result();
        $get_admin_mail_cc    = $this->db->query("select config_value,config_desc from acs_config where config_name='mail_cc'")->row();
        $expl_cc              = explode(",", $get_admin_mail_cc->config_value);
        $bcc                  = [];
        foreach ($expl_cc as $row) {
            $bcc[] = $row;
        }
        foreach ($get_account_admin_cc as $row) {
            $bcc[] = $row->email;
        }

        $message->setSubject($subject)
                ->setFrom([str_replace(' ', '', strip_tags($get_admin_mail->config_value)) => str_replace(' ', '', strip_tags($get_admin_mail->config_desc))])
                ->setTo($get_account_admin->email)
                ->setBcc($bcc)
                ->setBody(str_replace(' ', '', strip_tags($get_admin_mail->config_desc)))
                ->addPart($template, 'text/html');

        //Create the Mailer using your created Transport
        $mailer = Swift_Mailer::newInstance($transport);

        //Send the message
        $result = $mailer->send($message);
    }

    /* FUNCTION ACTION EDIT */
    protected function action_edit_process($parameter = [], $id = [], $output_mode = 'json')
    {
        $this->db->trans_start();
        $parameter = (object)$parameter;

        /* 1. VALIDATE WITH FORM VALIDATION */
        if ($this->form_validation->run() === false) {
            $output = $this->output_message($output_mode, 'success', ['status' => true, 'message' => validation_errors()], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        $var_id   = [
            $parameter->table_id => $this->input->post($parameter->table_id)
        ];
        $var_edit = $this->generate_array_from_table_column($parameter->table, $this->input->post(), 'edit');

        /* 2.1. VALIDATE VARIABLE NOT EMPTY */
        if (empty($var_edit)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 2.2. VALIDATE VARIABLE ID NOT EMPTY */
        if (empty($var_id)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 3. VALIDATE CHECK DUPLICATE */
        $output_batch = '';
        if (!empty($parameter->duplicate_check)) {
            foreach ($parameter->duplicate_check as $index => $row) {
                /* ACCESS MODEL CHECK TABLE COLUMN DUPLICATE */
                $output_status = $this->{$this->base_model}->is_check_table_column_duplicate(
                    [
                        'table'             => $parameter->table,
                        'table_check_id'    => $index,
                        'table_check_value' => ($index == 'slug') ? $this->input->post('title') : $this->input->post($index)
                    ],
                    (empty($parameter->duplicate_check_join)) ? [] : $parameter->duplicate_check_join,
                    (empty($parameter->duplicate_check_where)) ? [] : $parameter->duplicate_check_where
                );
                if ($output_batch != '') {
                    $output_batch .= '<br/>';
                }
                if (!$output_status) {
                    $output_batch .= "<span class='text-danger text-bold'>ERROR</span> Duplicate <b>{$row}</b>";
                }
            }
            if ($output_batch != '') {
                $output = $this->output_message($output_mode, 'error', ['status' => true, 'message' => $output_batch], $parameter->title);
                $this->set_output($output_mode, $output);
            }
        }

        if (!empty($parameter->table_file)) {
            if ($parameter->table_file == $parameter->table_file_related) {
                unset($var_edit['file_location']);
                unset($var_edit['file_thumb']);
                unset($var_edit['file_original']);
            }
        }

        $output_status = $this->{$this->base_model}->update_data($parameter->table, $parameter->table_id, $var_id, $var_edit);

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_status) {

                if ($output_status) {
                    if (!empty($parameter->table_file) && !empty($parameter->table_file_id)) {
                        $this->action_image_upload(
                            [
                                'table'           => $parameter->table_file,
                                'table_id'        => $parameter->table_file_id,
                                'table_file'      => $parameter->table_file_related,
                                'table_related'   => [
                                    'id'    => $parameter->table_file_related_id,
                                    'value' => $var_id[$parameter->table_id]
                                ],
                                'destination_url' => (empty($parameter->destination_url)) ? '' : $parameter->destination_url,
                                'disabled_edit'   => (empty($parameter->disabled_edit)) ? true : $parameter->disabled_edit,
                                'size'            => [
                                    'original'  => (empty($parameter->size_original)) ? $this->size['original'] : $parameter->size_original,
                                    'thumbnail' => (empty($parameter->size_thumbnail)) ? $this->size['thumbnail'] : $parameter->size_thumbnail
                                ]
                            ]
                        );
                    }
                    $output = $this->output_message($output_mode, 'success', ['status' => true], $parameter->title, 'edit');
                } else {
                    $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'edit');
                }

            } else {
                $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'edit');
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message($output_mode, 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        $this->set_output($output_mode, $output);
    }

    /* FUNCTION ACTION EDIT */
    protected function action_edit_lang_process($parameter = [], $id = [], $output_mode = 'json')
    {
        
//        echo '<pre>';
//        var_dump($parameter);
//        die();
        $this->db->trans_start();
        $parameter = (object)$parameter;

        /* 1. VALIDATE WITH FORM VALIDATION */
        if ($this->form_validation->run() === false) {
            $output = $this->output_message($output_mode, 'success', ['status' => true, 'message' => validation_errors()], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* SET PRIMARY KEY */
        $var_id = [
            $parameter->table_id => $this->input->post($parameter->table_id)
        ];

        /* INIT FOREIGN KEY VALUE TO VARIABLE LANG CHOOSEN BY COLUMN TABLE */
        $var_related = [
            $parameter->table_lang_fk => $var_id[$parameter->table_id]
        ];

        /* CREATE ARRAY FROM POST TO VAR EDIT CHOOSEN BY COLUMN TABLE */
        $var_edit = $this->generate_array_from_table_column($parameter->table, $this->input->post(), 'edit');
        if (!empty($var_edit['file_location'])) {
            if (!empty($_FILES['document']['name'])) {
                unlink('./' . $var_edit['file_location'] . $var_edit['file']);
                $array                     = explode('.', $_FILES['document']['name']);
                $extension                 = end($array);
                $var_edit['file_location'] = 'userfiles/post/';
                $var_edit['file']          = $var_edit['id'] . '.' . $extension;

                $config['file_name']     = $var_edit['file'];
                $config['upload_path']   = './userfiles/post/';
                $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg|mp4|ogg|webm';
                $this->load->library('upload', $config);
                $this->upload->do_upload('document');
            } else {
                unset($var_edit['file']);
                unset($var_edit['file_location']);
            }
        } else {
            if (!empty($_FILES['document']['name'])) {
                $array                     = explode('.', $_FILES['document']['name']);
                $extension                 = end($array);
                $var_edit['file_location'] = 'userfiles/post/';
                $var_edit['file']          = $var_edit['id'] . '.' . $extension;

                $config['file_name']     = $var_edit['file'];
                $config['upload_path']   = './userfiles/post/';
                $config['allowed_types'] = 'pdf|gif|jpg|png|jpeg|mp4|ogg|webm';
                $this->load->library('upload', $config);
                $this->upload->do_upload('document');
//                echo '<pre>';
//                var_dump($this->upload->do_upload('document'));
//                die();
            } else {
                unset($var_edit['file']);
                unset($var_edit['file_location']);
            }
        }
        /* CREATE ARRAY FROM POST TO VAR ADD LANG */
        $var_edit_lang = $this->generate_array_lang_from_table_column($parameter->table_lang, $this->input->post(), 'edit', $var_related);
        if(!empty($parameter->table_product)){
            $data_batch_temporary=[];
            $data_count=0;
            foreach($this->input->post($parameter->table_product_key) as $index => $key){
                $data_temporary=[];
                $data_batch_variant=[];
                $data_temporary=[
                    $parameter->table_lang_fk               => $var_edit[$parameter->table_id],
                    $parameter->table_product_key           => $key,
                    $parameter->table_product_price         => $this->input->post($parameter->table_product_price)[$index],
                    $parameter->table_product_price_promo   => $this->input->post($parameter->table_product_price_promo)[$index],
                    $parameter->table_product_stock         => $this->input->post($parameter->table_product_stock)[$index],
                        'weight'         => $this->input->post('weight')[$index],
                        'color_id'         => $this->input->post('color_id')[$index],
                ];
                $data_batch_temporary[$index]=$this->generate_array_from_table_column($parameter->table_product, $data_temporary, 'add');
                $data_batch_temporary[$index][$parameter->table_id]=$index;
                foreach($this->input->post('variant')[$index] as $index_variant => $row_variant){
                    $data_batch_variant=[
                        'variant'         =>  $row_variant,
                    ];
                    $data_batch_temporary_variant[$data_count]=$this->generate_array_from_table_column($parameter->table_product_variant, $data_batch_variant, 'edit');
                    $data_batch_temporary_variant[$data_count]['id']=$index_variant;
                    unset($data_batch_temporary_variant[$data_count]['variant_type_id']);
                    unset($data_batch_temporary_variant[$data_count]['post_product_id']);
                    $data_count++;
                }
            }
        }
        /* 2.1. VALIDATE VARIABLE NOT EMPTY */
        if (empty($var_edit)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 2.2. VALIDATE VARIABLE ID NOT EMPTY */
        if (empty($var_id)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 2.3. VALIDATE VARIABLE LANG NOT EMPTY */
        if (empty($var_edit_lang)) {
            $output_message = '<span class="text-danger text-bold">ERROR</span> Please try again.';
            $output         = $this->output_message($output_mode, 'success', ['status' => false, 'message' => $output_message], $parameter->title);
            $this->set_output($output_mode, $output);
        }

        /* 3. VALIDATE CHECK DUPLICATE */
        $output_batch = '';
        if (!empty($parameter->duplicate_check) && !empty($var_edit_lang)) {
            foreach ($var_edit_lang as $indet => $rowdet) {
                foreach ($parameter->duplicate_check as $index => $row) {
                    /* VALIDATE VALUE POST IS NOT EMPTY. IF EMPTY IGNORE DUPLICATE VALIDATION */
                    $index_value = ($index == 'slug') ? $this->input->post('title') : $this->input->post($index);
                    $index_value = $index_value[$indet];

                    if ($index_value != '') {
                        /* ACCESS MODEL CHECK TABLE COLUMN DUPLICATE */
                        $output_status = $this->{$this->base_model}->is_check_table_column_duplicate(
                            [
                                'table'             => $parameter->table_lang,
                                'table_check_id'    => $index,
                                'table_check_value' => $index_value
                            ],
                            (empty($parameter->duplicate_check_join)) ? [] : $parameter->duplicate_check_join,
                            (empty($parameter->duplicate_check_where)) ? [] : $parameter->duplicate_check_where
                        );
                        if ($output_batch != '') {
                            $output_batch .= '<br/>';
                        }
                        if (!$output_status) {
                            $output_batch .= "<span class='text-danger text-bold'>ERROR</span> Duplicate <b>{$row}</b>";
                        }
                    }
                }
                if ($output_batch != '') {
                    $output = $this->output_message($output_mode, 'error', ['status' => true, 'message' => $output_batch], $parameter->title);
                    $this->set_output($output_mode, $output);
                }
            }
        }

        if ($parameter->table_file == $parameter->table_file_related) {
            unset($var_edit['file_location']);
            unset($var_edit['file_thumb']);
            unset($var_edit['file_original']);
        }
        $output_status   = $this->{$this->base_model}->update_data($parameter->table, $parameter->table_id, $var_id, $var_edit);
        if(!empty($data_batch_temporary)){
            foreach($data_batch_temporary as $duplicate_index => $duplicate_row){
                $status_duplicate=$this->{$this->base_model}->product_checker($duplicate_row);
                if($status_duplicate){
                    $var_id_product=[
                        'id' => $duplicate_row['id']
                    ];
                    $output_status   = $this->{$this->base_model}->update_data($parameter->table_product, $parameter->table_id, $var_id_product, $duplicate_row);
                }else{
                    $output_status = $this->{$this->base_model}->add_data($parameter->table_product, $duplicate_row);
                }
            }
        }
        if(!empty($data_batch_temporary_variant)){
            foreach($data_batch_temporary_variant as $duplicate_index => $duplicate_row){
                $status_duplicate=$this->{$this->base_model}->variant_checker($duplicate_row);
                if($status_duplicate){
                    $var_id_variant=[
                        'id' => $duplicate_row['id']
                    ];
                    $output_status   = $this->{$this->base_model}->update_data($parameter->table_product_variant, $parameter->table_id, $var_id_variant, $duplicate_row);
                }else{
                    $output_status = $this->{$this->base_model}->add_data($parameter->table_product_variant, $duplicate_row);
                }
            }
        }
        $output_status_2 = false;
        if (!empty($var_edit_lang)) {
            $var_batch_add  = [];
            $var_batch_edit = [];

            /* CHECK ID EXIST ON DATABASE */
            foreach ($var_edit_lang as $index => $row) {

                $check = $this->{$this->base_model}->query_builder(
                    $parameter->table_lang,
                    "{$parameter->table_lang_id}",
                    [
                        $parameter->table_lang_id => $row[$parameter->table_lang_id]
                    ]
                );

                if (count($check) == 0) {
                    $var_batch_add[$index] = $row;
                } else {
                    $var_batch_edit[$index] = $row;
                }
            }
            if (!empty($var_batch_add)) {
                $output_status_2 = $this->{$this->base_model}->add_data_batch($parameter->table_lang, $var_batch_add);
            }
            if (!empty($var_batch_edit)) {
                $output_status_2 = $this->{$this->base_model}->update_data_batch($parameter->table_lang, $var_batch_edit, $parameter->table_lang_id);
            }
        }

         /* 5.4 ACTION POST TAGS (INSERT/DELETE) */
         if (!empty($parameter->table_tags)) {
             $parameter_tags = [
                 'table_id_ref_value'    => $parameter->table_id_ref_value,
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_id[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_tags)) ? $parameter->table_tags : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_tags_title)) ? $parameter->table_tags_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_tags_slug)) ? $parameter->table_tags_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_tags_id)) ? $parameter->table_tags_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_tags_related)) ? $parameter->table_tags_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_tags_related_id)) ? $parameter->table_tags_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_tags_fk)) ? $parameter->table_tags_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_tags_tg_id)) ? $parameter->table_tags_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_tags_kt_id)) ? $parameter->table_tags_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post($parameter_tags['table_tags_tg_id']));
         }
        

         if (!empty($parameter->table_brand)) {
             $parameter_tags = [
                 'table_id_ref_value'    => $parameter->table_id_ref_brand_value,
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_id[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_brand)) ? $parameter->table_brand : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_brand_title)) ? $parameter->table_brand_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_brand_slug)) ? $parameter->table_brand_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_brand_id)) ? $parameter->table_brand_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_brand_related)) ? $parameter->table_brand_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_brand_related_id)) ? $parameter->table_brand_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_brand_fk)) ? $parameter->table_brand_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_brand_tg_id)) ? $parameter->table_brand_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_brand_kt_id)) ? $parameter->table_brand_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post('brand_id'));
         }

         if (!empty($parameter->table_product_tags)) {
             $parameter_tags = [
                 'table_id_ref_value'    => $parameter->table_id_ref_product_tags_value,
                 'table_id'              => $parameter->table_id,                                                                    //Field ID Primary Key table post
                 'table_id_value'        => $var_id[$parameter->table_id],                                                          //Post ID value
                 'table_tags'            => (!empty($parameter->table_product_tags)) ? $parameter->table_product_tags : '',                          //Table tags
                 'table_tags_title'      => (!empty($parameter->table_product_tags_title)) ? $parameter->table_product_tags_title : '',              //Field Title table tags
                 'table_tags_slug'       => (!empty($parameter->table_product_tags_slug)) ? $parameter->table_product_tags_slug : '',                //Field Slug table tags
                 'table_tags_id'         => (!empty($parameter->table_product_tags_id)) ? $parameter->table_product_tags_id : '',                    //Field ID Primary Key table tags
                 'table_tags_related'    => (!empty($parameter->table_product_tags_related)) ? $parameter->table_product_tags_related : '',          //Table tags detail (related table with post)
                 'table_tags_related_id' => (!empty($parameter->table_product_tags_related_id)) ? $parameter->table_product_tags_related_id : '',    //Field ID Primary Key table tags detail
                 'table_tags_fk'         => (!empty($parameter->table_product_tags_fk)) ? $parameter->table_product_tags_fk : '',                    //Field ID Foreign Key table post on table tags detail
                 'table_tags_tg_id'      => (!empty($parameter->table_product_tags_tg_id)) ? $parameter->table_product_tags_tg_id : '',               //Field ID Foreign Key table tags on table tags detail
                 'table_tags_kt_id'      => (!empty($parameter->table_product_tags_kt_id)) ? $parameter->table_product_tags_kt_id : '',               //Field ID Foreign Key table tags on table tags detail
             ];
             $this->action_tags_process($parameter_tags, $this->input->post('product_tags_id'));
         }



        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_status) {

                if ($output_status) {
                    if (!empty($parameter->table_file) && !empty($parameter->table_file_id)) {
                        $this->action_image_upload(
                            [
                                'table'           => $parameter->table_file,
                                'table_id'        => $parameter->table_file_id,
                                'table_file'      => $parameter->table_file_related,
                                'table_related'   => [
                                    'id'        => $parameter->table_file_related_id,
                                    'value'     => $var_id[$parameter->table_id],
                                    'file_type' => !empty($parameter->table_file_type) ? $parameter->table_file_type : null,

                                ],
                                'destination_url' => (empty($parameter->destination_url)) ? '' : $parameter->destination_url,
                                'disabled_edit'   => (empty($parameter->disabled_edit)) ? true : $parameter->disabled_edit,
                                'size'            => [
                                    'original'  => (empty($parameter->size_original)) ? $this->size['original'] : $parameter->size_original,
                                    'thumbnail' => (empty($parameter->size_thumbnail)) ? $this->size['thumbnail'] : $parameter->size_thumbnail
                                ]
                            ]
                        );
                    }
                    $output = $this->output_message($output_mode, 'success', ['status' => true], $parameter->title, 'edit');
                } else {
                    $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'edit');
                }

            } else {
                $output = $this->output_message($output_mode, 'danger', ['status' => false], $parameter->title, 'edit');
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message($output_mode, 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        $this->set_output($output_mode, $output);
    }

    /* FUNCTION ACTION STATUS */
    protected function action_status_process($parameter = [], $id = [], $multiple = false)
    {
        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'                => 'Name table related',
         *   'table_id'             => 'Field column on table',
         *   'table_value'          => 'Value to check used on table'
         *   'table_status_column'  => 'Field column status on table',
         *   'table_status_value'   => 'Value of new status'
         * ];
         */
        $this->db->trans_start();
        $parameter     = (object)$parameter;
        $column_status = $parameter->table_status_column;
        /* INIT ID TO CHANGE STATUS */
        $var_id = (!empty($id)) ? $id : [$parameter->table_id => $this->input->post($parameter->table_id)];
        if (count($id)) {
            $var_id = [
                $parameter->table_id => $id[0]
            ];
        }

//        if (empty($parameter->table_status_value)) {
//            $result     = $this->table_single_data($parameter->table, $parameter->table_id, $var_id[$parameter->table_id], $column_status);
//            $var_status = [
//                $column_status => ($result[$column_status] == 0) ? 1 : 0
//            ];
//        } else {
        $var_status[$column_status] = $parameter->table_status_value;
//        }
        $output_status = $this->{$this->base_model}->update_data($parameter->table, $parameter->table_id, $var_id, $var_status);

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_status) {
                $output = ($multiple) ? true : $this->output_message('json', 'success', ['status' => true], $parameter->title, 'status');
            } else {
                $output = ($multiple) ? false : $this->output_message('json', 'success', ['status' => true], $parameter->title, 'status');
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        return $this->output->set_output(json_encode($output));
    }

    /* FUNCTION ACTION DELETE */
    protected function action_delete($parameter = [], $id = array(), $multiple = false)
    {
        if($this->check_previleges()){

        /*
         * DOCUMENTATION
         * $parameter = [
         *   'title'                => 'Title module',
         *   'table'                => 'Name table related',
         *   'table_id'             => 'Field column on table',
         * ];
         */
        $parameter     = (object)$parameter;
        $output_status = [];

        if (!empty($parameter)) {
            $var_delete = (!empty($id)) ? $id : [$parameter->table_id => $this->input->post($parameter->table_id)];
            if (count($id)) {
                $var_delete = [$parameter->table_id => $id[0]];
            }

            /* CHECK RELATED TABLE OR TABLE USED DATA */
            $output_related = true;
            if (!empty($parameter->table_related)) {
                $output_related = $this->is_check_related($parameter->table_related, $var_delete[$parameter->table_id]);
            };
//                echo 'text';
//                die();

            if (!$output_related) {
                $output = $this->output_message('json', 'error', ['status' => false], $parameter->title, 'check');
                return ($multiple) ? false : $this->output->set_output(json_encode($output));
            } else {
                $this->db->trans_start();

                if (!empty($parameter->table_file)) {
                    $this->func_image_delete(
                        [
                            'table'         => $parameter->table_file,
                            'table_id'      => $parameter->table_file_id,
                            'table_related' => [
                                $parameter->table_file_related_id => $var_delete[$parameter->table_id]
                            ],
                            'table_join'    => $parameter->table_file_join,
                            'table_groupby' => $parameter->table_file_groupby,
                            'title'         => $parameter->title
                        ],
                        'delete'
                    );
                }
                $data_deleted = '';
                foreach ($var_delete as $index => $row) {
                    if ($parameter->table == 'menu') {
                        $output_data       = $this->db->query("select b.* from menu a join menu_lang b on a.id=b.menu_id where a.id='$row'")->row();
                        $output_data_count = $this->db->query("select b.* from menu a join menu_lang b on a.id=b.menu_id where a.id='$row'")->num_rows();
                        if ($output_data_count > 0) {
                            $data_deleted = $output_data->title;
                        }
                    } else if ($parameter->table == 'post') {
                        $output_data       = $this->db->query("select b.* from post a join post_lang b on a.id=b.post_id where a.id='$row'")->row();
                        $output_data_count = $this->db->query("select b.* from post a join post_lang b on a.id=b.post_id where a.id='$row'")->num_rows();
                        if ($output_data_count > 0) {
                            $data_deleted = $output_data->title;
                        }
                    }
                }
                $output_status = $this->{$this->base_model}->delete_data($parameter->table, $var_delete);
                $this->db->trans_complete();
                if ($this->db->trans_status() === TRUE) {
                    $this->db->trans_commit();
                    if ($output_status) {
                        $output = ($multiple) ? true : $this->output_message('json', 'success', ['status' => true], $data_deleted, 'delete');
                    } else {
                        $output = ($multiple) ? false : $this->output_message('json', 'error', ['status' => false], $data_deleted, 'delete');
                    }
                } else {
                    $this->db->trans_rollback();
                    $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $data_deleted);
                }
            }
        }
            return ($multiple) ? $output : $this->output->set_output(json_encode($output));
        }else{
            $output=$this->output_message('json', 'success', ['status' => true,'message'=>'<strong>You dont have permission to do this :)</strong>']);
            return $this->output->set_output(json_encode($output));
        }
    }

    /* FUNCTION ACTION STATUS WITH CHECKBOX */
    protected function action_checkbox_status($parameter = [])
    {
        
        if($this->check_previleges()){
        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'                => 'Name table related',
         *   'table_id'             => 'Field column on table',
         *   'table_value'          => 'Value to check used on table'
         *   'table_status_column'  => 'Field column status on table',
         *   'table_status_value'   => 'Value of new status'
         * ];
         */
        $this->db->trans_start();
        $parameter    = (object)$parameter;
        $output_batch = '';


        if (!empty($parameter)) {
            if (count($this->input->post($parameter->table_id)) > 0) {

                foreach ($this->input->post($parameter->table_id) as $index => $row) {
                    $output_action = $this->action_status_process(
                        [
                            'table'               => $parameter->table,
                            'table_id'            => $parameter->table_id,
                            'table_status_column' => $parameter->table_status_column,
                            'table_status_value'  => $parameter->table_status_value,
                        ],
                        [$row],
                        true);


                    if ($output_batch != '') {
                        $output_batch .= "<br/>";
                    }
                    if (!$output_action) {
                        $output_batch .= "<b>Failed</b> to update status data ";
                    } else {
                        $output_batch .= "<b>Successfully</b> to update status data ";
                    }
                }
            } else {
                $output = $this->output_message('json', 'error', ['status' => false], $this->title, 'status');
            }
        }

        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if ($output_batch == '') {
                //$output = $this->output_message('json', 'error', ['status' => true], $this->title, 'status');
                $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $parameter->title);
            } else {
                $output = $this->output_message('json', 'success', ['status' => true, 'message' => $output_batch], $this->title);
            }
        } else {
            $this->db->trans_rollback();
            $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $parameter->title);
        }
        return $this->output->set_output(json_encode($output));
            
            
        }else{
            $output=$this->output_message('json', 'success', ['status' => true,'message'=>'<strong>You dont have permission to do this :)</strong>']);
            return $this->output->set_output(json_encode($output));
        }
    }

    /* FUNCTION ACTION DELETE WITH CHECKBOX */
    protected function action_checkbox_delete($parameter = [])
    {
        
        if($this->check_previleges()){
        $parameter    = (object)$parameter;
        $output_batch = '';

        if (!empty($parameter)) {
            if (count($this->input->post($parameter->table_id)) > 0) {

                $this->db->trans_start();

                foreach ($this->input->post($parameter->table_id) as $index => $row) {
                    $output_action = $this->action_delete(
                        [
                            'table'         => $parameter->table,
                            'table_id'      => $parameter->table_id,
                            'table_related' => (empty($parameter->table_check_delete)) ? '' : $parameter->table_check_delete,
                            'title'         => $parameter->title,

                            'table_file'            => (empty($parameter->table_file)) ? '' : $parameter->table_file,
                            'table_file_id'         => (empty($parameter->table_file_id)) ? '' : $parameter->table_file_id,
                            'table_file_related'    => (empty($parameter->table_file_related)) ? '' : $parameter->table_file_related,
                            'table_file_related_id' => (empty($parameter->table_file_related_id)) ? '' : $parameter->table_file_related_id,
                            'table_file_join'       => (empty($parameter->table_file_join)) ? '' : $parameter->table_file_join,
                            'table_file_groupby'    => (empty($parameter->table_file_groupby)) ? '' : $parameter->table_file_groupby,
                            'destination_url'       => (empty($parameter->destination_url)) ? '' : $parameter->destination_url,
                            'disabled_edit'         => true,
                            'size_original'         => (empty($parameter->size_original)) ? '' : $parameter->size_original,
                            'size_thumbnail'        => (empty($parameter->size_thumbnail)) ? '' : $parameter->size_thumbnail,
                        ],
                        [$row],
                        true);

                    if ($output_batch != '') {
                        $output_batch .= "<br/>";
                    }
                    if (!$output_action) {
                        $output_batch .= "<b>Failed</b> to deleted data. Still used in other data ";
                    } else {
                        $output_batch .= "<b>Successfully</b> to deleted data ";
                    }
                }

                $this->db->trans_complete();
                if ($this->db->trans_status() === TRUE) {
                    $this->db->trans_commit();
                    if ($output_batch == '') {
//                    $output = $this->output_message('json', 'error', ['status' => true], $this->title, 'delete');
                        $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $parameter->title);
                    } else {
                        $output = $this->output_message('json', 'success', ['status' => true, 'message' => $output_batch], $this->title);
                    }
                } else {
                    $this->db->trans_rollback();
                    $output = $this->output_message('json', 'error', ['status' => false, 'message' => ''], $parameter->title);
                }
            } else {
                $output = $this->output_message('json', 'error', ['status' => false], $this->title, 'delete');
            }
            return $this->output->set_output(json_encode($output));
        }
            
            
        }else{
            $output=$this->output_message('json', 'success', ['status' => true,'message'=>'<strong>You dont have permission to do this :)</strong>']);
            return $this->output->set_output(json_encode($output));
        }
    }

    /* ACTION IMAGE UPLOAD */
    protected function action_image_upload($parameter = null)
    {
        $parameter = (object)$parameter;
        $this->db->trans_start();

        $var_file = $this->func_image_upload(
            $parameter->destination_url,
            $parameter->table_related,
            [
                'table' => $parameter->table,
                'id'    => $parameter->table_id,
            ],
            $parameter->disabled_edit,
            (empty($parameter->size)) ? '' : $parameter->size
        );

        if (!empty($parameter->table_file)) {
            if (!empty($var_file['data']['add'])) {
                if ($parameter->table == $parameter->table_file) {
                    if (!empty($var_file['data']['add'])) {
                        foreach ($var_file['data']['add'] as $index => $row) {
                            $var_file['data']['add'][$index][$parameter->table_related['id']] = $parameter->table_related['value'];
                            $var_id[$parameter->table_related['id']]                          = $parameter->table_related['value'];
                            unset($var_file['data']['add'][$index][$parameter->table_related['id']]);
                            $this->{$this->base_model}->update_data($parameter->table, $parameter->table_related['id'], $var_id, $var_file['data']['add'][$index]);
                        }
                    }
                } else {
                    $this->{$this->base_model}->add_data_batch($parameter->table, $var_file['data']['add']);
                }
            }
            if (!empty($var_file['data']['edit'])) {
                $this->{$this->base_model}->update_data_batch($parameter->table, $var_file['data']['edit'], $parameter->table_id);
            }
        }

        $this->db->trans_complete();
        return ($this->db->trans_status() === TRUE) ? true : false;

    }

    /* FUNCTION IMAGE UPLOAD */
    protected function func_image_upload(
        $destination_url = null,
        $table_related = [],
        $table_file = [],
        $disable_edit = true,
        $size = ['original' => 1000, 'thumbnail' => 220]
    )
    {

        $url_original = $destination_url;
        $url_thumb    = $destination_url . '/thumb';

        $config["generate_image_file"]          = true;
        $config["generate_thumbnails"]          = true;
        $config["image_max_size"]               = $size['original'];                    // MAXIMUM IMAGE SIZE (HEIGHT AND WIDTH)
        $config["thumbnail_size"]               = $size['thumbnail'];                   // THUMBNAILS WILL BE CROPPED TO 200X200 PIXELS
        $config["thumbnail_prefix"]             = "thumb_";                             // NORMAL THUMB PREFIX
        $config["destination_folder"]           = "{$url_original}/";                   // UPLOAD DIRECTORY ENDS WITH / (SLASH)
        $config["thumbnail_destination_folder"] = "{$url_thumb}/";                      // UPLOAD DIRECTORY ENDS WITH / (SLASH)
        $config["quality"]                      = 90;                                   // JPEG QUALITY
        $config["random_file_name"]             = TRUE;                                 // RANDOMIZE EACH FILE NAME
        $config["upload_url"]                   = base_url($url_original) . '/';
        $config["upload_url_thumbs"]            = base_url($url_thumb) . '/';

        $this->load->library('image_upload_resize');
        $config["file_data"] = !empty($_FILES["__files"]) ? $_FILES["__files"] : null;

        $data_file = $config["file_data"]['type'][0];
        $type_file = explode('/', $data_file);

        $output_editable = array();
        if ($disable_edit) {
            if (!empty($table_file)) {
                $output_editable = $this->func_image_update(
                    $_FILES['__files'],
                    [
                        'table'         => $table_file['table'],
                        'table_id'      => $table_file['id'],
                        'table_related' => $table_related,
                        'table_join'    => null,
                        'table_groupby' => null,
                    ]
                );
            }

        }


        $responses = $this->image_upload_resize->resize($config);
        try {
            $variable['add']  = null;
            $variable['edit'] = null;

            if (!empty($responses['images'])) {
                $names = [];
                if (!empty($this->input->post('__files_name'))) {
                    $names = $this->input->post('__files_name');
                }
                foreach ($responses["images"] as $index => $response) {
                    $config["destination_folder"] = str_replace('//', '/', $config["destination_folder"]);
                    $file_name                    = '';
                    if (!empty($names[$index])) {
                        $file_name = $names[$index];
                    }

                    if ($response != '') {
                        if (!isset($output_editable[$index])) {
                            $variable['add'][$index] = [
                                $table_related['id'] => $table_related['value'],
                                $table_file['id']    => $this->generate_new_id_string() . get_random_alphanumeric(5),
                                'file_thumb'         => 'thumb_' . $response,
                                'file_name'          => $file_name,
                                'file_original'      => $response,
                                'file_location'      => $config["destination_folder"],
                                'file_type'          => $type_file[0],
                                'ctb'                => 1,
                                'ctd'                => date('Y-m-d H:i:s'),
                                'mdb'                => 1,
                                'mdd'                => date('Y-m-d H:i:s'),
                            ];
                            if ($table_related['id'] == $table_file['id']) {
                                unset($variable['add'][$index][$table_file['id']]);
                            }
                        } else {
                            $variable['edit'][] = array(
                                $table_file['id']    => $output_editable[$index],
                                $table_related['id'] => $table_related['value'],
                                'file_name'          => $file_name,
                                'file_thumb'         => 'thumb_' . $response,
                                'file_original'      => $response,
                                'file_location'      => $config["destination_folder"],
                                'file_type'          => $type_file[0],
                                'mdb'                => 1,
                                'mdd'                => date('Y-m-d H:i:s'),
                            );
                        }
                    }
                }

            }
            $output['data']    = $variable;
            $output['status']  = true;
            $output['message'] = '';
        } catch (Exception $e) {
            $output['data']    = null;
            $output['status']  = false;
            $output['message'] = $e->getMessage();
        }
        return $output;

    }

    protected function func_video_upload(
        $destination_url = null,
        $table_related = [],
        $data,
        $disable_edit = true,
        $size = ['original' => 1000, 'thumbnail' => 220]
    )
    {

        try {
            $variable['add']  = null;
            $variable['edit'] = null;

            if (!empty($data)) {


                $variable['add'] = [
                    $table_related['id'] => $table_related['value'],
                    'id'                 => $this->generate_new_id_string() . get_random_alphanumeric(5),
                    'file_thumb'         => '',
                    'file_name'          => '',
                    'file_original'      => $data,
                    'file_location'      => '',
                    'file_type'          => 'video',
                    'ctb'                => 1,
                    'ctd'                => date('Y-m-d H:i:s'),
                    'mdb'                => 1,
                    'mdd'                => date('Y-m-d H:i:s'),
                ];
                if ($table_related['id'] == 'id') {
                    unset($variable['add']);
                }
                // } else {
                //     $variable['edit'][] = array(
                //         $table_file['id']    => $output_editable[$index],
                //         $table_related['id'] => $table_related['value'],
                //         'file_name'          => $file_name,
                //         'file_thumb'         => 'thumb_' . $response,
                //         'file_original'      => $response,
                //         'file_location'      => $config["destination_folder"],
                //         'file_type'          =>  $type_file[0],
                //         'mdb'                => 1,
                //         'mdd'                => date('Y-m-d H:i:s'),
                //     );
                // }
            }


            $output['data']    = $variable;
            $output['status']  = true;
            $output['message'] = '';
        } catch (Exception $e) {
            $output['data']    = null;
            $output['status']  = false;
            $output['message'] = $e->getMessage();
        }
        return $output;

    }

    /* FUNCTION UPDATE DATABASE AFTER IMAGE UPLOAD */
    protected function func_image_update($parameter_files = null, $parameter = array())
    {
        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'                => 'Name table file',
         *   'table_id'             => 'Field column on table file',
         *   'table_related'        => 'Condition where, use name table relation from table file',
         *   'table_join'           => 'Join another table (optional)'
         *   'table_groupby'        => 'Condition groupby  (optional)'
         * ];
         */

        $output    = array();
        $parameter = (object)$parameter;
        if (!empty($parameter)) {
            $file_type = $parameter->table_related['file_type'];
            if (!empty($parameter->table_related)) {
                $parameter->table_related = [
                    $parameter->table_related['id'] => $parameter->table_related['value']
                ];
            }
            if (!empty($file_type)) {
                $parameter->table_related[$file_type] = '';
            }

            $result = $this->{$this->base_model}->query_builder(
                $parameter->table,
                "{$parameter->table_id}, file_location, file_thumb, file_original",
                (empty($parameter->table_related)) ? [] : $parameter->table_related,
                (empty($parameter->table_join)) ? [] : $parameter->table_join,
                (empty($parameter->table_groupby)) ? '' : $parameter->table_groupby,
                null,
                [
                    'output' => 'result',
                    'result' => 'array'
                ]
            );

            if (count($result) > 0) {
                foreach ($result as $index => $row) {
                    if (!empty($parameter_files['name'][$index]) && $parameter_files['name'][$index] != '') {
                        $file_thumbnail = "{$row['file_location']}thumb/{$row['file_thumb']}";
                        $file_original  = "{$row['file_location']}/{$row['file_original']}";

                        if (file_exists($file_thumbnail)) {
                            unlink($file_thumbnail);
                        }
                        if (file_exists($file_original)) {
                            unlink($file_original);
                        }
                        $output[$index] = $row[$parameter->table_id];
                    }
                }
            }
        }
        return $output;
    }

    /* FUNCTION IMAGE DELETE BY RELATED ID */
    protected function func_image_delete($parameter = null, $output_action = 'delete', $output_mode = 'json')
    {
        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'                => 'Name table file',
         *   'table_id'             => 'Field column on table file',
         *   'table_related'        => 'Condition where, use name table relation from table file',
         *   'table_join'           => 'Join another table (optional)'
         *   'table_groupby'        => 'Condition groupby  (optional)'
         * ];
         */

        $output        = array();
        $output_unlink = array();
        $parameter     = (object)$parameter;

        $this->db->trans_start();

        if (!empty($parameter)) {
            $result = $this->{$this->base_model}->query_builder(
                $parameter->table,
                "{$parameter->table_id}, file_location, file_thumb, file_original",
                (empty($parameter->table_related)) ? [] : $parameter->table_related,
                (empty($parameter->table_join)) ? [] : $parameter->table_join,
                (empty($parameter->table_groupby)) ? '' : $parameter->table_groupby,
                null,
                [
                    'output' => 'result',
                    'result' => 'array'
                ]
            );
            if (count($result) > 0) {
                foreach ($result as $index => $row) {
                    $file_thumbnail = "{$row['file_location']}thumb/{$row['file_thumb']}";
                    $file_original  = "{$row['file_location']}/{$row['file_original']}";

                    $output_unlink[$index] = [
                        'original'  => is_file($file_original) ? $file_original : null,
                        'thumbnail' => is_file($file_thumbnail) ? $file_thumbnail : null,
                    ];

                    if ($output_action == 'delete') {
                        $output[$index] = $row[$parameter->table_id];
                    } else {
                        $output[$index] = [
                            $parameter->table_id => $row[$parameter->table_id],
                            'file_thumb'         => null,
                            'file_original'      => null,
                            'file_location'      => null,
                        ];
                    }
                }
            }

        }
        if ($output_action == 'delete') {
            if (!empty($output)) {
                $this->{$this->base_model}->delete_data_batch($parameter->table, $parameter->table_id, $output);
            }
        } else if ($output_action == 'update') {
            if (!empty($output)) {
                $this->{$this->base_model}->update_data_batch($parameter->table, $output, $parameter->table_id);
            }
        }
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            $this->db->trans_commit();
            if (count($output_unlink) > 0) {
                foreach ($output_unlink as $index => $row) {
                    $file_thumbnail = $row['thumbnail'];
                    $file_original  = $row['original'];

                    if ($file_thumbnail != '') {
                        if (file_exists($file_thumbnail)) {
                            unlink($file_thumbnail);
                        }
                    }
                    if ($file_original != '') {
                        if (file_exists($file_original)) {
                            unlink($file_original);
                        }
                    }
                }
            }
            $message = [
                'status'  => true,
                'message' => "Picture {$parameter->title} deleted"
            ];
        } else {
            $this->db->trans_rollback();
            $message = [
                'status'  => false,
                'message' => "Picture {$parameter->title} can't delete"
            ];
        }
        $output_status = ($message['status'] == TRUE) ? 'success' : 'error';
        $output        = $this->output_message($output_mode, $output_status, $message, $parameter->title);
        $this->set_output($output_mode, $output);
    }

    protected function add_watermark($image = [])
    {
        $get_config = $this->{$this->base_model}->get_group_config();
        $this->load->library('image_lib');
        $config['image_library']    = 'GD2';
        $config['wm_text']          = strip_tags($this->config['web_title']);
        $config['wm_vrt_alignment'] = 'bottom';
        $config['wm_hor_alignment'] = 'right';
        $config['wm_padding']       = '-25';
        // $config['wm_type']          = 'text';
        // $config['wm_font_path']     = './system/fonts/texb.ttf';
        // $config['wm_font_size']     = '20';
        // $config['wm_font_color']    = 'ffffff';
        $config['wm_type']         = 'overlay';
        $config['wm_overlay_path'] = $get_config[1]['file_location'] . $get_config[1]['file_original'];
        $config['wm_opacity']      = '1';
        // $config['wm_x_transp']      = 1;
        // $config['wm_y_transp']      = 1;
        $config['wm_hor_offset'] = '10';
        $config['wm_vrt_offset'] = '10';
        try {
            foreach ($image as $key) {
                // echo $image;
                $config['source_image'] = $key['file_location'];
                $this->image_lib->initialize($config);
                $this->image_lib->watermark();
            }
            return $data = array(
                'status'  => True,
                'massage' => 'success'
            );
        } catch (Exception $e) {
            return $data = array(
                'status'  => false,
                'massage' => $e->getMessage()
            );

        }

    }

    public function get_menu_reseach_list($id = null)
    {
        return $result = $this->{$this->base_model}->query_builder(
            $this->config_menu['table'],
            "{$this->config_menu['table_related']}.slug,{$this->config_menu['table_related']}.title",
            ["{$this->config_menu['table']}.parent" => $id],
            [
                "{$this->config_menu['table_related']}" => [
                    "{$this->config_menu['table']}.{$this->config_menu['table_id']} ={$this->config_menu['table_related']}.{$this->config_menu['table_related_fk']}", 'left']
            ],
            null,
            null,
            [
                'output' => 'result',
                'result' => 'array'
            ]
        );
    }

    protected function action_tags_process($parameter = null, $data = null)
    {
        /**
         * Sample for parameter
         * $parameter = [
         *  'table_id'              => '', //Field ID Primary Key table post
         *  'table_id_ref_value'    => '', //Post ID value
         *  'table_id_value'        => '', //Post ID value
         *  'table_tags'            => '', //Table tags
         *  'table_tags_title'      => '', //Field Title table tags
         *  'table_tags_slug'       => '', //Field Slug table tags
         *  'table_tags_id'         => '', //Field ID Primary Key table tags
         *  'table_tags_related'    => '', //Table tags detail (related table with post)
         *  'table_tags_related_id' => '', //Field ID Primary Key table tags detail
         *  'table_tags_fk'         => '', //Field ID Foreign Key table post on table tags detail
         *  'table_tags_tg_id'     => '', //Field ID Foreign Key table tags on table tags detail
         * ];
         */
        $parameter = (object)$parameter;

        $var_tags_new              = [];
        $var_tags_related_existing = [];
        $var_tags_related_add      = [];
        $var_tags_related_add_temp = [];
        $var_tags_delete_check     = [];
        $var_tags_delete_value     = [];

        if (!empty($data)) {


            foreach ($data as $index => $row) {

                $check_tags_exist =
                    $this->{$this->base_model}->query_builder(
                        $parameter->table_tags,
                        "{$parameter->table_tags_id}",
                        [
                            $parameter->table_tags_id => $row,
                        ]
                    );


                /* Declare dataset for table tags related */
                $content_data = [
                    $parameter->table_tags_related_id => $this->generate_new_id_string(),
                    $parameter->table_tags_fk         => $parameter->table_id_value
                ];

                if (count($check_tags_exist) == 0) {
                    /* Declare dataset for table tags new data */
                    $var_tags_new[$index] = $this->generate_array_from_table_column($parameter->table_tags, null, 'add');

                    $var_tags_new[$index][$parameter->table_tags_id]    = $this->generate_new_id_string();
                    $var_tags_new[$index][$parameter->table_tags_title] = $row;
                    $var_tags_new[$index][$parameter->table_tags_slug]  = strtolower(url_title($var_tags_new[$index][$parameter->table_tags_title]));
                    $var_tags_new[$index]['status']                     = '1';
                    if (!empty($parameter->table_id_ref_value)) {
                        $var_tags_new[$index]['tags_kt_id'] = $parameter->table_id_ref_value;
                    }
                    $content_data[$parameter->table_tags_tg_id] = $var_tags_new[$index]['id'];


                } else {
                    $result_tags_exist = $check_tags_exist->row();

                    $check_tags_exist->free_result();
                    $content_data[$parameter->table_tags_tg_id] = $result_tags_exist->{$parameter->table_tags_id};

                }
                $var_tags_related_add_temp[] = $content_data;
                $var_tags_delete_check[]     = $content_data[$parameter->table_tags_tg_id];
            }

            // $result_tags_existing =
            //     $this->{$this->base_model}->query_builder(
            //         $parameter->table_tags_related,
            //         "{$parameter->table_tags_tg_id}",
            //         [
            //             $parameter->table_tags_fk => $parameter->table_id_value,
            //         ],
            //         null,
            //         null,
            //         null,
            //         ['output' => 'result', 'result' => 'array']
            //     );

            $result_tags_existing =
                $this->{$this->base_model}->query_builder(
                    $parameter->table_tags_related,
                    "{$parameter->table_tags_related}.{$parameter->table_tags_tg_id}",
                    [
                        $parameter->table_tags_fk => $parameter->table_id_value,
                        "tags.tags_kt_id"         => $parameter->table_id_ref_value,
                    ],
                    [
                        "tags" => "tags.id={$parameter->table_tags_related}.tags_id"
                    ],
                    null,
                    null,
                    ['output' => 'result', 'result' => 'array']
                );


            /*
             * ===========================================================================================
             * 1. GET EXISTING DATA LOCATION
             * 2. INIT VARIABLE EXISTING
             * 3. CREATE DATASET DELETE VALUE
             * ===========================================================================================
             */
            if (!empty($result_tags_existing)) {
                foreach ($result_tags_existing as $index => $row) {
                    $value                       = $row[$parameter->table_tags_tg_id];
                    $var_tags_related_existing[] = $value;

                    if (!in_array($value, $var_tags_delete_check)) {
                        $var_tags_delete_value[] = $value;
                    }
                }
            }

            /*
             * ===========================================================================================
             * VALIDATE ADDED DATA WITH EXISTING VALUE
             * ===========================================================================================
             */

            if (!empty($var_tags_related_add_temp)) {
                foreach ($var_tags_related_add_temp as $index => $row) {
                    if (!in_array($row[$parameter->table_tags_tg_id], $var_tags_related_existing)) {
                        $var_tags_related_add[] = $row;
                    }
                }
            }


            /**
             * ACTION ADD
             * NEW LOCATION ADDED
             */
            if (!empty($var_tags_new)) {
                $this->{$this->base_model}->add_data_batch($parameter->table_tags, $var_tags_new);
            }
            /**
             * ACTION ADD
             * NEW RELATED LOCATION ADDED
             */
            if (!empty($var_tags_related_add)) {
                $this->{$this->base_model}->add_data_batch($parameter->table_tags_related, $var_tags_related_add);
            }
            /**
             * ACTION DELETE
             * DATA NOT ON POST, DELETED
             */

            //   echo "<pre>";
            //    var_dump($var_tags_delete_value);


            if (!empty($var_tags_delete_value)) {
                $this->{$this->base_model}->delete_data_batch($parameter->table_tags_related, $parameter->table_tags_tg_id, $var_tags_delete_value);
            }
            // var_dump($this->db->last_query());
            // var_dump('saya masuk if delete');

        } else {
            /**
             * ACTION DELETE
             * ALL LOCATION FROM POST
             */
            $checking = $this->db->query("select post_tags.id from post_tags join tags on tags_id=tags.id where post_id='{$parameter->table_id_value}' and tags_kt_id='{$parameter->table_id_ref_value}'")->result_array();
//            echo '<pre>';
//            var_dump($checking);
//            var_dump($this->db->last_query());
//            die();
            foreach ($checking as $index => $row) {
                $var_tags_related_delete = [
                    'id' => $row['id']
                ];
                $this->{$this->base_model}->delete_data($parameter->table_tags_related, $var_tags_related_delete);

            }

        }
    }

    /**
     * ACTION POST TAGS
     */
    protected function action_tags_process_custom($parameter = null, $data = null)
    {

        /**
         * Sample for parameter
         * $parameter = [
         *  'table_id'              => '', //Field ID Primary Key table post
         *  'table_id_ref_value'    => '', //Post ID value
         *  'table_id_value'        => '', //Post ID value
         *  'table_tags'            => '', //Table tags
         *  'table_tags_title'      => '', //Field Title table tags
         *  'table_tags_slug'       => '', //Field Slug table tags
         *  'table_tags_id'         => '', //Field ID Primary Key table tags
         *  'table_tags_related'    => '', //Table tags detail (related table with post)
         *  'table_tags_related_id' => '', //Field ID Primary Key table tags detail
         *  'table_tags_fk'         => '', //Field ID Foreign Key table post on table tags detail
         *  'table_tags_tg_id'     => '', //Field ID Foreign Key table tags on table tags detail
         * ];
         */
        $parameter = (object)$parameter;


        $var_tags_new              = [];
        $var_tags_related_existing = [];
        $var_tags_related_add      = [];
        $var_tags_related_add_temp = [];
        $var_tags_delete_check     = [];
        $var_tags_delete_value     = [];
        if (!empty($data)) {
            $where_tags_kt_id['tags_kt_id'] = $parameter->table_id_ref_value;

            foreach ($data as $index => $row) {
                $check_tags_exist =
                    $this->{$this->base_model}->query_builder(
                        $parameter->table_tags,
                        "{$parameter->table_tags_id}",
                        [
                            $parameter->table_tags_id => $row,
                            $where_tags_kt_id
                        ]
                    );


                /* Declare dataset for table tags related */
                $content_data = [
                    $parameter->table_tags_related_id => $this->generate_new_id_string(),
                    $parameter->table_tags_fk         => $parameter->table_id_value
                ];


                if (count($check_tags_exist) == 0) {
                    /* Declare dataset for table tags new data */
                    $var_tags_new[$index] = $this->generate_array_from_table_column($parameter->table_tags, null, 'add');

                    $var_tags_new[$index][$parameter->table_tags_id]    = $this->generate_new_id_string();
                    $var_tags_new[$index][$parameter->table_tags_title] = $row;
                    $var_tags_new[$index][$parameter->table_tags_slug]  = strtolower($var_tags_new[$index][$parameter->table_tags_title]);
                    $var_tags_new[$index]['status']                     = '1';
                    if (!empty($parameter->table_id_ref_value)) {
                        $var_tags_new[$index]['tags_kt_id'] = $parameter->table_id_ref_value;
                    }
                    $content_data[$parameter->table_tags_tg_id] = $var_tags_new[$index]['id'];
                } else {
                    $result_tags_exist = $check_tags_exist->row();
                    $check_tags_exist->free_result();
                    $content_data[$parameter->table_tags_tg_id] = $result_tags_exist->{$parameter->table_tags_id};
                }
                $var_tags_related_add_temp[] = $content_data;
                $var_tags_delete_check[]     = $content_data[$parameter->table_tags_tg_id];

            }


            $result_tags_existing =
                $this->{$this->base_model}->query_builder(
                    $parameter->table_tags_related,
                    "{$parameter->table_tags_tg_id}",
                    [
                        $parameter->table_tags_fk => $parameter->table_id_value,
                        'tags_kt_id'              => $parameter->table_tags_kt_id,
                    ],
                    [
                        "tags" => "tags.id={$parameter->table_tags_related}.tags_id"
                    ],
                    null,
                    null,
                    ['output' => 'result', 'result' => 'array']
                );


            /*
             * ===========================================================================================
             * 1. GET EXISTING DATA LOCATION
             * 2. INIT VARIABLE EXISTING
             * 3. CREATE DATASET DELETE VALUE
             * ===========================================================================================
             */
            if (!empty($result_tags_existing)) {
                foreach ($result_tags_existing as $index => $row) {
                    $value                       = $row[$parameter->table_tags_tg_id];
                    $var_tags_related_existing[] = $value;

                    if (!in_array($value, $var_tags_delete_check)) {
                        if (!empty($parameter->table_id_ref_value)) {
                            $var_tags_delete_value[] = array(
                                'tags_kt_id' => $parameter->table_id_ref_value,
                                'tags_id'    => $value
                            );
                        } else {
                            $var_tags_delete_value[] = $value;
                        }
                    }
                }
            }


            /*
             * ===========================================================================================
             * VALIDATE ADDED DATA WITH EXISTING VALUE
             * ===========================================================================================
             */
            if (!empty($var_tags_related_add_temp)) {
                foreach ($var_tags_related_add_temp as $index => $row) {
                    if (!in_array($row[$parameter->table_tags_tg_id], $var_tags_related_existing)) {
                        $var_tags_related_add[] = $row;
                    }
                }
            }


            /**
             * ACTION ADD
             * NEW LOCATION ADDED
             */
            if (!empty($var_tags_new)) {
                $this->{$this->base_model}->add_data_batch($parameter->table_tags, $var_tags_new);
            }
            /**
             * ACTION ADD
             * NEW RELATED LOCATION ADDED
             */
            if (!empty($var_tags_related_add)) {
                $this->{$this->base_model}->add_data_batch($parameter->table_tags_related, $var_tags_related_add);
            }
            /**
             * ACTION DELETE
             * DATA NOT ON POST, DELETED
             */
            if (!empty($var_tags_delete_value)) {
                $this->{$this->base_model}->delete_data_batch($parameter->table_tags_related, $parameter->table_tags_tg_id, $var_tags_delete_value);
            }
            // if (!empty($var_tags_delete_value)) {
            //     if (!empty($parameter->table_id_ref_value)) {    
            //         $var_tags_delete_parameter[] = $parameter->table_tags_tg_id;
            //         $var_tags_delete_parameter[] = 'tags_kt_id';
            //     } else {                
            //         $var_tags_delete_parameter = $parameter->table_tags_tg_id;
            //     }
            //     $this->{$this->base_model}->delete_data_batch($parameter->table_tags_related, $var_tags_delete_parameter, $var_tags_delete_value);
            // }
        } else {
            /**
             * ACTION DELETE
             * ALL LOCATION FROM POST
             */


            $checking = $this->db->query("select post_tags.id from post_tags join tags on tags_id=tags.id where post_id='{$parameter->table_id_value}' and tags_kt_id='{$parameter->table_tags_kt_id}'")->result_array();
            foreach ($checking as $index => $row) {
                $var_tags_related_delete = [
                    'id' => $row['id']
                ];
                $this->{$this->base_model}->delete_data($parameter->table_tags_related, $var_tags_related_delete);

            }

//            $var_tags_related_delete = [
//                $parameter->table_tags_fk => $parameter->table_id_value
//            ];
//            $this->{$this->base_model}->delete_data($parameter->table_tags_related, $var_tags_related_delete);
        }
    }


    protected function generate_konten_by_menu_id($content_menu_value = null, $content_post_value = null, $content_table = null, $content_table_menu_id = null, $content_table_post_id = null)
    {
        $output = [];
        $result = $this->{$this->base_model}->admin_get_content_kt_by_menu_id($content_menu_value);
        if (!empty($result)) {
            foreach ($result as $index => $row) {
                if ($row['content_name'] != '') {
                    $result = [];
                    if ($content_table != '' && $content_table_post_id != '') {
                        $result = $this->{$this->base_model}->query_builder(
                            $content_table,
                            "id, content_value",
                            [
                                $content_table_menu_id => $row['content_kt_menu_id'],
                                $content_table_post_id => $content_post_value,
                            ],
                            null,
                            null,
                            null,
                            ['output' => 'row', 'result' => 'result']
                        );
                    }

                    $output[$row['content_name']] = [
                        'id_kt'  => $row['content_kt_menu_id'],
                        'id_ref' => $row['content_kt_id'],
                        'title'  => $row['title'],
                        'slug'   => $row['slug'],
                        'type'   => $row['content_type'],
                        'id'     => (!empty($result)) ? $result->id : null,
                        'value'  => (!empty($result)) ? $result->content_value : $row['content_format'],
                    ];
                }
            }
        }
        return $output;
    }
}
