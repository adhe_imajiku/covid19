<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['admins'] = 'mod_dashboard';

$route['home/lme']                     = 'home/get_market_lme';
$route['contact-us']                   = 'contactus/index';
$route['faq']                          = 'home/faq';
$route['news']                         = 'post/news';
$route['news/(:any)']                  = 'post/news/$1';
$route['news/post/(:any)']             = 'post/news_detail/$1';
$route['berita']                       = 'post/news';
$route['berita/(:any)']                = 'post/news/$1';
$route['berita/post/(:any)']           = 'post/news_detail/$1';
$route['event']                        = 'post/event';
$route['event/(:any)']                 = 'post/event/$1';
$route['event/post/(:any)']            = 'post/event_detail/$1';
$route['kegiatan']                     = 'post/event';
$route['kegiatan/(:any)']              = 'post/event/$1';
$route['kegiatan/post/(:any)']         = 'post/event_detail/$1';
$route['photo']                        = 'post/photo';
$route['market-updates']               = 'post/market_updates';
$route['photo/post/(:any)']            = 'post/photo_detail/$1';
$route['photo/(:any)/(:any)']          = 'post/photo/(:any)/(:any)';
$route['video']                        = 'post/video';
$route['video/(:any)']                 = 'post/video/(:any)';
$route['video/(:any)/(:any)']          = 'post/video/(:any)/(:any)';
$route['lang/(:any)']                  = 'home/lang/$1';
$route['login']                        = 'home/login';
$route['forgot']                       = 'home/forgot';
$route['key/(:any)/(:any)']            = 'home/key/$1/$2';
$route['unidentified']                 = 'home/unidentified';
$route['logout']                       = 'home/logout';
$route['disclaimer']                   = 'home/disclaimer';
$route['terms-condition']               = 'home/terms_condition';
$route['sitemaps']                     = 'home/sitemap';
$route['privacy']                      = 'home/privacy';
$route['career']                       = 'home/career';
$route['career/(:any)']                = 'home/career/$1';
$route['karir']                        = 'home/career';
$route['karir/(:any)']                 = 'home/career/$1';
//$route['blog/(:any)']                  = 'post/index/$1';
$route['blog/(:any)/(:any)']           = 'post/detail/$1/$2';

$route['dropdown/dropdown_tags/global_tags/(:any)/(:any)'] = 'dropdown/dropdown_tags/global_tags/$1/$2';
$route['dropdown/dropdown_tags/global_tags/(:any)']        = 'dropdown/dropdown_tags/global_tags/$1';
$route['dropdown/dropdown_tags/global_tags']               = 'dropdown/dropdown_tags/global_tags';

$route['mod_logout'] = 'mod_authentication/logout';
// $route['([a-zA-Z_-]+)']   = 'home/index/$1';
$route['mod_(:any)']                             = 'mod_$1';
$route['mod_(:any)/(:any)']                      = 'mod_$1/$2';
$route['mod_(:any)/(:any)/(:any)']               = 'mod_$1/$2/$3';
$route['mod_(:any)/(:any)/(:any)/(:any)']        = 'mod_$1/$2/$3/$4';
$route['mod_(:any)/(:any)/(:any)/(:any)/(:any)'] = 'mod_$1/$2/$3/$4/$5';

$route['users/(:any)']                             = 'users/$1';
$route['users/(:any)/(:any)']                      = 'users/$1/$2';
$route['users/(:any)/(:any)/(:any)']               = 'users/$1/$2/$3';
$route['users/(:any)/(:any)/(:any)/(:any)']        = 'users/$1/$2/$3/$4';
$route['users/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'users/$1/$2/$3/$4/$5';

//$route['(:any)']                             = 'home/index/$1';
//$route['(:any)/(:any)']                      = 'home/index/$1/$2';
//$route['(:any)/(:any)/(:any)']               = 'home/index/$1/$2/$3';
//$route['(:any)/(:any)/(:any)/(:any)']        = 'home/index/$1/$2/$3/$4';
//$route['(:any)/(:any)/(:any)/(:any)/(:any)'] = 'home/index/$1/$2/$3/$4/$5';

$route['default_controller']   = 'home';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;
