<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Facebook API Configuration
| -------------------------------------------------------------------
|
| To get an facebook app details you have to create a Facebook app
| at Facebook developers panel (https://developers.facebook.com)
|
|  facebook_app_id               string   Your Facebook App ID.
|  facebook_app_secret           string   Your Facebook App Secret.
|  facebook_login_redirect_url   string   URL to redirect back to after login. (do not include base URL)
|  facebook_logout_redirect_url  string   URL to redirect back to after logout. (do not include base URL)
|  facebook_login_type           string   Set login type. (web, js, canvas)
|  facebook_permissions          array    Your required permissions.
|  facebook_graph_version        string   Specify Facebook Graph version. Eg v3.2
|  facebook_auth_on_load         boolean  Set to TRUE to check for valid access token on every page load.
*/
//$config['serverKey']        = 'Mid-server-G51C8280i_D_fNCYMnbLcNKN';
//$config['clientKey']        = 'Mid-client-dD2L8YxWPodO_-h_';

$config['serverKey']        = 'SB-Mid-server-DHKJHCu_87xdhJ2DDDx2EXTy';
$config['clientKey']        = 'SB-Mid-client-7aXnkr4LTfgqU8CF';
$config['isProduction']     = FALSE;
$config['isSanitized']      = TRUE;
$config['is3ds']            = TRUE;

const SANDBOX_BASE_URL         = 'https://api.sandbox.midtrans.com/v2';
const PRODUCTION_BASE_URL      = 'https://api.midtrans.com/v2';
const SNAP_SANDBOX_BASE_URL    = 'https://app.sandbox.midtrans.com/snap/v1';
const SNAP_PRODUCTION_BASE_URL = 'https://app.midtrans.com/snap/v1';