<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$config['email']['smtp_host']    = IMJK_EMAILCONFIG_HOST;
$config['email']['smtp_crypto']  = 'TLS';
$config['email']['smtp_port']    = IMJK_EMAILCONFIG_PORT;
$config['email']['smtp_timeout'] = 90;
$config['email']['smtp_user']    = IMJK_EMAILCONFIG_USER;
$config['email']['smtp_pass']    = IMJK_EMAILCONFIG_PASS;

/* End of file email.php */
/* Location: ./application/config/email.php */