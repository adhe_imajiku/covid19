<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['admins']     = 'mod_dashboard';
$route['mod_logout'] = 'mod_authentication/logout';

$route['dropdown/dropdown_tags/global_tags/(:any)/(:any)'] = 'dropdown/dropdown_tags/global_tags/$1/$2';
$route['dropdown/dropdown_tags/global_tags/(:any)']        = 'dropdown/dropdown_tags/global_tags/$1';
$route['dropdown/dropdown_tags/global_tags']               = 'dropdown/dropdown_tags/global_tags';

$route['mod_(:any)']                             = 'mod_$1';
$route['mod_(:any)/(:any)']                      = 'mod_$1/$2';
$route['mod_(:any)/(:any)/(:any)']               = 'mod_$1/$2/$3';
$route['mod_(:any)/(:any)/(:any)/(:any)']        = 'mod_$1/$2/$3/$4';
$route['mod_(:any)/(:any)/(:any)/(:any)/(:any)'] = 'mod_$1/$2/$3/$4/$5';

$route['users/(:any)']                             = 'users/$1';
$route['users/(:any)/(:any)']                      = 'users/$1/$2';
$route['users/(:any)/(:any)/(:any)']               = 'users/$1/$2/$3';
$route['users/(:any)/(:any)/(:any)/(:any)']        = 'users/$1/$2/$3/$4';
$route['users/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'users/$1/$2/$3/$4/$5';

$route['(:any)/change-language']                         = 'home/change_language';
$route['(:any)']                                         = 'home/index';
$route['(:any)/home']                                    = 'home/index';
$route['(:any)/about']                                   = 'home/about';
$route['(:any)/formula']                                 = 'home/formula';
$route['(:any)/faq']                                     = 'home/faq';
$route['(:any)/home/(:any)']                             = 'home/$2';
$route['(:any)/term']                                    = 'home/term';
$route['(:any)/policy']                                  = 'home/policy';
$route['(:any)/disclaimer']                              = 'home/disclaimer';
$route['(:any)/sitemap']                                 = 'home/sitemap';
$route['(:any)/home/(:any)/(:any)']                      = 'home/$2/$3';
$route['(:any)/home/(:any)/(:any)/(:any)']               = 'home/$2/$3/$4';
$route['(:any)/home/(:any)/(:any)/(:any)/(:any)']        = 'home/$2/$3/$4/$5';
$route['(:any)/home/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'home/$2/$3/$4/$5/$6';

$route['(:any)/career']                                    = 'career/index';
$route['(:any)/career/(:any)']                             = 'career/$2';
$route['(:any)/career/(:any)/(:any)']                      = 'career/$2/$3';
$route['(:any)/career/(:any)/(:any)/(:any)']               = 'career/$2/$3/$4';
$route['(:any)/career/(:any)/(:any)/(:any)/(:any)']        = 'career/$2/$3/$4/$5';
$route['(:any)/career/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'career/$2/$3/$4/$5/$6';

$route['(:any)/contact']                                    = 'contact/index';
$route['(:any)/contact/(:any)']                             = 'contact/$2';
$route['(:any)/contact/(:any)/(:any)']                      = 'contact/$2/$3';
$route['(:any)/contact/(:any)/(:any)/(:any)']               = 'contact/$2/$3/$4';
$route['(:any)/contact/(:any)/(:any)/(:any)/(:any)']        = 'contact/$2/$3/$4/$5';
$route['(:any)/contact/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'contact/$2/$3/$4/$5/$6';

$route['(:any)/cart']                                    = 'cart/index';
$route['(:any)/cart/(:any)']                             = 'cart/$2';
$route['cart/notification']                              = 'cart/notification';
$route['(:any)/cart/(:any)/(:any)']                      = 'cart/$2/$3';
$route['(:any)/cart/(:any)/(:any)/(:any)']               = 'cart/$2/$3/$4';
$route['(:any)/cart/(:any)/(:any)/(:any)/(:any)']        = 'cart/$2/$3/$4/$5';
$route['(:any)/cart/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'cart/$2/$3/$4/$5/$6';
$route['(:any)/cart/update/(:any)/(:any)']               = 'cart/update/$2/$3';
$route['(:any)/payment']                                 = 'cart/payment';

$route['(:any)/product']                                    = 'product/index';
$route['(:any)/product/(:any)']                             = 'product/index/$2';
$route['(:any)/product/detail_product/(:any)']              = 'product/detail_product/$2';
$route['(:any)/product/inquiry/(:any)']                     = 'product/inquiry/$2';
$route['(:any)/product/detail/(:any)']                      = 'product/detail/$2';
$route['(:any)/product/city/(:any)']                        = 'product/city/$2';
$route['(:any)/product/city/(:any)/(:any)']                 = 'product/city/$2/$3';
$route['(:any)/product/subdistrict/(:any)']                 = 'product/subdistrict/$2';
$route['(:any)/product/subdistrict/(:any)/(:any)']          = 'product/subdistrict/$2/$3';
$route['(:any)/product/add_to_cart/(:any)/(:any)']          = 'product/add_to_cart/$2/$3';
$route['(:any)/product/province/(:any)']                    = 'product/province/$2';
$route['(:any)/product/get_price/(:any)']                   = 'product/get_price/$2';
$route['(:any)/product/locale/(:any)/(:any)']               = 'product/locale/$2/$3';
$route['(:any)/product/province/(:any)/(:any)']             = 'product/province/$2/$3';
$route['(:any)/product/(:any)/(:any)']                      = 'product/index/$2/$3';
$route['(:any)/product/(:any)/(:any)/(:any)']               = 'product/index/$2/$3/$4';
$route['(:any)/product/(:any)/(:any)/(:any)/(:any)']        = 'product/index/$2/$3/$4/$5';
$route['(:any)/product/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'product/index/$2/$3/$4/$5/$6';

//$route['(:any)/news-and-event']                          = 'news/index';
//$route['(:any)/news']                                    = 'news/lists/1';
//$route['(:any)/news/(:any)']                             = 'news/lists/$2';
//$route['(:any)/news/detail/(:any)']                      = 'news/detail/$2';
//$route['(:any)/event']                                   = 'news/event/1';
//$route['(:any)/event/(:any)']                            = 'news/event/$2';
//$route['(:any)/event/detail/(:any)']                     = 'news/event_detail/$2';

$route['(:any)/world-of-sada']                             = 'world_of_sada/index';

$route['(:any)/order']                                     = 'auth/order';
$route['(:any)/order/(:any)']                              = 'auth/order/$2';
$route['(:any)/auth/subdistrict/(:any)/(:any)']            = 'auth/subdistrict/$2/$3';
$route['(:any)/auth/city/(:any)/(:any)']                   = 'auth/city/$2/$3';
$route['(:any)/register']                                  = 'auth/register';
$route['(:any)/logout']                                    = 'auth/logout';
$route['(:any)/forgot']                                    = 'auth/forgot';
$route['(:any)/reset/(:any)']                              = 'auth/reset_password/$2';
$route['(:any)/login']                                     = 'auth/login';
$route['(:any)/profile']                                   = 'auth/profile';
$route['(:any)/change-password']                           = 'auth/change_password';

$route['default_controller']   = 'home';
$route['404_override']         = '';
$route['translate_uri_dashes'] = FALSE;
