<?php
defined('BASEPATH') or exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base_front.php';
require_once APPPATH . '../vendor/autoload.php';

class Home extends Base_front
{
    private $module_title = 'Home';
    private $module_base  = 'home/';
    private $module_model = 'Home_model';
    private $rajaongkir_key= '462e463e313dcb54ef9b5b54acce7fba';
    private $page         = [
        'index'        => 'index',
        'about'        => 'pages/about',
        'faq'          => 'pages/faq',
        'search'       => 'pages/search',
        'term'         => 'pages/term',
        'policy'       => 'pages/policy',
        'disclaimer'   => 'pages/disclaimer',
        'sitemap'      => 'pages/sitemap',
        'branch'       => 'pages/branch',
        'formula'      => 'pages/formula',
        'index_footer' => 'footer_index',
    ];

    /*
     * =============================================================================
     * FUNCTION CONSTRUCT
     * =============================================================================
     */

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->title = $this->module_title;
        $this->load->model($this->module_model);

        $this->module_config = [
            'module_base' => $this->module_base
        ];
        redirect('admins');
    }
    /*
     * =============================================================================
     * FUNCTION INDEX
     * =============================================================================
     */
    public function index()
    {
        
    }
}
