<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'models/Base_model.php';

class Front_model extends Base_model
{
    private $default_lang_id = 1;
    private $default_lang_pr = '';

    public function __construct()
    {
        parent::__construct();
        $this->set_default_lang(1);
    }


    public function get_category(){
        $query=$this->db->query("select tags.*,count(tags_b.id) as has_child from tags 
        left join tags tags_b on tags.id=tags_b.parent
        where tags.status='1' and tags.tags_kt_id='2001025E0D632F4F3B9' 
        group by tags.id");
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return [];
        }
    }
    
    public function get_cart($parameter = null)
    {
        $sql    = "SELECT 
        a.id as post_id,
        a.file as post_file,
        a.file_location as post_file_location, 
        a.ordering as post_ordering, 
        a.status as post_status, 
        a.published as post_published, 
        a.meta_title, 
        a.event_start, 
        a.event_end, 
        a.is_featured, 
        a.icon, 
        a.author, 
        a.meta_keywords, 
        a.meta_description, 
        a.can_buy, 
        a.subdistrict, 
        a.city, 
        a.ctd as post_created_at, 
        a.mdd as post_updated_at,
        a_lang.title as post_title,
        a_lang.title_sub as post_title_sub, 
        a_lang.slug as post_slug, 
        a_lang.short_description as post_shortdesc,
        a_lang.description as post_description,
        a_lang.note as post_note,
        b_lang.title as menu_title, 
        b_lang.slug as menu_slug,
        a_lang.title_sub as post_title_sub, 
        b_img.file_location as cover_file_location, 
        b_img.file_thumb as cover_file_thumb, 
        b_img.file_original as cover_file_original,
        a_img.file_location, 
        a_img.file_thumb, 
        a_img.file_original,
        sum(post_product.sold) as sold_item,
        post_product.weight,
        post_product.price,
        post_product.price_promo,
        post_product.stock,
        product_cart.qty,
        product_cart.id as cart_id,
        post_product_variant.variant,
        a.menu_id as menu_id,
        a.parent as post_parent,
        coupon_code.coupon_code as coupon_code,
        coupon_code.start_date as coupon_start_date,
        coupon_code.end_date as coupon_end_date,
        coupon_category.coupon_postal_free as coupon_postal_free,
        coupon_category.coupon_unit as coupon_unit,
        coupon_category.coupon_name as coupon_name,
        coupon_category.coupon_nominal as coupon_nominal,
        coupon_category.coupon_minimum_buy as coupon_minimum_buy
        FROM post a  JOIN post_lang a_lang ON a.id = a_lang.post_id
        JOIN acs_lang a_lang_conf ON a_lang_conf.id = a_lang.acs_lang_id
        JOIN menu b ON a.menu_id = b.id 
        JOIN menu_lang b_lang ON b.id = b_lang.menu_id
        JOIN acs_lang b_lang_conf ON b_lang_conf.id = b_lang.acs_lang_id
        LEFT JOIN coupon_code ON a.coupon_id = coupon_code.id
        LEFT JOIN coupon_category ON coupon_code.coupon_category_id = coupon_category.id
        JOIN post_product ON a.id = post_product.post_id
        JOIN post_product_variant ON post_product.id = post_product_variant.post_product_id
        JOIN product_cart ON post_product.id = product_cart.post_product_id
        LEFT JOIN
        (
        SELECT id as id_img,post_id, file_location, file_thumb, file_original 
        FROM post_image
        ) a_img ON a_img.post_id = a.id
        LEFT JOIN
        (
        SELECT post_id, file_location, file_thumb, file_original 
        FROM post_image
        WHERE status_cover = 1
        GROUP BY post_id
        ) b_img ON b_img.post_id = a.id
        LEFT JOIN (
        SELECT post_id, GROUP_CONCAT(a.title SEPARATOR ', ') as title, GROUP_CONCAT(a.slug SEPARATOR ', ') as slug,
        GROUP_CONCAT(b.title SEPARATOR ', ') as child_title, GROUP_CONCAT(c.slug SEPARATOR ', ') as child_slug,
        GROUP_CONCAT(c.title SEPARATOR ', ') as subchild_title, GROUP_CONCAT(c.slug SEPARATOR ', ') as subchild_slug
        FROM tags a
        JOIN post_tags ON a.id = post_tags.tags_id
        left JOIN tags b ON a.id = b.parent
        left JOIN tags c ON b.id = c.parent
        WHERE 1=1
        GROUP BY post_id
        ) a_tags ON a.id = a_tags.post_id
        WHERE 1=1 AND a_lang_conf.status = 1 AND a_lang.acs_lang_id = ? 
        AND a.status = 1 AND a.published <= NOW() AND b.id = ? and product_cart.user_id = ? and product_cart.product_order_id='0'
        group by product_cart.id
        ";
        $query  = $this->db->query($sql,$parameter);
        if ($query->num_rows() > 0) {
            $result = $query->num_rows();
            $query->free_result();
            return $result;
        } else {
            return 0;
        }
    }

}