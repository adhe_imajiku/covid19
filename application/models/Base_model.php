<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Base_model extends CI_Model
{
    private $default_lang_id = 1;
    private $default_lang_pr = '';

    public function __construct()
    {
        parent::__construct();
        $this->set_default_lang(1);
    }

    protected function set_default_lang($params = null)
    {
        $sql   = 'SELECT id, lang, title, slug FROM acs_lang WHERE status_default = ?';
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
        } else {
            $sql   = 'SELECT id, lang, title, slug FROM acs_lang';
            $query = $this->db->query($sql);
        }
        $result = $query->row_array();
        $query->free_result();

        $this->default_lang_id = $result['id'];
        $this->default_lang_pr = $result['lang'];
    }

    public function get_language_active($params = null)
    {
        $sql   = 'SELECT id, lang, title, slug, status, status_default FROM acs_lang WHERE status = ? ORDER BY status_default DESC';
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_language_default()
    {
        $sql   = 'SELECT id, lang, title, slug, status, status_default FROM acs_lang WHERE status_default = 1 ORDER BY status_default DESC';
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_group_config()
    {
        $sql   = 'SELECT config_id, config_name, config_value,config_value_id,config_desc,file_location,file_original,file_thumb FROM acs_config';
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function add_data($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert($tabel, $data);
    }

    public function add_data_batch($tabel, $data)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->insert_batch($tabel, $data);
    }

    public function update_data_batch($tabel, $data, $kolom)
    {
        if (empty($tabel) || empty($data))
            return false;
        return $this->db->update_batch($tabel, $data, $kolom);
    }

    public function update_data($tabel, $kolom, $id = null, $data)
    {
        if (empty($tabel) || empty($data) || empty($kolom) || empty($id)) {
            return false;
        }

        if (is_array($kolom)) {
            foreach ($kolom as $index => $row) {
                $this->db->where($row, $id[$row]);
            }
        } else {
            if ($id != '') {
                $this->db->where($kolom, $id[$kolom]);
            } else {
                $this->db->where($kolom);
            }
        }
        return $this->db->update($tabel, $data);
    }

    public function update_data_user($mail,$code)
    {
        if (empty($mail) || empty($code)) {
            return false;
        }
        if ($mail != '') {
            $this->db->where('email', $mail);
            $data=[
                'forgotten_password_code' => $code
            ];
        }
        
        return $this->db->update('acs_users', $data);
    }

    public function delete_data($tabel, $parameter)
    {
        if (empty($tabel) OR empty($parameter))
            return false;
        return $this->db->delete($tabel, $parameter);
    }
    public function delete_data_batch($tabel, $selection, $parameter)
    {
        if (empty($tabel) OR empty($parameter))
            return false;
        $this->db->where_in($selection, $parameter);
        return $this->db->delete($tabel);
    }

    public function delete_data_batch_2($tabel, $selection, $parameter)
    {

        if (empty($tabel) OR empty($parameter))
            return false;
            
        if(!is_array($selection)) {
            $this->db->where_in($selection, $parameter);
        } else {


            foreach($parameter as $index => $row) {
                $this->db->where_in($index, $row);
                // echo $parameter[$index][$row];
                //$this->db->where_in($row, $parameter[$index][$row]);
            }
        }
        return $this->db->delete($tabel);
    }

    public function get_db_error_message()
    {
        $string    = '';
        $get_error = $this->db->error();
        switch ($get_error['code']):
            case 1451:
                $string = 'Data sudah digunakan di tabel lain. Silahkan cek kembali';
                break;
            default:
                $string = $get_error['message'];
                break;
        endswitch;
        return $string;
    }

    /* GET LAST ID AFTER ACTION INSERT */
    public function last_id()
    {
        return $this->db->insert_id();
    }

    /* USE FOR GET DATATABLES WITH CONDITION FROM CONTROLLER */
    public function get_dataTable_without_language($dt = null, $params = null)
    {
        if (empty($dt) || empty($params))
            return false;

        $columns = implode(', ', $params['col-select']);
        $sql     = "SELECT {$columns} FROM {$params['table']} ";

//        if (isset($params['join']) && !empty($params['join'])) {
//            if (is_array($params['join'])) {
//                foreach ($params['join'] as $index => $row) {
//                    if (is_array($row)) {
//                        if (!isset($row[3])) {
//                            $sql .= " {$row[0]} JOIN {$row[1]} USING({$row[2]}) ";
//                        } else {
//                            $sql .= " {$row[0]} JOIN {$row[1]} ON {$row[1]}.{$row[2]} = {$row[3]}.{$row[4]} ";
//                        }
//                    }
//                }
//            } else {
//                $sql .= $params['join'];
//            }
//        }

        if (!empty($params['join'])) {
            $params['join'] = array_filter($params['join']);
            if (count($params['join']) > 0) {
                foreach ($params['join'] as $index => $row) {
                    if (!is_array($row)) {
                        $sql .= " JOIN {$index} ON {$row} ";
                    } else {
                        if (empty($row[1])) {
                            $sql .= " JOIN {$index} ON {$row[0]}";
                        } else {
                            $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                        }
                    }
                }
            }
        }

        /*
         * SET WHERE 1=1
         * INIT SQL WHERE
         */
        $sql .= " WHERE 1=1 ";

        /*
         * SET UP JOIN CONDITION PRIMARY
         * CONDITION PRIMARY IS REQUIRED CONDITION
         */
        if (isset($params['where_primary']) && $params['where_primary'] != '') {
            $sql .= " AND {$params['where_primary']}";
        }

        $data     = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();

        $columnd    = $params['col-select'];
        $columnused = $params['col-display'];
        $count_c    = count($columnd);
        $search     = $dt['search']['value'];
        $where      = '';

        if (isset($params['where']) && $params['where'] != '') {
            $where .= $params['where'];
        }

        if (
            // isset($params['where']) &&
            // $params['where'] != '' &&
            $search != ''
        ) {
            // $where .= ' AND ';
            $where .= ' ( ';
        }

//        if ($search != '') {
//            for ($i = 0; $i < $count_c; $i++) {
//                $where .= $columnd[$i] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
//                if ($i < $count_c - 1) {
//                    $where .= ' OR ';
//                }
//            }
//        }

        if ($search != '') {
            for ($i = 0; $i < $count_c; $i++) {
                $exploded_search = explode(' as ', $columnd[$i]);
                if (isset($exploded_search[1])) {
                    $where .= $exploded_search[0] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
                } else {
                    if (strpos($columnd[$i], 'CASE')) {
                        $exploded_search = explode(' as ', $columnd[$i]);
                        $where           .= '(' . $exploded_search[0] . ')' . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
                    } else {
                        $where .= $columnd[$i] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
                    }

                }

                if ($i < $count_c - 1) {
                    $where .= ' OR ';
                }
            }
        }

        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }


        if (
            // isset($params['where']) &&
            // $params['where'] != '' &&
            $search != ''
        ) {
            $where .= ' ) ';
        }

        if ($where != '') {
            $sql .= " AND " . $where;
        }

        if (isset($params['group']) && !empty($params['group'])) {
            $sql .= " GROUP BY {$params['group']} ";
        }

        if (isset($_POST['order']) && !empty($_POST['order'])) {
            $sql .= ' ORDER BY ';
            foreach ($dt['order'] as $index => $row) {
                if (is_array($columnused[$dt['order'][$index]['column']])) {
                    $array_order[] = "{$columnused[$dt['order'][$index]['column']][1]} {$dt['order'][$index]['dir']} ";
                } else {
                    $array_order[] = "{$columnused[$dt['order'][$index]['column']]} {$dt['order'][$index]['dir']} ";
                }
            }
            $sql .= implode(',', $array_order);
        }

        $filteredRow = $this->db->query($sql)->num_rows();

        $start  = $dt['start'];
        $length = $dt['length'];
        $sql    .= " LIMIT {$start}, {$length}";

        $list = $this->db->query($sql);

        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCount;
        $option['recordsFiltered'] = $filteredRow;
        $option['data']            = array();

        $no = $_POST['start'] + 1;
        foreach ($list->result() as $row) {
            $rows   = array();
            $rows[] = $no++ . '.';
            for ($i = 0; $i < $count_c; $i++) {
                $exploded = explode('.', $columnd[$i]);
                if (isset($exploded[1])) {
                    $exploded_as = array_map('trim', explode(' as ', $exploded[1]));
                    if (isset($exploded_as[1])) {
                        $rows[] = $row->$exploded_as[1];
                    } else {
                        $rows[] = $row->$exploded[1];
                    }
                } else {
                    $exploded_as = array_map('trim', explode(' as ', $columnd[$i]));
                    if (isset($exploded_as[1])) {
                        $rows[] = $row->$exploded_as[1];
                    } else {
                        $rows[] = $row->$columnd[$i];
                    }
                }
            }
            $option['data'][] = $rows;
        }
        return $option;
    }

    /*
     * USE FOR GET DATATABLES WITH CONDITION FROM CONTROLLER WITH LANGUAGE TABLE
     * TABLE LANGUAGE IS RELATION FROM MODULE TABLE. WITH MULTIPLE LANGUAGE
     */
    public function get_dataTable_with_language($dt = null, $params = null)
    {
        if (empty($dt) || empty($params)) {
            return false;
        }
        /* SET UP SELECT COLUMN FROM DATA ARRAY */
        $columns = implode(', ', $params['col-select']);

        /* SET UP SELECT AND TABLE PRIMARY */
        $sql = "SELECT {$columns} FROM {$params['table']} ";

        /* SET UP JOIN CONDITION */
        if (!empty($params['join'])) {
            $params['join'] = array_filter($params['join']);
            if (count($params['join']) > 0) {
                foreach ($params['join'] as $index => $row) {
                    if (!is_array($row)) {
                        $sql .= " JOIN {$index} ON {$row} ";
                    } else {
                        if (empty($row[1])) {
                            $sql .= " JOIN {$index} ON {$row[0]}";
                        } else {
                            $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                        }
                    }
                }
            }
        }

        /*
         * SET WHERE 1=1
         * INIT SQL WHERE
         */
        $sql .= " WHERE 1=1 ";

        /*
         * SET UP JOIN CONDITION PRIMARY
         * CONDITION PRIMARY IS REQUIRED CONDITION
         */
        if (isset($params['where_primary']) && $params['where_primary'] != '') {
            $sql .= " AND {$params['where_primary']}";
        }

        $data     = $this->db->query($sql);
        $rowCount = $data->num_rows();
        $data->free_result();

        $columnd    = $params['col-select'];
        $columnused = $params['col-display'];
        $count_c    = count($columnd);
        $search     = $dt['search']['value'];
        $where      = '';

        if (isset($params['where']) && $params['where'] != '') {
            $where .= $params['where'];
        }

        if (
            // isset($params['where']) &&
            // $params['where'] != '' &&
            $search != ''
        ) {
            // $where .= ' AND ';
            $where .= ' ( ';
        }
        
        $where_search = '';
        if(!empty($search)) {
            for($i = 0; $i < $count_c; $i++) {
                $where_search_on_loop = '';
                $exploded_search = explode(' as ', $columnd[$i]);
                if(!empty($exploded_search[1])) {
                    $re = '/(COUNT)/m';
                    $str = $columnd[$i];
                    $is_check = preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
                    if(!$is_check) {
                        $where_search_on_loop = $exploded_search[0] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';  
                    } else {
                        $where_search_on_loop = '';
                    }
                } else {
                    if (strpos($columnd[$i], 'CASE')) {
                        $exploded_search = explode(' as ', $columnd[$i]);
                        $where_search_on_loop = '(' . $exploded_search[0] . ')' . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
                    } else {
                        $where_search_on_loop = $columnd[$i] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
                    }
                }
                
                if(!empty($where_search_on_loop)) {
                    $where_search[] = " {$where_search_on_loop} ";
                }
            }
        }
        if(!empty($where_search)) {
            $where_dummy = implode(' OR ', $where_search);
            $where .= $where_dummy;
        }
        

//        if ($search != '') {
//            for ($i = 0; $i < $count_c; $i++) {
//                if(!empty($columnd[$i])) {
//                    
//                $is_check_where = false;
//                $exploded_search = explode(' as ', $columnd[$i]);
//                    
//                if (isset($exploded_search[1])) {
//                    $re = '/(COUNT)/m';
//                    $str = $columnd[$i];
//                    $is_check = preg_match_all($re, $str, $matches, PREG_SET_ORDER, 0);
//                    if(!$is_check) {
//                        $where .= $exploded_search[0] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
//                    } else {
//                        $is_check_where = true;
//                        $where .= '';
////                        $exploded_search[0] = str_replace('COUNT', '', $exploded_search[0]);
////                        $where .= $exploded_search[0] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
//                    }
//                } else {
//                    if (strpos($columnd[$i], 'CASE')) {
//                        $exploded_search = explode(' as ', $columnd[$i]);
//                        $where           .= '(' . $exploded_search[0] . ')' . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
//                    } else {
//                        $where .= $columnd[$i] . ' LIKE "%' . $this->db->escape_like_str($search) . '%" ESCAPE "!" ';
//                    }
//
//                }
//                    
//                    echo $columnd[$i];
//
//                if($is_check_where == false) {
//                    if ($i < ($count_c - 1)) {
//                        $where .= ' OR ';
//                    }       
//                }
//                $is_check_where = false;
//                    
//                }
//            }
//        }
        // echo $where;
        // exit;

        for ($i = 0; $i < $count_c; $i++) {
            $searchCol = $dt['columns'][$i]['search']['value'];
            if ($searchCol != '') {
                $where = $columnd[$i] . ' LIKE "%' . $searchCol . '%" ';
                break;
            }
        }

        if (
            // isset($params['where']) &&
            // $params['where'] != '' &&
            $search != ''
        ) {
            $where .= ' ) ';
        }

        if ($where != '') {
            $sql .= " AND " . $where;
        }
        // echo $sql;exit;

        if (isset($params['group']) && !empty($params['group'])) {
            $sql .= " GROUP BY {$params['group']} ";
        }

        if (isset($_POST['order']) && !empty($_POST['order'])) {
            $sql .= ' ORDER BY ';
            foreach ($dt['order'] as $index => $row) {
                if (is_array($columnused[$dt['order'][$index]['column']])) {
                    $array_order[] = "{$columnused[$dt['order'][$index]['column']][1]} {$dt['order'][$index]['dir']} ";
                } else {
                    $array_order[] = "{$columnused[$dt['order'][$index]['column']]} {$dt['order'][$index]['dir']} ";
                }
            }
            $sql .= implode(',', $array_order);
        }else if(!empty($params['order'])){
            $sql .= ' ORDER BY '.$params['order'];
        }

        $filteredRow = $this->db->query($sql)->num_rows();

        $start  = $dt['start'];
        $length = $dt['length'];
        $sql    .= " LIMIT {$start}, {$length}";

        $list = $this->db->query($sql);

        $option['draw']            = $dt['draw'];
        $option['recordsTotal']    = $rowCount;
        $option['recordsFiltered'] = $filteredRow;
        $option['data']            = array();

        $no = $_POST['start'] + 1;
        foreach ($list->result() as $row) {
            $rows   = array();
            $rows[] = $no++ . '.';
            for ($i = 0; $i < $count_c; $i++) {
                $exploded = explode(' as ', $columnd[$i]);
                if (isset($exploded[1])) {
                    $rows[] = $row->$exploded[1];
                } else {
                    $exploded_dot = explode('.', $columnd[$i]);
                    if (isset($exploded_dot[1])) {
                        $rows[] = $row->$exploded_dot[1];
                    } else {
                        $rows[] = $row->$columnd[$i];
                    }
                }
                // $exploded = explode('.', $columnd[$i]);
                // if (isset($exploded[1])) {
                //     $exploded_as = array_map('trim', explode(' as ', $exploded[1]));
                //     if (isset($exploded_as[1])) {
                //         $rows[] = $row->$exploded_as[1];
                //     } else {
                //         $rows[] = $row->$exploded[1];
                //     }
                // } else {
                //     $exploded_as = array_map('trim', explode(' as ', $columnd[$i]));
                //     if (isset($exploded_as[1])) {
                //         $rows[] = $row->$exploded_as[1];
                //     } else {
                //         $rows[] = $row->$columnd[$i];
                //     }
                // }
            }
            $option['data'][] = $rows;
        }
        return $option;
    }

    /* USE FOR GET COLUMN NAME FROM TABLE DATABASE */
    public function show_table_column($params = null)
    {
        $sql   = 'SHOW COLUMNS FROM ' . $params;
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /* USE FOR SIMPLE ACTION GET DATA FROM TABLE WITH SIMPLE CONDITION */
    public function show_table_by_column($tables, $column, $id, $select = null, $join = null)
    {
        $sql = 'SELECT ';
        $sql .= ($select == '') ? '*' : $select;
        $sql .= " FROM {$tables} ";

        /* SQL JOIN */
        if (!empty($join)) {
            $join = array_filter($join);
            if (count($join) > 0) {
//                print_r($join);
                foreach ($join as $index => $row) {
                    if (!is_array($row)) {
                        $sql .= " JOIN {$index} ON {$row} ";
                    } else {
                        if (empty($row[1])) {
                            $sql .= " JOIN {$index} ON {$row[0]}";
                        } else {
                            $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                        }
                    }
                }
            }
        }

        $sql .= " WHERE 1=1 ";
        if ($column != '') {
            $sql .= " AND {$column} = '{$this->db->escape_like_str($id)}'";
        }
        $query = $this->db->query($sql);

        if ($query->num_rows() > 0) {
            return $query;
        } else {
            return array();
        }
    }

    /* USE FOR SIMPLE ACTION GET DATA FROM TABLE WITH SIMPLE CONDITION */
    public function query_builder(
        $parameter_table,
        $parameter_select = '*',
        $parameter_where = [],
        $parameter_join = [],
        $parameter_groupby = null,
        $parameter_orderby = null,
        $parameter_output = ['output' => '', 'result' => 'array']
    )
    {
        
        $result = array();
        $sql    = "SELECT {$parameter_select} FROM {$parameter_table} ";


        /* SQL JOIN */
        if (!empty($parameter_join)) {
            $parameter_join = array_filter($parameter_join);
            if (count($parameter_join) > 0) {
                foreach ($parameter_join as $index => $row) {
                    if (!is_array($row)) {
                        $sql .= " JOIN {$index} ON {$row} ";
                    } else {
                        if (empty($row[1])) {
                            $sql .= " JOIN {$index} ON {$row[0]}";
                        } else {
                            $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                        }
                    }
                }
            }
        }

        /* SQL WHERE */
        $sql .= " WHERE 1=1 ";
        if (!empty($parameter_where)) {
//            $parameter_where = array_filter($parameter_where);
            if (count($parameter_where) > 0) {
                foreach ($parameter_where as $index => $row) {
                    if (!empty($index)) {
                        // var_dump($this->db->escape_like_str($row));
                        if (empty($row) && $row !== "0") {
                            $sql .= " AND {$index} ";
                        } else {
                            $sql .= " AND {$index}= '{$this->db->escape_like_str($row)}' ";
                        }
                    }
                }
            }
        }

        /* SQL GROUP BY */
        if (!empty($parameter_groupby)) {
            $sql .= " GROUP BY {$parameter_groupby} ";
        }

        /* SQL ORDER BY */
        if (!empty($parameter_orderby)) {
            $sql .= " ORDER BY {$parameter_orderby}";
        }

        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            /* SET CONDITION OUTPUT */
            if ($parameter_output['output'] == '') {
                $result = $query;
            } else {
                /* OUTPUT IF RESULT (MULTIPLE DATA) OR ROW (SINGLE DATA) */
                if ($parameter_output['output'] == 'result') {
                    if ($parameter_output['result'] == 'array') {
                        $result = $query->result_array();
                    } else {
                        $result = $query->result();
                    }
                } else if ($parameter_output['output'] == 'row') {
                    if ($parameter_output['result'] == 'array') {
                        $result = $query->row_array();
                    } else {
                        $result = $query->row();
                    }
                }
                /* FREE MEMORY AFTER GET RESULT */
                $query->free_result();
            }
        } else {
            // $result = $query;
            // $result  = $this->db->query($sql);
        }
        return $result;
    }

    public function is_check_table_related_value($parameter = null, $parameter_join = null, $parameter_where = null)
    {
        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'          => 'Name table related',
         *   'table_fk_id'    => 'Field column on table related',
         *   'table_fk_value' => 'Value to check used on table related'
         * ];
         * ---------------------------------------------------------------------------------------------------
         * $parameter_join = [
         *    'bookguest_example_join' => 'bookguest_subject.id = bookguest_example_join.bookguest_subject_id',
         *    'bookguest_example_join' => [
         *        'bookguest_subject.id = bookguest_example_join.bookguest_subject_id'
         *        'LEFT',
         *    ],
         * ];
         * ---------------------------------------------------------------------------------------------------
         * $parameter_where = [
         *   'column_id'  => 'value column',
         * ];
        */
        $parameter = (object)$parameter;
        try {
            $sql = "SELECT {$parameter->table_fk_id} FROM {$parameter->table} ";

            /* SQL JOIN */
            if (!empty($parameter_join)) {
                $parameter_join = array_filter($parameter_join);
                if (count($parameter_join) > 0) {
                    foreach ($parameter_join as $index => $row) {
                        if (!is_array($row)) {
                            $sql .= " JOIN {$index} ON {$row} ";
                        } else {
                            if (empty($row[1])) {
                                $sql .= " JOIN {$index} ON {$row[0]}";
                            } else {
                                $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                            }
                        }
                    }
                }
            }

            /* SQL WHERE */
            $sql .= " WHERE 1=1 ";
            if (!empty($parameter->table_fk_id)) {
                $sql .= " AND {$parameter->table_fk_id}='{$this->db->escape_like_str($parameter->table_fk_value)}' ";
            }

            /* SQL WHERE EXTRA */
            if (!empty($parameter_where)) {
//                $parameter_where = array_filter($parameter_where);
                if (count($parameter_where) > 0) {
                    foreach ($parameter_where as $index => $row) {
                        if (!empty($index)) {
                            $sql .= " AND {$index}='{$this->db->escape_like_str($row)}' ";
                        }
                    }
                }
            }

            $query = $this->db->query($sql);
            if ($query->num_rows() == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $error_text) {
            return false;
        }
    }

    public function is_check_table_column_duplicate($parameter = null, $parameter_join = null, $parameter_where = null)
    {
        $parameter = (object)$parameter;

        /*
         * DOCUMENTATION
         * $parameter = [
         *   'table'             => 'Name table',
         *   'table_check_id'    => 'Field column to check duplicate',
         *   'table_check_value' => 'Value to check duplicate'
         * ];
         * ---------------------------------------------------------------------------------------------------
         * $parameter_join = [
         *    'bookguest_example_join' => 'bookguest_subject.id = bookguest_example_join.bookguest_subject_id',
         *    'bookguest_example_join' => [
         *        'bookguest_subject.id = bookguest_example_join.bookguest_subject_id'
         *        'LEFT',
         *    ],
         * ];
         * ---------------------------------------------------------------------------------------------------
         * $parameter_where = [
         *   'column_id'  => 'value column',
         * ];
         */
        try {
            /* SQL BASE */
            $sql = " SELECT {$parameter->table_check_id} FROM {$parameter->table} ";

            /* SQL JOIN */
            if (!empty($parameter_join)) {
                $parameter_join = array_filter($parameter_join);
                if (count($parameter_join) > 0) {
                    foreach ($parameter_join as $index => $row) {
                        if (!is_array($row)) {
                            $sql .= " JOIN {$index} ON {$row} ";
                        } else {
                            if (empty($row[1])) {
                                $sql .= " JOIN {$index} ON {$row[0]}";
                            } else {
                                $sql .= " {$row[1]} JOIN {$index} ON {$row[0]}";
                            }
                        }
                    }
                }
            }

            /* SQL WHERE */
            $sql .= " WHERE 1=1 ";
            if (!empty($parameter->table_check_id)) {
                $sql .= " AND {$parameter->table_check_id}='{$this->db->escape_like_str($parameter->table_check_value)}' ";
            }

            /* SQL WHERE EXTRA */
            if (!empty($parameter_where)) {
                $parameter_where = array_filter($parameter_where);
                if (count($parameter_where) > 0) {
                    foreach ($parameter_where as $index => $row) {
                        if (!empty($index)) {
                            $sql .= " AND {$index}='{$this->db->escape_like_str($row)}' ";
                        }
                    }
                }
            }

            $query = $this->db->query($sql);
            if ($query->num_rows() == 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $error_text) {
            return false;
        }
    }

    public function show_data_image_by_table_and_column($parameter = null)
    {
        /*
         * [
         *    'table'        => 'Name table',
         *    'table_id'     => 'Name Primary Key',
         *    'where_column' => 'Name column on where',
         *    'where_value'  => 'Value column on where'
         * ]
         */
        $parameter = (object)$parameter;
        $sql       = "SELECT {$parameter->table_id}, file_name, file_location, file_thumb, file_original FROM {$parameter->table} WHERE {$parameter->where_column} = ?";
        $query     = $this->db->query($sql, $parameter->where_value);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function dropdown_type()
    {
        $sql       = "SELECT * FROM post_type where name != 'Video'";
        $query     = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
    public function get_dropdown_type($type)
    {
        $sql       = "SELECT * FROM post_type where form = '$type'";
        $query     = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row();
            $query->free_result();
            return $result->id;
        } else {
            return array();
        }
    }
    public function get_dropdown_type_by_id($type)
    {
        $sql       = "SELECT * FROM post_type where id = '$type'";
        $query     = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row();
            $query->free_result();
            return $result->form;
        } else {
            return array();
        }
    }
    public function dropdown_level()
    {
        $sql       = "SELECT * FROM acs_groups where id != 1 order by description asc";
        $query     = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
}