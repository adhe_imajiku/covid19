<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'models/Base_model.php';

class Admin_model extends Base_model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function previleges_checker($group, $uri)
    {
        $stat  = false;
        $query = $this->db->query('select * from acs_portal_previleges join acs_portal_menu on portal_menu_id=acs_portal_menu.id where group_id="' . $group . '" and link="' . $uri . '"');
        if ($query->num_rows() == 0) {
            $stat = false;
        } else {
            $temp = $query->row();
            if ($temp->grants == '0') {
                $stat = false;
            } else {
                $stat = true;
            }
        }
        return $stat;
    }

    public function previleges_menu_checker($group)
    {
        $stat  = false;
        $query = $this->db->query('select link,grants from acs_portal_previleges join acs_portal_menu on portal_menu_id=acs_portal_menu.id where group_id="' . $group . '"')->result_array();
        $temp  = [];
        foreach ($query as $index => $val) {
            $temp[$val['link']] = $val['grants'];
        }
        return $temp;
    }
    public function product_checker($product=[])
    {
        $stat=false;
        if(!empty($product)){
            $sql        = "select * from post_product where id='{$product['id']}'";
            $query      = $this->db->query($sql);
            if($query->num_rows()>0){
                $stat=true;
            }
        }
        return $stat;
    }
    public function variant_checker($variant=[])
    {
        $stat=false;
        if(!empty($variant)){
            $sql        = "select * from post_product_variant where id='{$variant['id']}'";
            $query      = $this->db->query($sql);
            if($query->num_rows()>0){
                $stat=true;
            }
        }
        return $stat;
    }

    public function admin_get_content_kt_by_menu_id($menu_id = null)
    {
        $sql   = "SELECT a.id as content_kt_id, a.ordering, a.content_name, a.content_type, a.content_format,
                  b.title, b.slug, c.id as content_kt_menu_id
                  FROM content_kt a 
                  LEFT JOIN content_kt_lang b ON a.id = b.content_kt_id 
                  LEFT JOIN content_kt_menu c ON a.id = c.content_kt_id 
                  WHERE c.menu_id = ?
                  ORDER BY a.ordering";
        $query = $this->db->query($sql, $menu_id);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_branch($post_id = null)
    {
        $sql   = "SELECT a.id as post_id, a.ordering as post_ordering, a.status as post_status, a.published as post_published, a.meta_title, a.meta_keywords, a.meta_description, a.ctd as post_created_at, a.mdd as post_updated_at, a_lang.title as post_title, a_lang.slug as post_slug, a_lang.short_description as post_shortdesc, b_lang.title as menu_title, b_lang.slug as menu_slug, a_tags.title as tags_title, a_tags.slug as tags_slug, a_lang.title_sub as post_title_sub, b_img.file_location as cover_file_location, b_img.file_thumb as cover_file_thumb, b_img.file_original as cover_file_original, a_img.file_location, a_img.file_thumb, a_img.file_original,a.menu_id as menu_id,a.parent as post_parent,post_type.form , a_lang.description as post_description FROM post a JOIN post_lang a_lang ON a.id = a_lang.post_id JOIN acs_lang a_lang_conf ON a_lang_conf.id = a_lang.acs_lang_id JOIN menu b ON a.menu_id = b.id JOIN menu_lang b_lang ON b.id = b_lang.menu_id JOIN acs_lang b_lang_conf ON b_lang_conf.id = b_lang.acs_lang_id JOIN post_type ON post_type.id = a.type_id LEFT JOIN ( SELECT post_id, file_location, file_thumb, file_original FROM post_image GROUP BY post_id ) a_img ON a_img.post_id = a.id LEFT JOIN ( SELECT post_id, file_location, file_thumb, file_original FROM post_image WHERE status_cover = 1 GROUP BY post_id ) b_img ON b_img.post_id = a.id LEFT JOIN ( SELECT post_id, GROUP_CONCAT(title SEPARATOR ', ') as title, GROUP_CONCAT(slug SEPARATOR ', ') as slug FROM tags JOIN post_tags ON tags.id = post_tags.tags_id WHERE 1=1 GROUP BY post_id ) a_tags ON a.id = a_tags.post_id WHERE 1=1 AND a_lang_conf.status = 1 AND a_lang.acs_lang_id = 'LGEN' AND a.published <= NOW() AND a.id = '{$post_id}' GROUP BY a.id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->row_array();
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    public function get_menu_by_parent_menu_id($parent = 0, $prefix = '', $counted_mode = false)
    {
        $title    = "CONCAT('{$prefix}', b.title) as title";
        $ordering = "CONCAT('{$prefix}', a.ordering) as ordering";
        $sql      = "SELECT
                       a.id as menu_id, {$ordering}, a.parent, 
                       a.status, a.status_mode, a.status_single, a.is_featured,
                       {$title}, b.slug, a.menu_cms_link, a.menu_site_link
                      FROM menu a
                      JOIN menu_lang b ON a.id = b.menu_id
                      JOIN acs_lang c ON b.acs_lang_id = c.id
                      WHERE c.status_default = ? AND c.status = ? AND a.parent = ?
                      ORDER BY a.ordering ASC
                      ";
        $query    = $this->db->query($sql, array(1, 1, $parent));
        if ($counted_mode) {
            return $query->num_rows();
        } else {
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                $query->free_result();
                return $result;
            }
        }
    }

}