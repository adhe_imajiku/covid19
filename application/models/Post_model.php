<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'models/Base_model.php';

class Post_model extends Base_model
{
    protected $table_lang                    = 'acs_lang';
    protected $table_lang_status_default     = 'status_default';
    protected $table_lang_status             = 'status';
    protected $table_menu                    = 'menu';
    protected $table_menu_lang               = 'menu_lang';
    protected $table_menu_image              = 'menu_image';
    protected $table_post                    = 'post';
    protected $table_post_lang               = 'post_lang';
    protected $table_post_image              = 'post_image';
    protected $table_post_tags               = 'post_tags';
    protected $table_post_counter            = 'post_counter';
    protected $table_post_counter_log        = 'post_counter_log';
    protected $table_post_content            = 'post_content';
    protected $table_tags_kt                 = 'tags_kt';
    protected $table_tags_kt_lang            = 'tags_kt_lang';
    protected $table_tags                    = 'tags';
    protected $table_content_kt              = 'content_kt';
    protected $table_content_kt_lang         = 'content_kt_lang';
    protected $table_content_kt_menu         = 'content_kt_menu';
    protected $table_content_kt_image        = 'content_kt_image';
    protected $lang                          = 'LGEN';
    private   $sql_general_select            = "";
    private   $sql_general_select_decription = "";
    private   $sql_general_from              = "";
    private   $sql_general_join              = "";
    private   $sql_general_where             = "";
    private   $sql_general_where_menu        = "";
    private   $sql_general_groupby           = "";
    private   $sql_general_orderby           = "";
    private   $sql_general_limit             = "";

    public function __construct()
    {
        parent::__construct();
        if(!empty($this->session->userdata('language'))){
            $this->lang=$this->session->userdata('language');
        }
        $this->sql_general_select     = "
        SELECT 
        a.id as post_id,
        a.file as post_file,
        a.file_location as post_file_location, 
        a.ordering as post_ordering, 
        a.status as post_status, 
        a.published as post_published, 
        a.meta_title, 
        a.event_start, 
        a.event_end, 
        a.is_featured, 
        a.icon, 
        a.author, 
        a.meta_keywords, 
        a.meta_description, 
        a.can_buy, 
        a.subdistrict, 
        a.city, 
        a.ctd as post_created_at, 
        a.mdd as post_updated_at,
        a_lang.title as post_title,
        a_lang.title_sub as post_title_sub, 
        a_lang.slug as post_slug, 
        a_lang.short_description as post_shortdesc,
        a_lang.description as post_description,
        a_lang.note as post_note,
        b_lang.title as menu_title, 
        b_lang.slug as menu_slug,
        a_tags.title as tags_title, 
        a_tags.slug as tags_slug,
        a_brand.title as brand_title, 
        a_brand.slug as brand_slug,
        a_product.title as a_product_title, 
        a_product.slug as a_product_slug,
        a_tags.subchild_title as subchild_title, 
        a_tags.subchild_slug as subchild_slug,
        a_tags.child_title as child_title, 
        a_tags.child_slug as child_slug,
        a_lang.title_sub as post_title_sub, 
        b_img.file_location as cover_file_location, 
        b_img.file_thumb as cover_file_thumb, 
        b_img.file_original as cover_file_original,
        a_img.file_location, 
        a_img.file_thumb, 
        a_img.file_original,
        c_img.file_location as menu_image_location, 
        c_img.file_thumb as menu_image_thumb, 
        c_img.file_original as menu_image_original,
        sum(post_product.sold) as sold_item,
        post_product.weight,
        post_product.price,
        post_product.price_promo,
        post_product.stock,
        a.menu_id as menu_id,
        a.parent as post_parent,
        coupon_code.coupon_code as coupon_code,
        coupon_code.start_date as coupon_start_date,
        coupon_code.end_date as coupon_end_date,
        coupon_category.coupon_postal_free as coupon_postal_free,
        coupon_category.coupon_name as coupon_name,
        coupon_category.coupon_unit as coupon_unit,
        coupon_category.coupon_nominal as coupon_nominal,
        coupon_category.coupon_maximum_nominal as coupon_maximum_nominal,
        coupon_category.coupon_minimum_buy as coupon_minimum_buy
        ";
        $this->sql_general_from       = " FROM {$this->table_post} a";
        $this->sql_general_join       = " JOIN {$this->table_post_lang} a_lang ON a.id = a_lang.post_id
        JOIN {$this->table_lang} a_lang_conf ON a_lang_conf.id = a_lang.acs_lang_id
        JOIN {$this->table_menu} b ON a.menu_id = b.id 
        JOIN {$this->table_menu_lang} b_lang ON b.id = b_lang.menu_id
        JOIN {$this->table_lang} b_lang_conf ON b_lang_conf.id = b_lang.acs_lang_id
        LEFT JOIN coupon_code ON a.coupon_id = coupon_code.id
        LEFT JOIN coupon_category ON coupon_code.coupon_category_id = coupon_category.id
        LEFT JOIN post_product ON a.id = post_product.post_id
        LEFT JOIN
        (
        SELECT id as id_img,post_id, file_location, file_thumb, file_original 
        FROM {$this->table_post_image}
        ) a_img ON a_img.post_id = a.id
        LEFT JOIN
        (
        SELECT id as id_img,menu_id, file_location, file_thumb, file_original 
        FROM menu_image
        ) c_img ON c_img.menu_id = b.id
        LEFT JOIN
        (
        SELECT post_id, file_location, file_thumb, file_original 
        FROM {$this->table_post_image}
        WHERE status_cover = 1
        GROUP BY post_id
        ) b_img ON b_img.post_id = a.id
        LEFT JOIN (
        SELECT post_id, GROUP_CONCAT(a.title SEPARATOR ', ') as title, GROUP_CONCAT(a.slug SEPARATOR ', ') as slug,
        GROUP_CONCAT(b.title SEPARATOR ', ') as child_title, GROUP_CONCAT(c.slug SEPARATOR ', ') as child_slug,
        GROUP_CONCAT(c.title SEPARATOR ', ') as subchild_title, GROUP_CONCAT(c.slug SEPARATOR ', ') as subchild_slug
        FROM tags a
        JOIN post_tags ON a.id = post_tags.tags_id
        left JOIN tags b ON a.id = b.parent
        left JOIN tags c ON b.id = c.parent
        WHERE 1=1 and a.status='1'
        GROUP BY post_id
        ) a_tags ON a.id = a_tags.post_id
        LEFT JOIN (
        SELECT post_id, GROUP_CONCAT(title SEPARATOR ', ') as title, GROUP_CONCAT(slug SEPARATOR ', ') as slug
        FROM tags 
        JOIN product_brand ON tags.id = product_brand.tags_id
        WHERE 1=1
        GROUP BY post_id
        ) a_brand ON a.id = a_brand.post_id
        LEFT JOIN (
        SELECT post_id, GROUP_CONCAT(title SEPARATOR ', ') as title, GROUP_CONCAT(slug SEPARATOR ', ') as slug
        FROM tags 
        JOIN product_tags ON tags.id = product_tags.tags_id
        WHERE 1=1
        GROUP BY post_id
        ) a_product ON a.id = a_product.post_id
        ";
        $this->sql_general_where      = " WHERE 1=1 AND a_lang_conf.{$this->table_lang_status} = 1 AND a_lang.acs_lang_id = ? 
        AND a.status = 1 AND a.published <= NOW() ";
        $this->sql_general_where_menu_slug = " AND b_lang.slug = ? ";
        $this->sql_general_where_post_id = " AND a.id = ? ";
        $this->sql_general_where_menu_id = " AND b.id = ? ";
        $this->sql_general_groupby    = " GROUP BY a.id";
        $this->sql_general_orderby    = " ORDER BY a.published DESC,a.ordering ASC";
        $this->sql_general_limit      = " LIMIT ?";
    }
    
     /*  eg : List Article on table post by MENU SLUG */
    public function get_article_by_id($parameter = null, $mode = 'array',$order='published')
    {
        if($order!='published'){
            $this->sql_general_orderby = " ORDER BY a.ordering ASC, a.published DESC ";
        }
        $sql = '';
        $sql .= $this->sql_general_select;
        $sql .= $this->sql_general_from;
        $sql .= $this->sql_general_join;
        $sql .= $this->sql_general_where.' and a.status=1 and a.parent="0" and a.parent_branch="0"';
        $sql .= $this->sql_general_where_post_id;
        $sql .= $this->sql_general_groupby;
        $sql .= $this->sql_general_orderby;
        $sql .= $this->sql_general_limit;
        $query = $this->db->query($sql, $parameter);
        if ($query->num_rows() > 0) {
            if ($mode == 'array') {
                $result = $query->result_array();
                
            } else {
                $result = $query->row_array();
            }
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }
     /*  eg : List Article on table post by MENU SLUG */
    public function get_article_by_menu_id($parameter = null, $mode = 'array', $order='published', $where=null, $related_id=null,$ordering_by=null,$group_by=null)
    {
        if(!empty($ordering_by)){
            $this->sql_general_orderby = " ORDER BY {$ordering_by}";
        }else{
            if($order!='published'){
                $this->sql_general_orderby = " ORDER BY a.ordering ASC, a.published DESC ";
            }  
        }
        $sql = '';
        $sql .= $this->sql_general_select;
        $sql .= $this->sql_general_from;
        $sql .= $this->sql_general_join;
//        echo $mode;
//        die();
        if($mode=='child'){
            $sql .= $this->sql_general_where.' and a.status=1 ';
            $mode='array';
        }else{
            $sql .= $this->sql_general_where.' and a.status=1 and a.parent="0" and a.parent_branch="0"';
        }
        if(!empty($related_id)){
            $sql .= ' and a.id!="'.$related_id.'" ';
        }
        $sql .= $this->sql_general_where_menu_id;
        if(!empty($where)){
            $sql .= " and ".$where." ";
        }
        if(!empty($group_by)){
            $this->sql_general_groupby    = " GROUP BY {$group_by}";
        }else{
            $this->sql_general_groupby    = " GROUP BY a.id";
        }
        
        $sql .= $this->sql_general_groupby;
        $sql .= $this->sql_general_orderby;
        
        if(!empty($parameter[3])){
            $sql .= "LIMIT ?,?";  
        }else{
            if(!empty($parameter[2])){
                $sql .= $this->sql_general_limit;    
            }
        }
        $query = $this->db->query($sql, $parameter);
        if ($query->num_rows() > 0) {
            if ($mode == 'array') {
                $result = $query->result_array();
                
            } else if ($mode == 'count') {
                $result['count'] = $query->num_rows();
                
            } else {
                $result = $query->row_array();
            }
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }

    /*
     *  eg : Detail Article on table post by POST SLUG */
    public function get_article_by_post_slug($parameter = null, $mode = 'array',$group_by=null)
    {
        $sql = '';
        $sql .= $this->sql_general_select;
        $sql .= $this->sql_general_from;
        $sql .= $this->sql_general_join;
        $sql .= $this->sql_general_where;
        $sql .= $this->sql_general_where_menu;
        $sql .= $this->sql_general_where_menu_id;
        $sql .= "AND a_lang.slug = ?";
        if(!empty($group_by)){
            $this->sql_general_groupby=" GROUP BY {$group_by} ";
        }
        $sql .= $this->sql_general_groupby;
        $query = $this->db->query($sql, $parameter);
        if ($query->num_rows() > 0) {
            if ($mode == 'array') {
                $result = $query->row_array();
            } else {
                $result = $query->result_array();
            }
            $query->free_result();
            return $result;
        } else {
            return array();
        }
    }


}
