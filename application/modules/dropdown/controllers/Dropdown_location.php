<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dropdown_location extends CI_Controller
{
    private $country_tb    = 'location_country';
    private $country_title = 'country';
    private $country_pk    = 'id';
    private $country_fk    = 'country_id';

    private $province_tb    = 'location_province';
    private $province_pk    = 'id';
    private $province_title = 'province';
    private $province_fk    = 'province_id';

    private $city_tb    = 'location_city';
    private $city_pk    = 'id';
    private $city_title = 'city';
    private $city_fk    = 'city_id';


    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function local_country($params = null)
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        $sql = "SELECT {$this->country_pk} as id, {$this->country_title} as text FROM {$this->country_tb} WHERE 1=1 ";
        if (isset($_POST) && !empty($_POST['q'])) {
            $term     = "%" . $_POST['q'] . "%";
            $sql      .= " AND {$this->country_title} LIKE ?";
            $params[] = $term;
        }
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $data['items'] = $result;
        } else {
            $data['items'] = [];
        }

        $this->output->set_output(json_encode($data));
    }

    public function local_province($params = null)
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }
        if (isset($_GET['country']) && !empty($_GET['country'])) {
            $params = $_GET['country'];
        }
        $sql = "SELECT {$this->province_pk} as id, {$this->province_title} as text FROM {$this->province_tb} WHERE 1=1 ";
        if (!empty($params)) {
            $sql      .= " AND {$this->country_fk} = ? ";
            $params[] = $params;
        }
        if (isset($_POST) && !empty($_POST['q'])) {
            $term     = "%" . $_POST['q'] . "%";
            $sql      .= " AND {$this->province_title} LIKE ? ";
            $params[] = $term;
        }

        $sql .= " ORDER BY {$this->province_title} ASC ";

        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $data['items'] = $result;
        } else {
            $data['items'] = [];
        }

        $this->output->set_output(json_encode($data));
    }

    public function local_city($params = null)
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }
        if (isset($_GET['province']) && !empty($_GET['province'])) {
            $params = $_GET['province'];
        }

        $sql = "SELECT {$this->city_pk} as id, {$this->city_title} as text FROM {$this->city_tb} WHERE 1=1";
        if (!empty($params)) {
            $province_id = $params;
            $params      = [];
            $sql         .= " AND {$this->province_fk} = ? ";
            $params[]    = $province_id;
        }
        if (isset($_POST) && !empty($_POST['q'])) {
            $term     = "%" . $_POST['q'] . "%";
            $sql      .= " AND {$this->city_title} LIKE ? ";
            $params[] = $term;
        }
        $sql .= " ORDER BY {$this->city_title} ASC ";
        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $data['items'] = $result;
        } else {
            $data['items'] = [];
        }

        $this->output->set_output(json_encode($data));
    }

    public function remote_city($params = null)
    {
        if (!$this->input->is_ajax_request()) {
            return;
        }

        if (isset($_GET['province']) && !empty($_GET['province'])) {
            $params =  base64_decode($_GET['province']);
        }

        $sql = "SELECT {$this->city_pk} as id, {$this->city_title} as text FROM {$this->city_tb} WHERE 1=1";
        if (!empty($params)) {
            $province_id = $params;
            $params      = [];
            $sql         .= " AND {$this->province_fk} = ? ";
            $params[]    = $province_id;
        }

        $query  = $this->db->query($sql, $params)->result_array();
        $array[''] = '选择城市';
        for($i=0;$i<count($query);$i++){
            $array[$query[$i]['id']] = $query[$i]['text'];
        }
        echo json_encode($array);
    }

}
