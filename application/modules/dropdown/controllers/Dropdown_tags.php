<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Dropdown_tags extends CI_Controller
{
    private $tags_tb    = 'tags';
    private $tags_title = 'title';
    private $tags_pk    = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function global_tags($params = null)
    {
//        if (!$this->input->is_ajax_request()) {
//            return;
//        }

        $sql = "SELECT {$this->tags_tb}.{$this->tags_pk} as id, {$this->tags_tb}.{$this->tags_title} as text , tags_b.{$this->tags_title} as text_2 , tags_c.{$this->tags_title} as text_3 
                FROM {$this->tags_tb} 
        left join {$this->tags_tb} tags_b on {$this->tags_tb}.parent = tags_b.id
        left join {$this->tags_tb} tags_c on tags_b.parent = tags_c.id
                WHERE 1=1 and {$this->tags_tb}.status='1'";
        if (!empty($params)) {
            $sql .= " AND {$this->tags_tb}.tags_kt_id = '{$params}'";
        }
        if (isset($_POST) && !empty($_POST['q'])) {
            $sql    .= " AND {$this->tags_tb}.{$this->tags_title} LIKE ?";
            $term   = "%" . $_POST['q'] . "%";
            $params = $term;
        }
        $sql .= " group by {$this->tags_tb}.id ";

        $query = $this->db->query($sql, $params);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            foreach($result as $index => $datas){
                $data['items'][$index] = $datas;
                if(!empty($datas['text_2']) && !empty($datas['text_3'])){
                    $data['items'][$index]['text'] = $datas['text_3'].' > '.$datas['text_2'].' > '.$datas['text'];
                }else if(!empty($datas['text_2']) && empty($datas['text_3'])){
                    $data['items'][$index]['text'] = $datas['text_2'].' > '.$datas['text'];
                }else if(empty($datas['text_2']) && empty($datas['text_3'])){
                    $data['items'][$index]['text'] = $datas['text'];
                }
                unset($data['items'][$index]['text_2']);
                unset($data['items'][$index]['text_3']);
            }
//            echo '<pre>';
//            var_dump($data['items']);
//            var_dump($result);
//            die();
//            $data['items'] = $result;
        } else {
            $data['items'] = [];
        }

        $this->output->set_output(json_encode($data));
    }

}
