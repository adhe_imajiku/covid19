<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="robots" content="noindex,nofollow">
    <meta name="googlebot" content="noindex">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo theme_global_locations() ?>favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_global_locations() ?>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo theme_global_locations() ?>favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo theme_global_locations() ?>favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo theme_global_locations() ?>favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="shortcut icon" href="<?php echo theme_global_locations() . "favicon.png" ?>"/>
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/Ionicons/css/ionicons.min.css"/>
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>dist/css/AdminLTE.min.css"/>
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>dist/css/skins/_all-skins.min.css"/>
    <style type="text/css">
        html,
        body {
            height: auto;
            background: transparent;
        }

        html {
            /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,0c1d6b+84,0c1d6b+84 */
            background: #ffffff; /* Old browsers */
            background: -moz-<?php echo strip_tags($config['background_login']); ?>
            background: -webkit-<?php echo strip_tags($config['background_login']); ?>
            background: <?php echo strip_tags($config['background_login']); ?>
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#0c1d6b',GradientType=0 ); /* IE6-9 */



            position: relative;
            background-size: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            background-position: 100% top;
            background-attachment: fixed;
        }

        .hold-transition .login-page {
            background: transparent;
        }

        .login-box-body {
            background: #fff;
            padding: 15px;
        }

        .login-box {
            margin: 13% auto;
        }

        .logo-image img {
            z-index: 999;
            margin: 0 auto;
        }
        .gold {
            color: <?php echo strip_tags($config['btn_link_hover_login']); ?>;
        }
        .gold:hover {
            color: #000000;
        }

        .btn-red {
            background-color: #f24036;
            border-color: #f24036;
            color: #fff;
            font: 14px "Sniglet-Regular", sans-serif;
            -webkit-border-radius: 4px;
            border-radius: 4px;
        }

        .login-box-body > p {
            color: red;
        }

        .btn-default {
            /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ff920a+0,eda63b+100 */
            background: #ff920a; /* Old browsers */
            background: -moz-<?php echo strip_tags($config['background_login']); ?>
            background: -webkit-<?php echo strip_tags($config['background_login']); ?>
            background: <?php echo strip_tags($config['background_login']); ?>
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff920a', endColorstr='#eda63b', GradientType=1); /* IE6-9 fallback on horizontal gradient */

            color: #fff;
            border: transparent;
        }

        .btn-default:hover,
        .btn-default:focus,
        .btn-default:active,
        .btn-default:active:focus,
        .btn-default:active:hover,
        .btn-default.active.focus,
        .btn-default.active:focus,
        .btn-default.active:hover,
        .btn-default.focus:active {
            /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#e69919+0,b36500+24,b36500+24 */
            background: <?php echo strip_tags($config['btn_link_hover_login']); ?>; /* Old browsers */
            color: #ffff; /* Old browsers */
            background: <?php echo strip_tags($config['btn_link_hover_login']); ?>;
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e69919', endColorstr='#b36500', GradientType=1); /* IE6-9 fallback on horizontal gradient */
        }

    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo base_url($base_url); ?>" class="logo-image">
            <img src="<?php echo theme_global_locations() . "logo.png"; ?>" class="img-responsive"/>
        </a>
    </div>
    <?php echo $this->session->flashdata('output'); ?>
    <div class="login-box-body">
        <form action="<?php echo base_url($base_url . 'do_login') ?>" method="post" style="margin-bottom: 10px!important;">
            <div class="form-group has-feedback">
                <input type="email" name="identity" class="form-control" autofocus="on" autocomplete="off" placeholder="Email"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" autocomplete="off" placeholder="Password"/>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <button style="" type="submit" class="btn btn-default btn-block btn-flat">Sign In </button>
        </form>
        <div class=" text-center">
            <a href="<?php echo site_url($base_url . 'forgot_password'); ?>" class="gold"><i class="fa fa-lock"></i> Forgot Password</a><br>
        </div>
    </div>
</div>
<footer class="container" style="color:white; text-align: center">
    <div class="row">
        <div class="col-md-12">
            <p><a href="https://imajiku.com" style="color:white">Powered by <img src="<?= theme_global_locations() ?>/logo-imajiku.png"></a></p>
            <p><strong>Copyright &copy; <?= date('Y') ?>. All rights reserved.</strong></p>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo(theme_admin_locations() . 'bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo(theme_admin_locations() . 'bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
</body>

</html>
