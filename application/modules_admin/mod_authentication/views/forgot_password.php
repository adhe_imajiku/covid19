<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="<?php echo base_url($THEMES_LOC . 'bower_components/bootstrap/dist/css/bootstrap.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url($THEMES_LOC . 'bower_components/font-awesome/css/font-awesome.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url($THEMES_LOC . 'bower_components/Ionicons/css/ionicons.min.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url($THEMES_LOC . 'dist/css/AdminLTE.min.css'); ?>"/>

    <style>
        .login-page, .register-page{
            background: #383331;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="<?php echo site_url($base_url); ?>">
            <img src="<?php echo base_url('themes/global/img/logo.png'); ?>" class="img-responsive"/>
        </a>
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">RESET PASSWORD</p>
        <?php
        if($this->session->flashdata('output') != '') {
            echo $this->session->flashdata('output');
        }
        ?>
        <form action="<?php echo base_url($base_url . 'forgot_password') ?>" method="post" style="margin-bottom: 10px!important;">
            <div class="form-group has-feedback">
                <input type="email" name="identity" class="form-control" autofocus="on" autocomplete="off" placeholder="Email"/>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <button type="submit" class="btn btn-primary btn-block btn-flat">Reset</button>
            <div class="clearfix"></div>
        </form>
        <a href="<?php echo site_url($base_url); ?>" class="text-center"><i class="fa fa-sign-in"></i> Login</a>

    </div>
</div>

<script type="text/javascript" src="<?php echo base_url($THEMES_LOC . 'bower_components/jquery/dist/jquery.min.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url($THEMES_LOC . 'bower_components/bootstrap/dist/js/bootstrap.min.js'); ?>"></script>
</body>
</html>
