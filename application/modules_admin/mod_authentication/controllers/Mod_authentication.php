<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_authentication extends CI_Controller
{
    private $title        = 'Administrator | Authentication';
    private $module_title = 'Authentication';                 /* TITLE MODULE */
    private $module_base  = 'mod_authentication/';             /* NAME MODULE */
    private $module_model = 'authentication_model';           /* NAME MODEL MODULE */
    private $tpl_loc      = 'themes/backend/';
    private $admin_group  = 1;
    private $dashboard_group = 4;
    private $configure = [];



    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->lang->load('auth');
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter', 'ion_auth'),
            $this->config->item('error_end_delimiter', 'ion_auth')
        );
        redirect('mod_dashboard');
    }

    public function set_default()
    {
        $data['title']      = $this->title;
        $data['title_page'] = $this->module_title;
        $data['base_url']   = $this->module_base;
        $sql   = 'SELECT config_id, config_name, config_value,config_value_id,config_desc,file_location,file_original,file_thumb FROM acs_config';
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            $query->free_result();
            $get_config=$result;
        } else {
            $get_config=[];
        }
        foreach ($get_config as $row_config) {
            $this->configure[$row_config['config_name']] = $row_config['config_value'];
        }
        $data['config']   = $this->configure;
//        echo '<pre>';
//        var_dump($data['config']);
//        die();
        $data['THEMES_LOC'] = $this->tpl_loc;
        return $data;
    }

    public function index()
    {
        if (!$this->ion_auth->logged_in()) {
            $this->ion_auth->logout();
            redirect($this->module_base . 'login', 'refresh');
        } else {
            redirect('mod_dashboard', 'refresh');
        }
    }


    public function login()
    {
        $data = $this->set_default();

        $this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
        $this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view($this->module_base . 'index', $data);
        } else {
            $this->do_login();
        }
    }

    public function do_login()
    {
        if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'))) {
            //if the login is successful
            //redirect them back to the home page
            if($this->ion_auth->in_group(2) || $this->ion_auth->in_group(3)){
                $this->session->set_flashdata('output', 'You cannot access this portal');
                redirect($this->module_base . 'login', 'refresh'); 
            }else{
                if ($this->ion_auth->in_group($this->admin_group))
                {
                    $this->session->set_flashdata('output', $this->ion_auth->messages());
                    redirect('mod_dashboard', 'refresh');
                    // $this->session->set_flashdata('message', 'You must be a gangsta to view this page');
                    // redirect('welcome/index');
                }
                else if ($this->ion_auth->in_group($this->dashboard_group)) {
                    $this->session->set_flashdata('output', $this->ion_auth->messages());
                    redirect('mod_dashboard', 'refresh');
                 
                }else{
                    $this->session->set_flashdata('output', $this->ion_auth->messages());
                    redirect('mod_dashboard', 'refresh');  
                }
            }
        } else {
            // if the login was un-successful
            // redirect them back to the login page
            $this->session->set_flashdata('output', $this->ion_auth->errors());
            redirect($this->module_base . 'login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
        }
    }

    public function forgot_password()
    {
        $data = $this->set_default();
        if ($this->config->item('identity', 'ion_auth') != 'email') {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
        } else {
            $this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
        }

        if ($this->form_validation->run() === FALSE) {
            $this->load->view($this->module_base . 'forgot_password', $data);
        } else {
            $this->do_forgot_password();
        }
    }

    public function do_forgot_password()
    {
        $identity_column = $this->config->item('identity', 'ion_auth');
        $identity        = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

        if (empty($identity)) {
            if ($this->config->item('identity', 'ion_auth') != 'email') {
                $this->ion_auth->set_error('forgot_password_identity_not_found');
            } else {
                $this->ion_auth->set_error('forgot_password_email_not_found');
            }

            $this->session->set_flashdata('output', $this->ion_auth->errors());
            redirect($this->module_base . "forgot_password", 'refresh');
        }

        // run the forgotten password method to email an activation code to the user
        $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

        if ($forgotten) {
            // if there were no errors
            $this->session->set_flashdata('output', $this->ion_auth->messages());
            redirect($this->module_base . "login", 'refresh'); //we should display a confirmation page here instead of the login page
        } else {
            $this->session->set_flashdata('output', $this->ion_auth->errors());
            redirect($this->module_base . "forgot_password", 'refresh');
        }
    }

    public function logout()
    {
        // log the user out
        $logout = $this->ion_auth->logout();

        // redirect them to the login page
        $this->session->set_flashdata('output', $this->ion_auth->messages());
        redirect($this->module_base . 'login', 'refresh');
    }
}
