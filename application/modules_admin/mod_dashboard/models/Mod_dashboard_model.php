<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . 'models/Admin_model.php';

class Mod_dashboard_model extends Admin_model
{

    public function __construct()
    {
        parent::__construct();
    }
    public function get_member(){
        $sql="select * from acs_users left join acs_users_groups on acs_users_groups.user_id=acs_users.id where group_id!='1'";
        $query=$this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    public function get_order($status=null){
        $sql="select * from product_order ";
        if(!empty($status)){
            $sql .="where status='{$status}'";
        }
        $query=$this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
    public function get_order_now(){
        $date=date('Y-m-d');
        $sql="select * from product_order where ctd='{$date}'";
        $query=$this->db->query($sql);
        if($query->num_rows() > 0){
            return $query->num_rows();
        }else{
            return 0;
        }
    }
}