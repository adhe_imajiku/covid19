<style>
    .box .box-header {
        padding: 20px 20px;
        padding-bottom: 10px;
        /* border-bottom: 2px solid #B36500; */
    }

    .box.box-primary {
        border-top-color: white;
    }

</style>
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="far fa-sad-tear"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Pasien Positif</span>
                    <span class="info-box-number" id="positif_dunia"><i class="fas fa-spinner fa-pulse"></i></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="far fa-smile-wink"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Pasien Sembuh</span>
                    <span class="info-box-number" id="sembuh_dunia"><i class="fas fa-spinner fa-pulse"></i></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="far fa-dizzy"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Pasien Meninggal</span>
                    <span class="info-box-number" id="meninggal_dunia"><i class="fas fa-spinner fa-pulse"></i></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-ios-people-outline"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Indonesia</span>
                    <span class="info-box-number positif_indonesia"><i class="fas fa-spinner fa-pulse"></i></span>
                    <span class="info-box-text">Positif</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">


        <div class="col-sm-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Ketahui sekarang</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <select name="option" class="form-control option">

                            <option value="">
                                Provinsi
                            </option>
                            <?php
                                foreach($indonesia as $index => $row){
                                ?>
                            <option value="<?= $row['attributes']['Kasus_Posi'] ?>~<?= $row['attributes']['Kasus_Semb'] ?>~<?= $row['attributes']['Kasus_Meni'] ?>">
                                <?= $row['attributes']['Provinsi'] ?>
                            </option>
                            <?php
                                }
                                ?>
                        </select>
                    </div>

                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-yellow"><i class="far fa-sad-tear"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pasien Positif</span>
                                    <span class="info-box-number" id="result_positif">0</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->
                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="far fa-smile-wink"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pasien Sembuh</span>
                                    <span class="info-box-number" id="result_sembuh">0</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                        <!-- /.col -->

                        <!-- fix for small devices only -->

                        <div class="col-md-4 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-red"><i class="far fa-dizzy"></i></span>

                                <div class="info-box-content">
                                    <span class="info-box-text">Pasien Meninggal</span>
                                    <span class="info-box-number" id="result_meninggal">0</span>
                                </div>
                                <!-- /.info-box-content -->
                            </div>
                            <!-- /.info-box -->
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
        <div class="col-sm-12">

            <div class="box box-primary bg-aqua">
                <div class="box-header">
                    <h3 class="box-title" style="color:white">Peta Sebaran Corona Dunia</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- Map box -->
                <!-- /.box -->
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-9 col-sm-8">
                            <div id="world-map" style="height: 400px; width: 100%;"></div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3 col-sm-4">
                            <div class="pad box-pane-right" style="min-height: 100%;background-color:rgba(255,255,255,0.3);color:white">
                                <div class="description-block margin-bottom">
                                    <h5 class="description-header">Kasus Covid 19 Indonesia</h5>
                                </div>
                                <div class="description-block margin-bottom">
                                    <div class="sparkbar pad" data-color="#fff">
                                        <i class="far fa-sad-tear"></i>
                                    </div>
                                    <h5 class="description-header positif_indonesia">0</h5>
                                    <span class="description-text">Positif</span>
                                </div>
                                <!-- /.description-block -->
                                <div class="description-block margin-bottom">
                                    <div class="sparkbar pad" data-color="#fff">
                                        <i class="far fa-smile-wink"></i>
                                    </div>
                                    <h5 class="description-header sembuh_indonesia">0</h5>
                                    <span class="description-text">Sembuh</span>
                                </div>
                                <!-- /.description-block -->
                                <div class="description-block">
                                    <div class="sparkbar pad" data-color="#fff">
                                        <i class="far fa-dizzy"></i>
                                    </div>
                                    <h5 class="description-header meninggal_indonesia">0</h5>
                                    <span class="description-text">Meninggal</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Sebaran Jumlah Kasus Positif Covid19</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p class="text-center">
                                <strong><?= date('d F Y H:i:s') ?></strong>
                            </p>

                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="max-height: 350px;"></canvas>
                            </div>
                            <!-- /.chart-responsive -->
                        </div>
                        <!-- /.col -->
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Jumlah Kasus Terkonfirmasi Positif Dunia</strong>
                            </p>

                            <div style="height: 400px;overflow: scroll;">
                                <?php
                            foreach($result as $index => $row){
                            ?>
                                <div class="progress-group">
                                    <span class="progress-text"><?= $row['attributes']['Country_Region'] ?></span>
                                    <span class="progress-number"><b><?= $row['attributes']['Confirmed'] ?></b></span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-yellow" style="width: 100%"></div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Jumlah Kasus Terkonfirmasi Sembuh Dunia</strong>
                            </p>

                            <div style="height: 400px;overflow: scroll;">
                                <?php
                            foreach($result as $index => $row){
                            ?>
                                <div class="progress-group">
                                    <span class="progress-text"><?= $row['attributes']['Country_Region'] ?></span>
                                    <span class="progress-number"><b><?= $row['attributes']['Recovered'] ?></b></span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-green" style="width: 100%"></div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <p class="text-center">
                                <strong>Jumlah Kasus Terkonfirmasi Meninggal Dunia</strong>
                            </p>

                            <div style="height: 400px;overflow: scroll;">
                                <?php
                            foreach($result as $index => $row){
                            ?>
                                <div class="progress-group">
                                    <span class="progress-text"><?= $row['attributes']['Country_Region'] ?></span>
                                    <span class="progress-number"><b><?= $row['attributes']['Deaths'] ?></b></span>

                                    <div class="progress sm">
                                        <div class="progress-bar progress-bar-red" style="width: 100%"></div>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </div>
            </div>
            <!-- /.box -->
        </div>
    </div>



    <div class="row">
        <div class="col-sm-12">
            <!-- BAR CHART -->
            <div class="box box-success">
                <div class="box-header">
                    <h3 class="box-title">Sebaran Covid19 Indonesia</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="chart">
                        <canvas id="barChart" style="height:230px"></canvas>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
    </div>
    <!-- solid sales graph -->
    <!-- /.box -->
</section>

<!-- /.row -->
