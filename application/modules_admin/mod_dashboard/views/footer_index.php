<script type="text/javascript">
    $(function() {
        $.ajax({
            url: '<?php echo base_url() ?>' + '/mod_dashboard/' + 'api/positif_dunia',
            type: "POST",
            data: "<?php echo $this->security->get_csrf_token_name() ?>=<?php echo $this->security->get_csrf_hash(); ?>",
            timeout: 180000,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    $('#positif_dunia').html(data.value);
                }
            }
        });
        $.ajax({
            url: '<?php echo base_url() ?>' + '/mod_dashboard/' + 'api/sembuh_dunia',
            type: "POST",
            data: "<?php echo $this->security->get_csrf_token_name() ?>=<?php echo $this->security->get_csrf_hash(); ?>",
            timeout: 180000,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    $('#sembuh_dunia').html(data.value);
                }
            }
        });
        $.ajax({
            url: '<?php echo base_url() ?>' + '/mod_dashboard/' + 'api/meninggal_dunia',
            type: "POST",
            data: "<?php echo $this->security->get_csrf_token_name() ?>=<?php echo $this->security->get_csrf_hash(); ?>",
            timeout: 180000,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    $('#meninggal_dunia').html(data.value);
                }
            }
        });
        $.ajax({
            url: '<?php echo base_url() ?>' + '/mod_dashboard/' + 'api/positif_indonesia',
            type: "POST",
            data: "<?php echo $this->security->get_csrf_token_name() ?>=<?php echo $this->security->get_csrf_hash(); ?>",
            timeout: 180000,
            dataType: "JSON",
            success: function(data) {
                if (data) {
                    $('.positif_indonesia').html(data.positif);
                    $('.sembuh_indonesia').html(data.sembuh);
                    $('.meninggal_indonesia').html(data.meninggal);
                }
            }
        });
        var visitorsData = {
            <?php
            foreach($result as $index => $row){
            ?>
            <?= !empty($consoling[$row['attributes']['Country_Region']]) ? $consoling[$row['attributes']['Country_Region']] : 'OT' ?>: <?= $row['attributes']['Confirmed'] ?>,
            <?php
            }
            ?>
        };
        // World map by jvectormap
        $('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'transparent',
            regionStyle: {
                initial: {
                    fill: '#e4e4e4',
                    'fill-opacity': 1,
                    stroke: 'none',
                    'stroke-width': 0,
                    'stroke-opacity': 1
                }
            },
            series: {
                regions: [{
                    values: visitorsData,
                    scale: ['#DD4B39'],
                    normalizeFunction: 'polynomial'
                }]
            },
            onRegionLabelShow: function(e, el, code) {
                if (typeof visitorsData[code] != 'undefined')
                    el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
            }
        });

        var salesChartCanvas = $('#salesChart').get(0).getContext('2d');
        // This will get the first returned node in the jQuery collection.
        var salesChart = new Chart(salesChartCanvas);

        var salesChartData = {
            labels: [<?php foreach($result as $index => $row){ if($index!=0){ ?>, <?php } ?> '<?= $row['attributes']['Country_Region'] ?>'
                <?php if($index>20){ break; } } ?>
            ],
            datasets: [{
                    label: 'Confirmed',
                    fillColor: '#F39C12',
                    strokeColor: '#F39C12',
                    pointColor: '#F39C12',
                    pointStrokeColor: '#F39C12',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: '#F39C12',
                    data: [<?php foreach($result as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Confirmed'] ?><?php if($index>20){ break; }  } ?>]
                },
                {
                    label: 'Recovered',
                    fillColor: '#00A65A',
                    strokeColor: '#00A65A',
                    pointColor: '#00A65A',
                    pointStrokeColor: '#00A65A',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: '#00A65A',
                    data: [<?php foreach($result as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Recovered'] ?> <?php if($index>20){ break; }  } ?>]
                },
                {
                    label: 'Death',
                    fillColor: '#DD4B39',
                    strokeColor: '#DD4B39',
                    pointColor: '#DD4B39',
                    pointStrokeColor: '#DD4B39',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: '#DD4B39',
                    data: [<?php foreach($result as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Deaths'] ?> <?php if($index>20){ break; }  } ?>]
                }
            ]
        };

        var salesChartOptions = {
            // Boolean - If we should show the scale at all
            showScale: true,
            // Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            // String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            // Number - Width of the grid lines
            scaleGridLineWidth: 1,
            // Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            // Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            // Boolean - Whether the line is curved between points
            bezierCurve: true,
            // Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            // Boolean - Whether to show a dot for each point
            pointDot: false,
            // Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            // Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            // Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            // Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            // Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            // Boolean - Whether to fill the dataset with a color
            datasetFill: true,
            // String - A legend template
            legendTemplate: '<ul class=\'<%=name.toLowerCase()%>-legend\'><% for (var i=0; i<datasets.length; i++){%><li><span style=\'background-color:<%=datasets[i].lineColor%>\'></span><%=datasets[i].label%></li><%}%></ul>',
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            // Boolean - whether to make the chart responsive to window resizing
            responsive: true
        };

        // Create the line chart
        salesChart.Line(salesChartData, salesChartOptions);
        
        var areaChartData = {
            labels: [<?php foreach($indonesia as $index => $row){ if($index!=0){ ?>, <?php } ?>'<?= $row['attributes']['Provinsi'] ?>'<?php if($index>10){ break; }  } ?> ],
            datasets: [{
                    label: 'Positif',
                    fillColor: '#F39C12',
                    strokeColor: 'F39C12',
                    pointColor: 'F39C12',
                    pointStrokeColor: '#c1c7d1',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(220,220,220,1)',
                    data: [<?php foreach($indonesia as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Kasus_Posi'] ?><?php if($index>10){ break; }  } ?>]
                },
                {
                    label: 'Sembuh',
                    fillColor: 'rgba(60,141,188,0.9)',
                    strokeColor: 'rgba(60,141,188,0.8)',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: [<?php foreach($indonesia as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Kasus_Semb'] ?><?php if($index>10){ break; }  } ?>]
                },
                {
                    label: 'Meninggal',
                    fillColor: '#DD4B39',
                    strokeColor: '#DD4B39',
                    pointColor: '#3b8bba',
                    pointStrokeColor: 'rgba(60,141,188,1)',
                    pointHighlightFill: '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data: [<?php foreach($indonesia as $index => $row){ if($index!=0){ ?>, <?php } ?> <?= $row['attributes']['Kasus_Meni'] ?><?php if($index>10){ break; }  } ?>]
                }
            ]
        }
        var barChartCanvas = $('#barChart').get(0).getContext('2d')
        var barChart = new Chart(barChartCanvas)
        var barChartData = areaChartData
        barChartData.datasets[1].fillColor = '#00a65a'
        barChartData.datasets[1].strokeColor = '#00a65a'
        barChartData.datasets[1].pointColor = '#00a65a'
        var barChartOptions = {
            //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
            scaleBeginAtZero: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: true,
            //String - Colour of the grid lines
            scaleGridLineColor: 'rgba(0,0,0,.05)',
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - If there is a stroke on each bar
            barShowStroke: true,
            //Number - Pixel width of the bar stroke
            barStrokeWidth: 2,
            //Number - Spacing between each of the X value sets
            barValueSpacing: 5,
            //Number - Spacing between data sets within X values
            barDatasetSpacing: 1,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
            //Boolean - whether to make the chart responsive
            responsive: true,
            maintainAspectRatio: true
        }

        barChartOptions.datasetFill = false
        barChart.Bar(barChartData, barChartOptions)
        
        $('.option').change(function() {
            var value=$('.option').val().split('~');
            $('#result_positif').html(value[0]);
            $('#result_sembuh').html(value[1]);
            $('#result_meninggal').html(value[2]);
        });
    })

</script>
