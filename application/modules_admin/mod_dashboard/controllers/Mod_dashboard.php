<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base_admin.php';
require_once APPPATH . '../vendor/autoload.php';

class Mod_dashboard extends Base_admin
{
    private $module_title      = 'Dashboard';
    private $module_title_icon = '<i class="fa fa-book"></i>';
    private $module_base       = 'mod_dashboard/';
    private $module_model      = 'Mod_dashboard_model';
    private $page              = [
        'index'        => 'index',
        'index_footer' => 'footer_index',
    ];

    /*
     * =============================================================================
     * FUNCTION CONSTRUCT
     * =============================================================================
     */

    public function __construct()
    {
        parent::__construct();
        $this->title = $this->module_title;
        $this->load->model($this->module_model);

        $this->module_config = [
            'module_base' => $this->module_base
        ];
    }

    /*
     * =============================================================================
     * FUNCTION INDEX
     * =============================================================================
     */

    public function index()
    {
        $data = [];
        $data['orders']     = $this->{$this->module_model}->get_order();
        $data['pending']    = $this->{$this->module_model}->get_order_now();
        $data['member']     = $this->{$this->module_model}->get_member();
        $sql="select * from apps_countries";
        $temper=$this->db->query($sql)->result_array();
        $data['consoling']=[];
        foreach($temper as $index => $row){
            $data['consoling'][$row['country_name']]=$row['country_code'];
        }
        $sql="select * from log_data_json where type='world' and date <= NOW()";
        $query=$this->db->query($sql);
        if($query->num_rows() > 0){
            $data['result']=json_decode($query->row_array()['json'],true);
        }else{
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $client     = new GuzzleHttp\Client([
                'headers' => $headers
            ]);
            try {
                $xdata=[];
                $response = $client->request( 'get', 'https://api.kawalcorona.com/',['form_params' => $xdata]);

                $return['message']= json_decode($response->getBody(),true);
                $return= $return['message'];

            } catch (GuzzleHttp\Exception\BadResponseException $e) {
                $return=[];
            }
            $data['result']=$return;
            
            $ins = array(
                'id' => date('YmdHis').uniqid(),
                'json' => json_encode($data['result']),
                'type' => 'world',
                'date' => date('Y-m-d H:i:s')
            );
            $this->db->insert('log_data_json', $ins);
        }
        
        $sql="select * from log_data_json where type='indonesia' and date <= NOW()";
        $query=$this->db->query($sql);
        if($query->num_rows() > 0){
            $data['indonesia']=json_decode($query->row_array()['json'],true);
        }else{
            $headers = [
                'Content-Type' => 'application/json',
            ];
            $client     = new GuzzleHttp\Client([
                'headers' => $headers
            ]);
            try {
                $xdata=[];
                $response = $client->request( 'get', 'https://api.kawalcorona.com/indonesia/provinsi',['form_params' => $xdata]);

                $return['message']= json_decode($response->getBody(),true);
                $return= $return['message'];

            } catch (GuzzleHttp\Exception\BadResponseException $e) {
                $return=[];
            }
            $data['indonesia']=$return;
            
            $ins = array(
                'id' => date('YmdHis').uniqid(),
                'json' => json_encode($data['indonesia']),
                'type' => 'indonesia',
                'date' => date('Y-m-d H:i:s')
            );
            $this->db->insert('log_data_json', $ins);
        }
//        echo '<pre>';
//        var_dump($data['result']);
//        die();
        $this->display($this->page['index'], $data, $this->page['index_footer']);
    }
    public function api($role='')
    {
        $url_link='';
        if($role=='positif_dunia'){
            $url_link='https://api.kawalcorona.com/positif';
        }else if($role=='sembuh_dunia'){
            $url_link='https://api.kawalcorona.com/sembuh';
        }else if($role=='meninggal_dunia'){
            $url_link='https://api.kawalcorona.com/meninggal';
        }else if($role=='positif_indonesia' ||$role=='sembuh_indonesia' ||$role=='meninggal_indonesia'){
            $url_link='https://api.kawalcorona.com/indonesia';
        }
        $headers = [
            'Content-Type' => 'application/json',
        ];
        $client     = new GuzzleHttp\Client([
            'headers' => $headers
        ]);
        $url        = $url_link;
        try {
            $xdata=[];
            $response = $client->request( 'get', $url,['form_params' => $xdata]);
        #guzzle repose for future use
                
            $return['message']= json_decode($response->getBody(),true);
            $return= $return['message'];

        } catch (GuzzleHttp\Exception\BadResponseException $e) {
                #guzzle repose for future use
            $return=[];
        }
        $data=$return;
        if($role=='positif_indonesia'){
            $data=$data[0];
        }
        echo json_encode($data);
        
    }
}
