<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once 'google-api-php-client/Google_Client.php';
include_once 'google-api-php-client/contrib/Google_Oauth2Service.php';

Class Google
{
    private $clientId     = '';
    private $clientSecret = '';
    private $redirectURL  = '';

    public function __construct()
    {
//        $this->CI =& get_instance();
//        $this->load->config('google');
//        $this->load->config('google');

        $this->clientId     = '538000909476-96sj3nik21tvn75s6uc62k6le587c1it.apps.googleusercontent.com';
        $this->clientSecret = 'tLzUTf_Nba2QoDxaEQAFemxr';

//        $this->clientId     = '412695710754-81td5ui7tku99gj3e5qc5v2djn2g39c7.apps.googleusercontent.com';
//        $this->clientSecret = 'qmCrtpuY-Q-PaHGCO8E8DAKH';
        $this->redirectURL = 'http://lovary.dojobox.xyz/';
//        var_dump($this->clientId);
//        die();

        /* Call Google API */
        $this->gClient = new Google_Client();
        $this->gClient->setApplicationName('Login to Lovary');
        $this->gClient->setClientId($this->clientId);
        $this->gClient->setClientSecret($this->clientSecret);
        $this->gClient->setRedirectUri($this->redirectURL);

        $this->google_oauthV2 = new Google_Oauth2Service($this->gClient);
    }

    public function login()
    {
//        $this->load->library('session');

        /* GET CODE OR TOKEN FROM LOGIN GOOGLE+ */
        if (isset($_GET['code'])) {
            $this->gClient->authenticate($this->input->get('code'));
//            $this->session->set_userdata('token', $this->gClient->getAccessToken());
            header('Location: ' . $this->redirectURL);
        }

//        if ($this->session->userdata('token')) {
//            $this->gClient->setAccessToken($this->session->userdata('token'));
//        }

        if (!$this->gClient->getAccessToken()) {
            /* GENERATE LOGIN URL */
            $authUrl = $this->gClient->createAuthUrl();
            redirect(filter_var($authUrl, FILTER_SANITIZE_URL));
        } else {
            $login = false;
            /* GET USER PROFILE DATA FROM GOOGLE */
            $gpUserProfile = (object)$this->google_oauthV2->userinfo->get();

            $name_explode = explode(' ', $gpUserProfile->name);
            $last_name    = $name_explode[((count($name_explode)) - 1)];
            unset($name_explode[((count($name_explode)) - 1)]);
            $first_name = implode(' ', $name_explode);

            /* HELPER LOGIN */
            return sm_login($gpUserProfile->email, $first_name, $last_name, false, 'google');
        }
    }

    public function logout()
    {
        $this->gClient->revokeToken();
    }

}

/* End of file Facebook_ion_auth.php */
