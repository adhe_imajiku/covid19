<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Guzzle {
  public  $CI         = '';
  private $client ;

  public function __construct()
  {
    $this->CI = &get_instance();
    $this->client = new GuzzleHttp\Client();
  }

  public function httpClient($method = 'GET', $url  = '', $data)
  {
    try {
      $response = $this->client->request($method,$url, $data);
      #guzzle repose for future use
      // echo $response->getStatusCode(); // 200
      $return =  [
        'statusCode' => $response->getStatusCode(),
        'response' => json_decode($response->getBody(),true) 
      ];
      // echo $response->getReasonPhrase(); // OK
      // echo $response->getProtocolVersion(); // 1.1
      // echo $response->getBody();
    } catch (GuzzleHttp\Exception\BadResponseException $e) {
    #guzzle repose for future use
      $return =  [
        'statusCode' => $e->getStatusCode(),
        'response' => $e->getBody()->getContents()
      ];

      // $response = $e->getResponse();
      // $responseBodyAsString = $response->getBody()->getContents();
      // print_r($responseBodyAsString);
    }
    return $return;;
  }
}
