<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/fastclick/lib/fastclick.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/datatables.net/js/dataTables.rowReorder.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>dist/js/fontawesome-iconpicker.js"></script>

<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>bower_components/bootbox/bootbox.min.js"></script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>dist/js/adminlte.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src="<?php echo theme_admin_locations(); ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo theme_admin_locations(); ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo theme_admin_locations(); ?>bower_components/chart.js/Chart.js"></script>
<!--<script src="<?php echo theme_admin_locations(); ?>dist/js/pages/dashboard.js"></script>-->
<!--<script src="<?php echo theme_admin_locations(); ?>dist/js/demo.js"></script>-->

<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-chained/1.0.1/jquery.chained.remote.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.js"></script> -->
<!-- <script type="text/javascript" src="<?php echo theme_admin_locations(); ?>plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->

<script type="text/javascript">
    var BASE_URL = '<?php echo base_url(); ?>';
    var VAR_NAME = '';
    var VAR_HASH = '';
    /* CHECK ON CONSTRUCT MODULE */
    var VAR_MODULE = jQuery.parseJSON('<?php echo json_encode($CONFIG['SET_DEFAULT_DATA']); ?>');
    var AJAX_ERROR_MSG = '<?php echo AJAX_ERROR_MSG; ?>';

    $('.select2').select2({ width: '100%'});
    $('.select2multiple').select2({ width: '100%', "multiple": true, "closeOnSelect": true});
    $(document.body).on('hide.bs.modal', function () { $('body').css('padding-right', '0'); });
    $(document.body).on('hidden.bs.modal', function () { $('body').css('padding-right', '0'); });
    $(document).ready(function () {
        window.setTimeout(function() {$("#alert").fadeTo(1500, 0).slideUp(500, function(){$(this).remove();});}, 1000);
    });
    $(document).ready(function () {
        $.ajaxSetup({ data: <?php echo $CSRF_JS; ?> });
        $.ajaxSetup({ type: 'POST', data: <?php echo $CSRF_JS; ?> });
        $('#icp_test').iconpicker()
    });
</script>
<script type="text/javascript" src="<?php echo theme_admin_locations(); ?>custom/js/custom.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/js/fontawesome-iconpicker.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/js/bootstrap-colorpicker.js"></script>