<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/bootstrap/dist/css/bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/font-awesome/css/font-awesome.min.css"/>
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/Ionicons/css/ionicons.min.css"/>

<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css"/>
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/datatables.net-bs/css/rowReorder.dataTables.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/select2/dist/css/select2.min.css"/>

<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>dist/css/AdminLTE.min.css"/>
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>dist/css/skins/_all-skins.min.css"/>
<link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>plugins/FontAwesome-Selector-master/assets/faSelectorStyle.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datepicker/0.6.5/datepicker.min.css" /> -->
<!-- <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>plugins/datetimepicker/css/bootstrap-datetimepicker.min.css"/> -->
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" /> -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/fontawesome-iconpicker/3.2.0/css/fontawesome-iconpicker.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.css"/>
  <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>bower_components/morris.js/morris.css">


