<footer class="main-footer bg-gray">
	<div class="container-fluid">
		<div class="pull-right hidden-xs">
            <strong> &copy; <?= date('Y') ?>. All rights reserved.</strong>
        </div>
		<div class="pull-right hidden-xs" style="padding: 0 5px">
            <img class="img-responsive" src="<?php echo theme_global_locations() ?>logo-imajiku.png">
		</div>
	</div>
</footer>