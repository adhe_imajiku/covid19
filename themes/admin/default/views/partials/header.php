<header class="main-header">
    <a href="<?php echo base_url('admins'); ?>" class="logo">
        <span class="logo-mini"><b>M</b></span>
        <span class="logo-lg">
            <img src="<?php echo theme_global_locations() . 'logo.png'; ?>" class="img-responsive img-logo" />
        </span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle hidden-lg hidden-md hidden-sm" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="cust"><?php echo ''//ucwords($user[0]['first_name']); ?> <span class="caret"></span></div>
                    </a>
                    <ul class="dropdown-menu custom-navbar" role="menu">
                        <li><a href="<?php echo site_url('mod_logout'); ?>"><i class="fas fa-power-off"></i> Sign Out</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
