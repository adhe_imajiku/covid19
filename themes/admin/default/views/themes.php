
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $template['title']; ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="author" content="<?php echo $meta_author; ?>">
    <meta name="description" content="<?php echo $meta_description; ?>"/>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>"/>

    <!-- ****** START FAVICONS ****** -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo theme_global_locations() ?>favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_global_locations() ?>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo theme_global_locations() ?>favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo theme_global_locations() ?>favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo theme_global_locations() ?>favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- ****** END FAVICONS ****** -->


    <?php echo (!empty($template['partials']['meta_css'])) ? $template['partials']['meta_css'] : ''; ?>
    <?php echo (isset($FILE_CSS)) ? $FILE_CSS : ''; ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>custom/css/custom.css?ver=<?php echo date('YmdHis'); ?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
    <style>
        #sticky-footer {
            position: absolute;
            bottom: 0;
            width: -webkit-fill-available;
        }
        .bg-gray {
            color: white;
            /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,1847c9+0,0c1d6b+84,0c1d6b+84 */
            background: #ffffff; /* Old browsers */
            background: -moz-<?php variable_set($CONFIG['background_login'], true, true); ?> /* FF3.6-15 */
            background: -webkit-<?php variable_set($CONFIG['background_login'], true, true); ?>
            background: <?php variable_set($CONFIG['background_login'], true, true); ?>
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#0c1d6b',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */


        }
        .skin-blue .main-header .navbar {
            /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#ffffff+0,1847c9+0,0c1d6b+84,0c1d6b+84 */
            background: #ffffff; /* Old browsers */
            background: -moz-<?php variable_set($CONFIG['background_login'], true, true); ?>
            background: -webkit-<?php variable_set($CONFIG['background_login'], true, true); ?>
            background: <?php variable_set($CONFIG['background_login'], true, true); ?>
            filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#0c1d6b',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */

        }
        .btn-primary{
            background: <?php variable_set($CONFIG['btn_link_hover_login'], true, true); ?>;
            border-color: <?php variable_set($CONFIG['btn_link_hover_login'], true, true); ?>;
        }
        .btn-primary:hover{
            background: <?php variable_set($CONFIG['btn_link_hovered'], true, true); ?>;
            border-color: <?php variable_set($CONFIG['btn_link_hovered'], true, true); ?>;
        }
        .bg-aqua{
            background-color: <?php variable_set($CONFIG['btn_link_hover_login'], true, true); ?> !important;
            border-color: <?php variable_set($CONFIG['btn_link_hover_login'], true, true); ?>;
        }
    </style>

</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<div class="wrapper">

    <?php include_once 'partials/header.php'; ?>

    <aside class="main-sidebar">
        <section class="sidebar">

            <ul class="sidebar-menu" data-widget="tree">

                <li><a href="<?php echo base_url('admins'); ?>">Dashboard</a></li>

                <?php echo $MENU_POST; ?>

                <li class="treeview hidden">
                    <a href="#"><span>Settings</span><span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span></a>
                    <ul class="treeview-menu">
                        <li><a href="<?php echo site_url('mod_acs_config'); ?>">General Setting</a></li>
                        <li><a href="<?php echo site_url('mod_acs_users'); ?>">Manage Admin</a></li>
                        <li><a href="<?php echo site_url('mod_logout'); ?>">Sign Out</a></li>
                    </ul>
                </li>
                <li class="header" id="sticky-footer">
                    <p class="text-white"><i class="fas fa-spinner fa-spin"></i> Site Benchmarking </p>
                    <p class="text-white"><strong><i class="fas fa-area-chart"></i> Memory Usage</strong> {memory_usage} </p>
                    <p class="text-white"><strong><i class="fas fa-clock-o"></i> Elapsed Time</strong> {elapsed_time} sec</p>
                </li>

            </ul>
        </section>
    </aside>

    <div class="content-wrapper" style="background-color:#dee0e2">
        <div class="container-fluid" style="padding-bottom:50px">
            <div id="alert_div"></div>
            <?php
            if ($this->session->flashdata('output') != '') {
                echo '<div id="alert" style="margin-top: 20px; margin-left: 10px; margin-right: 10px;">' .
                     '<div class="row">' .
                     '<div class="col-md-12">';
                echo $this->session->flashdata('output');
                echo '</div>' .'</div>' .'</div>';
            }
            ?>
            <?php echo (!empty($template['partials']['content'])) ? $template['partials']['content'] : ''; ?>
        </div>
    </div>
    <?php ?>
</div>
<?php echo (!empty($template['partials']['footer'])) ? $template['partials']['footer'] : ''; ?>
<?php echo (!empty($template['partials']['script'])) ? $template['partials']['script'] : ''; ?>
<?php echo (isset($FILE_JS)) ? $FILE_JS : ''; ?>
<?php echo (!empty($template['partials']['script_page'])) ? $template['partials']['script_page'] : ''; ?>
</body>
</html>
