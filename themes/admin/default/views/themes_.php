<?php
//!empty($SESS_USER) && in_array($CONFIG['user_group'], $SESS_USER['user_group'])
//echo '<pre>';
//var_dump($this->ion_auth);
//die();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $template['title']; ?></title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <meta name="author" content="<?php echo $meta_author; ?>">
    <meta name="description" content="<?php echo $meta_description;?>" />
    <meta name="keywords" content="<?php echo $meta_keywords;?>" />

    <!-- ****** START FAVICONS ****** -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo theme_global_locations()?>favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_global_locations()?>favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo theme_global_locations()?>favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo theme_global_locations()?>favicons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo theme_global_locations()?>favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- ****** END FAVICONS ****** -->


    <?php echo (!empty($template['partials']['meta_css'])) ? $template['partials']['meta_css'] : ''; ?>
    <?php echo (isset($FILE_CSS)) ? $FILE_CSS : ''; ?>
<!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->
    <link href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo theme_admin_locations(); ?>custom/css/custom.css?ver=<?php echo date('YmdHis'); ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Google Font -->
    <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">

</head>
<body class="hold-transition skin-red layout-top-nav">
<div class="wrapper">
    <?php include 'partials/header.php'; ?>
    <div class="content-wrapper" style="background-color:#dee0e2">
        <div class="container-fluid">
            <div id="alert_div"></div>
            <?php
            if($this->session->flashdata('output') != '') {
                echo '<div id="alert" style="margin-top: 20px; margin-left: 10px; margin-right: 10px;"><div class="row"><div class="col-md-12">';
                echo $this->session->flashdata('output');
                echo '</div></div></div>';
            }
            ?>
	        <?php echo (!empty($template['partials']['content'])) ? $template['partials']['content'] : ''; ?>
        </div>
    </div>
</div>
<?php echo (!empty($template['partials']['footer'])) ? $template['partials']['footer'] : ''; ?>
<?php echo (!empty($template['partials']['script'])) ? $template['partials']['script'] : ''; ?>
<?php echo (isset($FILE_JS)) ? $FILE_JS : ''; ?>
<?php echo (!empty($template['partials']['script_page'])) ? $template['partials']['script_page'] : ''; ?>
</body>
</html>
