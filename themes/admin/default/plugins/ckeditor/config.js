/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

var KCFINDER_PATH = 'themes/admin/default/plugins/kcfinder/browse.php?opener=ckeditor';
CKEDITOR.editorConfig = function (config) {
    config.height = 310;
    config.filebrowserBrowseUrl         = BASE_URL + KCFINDER_PATH + '&type=files';
    config.filebrowserImageBrowseUrl    = BASE_URL + KCFINDER_PATH + '&type=images';
    config.filebrowserFlashBrowseUrl    = BASE_URL + KCFINDER_PATH + '&type=flash';
    config.filebrowserUploadUrl         = BASE_URL + KCFINDER_PATH + '&type=files';
    config.filebrowserImageUploadUrl    = BASE_URL + KCFINDER_PATH + '&type=images';
    config.filebrowserFlashUploadUrl    = BASE_URL + KCFINDER_PATH + '&type=flash';
};
