/*
 * PT. IMAJIKU CIPTA MEDIA
 * Copyright 2019-2020 IMAJIKU.
 * USE FOR GLOBAAL FUNCTIONS
 */



/* document ready */
$(document).ready(function() {

	$("#form-signin").validate({
		errorPlacement: function(label, element) {
			label.addClass('error');
			element.parent().append(label);
		}
	});

	$("#form-signup").validate({
		errorPlacement: function(label, element) {
			label.addClass('error');
			element.parent().append(label);
		}
	});

	$("#form-forgot-password").validate({
		errorPlacement: function(label, element) {
			label.addClass('error');
			element.parent().append(label);
		}
	});

	$("#form-profile-password").validate({
		errorPlacement: function(label, element) {
			label.addClass('error');
			element.parent().append(label);
		}
	});	

	$("#form-contact").validate({
		errorPlacement: function(label, element) {
			label.addClass('error');
			element.parent().append(label);
		}
	});	
    
    
});