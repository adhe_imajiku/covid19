﻿/*
 * PT. IMAJIKU CIPTA MEDIA
 * Copyright 2019-2020 IMAJIKU.
 * USE FOR GLOBAAL FUNCTIONS
 */

 /* load welcome dialog first */
 if ($.cookie('pop') == null) {
 	$('#welcomedialog').modal('show');
 }

 /* begin.scroll back to top */
// function scrollFunction() {
// 	if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
// 		document.getElementById("BackToTop").classList.add('active');
// 	} else {
// 		document.getElementById("BackToTop").classList.remove('active');
// 	}
// }

 /* When the user clicks on the button, scroll to the top of the document */
 function topFunction() {
 	/* document.body.scrollTop = 0; */
 	/* document.documentElement.scrollTop = 0; */
 	$("html, body").animate({ scrollTop: 0 }, 1200, 'linear');
 }
 /* end.scroll back to top */

 function myheaders() {
 	if (window.pageYOffset > sticky) {
 		navheader.classList.add("sticky");
 	} else {
 		navheader.classList.remove("sticky");
 	}
 }

 /* header scroll */
 window.onscroll = function() {
 	myheaders()
// 	scrollFunction()
 };

 var navheader = document.getElementById("headertop");
 var sticky = navheader.offsetTop;


 /* document ready */
 $(document).ready(function() {

 	/* DISABLE SCROLLBAR WHEN SHOWING MAINMENU RESPOSIVE */
 	$('.finebody').click(function(e) {
 		e.preventDefault();
 		$(this).parents().find('body').toggleClass('disablescrollbar');
 		$(this).parents('html').toggleClass('disablescrollbar');
 	})

 	/* SEARCH */
 	$('.button-search').on('click', function(event) {
 		event.stopPropagation();
 		$(this).parents().find('#search-body').addClass('open');
 		$('#search-body input[type="search"]').focus();
 	});
 	$('.search-body').on('click keyup', function(event) {
 		event.stopPropagation();
 		if (event.target == this || event.target.className == 'close' || event.keyCode == 27) {
 			$(this).parents().find('#search-body').removeClass('open');
 		}
 	});
 	$('.search-body .close , .wrapper').on('click', function(event) {
 		$(this).parents().find('#search-body').removeClass('open');
 	});

 	/* EXTRA CONDITION FOR NAVBAR SUBMENU IN RESPONSIVE */
 	$('.dropdown-menu [data-toggle=dropdown]').on('click', function(event) {
 		event.preventDefault();
 		event.stopPropagation();
 		$(this).parent().siblings().removeClass('show');
 		$(this).parent().toggleClass('show');
 	});


 	/* SELECT2 GLOBAL */
 	$('.select-mjk').select2({
 		minimumResultsForSearch: Infinity
 	});


 	/* SELECT2 COLOR */
 	function formatColor (state) {
 		if (!state.id) {
 			return state.text;
 		}

 		var baseUrl = "./themes/images/icons/";
 		var $state = $(
 			'<span><img class="product-color" /> <span></span></span>'
 			);

 		$state.find("span").text(state.text);
 		$state.find("img").attr("src", baseUrl + state.element.value.toLowerCase() + ".png");

 		return $state;
 	};

 	$(".select-color").select2({
 		templateSelection: formatColor
 	});


 	/* SELECT2 GLOBAL WITH SEARCH ACTIVED */
 	$('.select-mjk-search-active').select2({
 		minimumResultsForSearch: 3
 	});

 	/* FORM DATE */
 	$('.datepicker').datepicker({
 		format: "dd/mm/yyyy",
 		autoclose: true, 
 		todayHighlight: true
 	});
 	$('.forbirthday').daterangepicker({
 		singleDatePicker: true,
 		showDropdowns: true,
 		minYear: 1901,
 		maxDate: moment().endOf("day"),
 	});

 	/* LANGUAGE */
 	$('.select-language').selectpicker();


 	/* hero slider */
 	$('.hero-slider').slick({
 		dots: false,
 		speed: 1500,
 		arrows: false,
 		autoplay: true,
 		autoplaySpeed: 3000,
 		infinite: true,
 		slidesToShow: 1,
 		slidesToScroll: 1
 	});
 	$('.hero-slider-prev').click(function() {
 		$('.hero-slider').slick('slickPrev');
 	});
 	$('.hero-slider-next').click(function() {
 		$('.hero-slider').slick('slickNext');
 	});

 	/* global slider for content */
 	$('.content-slider1').slick({
 		dots: false,
 		speed: 2000,
 		arrows: false,
 		autoplay: true,
 		autoplaySpeed: 2000,
 		infinite: true,
 		slidesToShow: 1,
 		slidesToScroll: 1,
 		responsive: [
 		{
 			breakpoint: 1024,
 			settings: {
 				slidesToShow: 3,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 992,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 812,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 768,
 			settings: {
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 767,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 640,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 480,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 320,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 300,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		}
 		]
 	});
 	$('.globalslider-prev').click(function() {
 		$('.content-slider1').slick('slickPrev');
 	});
 	$('.globalslider-next').click(function() {
 		$('.content-slider1').slick('slickNext');
 	});

 	/* global slider for content 3 slide */
 	$('.content-slider3').slick({
 		dots: false,
 		speed: 2000,
 		arrows: false,
 		autoplay: true,
 		autoplaySpeed: 2000,
 		infinite: true,
 		slidesToShow: 3,
 		slidesToScroll: 1,
 		responsive: [
 		{
 			breakpoint: 1024,
 			settings: {
 				slidesToShow: 3,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 992,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 812,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 768,
 			settings: {
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 767,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 640,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 480,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 320,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 300,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		}
 		]
 	});
 	$('.globalslider-prev').click(function() {
 		$('.content-slider3').slick('slickPrev');
 	});
 	$('.globalslider-next').click(function() {
 		$('.content-slider3').slick('slickNext');
 	});

 	/* global slider for content 4 slide */
 	$('.content-slider4').slick({
 		dots: false,
 		speed: 2000,
 		arrows: false,
 		autoplay: true,
 		autoplaySpeed: 3000,
 		infinite: true,
 		slidesToShow: 4,
 		slidesToScroll: 1,
 		responsive: [
 		{
 			breakpoint: 1024,
 			settings: {
 				slidesToShow: 3,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 992,
 			settings: {
 				slidesToShow: 3,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 812,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 768,
 			settings: {
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 767,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 640,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 2,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 480,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 320,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		},
 		{
 			breakpoint: 300,
 			settings: {
 				mobileFirst:true,
 				slidesToShow: 1,
 				slidesToScroll: 1
 			}
 		}
 		]
 	});
 	$('.globalslider-prev').click(function() {
 		$('.content-slider4').slick('slickPrev');
 	});
 	$('.globalslider-next').click(function() {
 		$('.content-slider4').slick('slickNext');
 	});

 	/* product-slider */
 	$('.product-slider').slick({
 		slidesToShow: 1,
 		slidesToScroll: 1,
 		arrows: false,
 		fade: true,
 		centerMode: true,
 		autoplay: false,
 		asNavFor: '.product-slider-nav'
 	});
 	$('.product-slider-nav').slick({
 		slidesToShow: 6,
 		slidesToScroll: 1,
 		arrows: false,
 		dots: false,
 		autoplay: false,
 		centerMode: true,
 		focusOnSelect: true,
 		centerPadding: '0px',
 		asNavFor: '.product-slider',
 		responsive: [
 		{
 			breakpoint: 992,
 			settings: {
 				slidesToShow: 4
 			}
 		},
 		{
 			breakpoint: 768,
 			settings: {
 				slidesToShow: 6
 			}
 		},
 		{
 			breakpoint: 480,
 			settings: {
 				slidesToShow: 4
 			}
 		}
 		]
 	});
 	$('.globalslider-prev').click(function() {
 		$('.product-slider-nav').slick('slickPrev');
 	});
 	$('.globalslider-next').click(function() {
 		$('.product-slider-nav').slick('slickNext');
 	});



 	$('.fancybox-video').fancybox();

 	/* FORM VALIDATION */

 	$("#form-login").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $("#form-login").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});
 	$("#form-forgot-password").validate({
 		submitHandler: function() {
 			window.location = $("#form-forgot-password").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});
 	$("#form-reset-password").validate({
 		submitHandler: function() {
 			window.location = $("#form-reset-password").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-register").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $("#form-register").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-contact").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $("#form-contact").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-checkout-guest").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $("#form-checkout-guest").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-profile-password").validate({
 		submitHandler: function() {
 			window.location = $("#form-profile-password").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-edit-profile").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $("#form-edit-profile").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});
 	$(".form-edit-address").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $(".form-edit-address").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});
 	$(".form-add-address").validate({
 		rules: {
 			phone: {
 				required: true,
 				number: true,
 				minlength: 6
 			}
 		},
 		submitHandler: function() {
 			window.location = $(".form-add-address").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});

 	$("#form-newsletter").validate({
 		submitHandler: function() {
 			window.location = $("#form-newsletter").attr("action");
 		},
 		errorPlacement: function(label, element) {
 			label.addClass('error');
 			element.parent().append(label);
 		}
 	});


 	/* CHECKED CHOICE WHEN ANY INPUT DISABLED */
 	$('input[name="writealetter"]').on("change", function () {
 		if ($(this).prop("checked") && $(this).val() != 2) 
 			$('#choice-spesific [name="spesific"]').prop("disabled", true);
 		else 
 			$('#choice-spesific [name="spesific"]').prop("disabled", false);
 	});

 	/* UPLOAD PHOTO or IMAGE */
 	function fuploadphoto(input) {
 		if (input.files && input.files[0]) {
 			var reader = new FileReader();

 			reader.onload = function(e) {
 				$('#thisuphoto').attr('src', e.target.result);
 			}
 			reader.readAsDataURL(input.files[0]);
 		}
 	}
 	$("#uphoto").change(function() {
 		fuploadphoto(this);
 	});

 	/* init input type files ( upload ) */
 	bsCustomFileInput.init();


 	/* clone form career */

 	var addressform = $('.clone-form-address').html();   

 	$(".add-form-address").click(function(e) {
 		e.preventDefault();
 		var i = $().length;   
 		var elmform = addressform.replace(/\[[0\]]\]/g, '['+i+++']'); 
 		$('.clone-form-address').append(elmform);  
 	});



 	/* FILTER SELECT VALUE WITH SHOW CONTENT */
 	$('#sort-mydata').change(function(){
 		$(this).parents().find('.show-mytitle').addClass('d-none');
 		$('#' + $(this).val()).removeClass('d-none');
 	});

 	$('#sort-mydata').change(function(){
 		$(this).parents().find('.show-mycontent').addClass('d-none');
 		$('.' + $(this).val()).removeClass('d-none');
 	});


 	/* only numeric */
 	$(".only-numeric").bind("keypress", function (e) {
 		var keyCode = e.which ? e.which : e.keyCode
 		if (!(keyCode >= 48 && keyCode <= 57)) {
 			$();
 			return false;
 		}else{
 			$();
 		}
 	});


 	/* CART QUANTYTY INPUT */
 	var quantitiy=0;
 	$('.sn-quantity-plus').click(function(e){
 		e.preventDefault();
 		$(this).siblings('input').val(parseInt($(this).siblings('input').val()) + 1);
 	});

 	$('.sn-quantity-minus').click(function(e){
 		e.preventDefault();
 		if(parseInt($(this).siblings('input').val())>0){
 			$(this).siblings('input').val(parseInt($(this).siblings('input').val()) - 1);
 		}
 	});

 	/* CART - CLEAR VALUE VOUCHER */
 	$('.voucher-code-value button').on('click', function() {
 		$('#coupon').val('');
 	});

 	/* CART - DELETE ITEM PRODUCT */
 	$('.delete-item').on('click', function() {
 		event.preventDefault();
 		$(this).parents('li').remove();

 		if($('.cart-tbody li').children().length == 0){
 			$('.cart-item-empty-box').addClass('show');
 		}
 	});
 	if($('.cart-tbody li').children().length == 0){
 		$('.cart-item-empty-box').addClass('show');
 	}


 	/* FILTER SIDEBAR PRODUCT */
 	$('.button-fine-filter').on('click', function(e) {
 		e.preventDefault();
 		$(this).parents().find('.filter-sidebar-box').toggleClass('open');
 		$(this).parents().find('body').addClass('disablescrollbar');
 	});

 	$('.filter-sidebar-box .close').on('click', function(e) {
 		e.preventDefault();
 		$(this).parents().find('.filter-sidebar-box').removeClass('open');
 		$(this).parents().find('body').removeClass('disablescrollbar');
 	});



 	/* CART GROUP */
 	$('.button-addtocart').on('click', function(e) {
 		e.preventDefault();
 		$(this).parents().find('.cart-group-box').toggleClass('open');
 	});

 	$('.btn-close').on('click', function(e) {
 		e.preventDefault();
 		$(this).parents().find('.cart-group-box').removeClass('open');
 	});

 	/* CART - DELETE ITEM PRODUCT */
 	$('.delete-item').on('click', function(e) {
 		e.preventDefault();
 		$(this).parents('.cart-group-body-item').remove();

 		if($('.cart-group-body-item').children().length == 0){
 			$('.cart-group-empty-box').addClass('show');
 		}
 	});
 	if($('.cart-group-body-item').children().length == 0){
 		$('.cart-group-empty-box').addClass('show');
 	}


 });