
<!-- begin fonts -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet"> 
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
<!-- end fonts -->

<!-- begin basic framework engine css -->
<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/css/bootstrap.min.css"/>
<!-- end basic framework engine css -->

<!-- begin jquery -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.theme.min.css">
<!-- end jquery -->

<!-- begin ADD ON / PLUGINS CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/plugins/animate/animate.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/plugins/select2/dist/css/select2.min.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.standalone.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker3.standalone.min.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/plugins/slick-carousel/slick/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/plugins/slick-carousel/slick/slick.css"/>

<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/plugins/fancybox/dist/jquery.fancybox.css"/>

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.css" rel="stylesheet" />

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

<!-- begin plugin datatables -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap4.min.css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"/>
<!-- end plugin datatables -->

<!-- end ADD ON / PLUGINS CSS -->





<!-- begin basic css in local -->
<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/css/styles.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/css/styles.min.css"/>
<!-- end basic css in local -->

<link rel="stylesheet" type="text/css" href="<?php echo theme_front_locations(); ?>/css/extra.css"/>
<!-- END BASIC CSS ENGINE -->

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css">
















