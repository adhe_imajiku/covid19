<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

<title><?php echo $template['title']; ?></title>

<meta name="title" content="<?php echo (!empty($meta_title)) ? $meta_title : ''; ?>" />
<meta name="author" content="<?php variable_set($CONFIG['variable']['web_title']['value'], true, true) ?>" />
<meta name="description" content="<?php echo (!empty($meta_description)) ? $meta_description : ''; ?>" />
<meta name="keywords" content="<?php echo (!empty($meta_keywords)) ? $meta_keywords : ''; ?>" />

<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo (!empty($meta_title)) ? $meta_title : ''; ?>" />
<meta property="og:url" content="<?php echo (!empty($meta_url)) ? $meta_url : ''; ?>" />
<meta property="og:image" content="<?php echo (!empty($meta_image)) ? $meta_image : ''; ?>" />
<meta property="og:image:secure_url" content="<?php echo (!empty($meta_image)) ? $meta_image : ''; ?>" />
<meta property="og:image:alt" content="<?php echo (!empty($meta_image_alt)) ? $meta_image_alt : ''; ?>" />
<meta property="og:description" content="<?php echo (!empty($meta_description)) ? $meta_description : ''; ?>" />
<meta property="og:site_name" content="<?php variable_set($CONFIG['variable']['web_title']['value'], true, true) ?>" />
<meta name="twitter:card" content="summary" />

    <!-- ****** START FAVICONS ****** -->
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo theme_global_locations() ?>favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo theme_global_locations() ?>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo theme_global_locations() ?>favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo theme_global_locations() ?>favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo theme_global_locations() ?>favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <!-- ****** END FAVICONS ****** -->