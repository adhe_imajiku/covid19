
<!-- begin jquery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- end jquery -->

<!-- begin basic framework engine js -->
<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/js/bootstrap.min.js"></script>
<!-- end basic framework engine js -->

<!-- begin jquery extra -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!-- end jquery extra -->


<!-- START ADD ON / PLUGINS JS -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/plugins/select2/dist/js/select2.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/plugins/slick-carousel/slick/slick.min.js"></script>

<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/plugins/fancybox/dist/jquery.fancybox.js"></script>

<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/plugins/masonry-layout/dist/masonry.pkgd.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.0/fullcalendar.min.js"></script>

<script type="text/javascript" src="https://unpkg.com/tooltip.js/dist/umd/tooltip.min.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>

<!-- begin plugin upload files -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<!-- end plugin upload files -->

<!-- begin plugin copy text -->
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-alpha.12/dist/html2canvas.min.js"></script>
<!-- end plugin copy text -->

<!-- begin plugin datatables -->
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
<!-- end plugin datatables -->

<!-- END ADD ON / PLUGINS JS -->

<!-- begin basic js in local -->
<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/js/main.js"></script>
<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/js/main.min.js"></script>
<script type="text/javascript" src="<?php echo theme_front_locations(); ?>/js/extra.js"></script>
<!-- END BASIC JAVASCRIPT ENGINE -->
<!-- Begin Mailchimp Signup Form -->

<script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.js'></script>
<script type="application/javascript">
    <?php
    if(!empty($this->session->flashdata('status'))){
    ?>      
    Swal.fire(
      '<?= $this->session->flashdata('header') ?>',
      '<?= $this->session->flashdata('message') ?>',
      '<?= $this->session->flashdata('status') ?>'
    )
    <?php
    }
    ?>
</script>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
<script type='text/javascript'>
    (function($) {
        window.fnames = new Array();
        window.ftypes = new Array();
        fnames[0] = 'EMAIL';
        ftypes[0] = 'email';
        fnames[1] = 'FNAME';
        ftypes[1] = 'text';
        fnames[2] = 'LNAME';
        ftypes[2] = 'text';
        fnames[3] = 'ADDRESS';
        ftypes[3] = 'address';
        fnames[4] = 'PHONE';
        ftypes[4] = 'phone';
        fnames[5] = 'BIRTHDAY';
        ftypes[5] = 'birthday';
    }(jQuery));
    var $mcj = jQuery.noConflict(true);

</script>


