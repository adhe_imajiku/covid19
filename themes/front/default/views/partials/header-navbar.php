
<!---------------------Batas------------------------------>

<noscript>
    <div class="alert alert-danger">
        <span><strong>For full functionality of this site it is necessary to enable JavaScript. </strong> Here are the <a href="http://www.enable-javascript.com/" class="alert-link" target="_blank"> instructions how to enable JavaScript in your web browser</a>.</span>
    </div>
</noscript>

<!-- function class active on mainmenu -->
<header id="headertop">
    <div id="search-body" class="search-body">
        <form role="search" action="<?= url_base('search') ?>" method="post">
            <input name="search" type="search" placeholder="type to search" />
        </form>
        <a href="javascript:void(0);" class="close"><i class="fas fa-times"></i></a>
    </div>
    <!-- begin.navbar header -->
    <nav class="navbar navbar-expand-lg navbar-light fixed-top flex-row shadow">
<!--
        <div class="navbar-mjk-top hide-for-desktop">
            <ul class="navbar-nav">
                <li>
                    <h4><strong>FREE SHIPPING</strong> - on all orders over IDR250.000</h4>
                </li>
            </ul>
        </div>
-->

        <a class="navbar-brand hide-for-desktop" href="<?= url_base('home') ?>">
            <img src="<?= theme_global_locations() ?>/logo.png" alt="" title="SADA" />
        </a>
        <button class="navbar-toggler finebody" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="icon-bar top-bar"></span>
            <span class="icon-bar middle-bar"></span>
            <span class="icon-bar bottom-bar"></span>
        </button>

        <div class="nav-item badges-bell-box hide-for-desktop">
            <a class="nav-link button-addtocart" href="javascript:void(0);" title="add to cart">
                <div class="badges-bell">
                    <i class="fas fa-shopping-bag"></i>
                    <div class="badges-balloon">0</div>
                </div>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-mjk-top hide-for-mobile">
                <ul class="navbar-nav">
                    <li>
                        <h4><?php echo str_replace('</p>','',str_replace('<p>','',$CONFIG['variable']['text_promo']['value'])); ?></h4>
                    </li>
                </ul>
            </div>
            <div class="navbar-mjk-head">
                <div class="container">
                    <div class="navbar-nav-box">

                        <ul class="navbar-nav">
                            <li class="nav-item">
                                
                                <select class="selectpicker select-language nav-link" data-width="fit" onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
                                    <?php
                                    foreach($lang_set as $index => $row){
                                        $selected='';
                                        if($row['lang']==$lang_active['lang']){
                                            $selected='selected';
                                        }
                                        if($row['lang']=='ID'){
                                    ?>
                                    <option <?= $selected ?>  value="<?= site_url('id/change-language') ?>">ID</option>
                                    <?php    
                                        }else{
                                    ?>
                                    <option <?= $selected ?>  value="<?= site_url('en/change-language') ?>">EN</option>
                                    <?php    
                                        }
                                    }
                                    ?>
                                </select>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link button-search" href="javascript:void(0);" title="search"><span>SEARCH</span> <i class="fas fa-search ml-1"></i></a>
                            </li>
                        </ul>

                        <ul class="navbar-nav hide-for-mobile">
                            <li class="nav-item navbar-brand-box">
                                <a class="navbar-brand" href="<?= url_base('home') ?>">
                                    <img src="<?= theme_global_locations() ?>/logo.png" alt="" title="" />
                                </a>
                            </li>
                        </ul>

                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link reorder" href="<?= url_base('cart') ?>" title="reorder"><i class="fas fa-redo-alt fa-flip-horizontal mr-1"></i> <span>REORDER</span></a>
                            </li>

                            <!-- begin. if user registered -->
                            <li class="nav-item dropdown nav-myaccount">
                                <?php
                                if($this->ion_auth->logged_in()){
                                ?>
                                <a class="nav-link dropdown-toggle hide-for-desktop" href="#" title="my account" id="menuddmember" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-user-circle"></i>
                                </a>
                                <?php
                                }else{
                                ?>
                                <a class="nav-link hide-for-desktop" href="<?= url_base('login') ?>">
                                    <i class="fas fa-key"></i>
                                </a>
                                <?php
                                }
                                ?>
                                
                    
                                <?php
                                if($this->ion_auth->logged_in()){
                                ?>
                                <a class="nav-link dropdown-toggle hide-for-mobile" href="#" id="menuddmember" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hi, <?= $this->ion_auth->user()->row()->first_name ?></a>
                                <ul class="dropdown-menu menuddmember-dropdown shadow" aria-labelledby="menuddmember">
                                    <li><a class="dropdown-item" href="<?= url_base('profile') ?>">My Profile</a></li>
                                    <li><a class="dropdown-item" href="<?= url_base('logout') ?>">Logout</a></li>
                                </ul>
                                <?php
                                }else{
                                ?>
                                <a class="nav-link hide-for-mobile" href="<?= url_base('login') ?>" >Login</a>
                                <?php
                                }
                                ?>
                            </li>
                            <!-- end. if user registered -->

                            <li class="nav-item badges-bell-box hide-for-mobile">
                                <a class="nav-link button-addtocart" href="#" title="add to cart">
                                    <div class="badges-bell">
                                        <i class="fas fa-shopping-bag"></i>
                                        <div class="badges-balloon">0</div>
                                    </div>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
            <div class="navbar-mjk-mainmenu">
                <div class="container">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown megamenu">
<!--
                            <a class="nav-link" href="#">BEST SELLER</a>
                            <a href="javascript:void(0);" class="caret-mjk-box dropdown-toggle" id="menudd1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="caret-mjk-desktop">
                                    <i class="fas fa-caret-down"></i>
                                </span>
                                <span class="caret-mjk-mobile">
                                    <i class="fas fa-plus"></i>
                                    <i class="fas fa-minus"></i>
                                </span>
                            </a>
                            <div class="dropdown-menu megamenu-dropdown" aria-labelledby="menudd1">
                                <div class="fluid-container">
                                    <div class="megamenu-body shadow">
                                        <div class="row">
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                                <div class="megamenu-content-list">
                                                    <h3>CATERGORY PRODUCT 1</h3>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 1</a></li>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 2</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                                <div class="megamenu-content-list">
                                                    <h3>CATERGORY PRODUCT 1</h3>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 1</a></li>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 2</a></li>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 3</a></li>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 4</a></li>
                                                        <li><a class="dropdown-item" href="<?= url_base('product') ?>">Product 5</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-12 col-sm-12 col-md-12 col-lg-4 hidden">
                                                <div class="megamenu-content-list hide-for-mobile">
                                                    <div class="megamenu-content-img">
                                                        <img src="./themes/images/dummy/img-article-3.jpg" alt="" title="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
--> 
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= url_base('world-of-sada') ?>">THE WORLD OF SADA</a>

                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <!-- end.navbar header -->
</header>
