<div class="cart-group-box">
    <div class="cart-group">
        <div class="cart-group-head">
            <div class="title-primary">
                <h1>Cart</h1>
            </div>
            <div class="btn-close">
                <a href="" class="btn btn-close"><i class="fas fa-chevron-circle-right"></i></a>
            </div>
        </div>
        <div class="cart-group-body">
            <!--
				<div class="cart-group-body-item">
					<div class="cart-item-img">
						<img src="./themes/images/dummy/img-product-1.jpg" class="img-fluid" alt="" />
					</div>
					<div class="cart-item-desc">
						<div class="cart-item-desc-product">
							<a href="#" title="" target="_blank"><h3>
								Ghanda Powder Bronze and
								Highlight
							</h3></a>
							<p>01 Sogan</p>
						</div>
						<div class="cart-item-desc-price">
							<h4>Rp 119.000</h4>
						</div>
						<div class="cart-item-desc-action">
							<div class="spinner-number-item">
								<button type="button" class="sn-quantity-minus"  data-type="minus" data-field="">
									<i class="fas fa-minus"></i>
								</button>
								<input type="text" id="" name="qty[]" value="0" min="1" max="999" data-initval="1">
								<button type="button" class="sn-quantity-plus" data-type="plus" data-field="">
									<i class="fas fa-plus"></i>
								</button>
							</div>
							<div class="cart-item-delete">
								<a class="delete-item" href="#"><i class="far fa-trash-alt"></i></a>
							</div>
						</div>
					</div>
				</div>	
-->
            <div class="cart-group-empty-box">
                <div class="cart-item-empty">
                    <i class="fas fa-shopping-basket"></i>
                    <p>Your Cart is Empty</p>
                </div>
            </div>
        </div>
        <div class="cart-group-bottom">
            <div class="cart-group-bottom-total">
                <h3>
                    Total
                </h3>
                <h3>
                    Rp 0
                </h3>
            </div>
            <div class="cart-group-bottom-btn">
                <a href="<?= url_base('cart') ?>" title="" class="btn btn-mjk bg-black w-100">checkout</a>
            </div>
            <div class="cart-group-bottom-btn">
                <a href="<?= url_base('product') ?>" title="" class="btn btn-mjk bg-brown w-100">continue shopping</a>
            </div>
        </div>
    </div>
    <div class="cart-group-overlay"></div>
</div>
<footer>
    <div class="footer">
        <div class="footer-middle">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-12 col-lg-3 mb-5">

                        <div class="foo-logo">
                            <figure>
                                <img src="<?= theme_global_locations() ?>/logo-sada.png" alt="" title="SADA" />
                            </figure>
                        </div>

                        <div class="social-network">
                            <ul>
                                <li><a href="<?php variable_set($CONFIG['variable']['socmed_facebook']['value'], TRUE, TRUE); ?>" title="" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="<?php variable_set($CONFIG['variable']['socmed_twitter']['value'], TRUE, TRUE); ?>" title="" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                <li><a href="<?php variable_set($CONFIG['variable']['socmed_instagram']['value'], TRUE, TRUE); ?>" title="" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="<?php variable_set($CONFIG['variable']['socmed_youtube']['value'], TRUE, TRUE); ?>" title="" target="_blank"><i class="fab fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-3 mb-sm-5">
                        <div class="foo-menu">
                            <?php variable_set($CONFIG['variable']['first_footer_section']['value'], false, false); ?>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-3 mb-3 mb-sm-5">
                        <div class="foo-menu">
                            <?php variable_set($CONFIG['variable']['second_footer_section']['value'], false, false); ?>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-3">
                        <div class="foo-address mb-5">
                            <h3>CUSTOMER CARE</h3>
                            <ul>
                                <li><i class="fas fa-phone mr-2"></i> <a href="tel:<?php variable_set($CONFIG['variable']['contact_costumer_phone']['value'], TRUE, TRUE); ?>" title=""><?php variable_set($CONFIG['variable']['contact_costumer_phone']['value'], TRUE, TRUE); ?></a></li>
                                <li><i class="fas fa-envelope mr-2"></i> <a href="mailto:<?php variable_set($CONFIG['variable']['contact_costumer_mail']['value'], TRUE, TRUE); ?>" title=""><?php variable_set($CONFIG['variable']['contact_costumer_mail']['value'], TRUE, TRUE); ?></a></li>
                            </ul>
                        </div>

                        <div class="foo-subscribe">
                            <?php variable_set($CONFIG['variable']['text_subscribe']['value'], false, false); ?>
                            <form action="https://gmail.us5.list-manage.com/subscribe/post?u=ff3e05a96c5a3e45a9777fedb&amp;id=b81cecebce" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

                                <div class="input-group input-group-mjk subscribe w-100">
                                    <input type="email" value="" name="EMAIL" class="required email form-control pl-0" id="mce-EMAIL" placeholder="Email Address">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-secondary" type="submit" id="SCE1"><i class="fas fa-chevron-right"></i></button>
                                    </div>
                                </div>
                                <div id="mce-responses" class="clear">
                                    <div class="response" id="mce-error-response" style="display:none"></div>
                                    <div class="response" id="mce-success-response" style="display:none"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="copyright text-center">
                <p>&copy; <?php echo date("Y"); ?> SADA. All right reserved.</p>
            </div>
        </div>

    </div>
</footer>
