

<!DOCTYPE html>
<html lang="en-US">
<head>
    <?php include 'partials/head-meta.php'; ?>
    <?php include 'partials/head-register-css.php'; ?>
</head>
<body>
<div id="wrapper">
    <?php
    function url_base($link = null , $mode = 'base_url'){
        $active_lang = $_SESSION["lang_setting"];
        if($mode=='base_url'){
            $link=base_url().$active_lang.'/'.$link;
        }else{
            $link=base_url().$active_lang.'/'.$link.'.html';
        }
        return $link;  
    }
    ?>
    <?php include 'partials/header-navbar.php'; ?>
    <div class="layout-page">
        <?php echo (!empty($template['partials']['content'])) ? $template['partials']['content'] : ''; ?>
    </div>
    <?php include 'partials/footer.php'; ?>
</div>
<?php include 'partials/footer-register-js.php'; ?>
    <?php echo (!empty($template['partials']['footer'])) ? $template['partials']['footer'] : ''; ?>
</body>
</html>