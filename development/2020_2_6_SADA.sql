/*
SQLyog Professional v12.5.1 (32 bit)
MySQL - 5.6.26 : Database - admin_sada
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`admin_sada` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `admin_sada`;

/*Table structure for table `acs_address` */

DROP TABLE IF EXISTS `acs_address`;

CREATE TABLE `acs_address` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `long_format` text,
  `zip` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_address` */

/*Table structure for table `acs_config` */

DROP TABLE IF EXISTS `acs_config`;

CREATE TABLE `acs_config` (
  `config_id` varchar(25) NOT NULL,
  `config_group` varchar(100) NOT NULL,
  `config_name` varchar(200) NOT NULL,
  `config_value` mediumtext,
  `config_value_id` mediumtext NOT NULL,
  `config_desc` mediumtext,
  `file_location` mediumtext,
  `file_thumb` mediumtext,
  `file_original` mediumtext,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acs_config` */

insert  into `acs_config`(`config_id`,`config_group`,`config_name`,`config_value`,`config_value_id`,`config_desc`,`file_location`,`file_thumb`,`file_original`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1909275D8D8759E56D5','web_config','web_title','<p>SADA</p>\r\n','<p>SADA</p>\r\n','',NULL,NULL,NULL,1,'2019-09-27 10:51:53',6,'2020-02-04 15:31:30'),
('1909275D8D8AEEB2A60','web_config','socmed_facebook','<p>https://facebook.com/</p>\r\n','','',NULL,NULL,NULL,1,'2019-09-27 11:07:10',6,'2019-12-04 10:26:37'),
('1909275D8D8B0AC221C','web_config','socmed_instagram','<p>https://instagram.com/</p>\r\n','<p>https://instagram.com/</p>\r\n',NULL,NULL,NULL,NULL,1,'2019-09-27 11:07:38',1,'2019-10-16 09:29:12'),
('1909275D8D8B3D59E7A','web_config','socmed_youtube','<p>https://youtube.com/</p>\r\n','',NULL,NULL,NULL,NULL,1,'2019-09-27 11:08:29',1,'2019-10-16 09:33:00'),
('1910225DAE769A6D6DB','web_config','admin_mail','<p>admin@sada.com</p>\r\n','<p>admin@sada.com</p>\r\n','admin@sada.com',NULL,NULL,NULL,6,'2019-10-22 10:25:14',6,'2020-02-04 15:31:51'),
('1910225DAE772DD93F0','web_config','mail_cc','<p>testing.imajiku2@gmail.com</p>\r\n','','',NULL,NULL,NULL,6,'2019-10-22 10:27:41',6,'2020-01-22 15:33:05'),
('1911215DD64054EB734','web_config','socmed_twitter','<p>https://twitter.com</p>\r\n','','',NULL,NULL,NULL,6,'2019-11-21 14:44:20',6,'2019-11-21 14:44:20'),
('1912195DFB2247A01A1','web_config','text_socmed','<h3>Follow us! We&#39;re friendly</h3>\r\n','','',NULL,NULL,NULL,6,'2019-12-19 14:09:59',6,'2019-12-19 14:09:59'),
('1912195DFB22CBF22E2','web_config','text_subscribe','<h3>Subscribe to Our Newsletter</h3>\r\n','','',NULL,NULL,NULL,6,'2019-12-19 14:12:11',6,'2019-12-19 14:12:11'),
('2002045E392C339612C','site_config','background_login','<p>linear-gradient(to right, #b39264 11%, #000000 134%);</p>\r\n','<p>linear-gradient(to right, #b39264 11%, #000000 99%);</p>\r\n','',NULL,NULL,NULL,6,'2020-02-04 15:32:51',6,'2020-02-05 15:30:04'),
('2002045E3930F842E6C','site_config','btn_link_hover_login','<p>#D4A24C</p>\r\n','<p>#D4A24C</p>\r\n','',NULL,NULL,NULL,6,'2020-02-04 15:53:12',6,'2020-02-05 15:29:56'),
('2002055E3A7C30E10AF','site_config','btn_link_hovered','<p>#50422D</p>\r\n','<p>#50422D</p>\r\n','',NULL,NULL,NULL,6,'2020-02-05 15:26:24',6,'2020-02-05 15:29:46');

/*Table structure for table `acs_groups` */

DROP TABLE IF EXISTS `acs_groups`;

CREATE TABLE `acs_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `acs_groups` */

insert  into `acs_groups`(`id`,`name`,`description`) values 
(1,'admin','Administrator'),
(2,'member','Member');

/*Table structure for table `acs_lang` */

DROP TABLE IF EXISTS `acs_lang`;

CREATE TABLE `acs_lang` (
  `id` varchar(25) CHARACTER SET latin1 NOT NULL,
  `lang` varchar(5) CHARACTER SET latin1 NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `slug` varchar(100) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_default` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acs_lang` */

insert  into `acs_lang`(`id`,`lang`,`title`,`slug`,`status`,`status_default`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('LGEN','EN','ENGLISH','english',1,1,1,'2018-02-19 00:00:00',6,'2019-10-22 12:07:43'),
('LGID','ID','INDONESIA','indonesia',1,0,1,'2018-02-19 04:50:33',1,'2018-07-11 14:23:05');

/*Table structure for table `acs_login_attempts` */

DROP TABLE IF EXISTS `acs_login_attempts`;

CREATE TABLE `acs_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acs_login_attempts` */

/*Table structure for table `acs_portal` */

DROP TABLE IF EXISTS `acs_portal`;

CREATE TABLE `acs_portal` (
  `id` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal` */

insert  into `acs_portal`(`id`,`title`,`description`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1910075D9AE8C77B958','admin','Admin',1,1,'2019-10-07 14:27:03',6,'2020-01-29 10:38:56');

/*Table structure for table `acs_portal_menu` */

DROP TABLE IF EXISTS `acs_portal_menu`;

CREATE TABLE `acs_portal_menu` (
  `id` varchar(25) NOT NULL,
  `portal_id` varchar(25) NOT NULL,
  `parent` varchar(25) NOT NULL DEFAULT '0',
  `link` text NOT NULL,
  `description` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal_menu` */

/*Table structure for table `acs_portal_previleges` */

DROP TABLE IF EXISTS `acs_portal_previleges`;

CREATE TABLE `acs_portal_previleges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `list` tinyint(1) NOT NULL DEFAULT '0',
  `add` tinyint(1) DEFAULT '0',
  `edit` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=299 DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal_previleges` */

insert  into `acs_portal_previleges`(`id`,`group_id`,`menu_id`,`list`,`add`,`edit`,`delete`) values 
(252,1,'1912185DF98252958B0',1,1,1,1),
(253,1,'1912185DF9832DD1E4F',1,1,1,1),
(254,1,'1912195DFAF689B8322',1,1,1,1),
(255,1,'1912185DF98256171B4',1,1,1,1),
(256,1,'1912185DF98347DBA6D',1,1,1,1),
(257,1,'1912185DF9835B603E4',1,1,1,1),
(258,1,'1912185DF98259D1A67',1,1,1,1),
(259,1,'1912185DF9838C62DC5',1,1,1,1),
(260,1,'1912185DF982D2ECBD6',1,1,1,1),
(261,1,'1912185DF9844950D3A',1,1,1,1),
(262,1,'2001205E24993BBD939',1,1,1,1),
(263,1,'2001225E27A75AECB24',1,1,1,1),
(264,1,'1912185DF982E6C0E2C',1,1,1,1),
(265,1,'1912185DF987141323F',1,1,1,1),
(266,1,'1912185DF987262FE31',1,1,1,1),
(267,1,'2001025E0D71F255709',1,1,1,1),
(268,1,'1912185DF982AF5AE59',1,1,1,1),
(269,1,'2001085E152FE2811FB',1,1,1,1),
(270,1,'2001085E15302B7D589',1,1,1,1),
(271,1,'2001085E15306149976',1,1,1,1),
(272,1,'2002055E3A753424D40',1,1,1,1),
(273,1,'2002055E3A77B090B5C',1,1,1,1),
(274,1,'2002055E3A78314CC64',1,1,1,1),
(275,1,'2002055E3A784C9C8FB',1,1,1,1),
(276,1,'2002055E3A7868AA62B',1,1,1,1),
(277,1,'2002055E3A789D4A458',1,1,1,1),
(278,1,'2002055E3A709CCB117',1,1,1,1),
(279,1,'1912185DF982DE2D5F3',1,1,1,1),
(280,1,'2001225E27FA3BD6214',1,1,1,1),
(281,1,'2002055E3A726DC02DC',1,1,1,1),
(282,1,'1912185DF982BF2E2B8',1,1,1,1),
(283,1,'1912185DF984FA7F533',1,1,1,1),
(284,1,'1912185DF98690C8FA5',1,1,1,1),
(285,1,'1912185DF986A6700B4',1,1,1,1),
(286,1,'1912185DF987023CC3A',1,1,1,1),
(287,1,'2002055E3A6A479F4EA',1,1,1,1),
(288,1,'1912185DF982C950CA0',1,1,1,1),
(289,1,'1912185DF983BDF2925',1,1,1,1),
(290,1,'1912185DF983D281175',1,1,1,1),
(291,1,'2002055E3A72932EA01',1,1,1,1),
(292,1,'2002055E3A72C02F41C',1,1,1,1),
(293,1,'2002055E3A73E42E463',1,1,1,1),
(294,1,'2002055E3A73F9F2E45',1,1,1,1),
(295,1,'2002055E3A740D61A4F',1,1,1,1),
(296,1,'2002055E3A7491496C8',1,1,1,1),
(297,1,'2002055E3A74AF67E22',1,1,1,1),
(298,1,'2002055E3A74C33A375',1,1,1,1);

/*Table structure for table `acs_users` */

DROP TABLE IF EXISTS `acs_users`;

CREATE TABLE `acs_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `oauth_provider` enum('facebook','google','twitter','') NOT NULL,
  `oauth_uid` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `profile` text,
  `gender` varchar(100) DEFAULT NULL,
  `extra_payment` tinyint(1) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `file_original` varchar(255) DEFAULT NULL,
  `file_thumb` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `address` text,
  `bank_number` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `acs_users` */

insert  into `acs_users`(`id`,`oauth_provider`,`oauth_uid`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`,`province`,`city`,`subdistrict`,`dob`,`zip`,`profile`,`gender`,`extra_payment`,`locale`,`file_original`,`file_thumb`,`file_name`,`picture`,`link`,`address`,`bank_number`,`bank_name`) values 
(6,'facebook',NULL,'182.1.79.6','user_5da904f1b954c','$2y$08$XIP.2oESbcSXYaV7TickXeVnTmWYMwStRa6oGm0Nw1x5nCjppyqsi',NULL,'admin@admin.com',NULL,NULL,NULL,NULL,1571357937,1580971242,1,'admin','admin',NULL,'0897379434',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `acs_users_groups` */

DROP TABLE IF EXISTS `acs_users_groups`;

CREATE TABLE `acs_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `acs_users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `acs_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acs_users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `acs_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `acs_users_groups` */

insert  into `acs_users_groups`(`id`,`user_id`,`group_id`) values 
(5,6,1);

/*Table structure for table `book_newsletter` */

DROP TABLE IF EXISTS `book_newsletter`;

CREATE TABLE `book_newsletter` (
  `id` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `client_browser` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctd` datetime NOT NULL,
  `mdd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `book_newsletter` */

/*Table structure for table `career_applicants` */

DROP TABLE IF EXISTS `career_applicants`;

CREATE TABLE `career_applicants` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `curriculum_vitae` varchar(100) NOT NULL,
  `cv_link` varchar(100) DEFAULT NULL,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_career` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `career_applicants` */

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` varchar(25) NOT NULL,
  `subject_id` varchar(25) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `region` varchar(25) DEFAULT NULL,
  `message` text,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `contact` */

/*Table structure for table `contact_newsletter` */

DROP TABLE IF EXISTS `contact_newsletter`;

CREATE TABLE `contact_newsletter` (
  `id` varchar(25) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contact_newsletter` */

/*Table structure for table `contact_subject` */

DROP TABLE IF EXISTS `contact_subject`;

CREATE TABLE `contact_subject` (
  `id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `contact_subject` */

insert  into `contact_subject`(`id`,`title`,`slug`,`email`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1912195DFAF85EE9147','Product','product','adherifai6@gmail.com',1,'6','2019-12-19 11:11:10','6','2020-01-07 15:26:27');

/*Table structure for table `content_kt` */

DROP TABLE IF EXISTS `content_kt`;

CREATE TABLE `content_kt` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `content_name` varchar(255) NOT NULL,
  `content_type` enum('input','textarea','editor','tagging') NOT NULL,
  `content_format` text,
  `published` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt` */

/*Table structure for table `content_kt_image` */

DROP TABLE IF EXISTS `content_kt_image`;

CREATE TABLE `content_kt_image` (
  `id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `file_name` mediumtext,
  `file_location` mediumtext,
  `file_thumb` mediumtext,
  `file_original` mediumtext,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tags_kt_id` (`content_kt_id`) USING BTREE,
  CONSTRAINT `content_kt_image_ibfk_1` FOREIGN KEY (`content_kt_id`) REFERENCES `content_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt_image` */

/*Table structure for table `content_kt_lang` */

DROP TABLE IF EXISTS `content_kt_lang`;

CREATE TABLE `content_kt_lang` (
  `id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acs_lang_id` (`acs_lang_id`) USING BTREE,
  KEY `fk_tags_kt_id` (`content_kt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt_lang` */

/*Table structure for table `content_kt_menu` */

DROP TABLE IF EXISTS `content_kt_menu`;

CREATE TABLE `content_kt_menu` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_kt_Id` (`content_kt_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel ini digunakan untuk setting konten berdasarkan menu';

/*Data for the table `content_kt_menu` */

/*Table structure for table `coupon_category` */

DROP TABLE IF EXISTS `coupon_category`;

CREATE TABLE `coupon_category` (
  `id` varchar(25) NOT NULL,
  `coupon_name` tinytext,
  `coupon_postal_free` tinyint(1) DEFAULT '0',
  `coupon_unit` enum('percentage','nominal') DEFAULT NULL,
  `coupon_nominal` bigint(50) DEFAULT '0',
  `coupon_minimum_buy` bigint(50) DEFAULT '0',
  `coupon_maximum_nominal` bigint(50) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  `ctd` date DEFAULT NULL,
  `ctb` varchar(25) DEFAULT NULL,
  `mdd` date DEFAULT NULL,
  `mdb` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `coupon_category` */

/*Table structure for table `coupon_code` */

DROP TABLE IF EXISTS `coupon_code`;

CREATE TABLE `coupon_code` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `coupon_category_id` varchar(25) NOT NULL,
  `coupon_code` varchar(25) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ctd` date DEFAULT NULL,
  `ctb` varchar(25) DEFAULT NULL,
  `mdd` date DEFAULT NULL,
  `mdb` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `coupon_code` */

/*Table structure for table `courier` */

DROP TABLE IF EXISTS `courier`;

CREATE TABLE `courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `ctb` tinyint(1) DEFAULT '1',
  `ctd` datetime NOT NULL,
  `mdb` tinyint(1) DEFAULT '1',
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `courier` */

insert  into `courier`(`id`,`code`,`name`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
(1,'jne','Jalur Nugraha Ekakurir (JNE)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(2,'pos','POS Indonesia (POS)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(3,'tiki','Citra Van Titipan Kilat (TIKI)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(4,'rpx','RPX Holding (RPX)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(5,'esl','Eka Sari Lorena (ESL)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(6,'pcp','Priority Cargo and Package (PCP)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(7,'pandu','Pandu Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(8,'wahana','Wahana Prestasi Logistik',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(9,'sicepat','SiCepat Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(11,'pahala','Pahala Kencana Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(12,'cahaya','Cahaya Ekspress Logistik',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(13,'sap','SAP Express Courier',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(14,'jet','JET Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(15,'indah','Indah Logistic',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(16,'dse','21 Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(17,'slis','Synergy First Logistics',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(18,'ncs','Nusantara Card Semesta',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(19,'star','Star Cargo',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(20,'ninja','Ninja Xpress',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(21,'lion','Lion Parcel',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(22,'idl','Indotama Domestik Lestari',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(23,'rex','Royal Express Indonesia',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(24,'jnt','J&T Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00');

/*Table structure for table `inquiry_form` */

DROP TABLE IF EXISTS `inquiry_form`;

CREATE TABLE `inquiry_form` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` text NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inquiry_form` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `status_mode` enum('post','product','tags','other') NOT NULL,
  `status_single` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` int(1) DEFAULT '0',
  `filter_author` tinyint(1) DEFAULT '0',
  `filter_ordering` tinyint(1) NOT NULL DEFAULT '1',
  `filter_parent` tinyint(1) NOT NULL DEFAULT '1',
  `filter_title_sub` tinyint(1) DEFAULT '0',
  `filter_shortdesc` tinyint(1) NOT NULL DEFAULT '1',
  `filter_note` tinyint(1) DEFAULT '0',
  `filter_description` tinyint(1) NOT NULL DEFAULT '1',
  `filter_dashboard` int(11) NOT NULL DEFAULT '0',
  `filter_image` tinyint(1) NOT NULL DEFAULT '1',
  `filter_tags` tinyint(1) NOT NULL DEFAULT '1',
  `filter_featured` tinyint(1) DEFAULT '0',
  `filter_promo` tinyint(1) DEFAULT '0',
  `filter_icon` tinyint(1) DEFAULT '0',
  `filter_event_start` tinyint(1) DEFAULT '0',
  `filter_event_end` tinyint(1) DEFAULT '0',
  `filter_product_tags` tinyint(1) DEFAULT '0',
  `required_title` tinyint(1) DEFAULT '1',
  `required_title_sub` tinyint(1) DEFAULT '0',
  `required_shortdesc` tinyint(1) DEFAULT '0',
  `required_description` tinyint(1) DEFAULT '0',
  `required_note` tinyint(1) DEFAULT '0',
  `filter_brand` tinyint(1) DEFAULT '0',
  `recomended_image_size` varchar(50) DEFAULT NULL,
  `menu_cms_link` varchar(255) DEFAULT NULL,
  `menu_site_link` varchar(255) DEFAULT NULL,
  `published` datetime DEFAULT NULL,
  `text_product_tags` varchar(50) DEFAULT 'Product Tags',
  `text_brand` varchar(50) DEFAULT 'Brand',
  `text_ordering` varchar(50) DEFAULT 'Ordering',
  `text_parent` varchar(50) DEFAULT 'Parent',
  `text_title` varchar(50) DEFAULT 'Title',
  `text_title_sub` varchar(50) DEFAULT 'Subtitle',
  `text_shortdesc` varchar(50) DEFAULT 'Short Description',
  `text_note` varchar(50) DEFAULT 'Note',
  `text_description` varchar(50) DEFAULT 'Description',
  `text_image` varchar(50) DEFAULT 'Image',
  `text_promo` varchar(50) DEFAULT 'Promotion',
  `text_tags` varchar(50) DEFAULT 'Tags',
  `text_featured` varchar(50) DEFAULT 'Featured',
  `text_icon` varchar(50) DEFAULT 'Icon',
  `text_event_start` varchar(50) DEFAULT 'Event Start',
  `text_author` varchar(50) DEFAULT 'Author',
  `text_event_end` varchar(50) DEFAULT 'Event End',
  `disable_add` tinyint(1) DEFAULT '0',
  `disable_delete` tinyint(1) DEFAULT '0',
  `disable_status` tinyint(1) DEFAULT '0',
  `disabled_gallery` tinyint(1) DEFAULT '1',
  `mode_title_sub` enum('text','textarea limit','editor') DEFAULT 'text',
  `mode_shortdesc` enum('text','textarea limit','editor') DEFAULT 'textarea limit',
  `mode_description` enum('text','textarea limit','editor') DEFAULT 'editor',
  `mode_note` enum('text','textarea limit','editor') DEFAULT 'text',
  `mode_tags` enum('single','multiple') DEFAULT NULL,
  `mode_image` enum('single','multiple') DEFAULT 'single',
  `image_qty` int(11) DEFAULT '1',
  `tags_kt_id` varchar(25) DEFAULT NULL,
  `tags_brand_id` varchar(25) DEFAULT NULL,
  `tags_product_id` varchar(25) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `hit` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(25) DEFAULT NULL,
  `ctd` datetime NOT NULL,
  `ctb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id`,`parent`,`ordering`,`status`,`status_mode`,`status_single`,`is_featured`,`filter_author`,`filter_ordering`,`filter_parent`,`filter_title_sub`,`filter_shortdesc`,`filter_note`,`filter_description`,`filter_dashboard`,`filter_image`,`filter_tags`,`filter_featured`,`filter_promo`,`filter_icon`,`filter_event_start`,`filter_event_end`,`filter_product_tags`,`required_title`,`required_title_sub`,`required_shortdesc`,`required_description`,`required_note`,`filter_brand`,`recomended_image_size`,`menu_cms_link`,`menu_site_link`,`published`,`text_product_tags`,`text_brand`,`text_ordering`,`text_parent`,`text_title`,`text_title_sub`,`text_shortdesc`,`text_note`,`text_description`,`text_image`,`text_promo`,`text_tags`,`text_featured`,`text_icon`,`text_event_start`,`text_author`,`text_event_end`,`disable_add`,`disable_delete`,`disable_status`,`disabled_gallery`,`mode_title_sub`,`mode_shortdesc`,`mode_description`,`mode_note`,`mode_tags`,`mode_image`,`image_qty`,`tags_kt_id`,`tags_brand_id`,`tags_product_id`,`link`,`hit`,`icon`,`ctd`,`ctb`,`mdd`,`mdb`) values 
('1912185DF98252958B0','0',1,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:14',6,'2019-12-18 08:39:05',6),
('1912185DF98256171B4','0',2,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:18',6,'2019-12-18 08:49:10',6),
('1912185DF98259D1A67','0',3,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:21',6,'2019-12-18 08:36:40',6),
('1912185DF982AF5AE59','2001025E0D71F255709',4,1,'product',0,1,0,0,0,0,1,0,1,0,1,1,1,0,0,0,0,1,1,0,0,0,0,1,'540 x 555 pixel','mod_posted/index/product','','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Product Name','Subtitle','Description','Note','Specification','Image','Promotion','Category','Recommended','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','multiple',12,'All','All','All',NULL,0,NULL,'2019-12-18 08:36:47',6,'2020-02-05 11:36:12',6),
('1912185DF982BF2E2B8','2002055E3A726DC02DC',5,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:37:03',6,'2020-02-05 14:44:57',6),
('1912185DF982C950CA0','2002055E3A726DC02DC',113,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:37:13',6,'2020-02-05 14:53:16',6),
('1912185DF982D2ECBD6','0',7,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:22',6,'2019-12-18 08:37:22',6),
('1912185DF982DE2D5F3','2002055E3A709CCB117',99,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_config','','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:37:34',6,'2020-02-05 14:42:14',6),
('1912185DF982E6C0E2C','0',9,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:42',6,'2019-12-18 08:37:42',6),
('1912185DF9832DD1E4F','1912185DF98252958B0',11,1,'other',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_contact','contact-us','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:38:53',6,'2020-02-05 11:16:13',6),
('1912185DF98347DBA6D','1912185DF98256171B4',12,1,'other',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_members','','2020-01-21 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:39:19',6,'2020-01-21 09:36:43',6),
('1912185DF9835B603E4','1912185DF98256171B4',13,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_address','','2020-01-21 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:39:39',6,'2020-01-21 10:14:59',6),
('1912185DF9838C62DC5','1912185DF98259D1A67',15,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_orders','','2020-01-22 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:40:28',6,'2020-01-22 08:38:09',6),
('1912185DF983BDF2925','1912185DF982C950CA0',16,1,'post',0,1,0,1,0,1,1,0,1,0,1,0,0,0,0,0,0,0,1,1,1,0,0,0,'1366 x 515 pixel','mod_posted/index/home-banner','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Button Text','Link ( must be include http/https )','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,0,1,'text','text','textarea limit','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:41:17',6,'2020-02-06 13:50:17',6),
('1912185DF983D281175','1912185DF982C950CA0',17,1,'post',0,1,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'1000 x 260 px','mod_posted/index/page-banner','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:41:38',6,'2020-02-06 15:23:37',6),
('1912185DF9844950D3A','1912185DF982D2ECBD6',21,1,'post',0,1,0,1,0,1,1,1,1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/branch-office','','2020-01-23 00:00:00','Product Tags','Brand','Ordering','Parent','Office','Address','Phone','EMAIL','FAX','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'editor','text','text','text','single','single',1,'1912315E0AB6C14C998','All','All',NULL,0,NULL,'2019-12-18 08:43:37',6,'2020-01-23 15:53:51',6),
('1912185DF984FA7F533','1912185DF982BF2E2B8',28,1,'post',0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/about','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Link Youtube','Description','Introduction','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,0,1,'text','text','text','editor','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:46:34',6,'2020-02-06 14:59:17',6),
('1912185DF98690C8FA5','1912185DF982BF2E2B8',39,1,'post',0,1,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/careers','careers','2020-01-06 00:00:00','Product Tags','Brand','Ordering','Parent','Career Name','Email Career PIC','Description','Note','Job Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','single',1,'All',NULL,NULL,NULL,0,NULL,'2019-12-18 08:53:20',6,'2020-01-06 14:11:09',6),
('1912185DF986A6700B4','1912185DF982BF2E2B8',40,1,'post',0,1,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,'','mod_posted/index/faq','faq','2020-02-06 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','single',1,'2002055E3A76CA96A6F','All','All',NULL,0,NULL,'2019-12-18 08:53:42',6,'2020-02-06 15:19:59',6),
('1912185DF987023CC3A','1912185DF982BF2E2B8',42,1,'post',0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/static-page','','2020-02-05 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:14',6,'2020-02-05 14:10:57',6),
('1912185DF987141323F','1912185DF982E6C0E2C',43,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_users','','2020-01-29 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:32',6,'2020-01-29 11:55:02',6),
('1912185DF987262FE31','1912185DF982E6C0E2C',44,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_group','','2020-01-29 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:50',6,'2020-01-29 11:55:17',6),
('1912195DFAF689B8322','1912185DF98252958B0',100,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_contact_subject','','2020-02-05 00:00:00','Product Tags','Brand','','','Title','Subtitle','','Note','','','Promotion','','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-19 11:03:21',6,'2020-02-05 13:59:05',6),
('2001025E0D71F255709','0',4,1,'other',0,1,0,1,1,1,1,1,1,0,1,1,1,0,0,0,0,0,1,0,0,0,0,0,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-02 11:30:42',6,'2020-02-05 11:24:29',6),
('2001085E152FE2811FB','2001025E0D71F255709',4,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-08 08:26:58',6,'2020-02-05 14:05:19',6),
('2001085E15302B7D589','2001085E152FE2811FB',104,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','mod_coupon_category','','2020-01-08 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-08 08:28:11',6,'2020-01-08 08:29:24',6),
('2001085E15306149976','2001085E152FE2811FB',105,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','mod_coupon','','2020-01-08 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-08 08:29:05',6,'2020-01-08 08:29:05',6),
('2001205E24993BBD939','1912185DF982D2ECBD6',106,1,'tags',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/branch-office-category','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-20 01:00:27',6,'2020-01-23 15:49:29',6),
('2001225E27A75AECB24','1912185DF982D2ECBD6',109,1,'other',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_courier','','2020-01-22 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-22 08:37:30',6,'2020-01-22 08:37:30',6),
('2001225E27FA3BD6214','2002055E3A709CCB117',110,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/faq','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-22 14:31:07',6,'2020-02-05 14:43:25',6),
('2002055E3A6A479F4EA','1912185DF982BF2E2B8',29,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/our-formula','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:09:59',6,'2020-02-05 14:11:14',6),
('2002055E3A709CCB117','0',111,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:37:00',6,'2020-02-05 14:37:00',6),
('2002055E3A726DC02DC','0',5,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:44:45',6,'2020-02-05 14:44:45',6),
('2002055E3A72932EA01','2002055E3A726DC02DC',112,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:45:23',6,'2020-02-05 14:45:23',6),
('2002055E3A72C02F41C','2002055E3A72932EA01',113,1,'post',0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,'','mod_posted/index/video-introduction','','2020-02-06 00:00:00','Product Tags','Brand','','','Title','Link Youtube','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:46:08',6,'2020-02-06 16:17:32',6),
('2002055E3A73E42E463','2002055E3A72932EA01',114,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/tutorial','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:51:00',6,'2020-02-05 14:51:00',6),
('2002055E3A73F9F2E45','2002055E3A72932EA01',115,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/events','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:51:21',6,'2020-02-05 14:51:21',6),
('2002055E3A740D61A4F','2002055E3A72932EA01',116,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/people','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:51:41',6,'2020-02-05 14:51:55',6),
('2002055E3A7491496C8','2002055E3A726DC02DC',117,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:53:53',6,'2020-02-05 14:53:53',6),
('2002055E3A74AF67E22','2002055E3A7491496C8',118,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/store','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:54:23',6,'2020-02-05 14:54:23',6),
('2002055E3A74C33A375','2002055E3A7491496C8',119,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_posted/index/headquarter','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:54:43',6,'2020-02-05 14:54:43',6),
('2002055E3A753424D40','2001025E0D71F255709',120,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 14:56:36',6,'2020-02-05 14:56:36',6),
('2002055E3A77B090B5C','2002055E3A753424D40',121,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/category-menu','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 15:07:12',6,'2020-02-05 15:07:12',6),
('2002055E3A78314CC64','2002055E3A753424D40',122,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/product-category','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 15:09:21',6,'2020-02-05 15:09:21',6),
('2002055E3A784C9C8FB','2001025E0D71F255709',123,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 15:09:48',6,'2020-02-05 15:09:48',6),
('2002055E3A7868AA62B','2002055E3A784C9C8FB',124,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/collection','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 15:10:16',6,'2020-02-05 15:10:36',6),
('2002055E3A789D4A458','2002055E3A784C9C8FB',125,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/color','','2020-02-05 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-05 15:11:09',6,'2020-02-05 15:11:09',6),
('2002065E3BB687CDF3A','1912185DF982C950CA0',126,1,'post',0,1,0,1,0,1,1,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,1,0,'792 x 397 pixel','mod_posted/index/home-section','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','','Title','Button Text','Link','','','Image','Featured','Tags','Featured','Featured','Event Start','Author','Event End',0,0,0,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-06 13:47:35',6,'2020-02-06 13:47:35',6),
('2002065E3BC8F091161','2002055E3A6A479F4EA',127,1,'post',0,1,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,1,1,1,0,'60 x 65 pixel','mod_posted/index/introduction','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','','Title','','','','','Image Thumbnails','Featured','','Featured','Featured','Event Start','Author','Event End',0,0,0,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-06 15:06:08',6,'2020-02-06 15:08:24',6),
('2002065E3BCA21ED0C9','2002055E3A6A479F4EA',128,1,'post',0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'445 x 445 pixel','mod_posted/index/our-formula-article','','2020-02-06 00:00:00','Product Tags','Brand','Ordering','','Title','','','','Description','Image Thumbnails','Featured','','Featured','Featured','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-02-06 15:11:13',6,'2020-02-06 15:12:45',6);

/*Table structure for table `menu_counter` */

DROP TABLE IF EXISTS `menu_counter`;

CREATE TABLE `menu_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acs_users_id` int(11) NOT NULL,
  `client_ip` text NOT NULL,
  `client_browser` text NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_counter` */

/*Table structure for table `menu_image` */

DROP TABLE IF EXISTS `menu_image`;

CREATE TABLE `menu_image` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `file_type` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`menu_id`),
  CONSTRAINT `menu_image_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_image` */

/*Table structure for table `menu_lang` */

DROP TABLE IF EXISTS `menu_lang`;

CREATE TABLE `menu_lang` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_aliases` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`menu_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `menu_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_lang_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_lang` */

insert  into `menu_lang`(`id`,`menu_id`,`acs_lang_id`,`title`,`title_aliases`,`slug`,`description`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1912185DF9825295B0D','1912185DF98252958B0','LGEN','Contact','','contact','',6,'2019-12-18 08:35:14',1,'2019-12-18 08:39:05'),
('1912185DF9825295BC1','1912185DF98252958B0','LGID','Contact Us','','contact-us','',6,'2019-12-18 08:35:14',1,'2019-12-18 08:39:05'),
('1912185DF98256173EE','1912185DF98256171B4','LGEN','Portal Customers','','portal-customers','',6,'2019-12-18 08:35:18',1,'2019-12-18 08:49:10'),
('1912185DF982561745C','1912185DF98256171B4','LGID','Customers','','customers','',6,'2019-12-18 08:35:18',1,'2019-12-18 08:49:10'),
('1912185DF98259D1C8C','1912185DF98259D1A67','LGEN','Orders','','orders','',6,'2019-12-18 08:35:21',1,'2019-12-18 08:36:40'),
('1912185DF98259D1CF3','1912185DF98259D1A67','LGID','Orders','','orders','',6,'2019-12-18 08:35:21',1,'2019-12-18 08:36:40'),
('1912185DF982AF5AFF4','1912185DF982AF5AE59','LGEN','Product','','product','',6,'2019-12-18 08:36:47',1,'2020-02-05 11:36:12'),
('1912185DF982AF5B040','1912185DF982AF5AE59','LGID','Product','','product','',6,'2019-12-18 08:36:47',1,'2020-02-05 11:36:12'),
('1912185DF982BF2E4C3','1912185DF982BF2E2B8','LGEN','Pages','','pages','',6,'2019-12-18 08:37:03',1,'2020-02-05 14:44:57'),
('1912185DF982BF2E511','1912185DF982BF2E2B8','LGID','Pages','','pages','',6,'2019-12-18 08:37:03',1,'2020-02-05 14:44:57'),
('1912185DF982C950E8B','1912185DF982C950CA0','LGEN','Media','','media','',6,'2019-12-18 08:37:13',1,'2020-02-05 14:53:16'),
('1912185DF982C950EDD','1912185DF982C950CA0','LGID','Media','','media','',6,'2019-12-18 08:37:13',1,'2020-02-05 14:53:16'),
('1912185DF982D2ECE91','1912185DF982D2ECBD6','LGEN','Master','','master','',6,'2019-12-18 08:37:22',1,'2019-12-18 08:37:22'),
('1912185DF982D2ECEFD','1912185DF982D2ECBD6','LGID','Master','','master','',6,'2019-12-18 08:37:22',1,'2019-12-18 08:37:22'),
('1912185DF982DE2DA30','1912185DF982DE2D5F3','LGEN','General Settings','','general-settings','',6,'2019-12-18 08:37:34',1,'2020-02-05 14:42:14'),
('1912185DF982DE2DAFC','1912185DF982DE2D5F3','LGID','General Settings','','general-settings','',6,'2019-12-18 08:37:34',1,'2020-02-05 14:42:14'),
('1912185DF982E6C0FDB','1912185DF982E6C0E2C','LGEN','Administrator','','administrator','',6,'2019-12-18 08:37:42',1,'2019-12-18 08:37:42'),
('1912185DF982E6C1075','1912185DF982E6C0E2C','LGID','Administrator','','administrator','',6,'2019-12-18 08:37:42',1,'2019-12-18 08:37:42'),
('1912185DF9832DD2007','1912185DF9832DD1E4F','LGEN','Contact List','','contact-list','',6,'2019-12-18 08:38:53',1,'2020-02-05 11:16:13'),
('1912185DF9832DD2059','1912185DF9832DD1E4F','LGID','Contact List','','contact-list','',6,'2019-12-18 08:38:53',1,'2020-02-05 11:16:13'),
('1912185DF98347DBE3F','1912185DF98347DBA6D','LGEN','Customer List','','customer-list','',6,'2019-12-18 08:39:19',1,'2020-01-21 09:36:43'),
('1912185DF98347DBEFD','1912185DF98347DBA6D','LGID','Customer List','','customer-list','',6,'2019-12-18 08:39:19',1,'2020-01-21 09:36:43'),
('1912185DF9835B605A6','1912185DF9835B603E4','LGEN','Customer Address','','customer-address','',6,'2019-12-18 08:39:39',1,'2020-01-21 10:14:59'),
('1912185DF9835B60604','1912185DF9835B603E4','LGID','Customer Address','','customer-address','',6,'2019-12-18 08:39:39',1,'2020-01-21 10:14:59'),
('1912185DF9838C62FC0','1912185DF9838C62DC5','LGEN','Orders Product','','orders-product','',6,'2019-12-18 08:40:28',1,'2020-01-22 08:38:09'),
('1912185DF9838C6301B','1912185DF9838C62DC5','LGID','Orders Product','','orders-product','',6,'2019-12-18 08:40:28',1,'2020-01-22 08:38:09'),
('1912185DF983BDF2B07','1912185DF983BDF2925','LGEN','Home Banner','','home-banner','',6,'2019-12-18 08:41:17',1,'2020-02-06 13:50:17'),
('1912185DF983BDF2B5D','1912185DF983BDF2925','LGID','Home Banner','','home-banner','',6,'2019-12-18 08:41:17',1,'2020-02-06 13:50:17'),
('1912185DF983D281328','1912185DF983D281175','LGEN','Page Banner','','page-banner','',6,'2019-12-18 08:41:38',1,'2020-02-06 15:23:37'),
('1912185DF983D281376','1912185DF983D281175','LGID','Page Banner','','page-banner','',6,'2019-12-18 08:41:38',1,'2020-02-06 15:23:37'),
('1912185DF9844950ECD','1912185DF9844950D3A','LGEN','Branch Office','','branch-office','',6,'2019-12-18 08:43:37',1,'2020-01-23 15:53:51'),
('1912185DF9844950F1A','1912185DF9844950D3A','LGID','Branch Office','','branch-office','',6,'2019-12-18 08:43:37',1,'2020-01-23 15:53:51'),
('1912185DF984FA7F76D','1912185DF984FA7F533','LGEN','About','','about','',6,'2019-12-18 08:46:34',1,'2020-02-06 14:59:17'),
('1912185DF984FA7F7BC','1912185DF984FA7F533','LGID','About','','about','',6,'2019-12-18 08:46:34',1,'2020-02-06 14:59:17'),
('1912185DF98690C961C','1912185DF98690C8FA5','LGEN','Careers','','careers','',6,'2019-12-18 08:53:20',1,'2020-01-06 14:11:09'),
('1912185DF98690C96AE','1912185DF98690C8FA5','LGID','Careers','','careers','',6,'2019-12-18 08:53:20',1,'2020-01-06 14:11:09'),
('1912185DF986A67046D','1912185DF986A6700B4','LGEN','FAQ','','faq','',6,'2019-12-18 08:53:42',1,'2020-02-06 15:19:59'),
('1912185DF986A6704D0','1912185DF986A6700B4','LGID','FAQ','','faq','',6,'2019-12-18 08:53:42',1,'2020-02-06 15:19:59'),
('1912185DF987023D078','1912185DF987023CC3A','LGEN','Static Page','','static-page','',6,'2019-12-18 08:55:14',1,'2020-02-05 14:10:57'),
('1912185DF987023D108','1912185DF987023CC3A','LGID','Static Page','','static-page','',6,'2019-12-18 08:55:14',1,'2020-02-05 14:10:57'),
('1912185DF98714134B2','1912185DF987141323F','LGEN','User Management','','user-management','',6,'2019-12-18 08:55:32',1,'2020-01-29 11:55:02'),
('1912185DF987141350D','1912185DF987141323F','LGID','User Management','','user-management','',6,'2019-12-18 08:55:32',1,'2020-01-29 11:55:02'),
('1912185DF987263000D','1912185DF987262FE31','LGEN','Access Level','','access-level','',6,'2019-12-18 08:55:50',1,'2020-01-29 11:55:17'),
('1912185DF9872630062','1912185DF987262FE31','LGID','Access Level','','access-level','',6,'2019-12-18 08:55:50',1,'2020-01-29 11:55:17'),
('1912195DFAF689BB4E8','1912195DFAF689B8322','LGEN','Contact Subject','','contact-subject','',6,'2019-12-19 11:03:21',1,'2020-02-05 13:59:05'),
('1912195DFAF689BB70A','1912195DFAF689B8322','LGID','Contact Subject','','contact-subject','',6,'2019-12-19 11:03:21',1,'2020-02-05 13:59:05'),
('2001025E0D71F257F29','2001025E0D71F255709','LGEN','Product Portal','','product-portal','',6,'2020-01-02 11:30:42',1,'2020-02-05 11:24:29'),
('2001025E0D71F258018','2001025E0D71F255709','LGID','Product Portal','','product-portal','',6,'2020-01-02 11:30:42',1,'2020-02-05 11:24:29'),
('2001085E152FE28496A','2001085E152FE2811FB','LGEN','Promotion Management','','promotion-management','',6,'2020-01-08 08:26:58',1,'2020-02-05 14:05:19'),
('2001085E152FE284A6B','2001085E152FE2811FB','LGID','Promotion Management','','promotion-management','',6,'2020-01-08 08:26:58',1,'2020-02-05 14:05:19'),
('2001085E15302B80C96','2001085E15302B7D589','LGEN','Promo Category','','promo-category','',6,'2020-01-08 08:28:11',1,'2020-01-08 08:29:24'),
('2001085E15302B80D54','2001085E15302B7D589','LGID','Promo Category','','promo-category','',6,'2020-01-08 08:28:11',1,'2020-01-08 08:29:24'),
('2001085E1530614D18A','2001085E15306149976','LGEN','Promo Code','','promo-code','',6,'2020-01-08 08:29:05',1,'2020-01-08 08:29:05'),
('2001085E1530614D274','2001085E15306149976','LGID','Promo Code','','promo-code','',6,'2020-01-08 08:29:05',1,'2020-01-08 08:29:05'),
('2001205E24993BC0541','2001205E24993BBD939','LGEN','Branch Category','','branch-category','',6,'2020-01-20 01:00:27',1,'2020-01-23 15:49:29'),
('2001205E24993BC0606','2001205E24993BBD939','LGID','Branch Category','','branch-category','',6,'2020-01-20 01:00:27',1,'2020-01-23 15:49:29'),
('2001225E27A75AED016','2001225E27A75AECB24','LGEN','Courier','','courier','',6,'2020-01-22 08:37:30',1,'2020-01-22 08:37:30'),
('2001225E27A75AED1A0','2001225E27A75AECB24','LGID','Courier','','courier','',6,'2020-01-22 08:37:30',1,'2020-01-22 08:37:30'),
('2001225E27FA3BD6724','2001225E27FA3BD6214','LGEN','FAQ Category','','faq-category','',6,'2020-01-22 14:31:07',1,'2020-02-05 14:43:25'),
('2001225E27FA3BD68C8','2001225E27FA3BD6214','LGID','FAQ Category','','faq-category','',6,'2020-01-22 14:31:07',1,'2020-02-05 14:43:25'),
('2002055E3A6A47A2886','2002055E3A6A479F4EA','LGEN','Our Formula','','our-formula','',6,'2020-02-05 14:09:59',1,'2020-02-05 14:11:14'),
('2002055E3A6A47A2935','2002055E3A6A479F4EA','LGID','Our Formula','','our-formula','',6,'2020-02-05 14:09:59',1,'2020-02-05 14:11:14'),
('2002055E3A709CCED47','2002055E3A709CCB117','LGEN','All Setting','','all-setting','',6,'2020-02-05 14:37:00',1,'2020-02-05 14:37:00'),
('2002055E3A709CCEDE4','2002055E3A709CCB117','LGID','All Setting','','all-setting','',6,'2020-02-05 14:37:00',1,'2020-02-05 14:37:00'),
('2002055E3A726DC3342','2002055E3A726DC02DC','LGEN','Portal Pages','','portal-pages','',6,'2020-02-05 14:44:45',1,'2020-02-05 14:44:45'),
('2002055E3A726DC341B','2002055E3A726DC02DC','LGID','Portal Pages','','portal-pages','',6,'2020-02-05 14:44:45',1,'2020-02-05 14:44:45'),
('2002055E3A729331521','2002055E3A72932EA01','LGEN','The World of Sada','','the-world-of-sada','',6,'2020-02-05 14:45:23',1,'2020-02-05 14:45:23'),
('2002055E3A7293315A7','2002055E3A72932EA01','LGID','The World of Sada','','the-world-of-sada','',6,'2020-02-05 14:45:23',1,'2020-02-05 14:45:23'),
('2002055E3A72C0315CC','2002055E3A72C02F41C','LGEN','Video Introduction','','video-introduction','',6,'2020-02-05 14:46:08',1,'2020-02-06 16:17:32'),
('2002055E3A72C0316EB','2002055E3A72C02F41C','LGID','Video Introduction','','video-introduction','',6,'2020-02-05 14:46:08',1,'2020-02-06 16:17:32'),
('2002055E3A73E431DA8','2002055E3A73E42E463','LGEN','Tutorial','','tutorial','',6,'2020-02-05 14:51:00',1,'2020-02-05 14:51:00'),
('2002055E3A73E431E50','2002055E3A73E42E463','LGID','Tutorial','','tutorial','',6,'2020-02-05 14:51:00',1,'2020-02-05 14:51:00'),
('2002055E3A73FA00E73','2002055E3A73F9F2E45','LGEN','Events','','events','',6,'2020-02-05 14:51:22',1,'2020-02-05 14:51:22'),
('2002055E3A73FA00EF2','2002055E3A73F9F2E45','LGID','Events','','events','',6,'2020-02-05 14:51:22',1,'2020-02-05 14:51:22'),
('2002055E3A740D6486A','2002055E3A740D61A4F','LGEN','People','','people','',6,'2020-02-05 14:51:41',1,'2020-02-05 14:51:55'),
('2002055E3A740D648F7','2002055E3A740D61A4F','LGID','People','','people','',6,'2020-02-05 14:51:41',1,'2020-02-05 14:51:55'),
('2002055E3A74914C205','2002055E3A7491496C8','LGEN','Places','','places','',6,'2020-02-05 14:53:53',1,'2020-02-05 14:53:53'),
('2002055E3A74914C282','2002055E3A7491496C8','LGID','Places','','places','',6,'2020-02-05 14:53:53',1,'2020-02-05 14:53:53'),
('2002055E3A74AF6B67E','2002055E3A74AF67E22','LGEN','Store','','store','',6,'2020-02-05 14:54:23',1,'2020-02-05 14:54:23'),
('2002055E3A74AF6B753','2002055E3A74AF67E22','LGID','Store','','store','',6,'2020-02-05 14:54:23',1,'2020-02-05 14:54:23'),
('2002055E3A74C33CB8B','2002055E3A74C33A375','LGEN','Headquarter','','headquarter','',6,'2020-02-05 14:54:43',1,'2020-02-05 14:54:43'),
('2002055E3A74C33CC08','2002055E3A74C33A375','LGID','Headquarter','','headquarter','',6,'2020-02-05 14:54:43',1,'2020-02-05 14:54:43'),
('2002055E3A753429666','2002055E3A753424D40','LGEN','Category Product','','category-product','',6,'2020-02-05 14:56:36',1,'2020-02-05 14:56:36'),
('2002055E3A753429741','2002055E3A753424D40','LGID','Category Product','','category-product','',6,'2020-02-05 14:56:36',1,'2020-02-05 14:56:36'),
('2002055E3A77B09AF4D','2002055E3A77B090B5C','LGEN','Category Menu','','category-menu','',6,'2020-02-05 15:07:12',1,'2020-02-05 15:07:12'),
('2002055E3A77B09AFD7','2002055E3A77B090B5C','LGID','Category Menu','','category-menu','',6,'2020-02-05 15:07:12',1,'2020-02-05 15:07:12'),
('2002055E3A78314F61A','2002055E3A78314CC64','LGEN','Product Category','','product-category','',6,'2020-02-05 15:09:21',1,'2020-02-05 15:09:21'),
('2002055E3A78314F6A7','2002055E3A78314CC64','LGID','Product Category','','product-category','',6,'2020-02-05 15:09:21',1,'2020-02-05 15:09:21'),
('2002055E3A784CA0D55','2002055E3A784C9C8FB','LGEN','Tags','','tags','',6,'2020-02-05 15:09:48',1,'2020-02-05 15:09:48'),
('2002055E3A784CA0E25','2002055E3A784C9C8FB','LGID','Tags','','tags','',6,'2020-02-05 15:09:48',1,'2020-02-05 15:09:48'),
('2002055E3A7868ADD26','2002055E3A7868AA62B','LGEN','Collection','','collection','',6,'2020-02-05 15:10:16',1,'2020-02-05 15:10:36'),
('2002055E3A7868ADDCA','2002055E3A7868AA62B','LGID','Collection','','collection','',6,'2020-02-05 15:10:16',1,'2020-02-05 15:10:36'),
('2002055E3A789D50205','2002055E3A789D4A458','LGEN','Color','','color','',6,'2020-02-05 15:11:09',1,'2020-02-05 15:11:09'),
('2002055E3A789D5028A','2002055E3A789D4A458','LGID','Color','','color','',6,'2020-02-05 15:11:09',1,'2020-02-05 15:11:09'),
('2002065E3BB687D1520','2002065E3BB687CDF3A','LGEN','Home Section','','home-section','',6,'2020-02-06 13:47:35',1,'2020-02-06 13:47:35'),
('2002065E3BB687D15B0','2002065E3BB687CDF3A','LGID','Home Section','','home-section','',6,'2020-02-06 13:47:35',1,'2020-02-06 13:47:35'),
('2002065E3BC8F094502','2002065E3BC8F091161','LGEN','Introduction','','introduction','',6,'2020-02-06 15:06:08',1,'2020-02-06 15:08:24'),
('2002065E3BC8F0945DA','2002065E3BC8F091161','LGID','Introduction','','introduction','',6,'2020-02-06 15:06:08',1,'2020-02-06 15:08:24'),
('2002065E3BCA21EF59B','2002065E3BCA21ED0C9','LGEN','Our Formula Article','','our-formula-article','',6,'2020-02-06 15:11:13',1,'2020-02-06 15:12:45'),
('2002065E3BCA21EF622','2002065E3BCA21ED0C9','LGID','Our Formula Article','','our-formula-article','',6,'2020-02-06 15:11:13',1,'2020-02-06 15:12:45');

/*Table structure for table `notification_log` */

DROP TABLE IF EXISTS `notification_log`;

CREATE TABLE `notification_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_order_id` varchar(40) NOT NULL,
  `transaction_status` varchar(50) NOT NULL,
  `fraud_status` varchar(50) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `transaction_time` varchar(50) DEFAULT NULL,
  `notif` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

/*Data for the table `notification_log` */

/*Table structure for table `order_billing` */

DROP TABLE IF EXISTS `order_billing`;

CREATE TABLE `order_billing` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_billing` */

/*Table structure for table `order_customer` */

DROP TABLE IF EXISTS `order_customer`;

CREATE TABLE `order_customer` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_customer` */

/*Table structure for table `order_shiping` */

DROP TABLE IF EXISTS `order_shiping`;

CREATE TABLE `order_shiping` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_shiping` */

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `parent` varchar(25) DEFAULT '0',
  `parent_branch` varchar(25) DEFAULT '0',
  `ordering` smallint(6) DEFAULT NULL,
  `published` datetime NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `flag` int(11) DEFAULT '0',
  `is_featured` int(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `file_location` varchar(100) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `coupon_id` varchar(25) DEFAULT NULL,
  `event_start` date DEFAULT NULL,
  `event_end` date DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `can_buy` tinyint(1) DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`menu_id`),
  KEY `post_city_fk` (`city`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post` */

insert  into `post`(`id`,`menu_id`,`type_id`,`parent`,`parent_branch`,`ordering`,`published`,`meta_title`,`meta_keywords`,`meta_description`,`status`,`flag`,`is_featured`,`icon`,`file`,`file_location`,`province`,`city`,`subdistrict`,`coupon_id`,`event_start`,`event_end`,`author`,`can_buy`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002065E3BB8BE00E81','1912185DF983BDF2925',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 13:57:02',6,'2020-02-06 13:57:02'),
('2002065E3BB8F3645AD','1912185DF983BDF2925',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 13:57:55',6,'2020-02-06 13:57:55'),
('2002065E3BB9CDAAA91','2002065E3BB687CDF3A',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:01:33',6,'2020-02-06 14:01:33'),
('2002065E3BBA08D6ECA','2002065E3BB687CDF3A',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:02:32',6,'2020-02-06 14:02:32'),
('2002065E3BBA1E794A9','2002065E3BB687CDF3A',1,'0','0',3,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:02:54',6,'2020-02-06 14:02:54'),
('2002065E3BBA35B82F7','2002065E3BB687CDF3A',1,'0','0',4,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:03:17',6,'2020-02-06 14:03:17'),
('2002065E3BBAF75C7DE','1912185DF983D281175',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:06:31',6,'2020-02-06 14:06:31'),
('2002065E3BBBA31F94E','1912185DF984FA7F533',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 14:09:23',6,'2020-02-06 14:58:56'),
('2002065E3BC81ACE28D','1912185DF983D281175',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:02:34',6,'2020-02-06 15:02:34'),
('2002065E3BC9B255770','2002065E3BC8F091161',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:09:22',6,'2020-02-06 15:09:22'),
('2002065E3BC9C155242','2002065E3BC8F091161',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:09:37',6,'2020-02-06 15:09:37'),
('2002065E3BC9CB051B4','2002065E3BC8F091161',1,'0','0',3,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:09:47',6,'2020-02-06 15:09:47'),
('2002065E3BC9DC93DD9','2002065E3BC8F091161',1,'0','0',4,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:10:04',6,'2020-02-06 15:10:04'),
('2002065E3BCAB6A272E','2002065E3BCA21ED0C9',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:13:42',6,'2020-02-06 15:13:42'),
('2002065E3BCAC985E58','2002065E3BCA21ED0C9',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:14:01',6,'2020-02-06 15:14:01'),
('2002065E3BCB3240FEF','2002065E3BCA21ED0C9',1,'0','0',3,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:15:46',6,'2020-02-06 15:15:46'),
('2002065E3BCB6D8128E','1912185DF98690C8FA5',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:16:45',6,'2020-02-06 15:16:45'),
('2002065E3BCB87718B8','1912185DF98690C8FA5',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:17:11',6,'2020-02-06 15:17:11'),
('2002065E3BCB9C199F5','1912185DF983D281175',1,'0','0',3,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:17:32',6,'2020-02-06 15:17:32'),
('2002065E3BCC32A9576','1912185DF986A6700B4',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:20:02',6,'2020-02-06 15:20:10'),
('2002065E3BCC4B9B2A3','1912185DF986A6700B4',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:20:27',6,'2020-02-06 15:20:27'),
('2002065E3BCCABAA62D','1912185DF983D281175',1,'0','0',4,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:22:03',6,'2020-02-06 15:22:03'),
('2002065E3BCD2F95F26','1912185DF983D281175',1,'0','0',5,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:24:15',6,'2020-02-06 15:24:15'),
('2002065E3BCD613A10D','1912185DF987023CC3A',1,'0','0',1,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:25:05',6,'2020-02-06 15:25:05'),
('2002065E3BCD7BAD505','1912185DF987023CC3A',1,'0','0',2,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:25:31',6,'2020-02-06 15:25:31'),
('2002065E3BCD898C93A','1912185DF987023CC3A',1,'0','0',3,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:25:45',6,'2020-02-06 15:46:11'),
('2002065E3BCDAE78A82','1912185DF983D281175',1,'0','0',6,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:26:22',6,'2020-02-06 15:26:22'),
('2002065E3BCDBCCBB1E','1912185DF983D281175',1,'0','0',7,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:26:36',6,'2020-02-06 15:26:36'),
('2002065E3BCDC2BFC12','1912185DF983D281175',1,'0','0',8,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:26:42',6,'2020-02-06 15:26:42'),
('2002065E3BCDCD077F2','1912185DF983D281175',1,'0','0',9,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:26:53',6,'2020-02-06 15:26:53'),
('2002065E3BCE782884D','1912185DF983D281175',1,'0','0',10,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 15:29:44',6,'2020-02-06 15:29:44'),
('2002065E3BDA1294E27','1912185DF983D281175',1,'0','0',11,'2020-02-06 00:00:00','','','',1,0,0,'',NULL,NULL,NULL,NULL,NULL,NULL,'2020-02-06','2020-02-06','',0,6,'2020-02-06 16:19:14',6,'2020-02-06 16:19:14');

/*Table structure for table `post_counter` */

DROP TABLE IF EXISTS `post_counter`;

CREATE TABLE `post_counter` (
  `post_id` varchar(25) NOT NULL,
  `tanggal` datetime NOT NULL,
  `counter` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_counter` */

/*Table structure for table `post_image` */

DROP TABLE IF EXISTS `post_image`;

CREATE TABLE `post_image` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `file_name` text,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_caption` varchar(255) DEFAULT NULL,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `file_type` varchar(50) NOT NULL,
  `status_cover` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_image_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_image` */

insert  into `post_image`(`id`,`post_id`,`file_name`,`image_alt`,`image_caption`,`file_location`,`file_thumb`,`file_original`,`file_type`,`status_cover`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002065E3BB8BE547C2Y48A5','2002065E3BB8BE00E81','',NULL,NULL,'userfiles/post/','thumb_5e3bb8be290e0.jpg','5e3bb8be290e0.jpg','image',0,1,'2020-02-06 13:57:02',1,'2020-02-06 13:57:02'),
('2002065E3BB8F3BAAA2IWYTX','2002065E3BB8F3645AD','',NULL,NULL,'userfiles/post/','thumb_5e3bb8f39c9b7.jpg','5e3bb8f39c9b7.jpg','image',0,1,'2020-02-06 13:57:55',1,'2020-02-06 13:57:55'),
('2002065E3BB9CDD1C37TI972','2002065E3BB9CDAAA91','',NULL,NULL,'userfiles/post/','thumb_5e3bb9cdc1204.jpg','5e3bb9cdc1204.jpg','image',0,1,'2020-02-06 14:01:33',1,'2020-02-06 14:01:33'),
('2002065E3BBA0914157D5DNZ','2002065E3BBA08D6ECA','',NULL,NULL,'userfiles/post/','thumb_5e3bba0901f7b.jpg','5e3bba0901f7b.jpg','image',0,1,'2020-02-06 14:02:33',1,'2020-02-06 14:02:33'),
('2002065E3BBA1E9A1DEKVFTO','2002065E3BBA1E794A9','',NULL,NULL,'userfiles/post/','thumb_5e3bba1e89c96.jpg','5e3bba1e89c96.jpg','image',0,1,'2020-02-06 14:02:54',1,'2020-02-06 14:02:54'),
('2002065E3BBA35F1447ODZEP','2002065E3BBA35B82F7','',NULL,NULL,'userfiles/post/','thumb_5e3bba35dfce6.jpg','5e3bba35dfce6.jpg','image',0,1,'2020-02-06 14:03:17',1,'2020-02-06 14:03:17'),
('2002065E3BBAF78D11DZSNN5','2002065E3BBAF75C7DE','',NULL,NULL,'userfiles/post/','thumb_5e3bbaf76dc8c.jpg','5e3bbaf76dc8c.jpg','image',0,1,'2020-02-06 14:06:31',1,'2020-02-06 14:06:31'),
('2002065E3BC81B03ED3KK589','2002065E3BC81ACE28D','',NULL,NULL,'userfiles/post/','thumb_5e3bc81addd40.jpg','5e3bc81addd40.jpg','image',0,1,'2020-02-06 15:02:35',1,'2020-02-06 15:02:35'),
('2002065E3BC9B26E406I2MY9','2002065E3BC9B255770','',NULL,NULL,'userfiles/post/','thumb_5e3bc9b2659d6.png','5e3bc9b2659d6.png','image',0,1,'2020-02-06 15:09:22',1,'2020-02-06 15:09:22'),
('2002065E3BC9C18F848M3R5J','2002065E3BC9C155242','',NULL,NULL,'userfiles/post/','thumb_5e3bc9c17b753.png','5e3bc9c17b753.png','image',0,1,'2020-02-06 15:09:37',1,'2020-02-06 15:09:37'),
('2002065E3BC9CB15575QJ4GZ','2002065E3BC9CB051B4','',NULL,NULL,'userfiles/post/','thumb_5e3bc9cb103b4.png','5e3bc9cb103b4.png','image',0,1,'2020-02-06 15:09:47',1,'2020-02-06 15:09:47'),
('2002065E3BC9DCB47B9AGRRX','2002065E3BC9DC93DD9','',NULL,NULL,'userfiles/post/','thumb_5e3bc9dca28ee.png','5e3bc9dca28ee.png','image',0,1,'2020-02-06 15:10:04',1,'2020-02-06 15:10:04'),
('2002065E3BCAB6DBD845NYBM','2002065E3BCAB6A272E','',NULL,NULL,'userfiles/post/','thumb_5e3bcab6c65af.jpg','5e3bcab6c65af.jpg','image',0,1,'2020-02-06 15:13:42',1,'2020-02-06 15:13:42'),
('2002065E3BCAC9A50B8G2W7B','2002065E3BCAC985E58','',NULL,NULL,'userfiles/post/','thumb_5e3bcac993dc7.jpg','5e3bcac993dc7.jpg','image',0,1,'2020-02-06 15:14:01',1,'2020-02-06 15:14:01'),
('2002065E3BCD2FB5CA6RVCE3','2002065E3BCD2F95F26','',NULL,NULL,'userfiles/post/','thumb_5e3bcd2fa54cf.jpg','5e3bcd2fa54cf.jpg','image',0,1,'2020-02-06 15:24:15',1,'2020-02-06 15:24:15'),
('2002065E3BDA12CEB7DX5WF3','2002065E3BDA1294E27','',NULL,NULL,'userfiles/post/','thumb_5e3bda12a9c44.jpg','5e3bda12a9c44.jpg','image',0,1,'2020-02-06 16:19:14',1,'2020-02-06 16:19:14');

/*Table structure for table `post_lang` */

DROP TABLE IF EXISTS `post_lang`;

CREATE TABLE `post_lang` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title_sub` text,
  `short_description` text,
  `description` text,
  `note` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`post_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `post_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_lang_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_lang` */

insert  into `post_lang`(`id`,`post_id`,`acs_lang_id`,`title`,`slug`,`title_sub`,`short_description`,`description`,`note`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002065E3BB8BE07D67','2002065E3BB8BE00E81','LGEN','See Asian Beauty in a Different light','see-asian-beauty-in-a-different-light','DIscover','https://google.com','Introducing SADA by Cathy Sharon','',6,'2020-02-06 13:57:02',1,'2020-02-06 13:57:02'),
('2002065E3BB8BE07E01','2002065E3BB8BE00E81','LGID','See Asian Beauty in a Different light','see-asian-beauty-in-a-different-light','DIscover','https://google.com','Introducing SADA by Cathy Sharon','',6,'2020-02-06 13:57:02',1,'2020-02-06 13:57:02'),
('2002065E3BB8F3692DA','2002065E3BB8F3645AD','LGEN','Formulated Specifically for Asian Weather','formulated-specifically-for-asian-weather','Learn More','https://google.com','','',6,'2020-02-06 13:57:55',1,'2020-02-06 13:57:55'),
('2002065E3BB8F3693DF','2002065E3BB8F3645AD','LGID','Formulated Specifically for Asian Weather','formulated-specifically-for-asian-weather','Learn More','https://google.com','','',6,'2020-02-06 13:57:55',1,'2020-02-06 13:57:55'),
('2002065E3BB9CDB02CA','2002065E3BB9CDAAA91','LGEN','Sada Product','sada-product','Discover','https://google.com','','',6,'2020-02-06 14:01:33',1,'2020-02-06 14:01:33'),
('2002065E3BB9CDB03AA','2002065E3BB9CDAAA91','LGID','Sada Product','sada-product','Discover','https://google.com','','',6,'2020-02-06 14:01:33',1,'2020-02-06 14:01:33'),
('2002065E3BBA08DACF2','2002065E3BBA08D6ECA','LGEN','Sada Make Up','sada-make-up','DIscover','https://google.com','','',6,'2020-02-06 14:02:32',1,'2020-02-06 14:02:32'),
('2002065E3BBA08DAD97','2002065E3BBA08D6ECA','LGID','Sada Make Up','sada-make-up','DIscover','https://google.com','','',6,'2020-02-06 14:02:32',1,'2020-02-06 14:02:32'),
('2002065E3BBA1E7CC6C','2002065E3BBA1E794A9','LGEN','Sada Sale','sada-sale','DIscover','https://google.com','','',6,'2020-02-06 14:02:54',1,'2020-02-06 14:02:54'),
('2002065E3BBA1E7CD67','2002065E3BBA1E794A9','LGID','Sada Sale','sada-sale','DIscover','https://google.com','','',6,'2020-02-06 14:02:54',1,'2020-02-06 14:02:54'),
('2002065E3BBA35BBE50','2002065E3BBA35B82F7','LGEN','Sada','sada','Discover','https://google.com','','',6,'2020-02-06 14:03:17',1,'2020-02-06 14:03:17'),
('2002065E3BBA35BBEF1','2002065E3BBA35B82F7','LGID','Sada','sada','Discover','https://google.com','','',6,'2020-02-06 14:03:17',1,'2020-02-06 14:03:17'),
('2002065E3BBAF75EF82','2002065E3BBAF75C7DE','LGEN','About Us','about-us','','','','',6,'2020-02-06 14:06:31',1,'2020-02-06 14:06:31'),
('2002065E3BBAF75F071','2002065E3BBAF75C7DE','LGID','About Us','about-us','','','','',6,'2020-02-06 14:06:31',1,'2020-02-06 14:06:31'),
('2002065E3BBBA32314A','2002065E3BBBA31F94E','LGEN','HELLO PRETTY!','hello-pretty','WOMEN CAN BE ANYTHING, JUST WITH A BIT OF CONFIDENCE','https://www.youtube.com/watch?v=_32kvI3IgrI','HELLO PRETTY!','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Break barriers,&nbsp;<strong>SEE ASIAN BEAUTY IN A DIFFERENT LIGHT.</strong></p>\r\n',6,'2020-02-06 14:09:23',1,'2020-02-06 14:58:56'),
('2002065E3BBBA32328A','2002065E3BBBA31F94E','LGID','HELLO PRETTY!','hello-pretty','WOMEN CAN BE ANYTHING, JUST WITH A BIT OF CONFIDENCE','https://www.youtube.com/watch?v=_32kvI3IgrI','HELLO PRETTY!','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Break barriers,&nbsp;<strong>SEE ASIAN BEAUTY IN A DIFFERENT LIGHT.</strong></p>\r\n',6,'2020-02-06 14:09:23',1,'2020-02-06 14:58:56'),
('2002065E3BC81AD146D','2002065E3BC81ACE28D','LGEN','Our Formula','our-formula','','WOMEN CAN BE ANYTHING, JUST WITH A BIT OF CONFIDENCE','','',6,'2020-02-06 15:02:34',1,'2020-02-06 15:02:34'),
('2002065E3BC81AD155E','2002065E3BC81ACE28D','LGID','Our Formula','our-formula','','WOMEN CAN BE ANYTHING, JUST WITH A BIT OF CONFIDENCE','','',6,'2020-02-06 15:02:34',1,'2020-02-06 15:02:34'),
('2002065E3BC9B2598E0','2002065E3BC9B255770','LGEN','PARABEN FREE','paraben-free','','','','',6,'2020-02-06 15:09:22',1,'2020-02-06 15:09:22'),
('2002065E3BC9B259A1F','2002065E3BC9B255770','LGID','PARABEN FREE','paraben-free','','','','',6,'2020-02-06 15:09:22',1,'2020-02-06 15:09:22'),
('2002065E3BC9C1588F8','2002065E3BC9C155242','LGEN','CRUELTY FREE','cruelty-free','','','','',6,'2020-02-06 15:09:37',1,'2020-02-06 15:09:37'),
('2002065E3BC9C158984','2002065E3BC9C155242','LGID','CRUELTY FREE','cruelty-free','','','','',6,'2020-02-06 15:09:37',1,'2020-02-06 15:09:37'),
('2002065E3BC9CB08502','2002065E3BC9CB051B4','LGEN','HALAL','halal','','','','',6,'2020-02-06 15:09:47',1,'2020-02-06 15:09:47'),
('2002065E3BC9CB085D5','2002065E3BC9CB051B4','LGID','HALAL','halal','','','','',6,'2020-02-06 15:09:47',1,'2020-02-06 15:09:47'),
('2002065E3BC9DC975C6','2002065E3BC9DC93DD9','LGEN','FREE DELIVERY > 250K','free-delivery-250k','','','','',6,'2020-02-06 15:10:04',1,'2020-02-06 15:10:04'),
('2002065E3BC9DC97662','2002065E3BC9DC93DD9','LGID','FREE DELIVERY > 250K','free-delivery-250k','','','','',6,'2020-02-06 15:10:04',1,'2020-02-06 15:10:04'),
('2002065E3BCAB6A658F','2002065E3BCAB6A272E','LGEN','LOREM IPSUM DOLOR SIT AMET','lorem-ipsum-dolor-sit-amet','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:13:42',1,'2020-02-06 15:13:42'),
('2002065E3BCAB6A661B','2002065E3BCAB6A272E','LGID','LOREM IPSUM DOLOR SIT AMET','lorem-ipsum-dolor-sit-amet','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:13:42',1,'2020-02-06 15:13:42'),
('2002065E3BCAC988DFA','2002065E3BCAC985E58','LGEN','LOREM IPSUM DOLOR SIT AMET','lorem-ipsum-dolor-sit-amet','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:14:01',1,'2020-02-06 15:14:01'),
('2002065E3BCAC988E89','2002065E3BCAC985E58','LGID','LOREM IPSUM DOLOR SIT AMET','lorem-ipsum-dolor-sit-amet','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:14:01',1,'2020-02-06 15:14:01'),
('2002065E3BCB3243947','2002065E3BCB3240FEF','LGEN','BREAK BARRIERS,','break-barriers','','','<blockquote>\r\n<h3 style=\"text-align: center;\">CHALLENGING THE NOTION OF STEREOTYPICAL BEAUTY. SADA BY CATHY SHARON IS A BEAUTY BRAND WITH A MISSION: TO HIGHLIGHT ASIAN BEAUTY IN ALL ITS FORMS.</h3>\r\n\r\n<h3 style=\"text-align: center;\">BREAK BARRIERS,<br />\r\nAND SEE ASIAN BEAUTY&nbsp;<span style=\"color:#ffffcc\">IN A DIFFERENT LIGHT</span></h3>\r\n</blockquote>\r\n','',6,'2020-02-06 15:15:46',1,'2020-02-06 15:15:46'),
('2002065E3BCB32439D5','2002065E3BCB3240FEF','LGID','BREAK BARRIERS,','break-barriers','','','<blockquote>\r\n<h3 style=\"text-align: center;\">CHALLENGING THE NOTION OF STEREOTYPICAL BEAUTY. SADA BY CATHY SHARON IS A BEAUTY BRAND WITH A MISSION: TO HIGHLIGHT ASIAN BEAUTY IN ALL ITS FORMS.</h3>\r\n\r\n<h3 style=\"text-align: center;\">BREAK BARRIERS,<br />\r\nAND SEE ASIAN BEAUTY&nbsp;<span style=\"color:#ffffcc\">IN A DIFFERENT LIGHT</span></h3>\r\n</blockquote>\r\n','',6,'2020-02-06 15:15:46',1,'2020-02-06 15:15:46'),
('2002065E3BCB6D8451C','2002065E3BCB6D8128E','LGEN','Beauty Advisor','beauty-advisor','career@sada.com','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod:</p>\r\n\r\n<ul>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n</ul>\r\n','','',6,'2020-02-06 15:16:45',1,'2020-02-06 15:16:45'),
('2002065E3BCB6D8460F','2002065E3BCB6D8128E','LGID','Beauty Advisor','beauty-advisor','career@sada.com','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod:</p>\r\n\r\n<ul>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n</ul>\r\n','','',6,'2020-02-06 15:16:45',1,'2020-02-06 15:16:45'),
('2002065E3BCB8775896','2002065E3BCB87718B8','LGEN','Team Leader','team-leader','sada@career.com','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod:</p>\r\n\r\n<ul>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n</ul>\r\n','','',6,'2020-02-06 15:17:11',1,'2020-02-06 15:17:11'),
('2002065E3BCB8775921','2002065E3BCB87718B8','LGID','Team Leader','team-leader','sada@career.com','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Enim eiusmod high life accusamus terry richardson ad squid. Food truck quinoa nesciunt laborum eiusmod:</p>\r\n\r\n<ul>\r\n	<li>non cupidatat skateboard dolor brunch</li>\r\n	<li>Food truck quinoa nesciunt laborum eiusmod</li>\r\n</ul>\r\n','','',6,'2020-02-06 15:17:11',1,'2020-02-06 15:17:11'),
('2002065E3BCB9C1BD34','2002065E3BCB9C199F5','LGEN','Careers','careers','','LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT','','',6,'2020-02-06 15:17:32',1,'2020-02-06 15:17:32'),
('2002065E3BCB9C1BE34','2002065E3BCB9C199F5','LGID','Careers','careers','','LOREM IPSUM DOLOR SIT AMET, CONSECTETUR ADIPISICING ELIT','','',6,'2020-02-06 15:17:32',1,'2020-02-06 15:17:32'),
('2002065E3BCC32ABB53','2002065E3BCC32A9576','LGEN','Anim pariatur cliche reprehenderit ?','anim-pariatur-cliche-reprehenderit','','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.</p>\r\n','','',6,'2020-02-06 15:20:02',1,'2020-02-06 15:20:10'),
('2002065E3BCC32ABBE2','2002065E3BCC32A9576','LGID','Anim pariatur cliche reprehenderit ?','anim-pariatur-cliche-reprehenderit','','<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven&#39;t heard of them accusamus labore sustainable VHS.</p>\r\n','','',6,'2020-02-06 15:20:02',1,'2020-02-06 15:20:10'),
('2002065E3BCC4B9DE54','2002065E3BCC4B9B2A3','LGEN','How to Orders','how-to-orders','','<p>Potenti vivamus proin? Bibendum, voluptate eget facilisis aut, fames, autem auctor tellus luctus, hac, mus nibh? Ipsa amet inventore sint! Quidem velit, tenetur quaerat! Sollicitudin torquent eligendi eros eiusmod habitant esse lacus officiis consequatur voluptas, vitae? Perferendis volutpat lorem, ratione, ullamcorper sociis? Primis taciti! Fringilla? Nisi modi aperiam adipiscing aut.</p>\r\n\r\n<p>Class consequuntur occaecat provident odit. Ipsa iure vel dictum et magnam cupiditate? Mauris distinctio commodi, inceptos mollis possimus curae rem, facere unde eu expedita minus. Fermentum viverra sequi. Rutrum ipsam libero cursus dictum hac anim hac. Fames rhoncus vero luctus, eos blandit atque quidem porta! Proident parturient sagittis, eum! Corporis.</p>\r\n','','',6,'2020-02-06 15:20:27',1,'2020-02-06 15:20:27'),
('2002065E3BCC4B9DEE0','2002065E3BCC4B9B2A3','LGID','How to Orders','how-to-orders','','<p>Potenti vivamus proin? Bibendum, voluptate eget facilisis aut, fames, autem auctor tellus luctus, hac, mus nibh? Ipsa amet inventore sint! Quidem velit, tenetur quaerat! Sollicitudin torquent eligendi eros eiusmod habitant esse lacus officiis consequatur voluptas, vitae? Perferendis volutpat lorem, ratione, ullamcorper sociis? Primis taciti! Fringilla? Nisi modi aperiam adipiscing aut.</p>\r\n\r\n<p>Class consequuntur occaecat provident odit. Ipsa iure vel dictum et magnam cupiditate? Mauris distinctio commodi, inceptos mollis possimus curae rem, facere unde eu expedita minus. Fermentum viverra sequi. Rutrum ipsam libero cursus dictum hac anim hac. Fames rhoncus vero luctus, eos blandit atque quidem porta! Proident parturient sagittis, eum! Corporis.</p>\r\n','','',6,'2020-02-06 15:20:27',1,'2020-02-06 15:20:27'),
('2002065E3BCCABAD22F','2002065E3BCCABAA62D','LGEN','FAQ','faq','','','','',6,'2020-02-06 15:22:03',1,'2020-02-06 15:22:03'),
('2002065E3BCCABAD2F6','2002065E3BCCABAA62D','LGID','FAQ','faq','','','','',6,'2020-02-06 15:22:03',1,'2020-02-06 15:22:03'),
('2002065E3BCD2F98E74','2002065E3BCD2F95F26','LGEN','Contact Us','contact-us','','WOMEN CAN BE ANYTHING, JUST WITH A BIT CONFIDENCE','<p>For questions about your order, please contact us at 021-789456<br />\r\nIf you have other comments, concerns or suggestions, please feel free to share them by filling out the form below.</p>\r\n','',6,'2020-02-06 15:24:15',1,'2020-02-06 15:24:15'),
('2002065E3BCD2F98F19','2002065E3BCD2F95F26','LGID','Contact Us','contact-us','','WOMEN CAN BE ANYTHING, JUST WITH A BIT CONFIDENCE','<p>For questions about your order, please contact us at 021-789456<br />\r\nIf you have other comments, concerns or suggestions, please feel free to share them by filling out the form below.</p>\r\n','',6,'2020-02-06 15:24:15',1,'2020-02-06 15:24:15'),
('2002065E3BCD613D755','2002065E3BCD613A10D','LGEN','Terms and Conditions','terms-and-conditions','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:05',1,'2020-02-06 15:25:05'),
('2002065E3BCD613D847','2002065E3BCD613A10D','LGID','Terms and Conditions','terms-and-conditions','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:05',1,'2020-02-06 15:25:05'),
('2002065E3BCD7BB0B3D','2002065E3BCD7BAD505','LGEN','Privacy Policy','privacy-policy','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:31',1,'2020-02-06 15:25:31'),
('2002065E3BCD7BB0C63','2002065E3BCD7BAD505','LGID','Privacy Policy','privacy-policy','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:31',1,'2020-02-06 15:25:31'),
('2002065E3BCD8990043','2002065E3BCD898C93A','LGEN','Disclaimer','disclaimer','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:45',1,'2020-02-06 15:46:11'),
('2002065E3BCD89900D1','2002065E3BCD898C93A','LGID','Dsiclaimer','dsiclaimer','','','<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n','',6,'2020-02-06 15:25:45',1,'2020-02-06 15:46:11'),
('2002065E3BCDAE7BBD1','2002065E3BCDAE78A82','LGEN','Terms and Conditions','terms-and-conditions','','','','',6,'2020-02-06 15:26:22',1,'2020-02-06 15:26:22'),
('2002065E3BCDAE7BC60','2002065E3BCDAE78A82','LGID','Terms and Conditions','terms-and-conditions','','','','',6,'2020-02-06 15:26:22',1,'2020-02-06 15:26:22'),
('2002065E3BCDBCCE8C8','2002065E3BCDBCCBB1E','LGEN','Privacy Policy','privacy-policy','','','','',6,'2020-02-06 15:26:36',1,'2020-02-06 15:26:36'),
('2002065E3BCDBCCE954','2002065E3BCDBCCBB1E','LGID','Privacy Policy','privacy-policy','','','','',6,'2020-02-06 15:26:36',1,'2020-02-06 15:26:36'),
('2002065E3BCDC2C2AF9','2002065E3BCDC2BFC12','LGEN','Disclaimer','disclaimer','','','','',6,'2020-02-06 15:26:42',1,'2020-02-06 15:26:42'),
('2002065E3BCDC2C2B84','2002065E3BCDC2BFC12','LGID','Disclaimer','disclaimer','','','','',6,'2020-02-06 15:26:42',1,'2020-02-06 15:26:42'),
('2002065E3BCDCD09B8B','2002065E3BCDCD077F2','LGEN','Sitemap','sitemap','','','','',6,'2020-02-06 15:26:53',1,'2020-02-06 15:26:53'),
('2002065E3BCDCD09C14','2002065E3BCDCD077F2','LGID','Sitemap','sitemap','','','','',6,'2020-02-06 15:26:53',1,'2020-02-06 15:26:53'),
('2002065E3BCE782AF40','2002065E3BCE782884D','LGEN','Shipping','shipping','','','','',6,'2020-02-06 15:29:44',1,'2020-02-06 15:29:44'),
('2002065E3BCE782AFD3','2002065E3BCE782884D','LGID','Shipping','shipping','','','','',6,'2020-02-06 15:29:44',1,'2020-02-06 15:29:44'),
('2002065E3BDA129772C','2002065E3BDA1294E27','LGEN','The World of SADA','the-world-of-sada','','A BRAND THAT STANDS TO BREAK BARRIERS AND STEREOTYPES ON ASIAN BEAUTY','','',6,'2020-02-06 16:19:14',1,'2020-02-06 16:19:14'),
('2002065E3BDA1297844','2002065E3BDA1294E27','LGID','The World of SADA','the-world-of-sada','','A BRAND THAT STANDS TO BREAK BARRIERS AND STEREOTYPES ON ASIAN BEAUTY','','',6,'2020-02-06 16:19:14',1,'2020-02-06 16:19:14');

/*Table structure for table `post_menu` */

DROP TABLE IF EXISTS `post_menu`;

CREATE TABLE `post_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produk_id` (`post_id`),
  KEY `post_kt_id` (`menu_id`),
  CONSTRAINT `post_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_menu_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_menu` */

/*Table structure for table `post_product` */

DROP TABLE IF EXISTS `post_product`;

CREATE TABLE `post_product` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) DEFAULT NULL,
  `sku` varchar(25) DEFAULT NULL,
  `price` bigint(25) DEFAULT '0',
  `price_promo` bigint(25) DEFAULT '0',
  `weight` int(11) DEFAULT '1',
  `stock` bigint(25) DEFAULT '0',
  `sold` int(11) DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_product_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_product` */

/*Table structure for table `post_product_variant` */

DROP TABLE IF EXISTS `post_product_variant`;

CREATE TABLE `post_product_variant` (
  `id` varchar(25) NOT NULL,
  `post_product_id` varchar(25) DEFAULT NULL,
  `variant_type_id` varchar(25) DEFAULT NULL,
  `variant` varchar(50) DEFAULT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `variant_type_id` (`variant_type_id`),
  KEY `post_product_id` (`post_product_id`),
  CONSTRAINT `post_product_variant_ibfk_1` FOREIGN KEY (`variant_type_id`) REFERENCES `variant_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_product_variant_ibfk_2` FOREIGN KEY (`post_product_id`) REFERENCES `post_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_product_variant` */

/*Table structure for table `post_room` */

DROP TABLE IF EXISTS `post_room`;

CREATE TABLE `post_room` (
  `id` varchar(50) NOT NULL,
  `post_id` varchar(50) DEFAULT NULL,
  `metres` varchar(25) DEFAULT NULL,
  `feet` varchar(25) DEFAULT NULL,
  `banquet` varchar(25) DEFAULT NULL,
  `classroom` varchar(25) DEFAULT NULL,
  `theatre` varchar(25) DEFAULT NULL,
  `reception` varchar(25) DEFAULT NULL,
  `ctd` datetime DEFAULT NULL,
  `ctb` datetime DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  `mdb` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_room` */

/*Table structure for table `post_tags` */

DROP TABLE IF EXISTS `post_tags`;

CREATE TABLE `post_tags` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `post_tags_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_tags_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_tags` */

insert  into `post_tags`(`id`,`post_id`,`tags_id`) values 
('2002065E3BCC3AB7671','2002065E3BCC32A9576','2002065E3BCBC875638'),
('2002065E3BCC4BB1A08','2002065E3BCC4B9B2A3','2002065E3BCBD418335');

/*Table structure for table `post_type` */

DROP TABLE IF EXISTS `post_type`;

CREATE TABLE `post_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `form` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `post_type` */

/*Table structure for table `product_brand` */

DROP TABLE IF EXISTS `product_brand`;

CREATE TABLE `product_brand` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `product_brand_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_brand_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_brand` */

/*Table structure for table `product_cart` */

DROP TABLE IF EXISTS `product_cart`;

CREATE TABLE `product_cart` (
  `id` varchar(25) NOT NULL,
  `post_product_id` varchar(25) NOT NULL,
  `user_id` varchar(25) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `product_order_id` varchar(40) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `product_cart` */

/*Table structure for table `product_order` */

DROP TABLE IF EXISTS `product_order`;

CREATE TABLE `product_order` (
  `id` varchar(40) NOT NULL,
  `order_number` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `note` text,
  `courier_code` varchar(25) NOT NULL,
  `courier_package` varchar(25) DEFAULT NULL,
  `coupon_code` varchar(25) DEFAULT NULL,
  `coupon_nominal` bigint(20) DEFAULT NULL,
  `postal_fee` bigint(20) DEFAULT NULL,
  `cart_total` bigint(20) DEFAULT NULL,
  `price_total` bigint(20) DEFAULT NULL,
  `payment_method` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `snap` varchar(50) DEFAULT NULL,
  `status_dropship` tinyint(1) DEFAULT '0',
  `registration_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `product_order` */

/*Table structure for table `product_tags` */

DROP TABLE IF EXISTS `product_tags`;

CREATE TABLE `product_tags` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `product_tags_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_tags_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_tags` */

/*Table structure for table `service_type` */

DROP TABLE IF EXISTS `service_type`;

CREATE TABLE `service_type` (
  `id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `service_type` */

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` varchar(25) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  `company_address` text,
  `city` int(11) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `brand_id` varchar(25) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_serial_number` varchar(50) DEFAULT NULL,
  `product_invoice_number` varchar(50) DEFAULT NULL,
  `product_store_name` varchar(50) DEFAULT NULL,
  `product_store_address` text,
  `product_city` int(11) DEFAULT NULL,
  `product_province` int(11) DEFAULT NULL,
  `pic_name` varchar(50) DEFAULT NULL,
  `pic_phone` varchar(50) DEFAULT NULL,
  `pic_email` varchar(50) DEFAULT NULL,
  `file_location` varchar(100) DEFAULT NULL,
  `file_thumb` varchar(100) DEFAULT NULL,
  `file_original` varchar(100) DEFAULT NULL,
  `service_type_id` varchar(25) DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `service_problem` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `services` */

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) NOT NULL DEFAULT '0',
  `tags_kt_id` varchar(25) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `file_location` varchar(50) DEFAULT NULL,
  `file_original` varchar(50) DEFAULT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_tags_kt_id` (`tags_kt_id`),
  CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags` */

insert  into `tags`(`id`,`parent`,`tags_kt_id`,`title`,`slug`,`status`,`file_location`,`file_original`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002065E3BCBC875638','0','2002055E3A76CA96A6F','Account','account',1,NULL,NULL,6,'2020-02-06 15:18:16',6,'2020-02-06 15:18:16'),
('2002065E3BCBD418335','0','2002055E3A76CA96A6F','How To Order','how-to-order',1,NULL,NULL,6,'2020-02-06 15:18:28',6,'2020-02-06 15:18:28'),
('2002065E3BCBDB83D10','0','2002055E3A76CA96A6F','Services','services',1,NULL,NULL,6,'2020-02-06 15:18:35',6,'2020-02-06 15:18:35');

/*Table structure for table `tags_kt` */

DROP TABLE IF EXISTS `tags_kt`;

CREATE TABLE `tags_kt` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `status_mode` enum('post','product','tags','other') NOT NULL,
  `status_single` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt` */

insert  into `tags_kt`(`id`,`parent`,`ordering`,`published`,`status`,`status_mode`,`status_single`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002055E3A76CA96A6F','0',1,'2020-02-05 00:00:00',1,'post',0,6,'2020-02-05 15:03:22',6,'2020-02-05 15:03:22'),
('2002055E3A773F285AF','0',2,'2020-02-05 00:00:00',1,'post',0,6,'2020-02-05 15:05:19',6,'2020-02-05 15:05:19'),
('2002055E3A775E6CE5F','0',3,'2020-02-05 00:00:00',1,'post',0,6,'2020-02-05 15:05:50',6,'2020-02-05 15:05:50'),
('2002055E3A776DF1733','0',4,'2020-02-05 00:00:00',1,'post',0,6,'2020-02-05 15:06:05',6,'2020-02-05 15:06:05'),
('2002055E3A77759C904','0',5,'2020-02-05 00:00:00',1,'post',0,6,'2020-02-05 15:06:13',6,'2020-02-05 15:06:13');

/*Table structure for table `tags_kt_image` */

DROP TABLE IF EXISTS `tags_kt_image`;

CREATE TABLE `tags_kt_image` (
  `id` varchar(25) NOT NULL,
  `tags_kt_id` varchar(25) NOT NULL,
  `file_name` text,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`tags_kt_id`),
  CONSTRAINT `tags_kt_image_ibfk_1` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt_image` */

/*Table structure for table `tags_kt_lang` */

DROP TABLE IF EXISTS `tags_kt_lang`;

CREATE TABLE `tags_kt_lang` (
  `id` varchar(25) NOT NULL,
  `tags_kt_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`tags_kt_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `tags_kt_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tags_kt_lang_ibfk_2` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt_lang` */

insert  into `tags_kt_lang`(`id`,`tags_kt_id`,`acs_lang_id`,`title`,`slug`,`description`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2002055E3A76CA9A583','2002055E3A76CA96A6F','LGEN','FAQ','faq','',6,'2020-02-05 15:03:22',1,'2020-02-05 15:03:22'),
('2002055E3A76CA9A5F9','2002055E3A76CA96A6F','LGID','FAQ','faq','',6,'2020-02-05 15:03:22',1,'2020-02-05 15:03:22'),
('2002055E3A773F2BF3D','2002055E3A773F285AF','LGEN','Category Menu','category-menu','',6,'2020-02-05 15:05:19',1,'2020-02-05 15:05:19'),
('2002055E3A773F2BFC6','2002055E3A773F285AF','LGID','Category Menu','category-menu','',6,'2020-02-05 15:05:19',1,'2020-02-05 15:05:19'),
('2002055E3A775E6F4ED','2002055E3A775E6CE5F','LGEN','Product Category','product-category','',6,'2020-02-05 15:05:50',1,'2020-02-05 15:05:50'),
('2002055E3A775E6F561','2002055E3A775E6CE5F','LGID','Product Category','product-category','',6,'2020-02-05 15:05:50',1,'2020-02-05 15:05:50'),
('2002055E3A776E0026E','2002055E3A776DF1733','LGEN','Collection','collection','',6,'2020-02-05 15:06:06',1,'2020-02-05 15:06:06'),
('2002055E3A776E002EE','2002055E3A776DF1733','LGID','Collection','collection','',6,'2020-02-05 15:06:06',1,'2020-02-05 15:06:06'),
('2002055E3A77759EE61','2002055E3A77759C904','LGEN','Color','color','',6,'2020-02-05 15:06:13',1,'2020-02-05 15:06:13'),
('2002055E3A77759EED8','2002055E3A77759C904','LGID','Color','color','',6,'2020-02-05 15:06:13',1,'2020-02-05 15:06:13');

/*Table structure for table `variant_type` */

DROP TABLE IF EXISTS `variant_type`;

CREATE TABLE `variant_type` (
  `id` varchar(25) NOT NULL,
  `name` text,
  `status` tinyint(1) DEFAULT '1',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `variant_type` */

insert  into `variant_type`(`id`,`name`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2001025E0DBFD296981','Size',1,6,'2020-01-02 17:02:58',6,'2020-01-02 17:02:58');

/* Function  structure for function  `strip_tags` */

/*!50003 DROP FUNCTION IF EXISTS `strip_tags` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `strip_tags`($str TEXT) RETURNS text CHARSET latin1
BEGIN

	    DECLARE $START, $END INT DEFAULT 1;
	    LOOP
		SET $START = LOCATE("<", $str, $START);
		IF (!$START) THEN RETURN $str; END IF;
		SET $END = LOCATE(">", $str, $START);
		IF (!$END) THEN SET $END = $START; END IF;
		SET $str = INSERT($str, $START, $END - $START + 1, "");
	    END LOOP;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
