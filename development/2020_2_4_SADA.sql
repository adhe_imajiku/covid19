/*
SQLyog Professional v12.5.1 (32 bit)
MySQL - 5.6.26 : Database - admin_sada
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`admin_sada` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `admin_sada`;

/*Table structure for table `acs_address` */

DROP TABLE IF EXISTS `acs_address`;

CREATE TABLE `acs_address` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) NOT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `long_format` text,
  `zip` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_address` */

/*Table structure for table `acs_config` */

DROP TABLE IF EXISTS `acs_config`;

CREATE TABLE `acs_config` (
  `config_id` varchar(25) NOT NULL,
  `config_group` varchar(100) NOT NULL,
  `config_name` varchar(200) NOT NULL,
  `config_value` mediumtext,
  `config_value_id` mediumtext NOT NULL,
  `config_desc` mediumtext,
  `file_location` mediumtext,
  `file_thumb` mediumtext,
  `file_original` mediumtext,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acs_config` */

insert  into `acs_config`(`config_id`,`config_group`,`config_name`,`config_value`,`config_value_id`,`config_desc`,`file_location`,`file_thumb`,`file_original`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1909275D8D8759E56D5','web_config','web_title','<p>SADA</p>\r\n','<p>SADA</p>\r\n','',NULL,NULL,NULL,1,'2019-09-27 10:51:53',6,'2020-02-04 15:31:30'),
('1909275D8D8AEEB2A60','web_config','socmed_facebook','<p>https://facebook.com/</p>\r\n','','',NULL,NULL,NULL,1,'2019-09-27 11:07:10',6,'2019-12-04 10:26:37'),
('1909275D8D8B0AC221C','web_config','socmed_instagram','<p>https://instagram.com/</p>\r\n','<p>https://instagram.com/</p>\r\n',NULL,NULL,NULL,NULL,1,'2019-09-27 11:07:38',1,'2019-10-16 09:29:12'),
('1909275D8D8B3D59E7A','web_config','socmed_youtube','<p>https://youtube.com/</p>\r\n','',NULL,NULL,NULL,NULL,1,'2019-09-27 11:08:29',1,'2019-10-16 09:33:00'),
('1910225DAE769A6D6DB','web_config','admin_mail','<p>admin@sada.com</p>\r\n','<p>admin@sada.com</p>\r\n','admin@sada.com',NULL,NULL,NULL,6,'2019-10-22 10:25:14',6,'2020-02-04 15:31:51'),
('1910225DAE772DD93F0','web_config','mail_cc','<p>testing.imajiku2@gmail.com</p>\r\n','','',NULL,NULL,NULL,6,'2019-10-22 10:27:41',6,'2020-01-22 15:33:05'),
('1911215DD64054EB734','web_config','socmed_twitter','<p>https://twitter.com</p>\r\n','','',NULL,NULL,NULL,6,'2019-11-21 14:44:20',6,'2019-11-21 14:44:20'),
('1912195DFB2247A01A1','web_config','text_socmed','<h3>Follow us! We&#39;re friendly</h3>\r\n','','',NULL,NULL,NULL,6,'2019-12-19 14:09:59',6,'2019-12-19 14:09:59'),
('1912195DFB22CBF22E2','web_config','text_subscribe','<h3>Subscribe to Our Newsletter</h3>\r\n','','',NULL,NULL,NULL,6,'2019-12-19 14:12:11',6,'2019-12-19 14:12:11'),
('2002045E392C339612C','web_config','background_login','<p>linear-gradient(to right, #b39264 11%, #000000 99%);</p>\r\n','<p>linear-gradient(to right, #b39264 11%, #000000 99%);</p>\r\n','',NULL,NULL,NULL,6,'2020-02-04 15:32:51',6,'2020-02-04 16:13:55'),
('2002045E3930F842E6C','web_config','btn_link_hover_login','<p>#D4A24C</p>\r\n','<p>#D4A24C</p>\r\n','',NULL,NULL,NULL,6,'2020-02-04 15:53:12',6,'2020-02-04 15:53:44');

/*Table structure for table `acs_groups` */

DROP TABLE IF EXISTS `acs_groups`;

CREATE TABLE `acs_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `acs_groups` */

insert  into `acs_groups`(`id`,`name`,`description`) values 
(1,'admin','Administrator'),
(2,'member','Member'),
(3,'Admin Produk','Admin Produk'),
(4,'Admin Post','admin khusus artikel dan berita\r\n');

/*Table structure for table `acs_lang` */

DROP TABLE IF EXISTS `acs_lang`;

CREATE TABLE `acs_lang` (
  `id` varchar(25) CHARACTER SET latin1 NOT NULL,
  `lang` varchar(5) CHARACTER SET latin1 NOT NULL,
  `title` varchar(100) CHARACTER SET latin1 NOT NULL,
  `slug` varchar(100) CHARACTER SET latin1 NOT NULL,
  `status` tinyint(1) NOT NULL,
  `status_default` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `acs_lang` */

insert  into `acs_lang`(`id`,`lang`,`title`,`slug`,`status`,`status_default`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('LGEN','EN','ENGLISH','english',1,1,1,'2018-02-19 00:00:00',6,'2019-10-22 12:07:43'),
('LGID','ID','INDONESIA','indonesia',1,0,1,'2018-02-19 04:50:33',1,'2018-07-11 14:23:05');

/*Table structure for table `acs_login_attempts` */

DROP TABLE IF EXISTS `acs_login_attempts`;

CREATE TABLE `acs_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `acs_login_attempts` */

/*Table structure for table `acs_portal` */

DROP TABLE IF EXISTS `acs_portal`;

CREATE TABLE `acs_portal` (
  `id` varchar(25) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal` */

insert  into `acs_portal`(`id`,`title`,`description`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1910075D9AE8C77B958','admin','Admin',1,1,'2019-10-07 14:27:03',6,'2020-01-29 10:38:56');

/*Table structure for table `acs_portal_menu` */

DROP TABLE IF EXISTS `acs_portal_menu`;

CREATE TABLE `acs_portal_menu` (
  `id` varchar(25) NOT NULL,
  `portal_id` varchar(25) NOT NULL,
  `parent` varchar(25) NOT NULL DEFAULT '0',
  `link` text NOT NULL,
  `description` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal_menu` */

/*Table structure for table `acs_portal_previleges` */

DROP TABLE IF EXISTS `acs_portal_previleges`;

CREATE TABLE `acs_portal_previleges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `list` tinyint(1) NOT NULL DEFAULT '0',
  `add` tinyint(1) DEFAULT '0',
  `edit` tinyint(1) DEFAULT '0',
  `delete` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;

/*Data for the table `acs_portal_previleges` */

insert  into `acs_portal_previleges`(`id`,`group_id`,`menu_id`,`list`,`add`,`edit`,`delete`) values 
(103,3,'1912185DF98252958B0',0,1,1,1),
(104,3,'1912185DF98256171B4',1,1,1,1),
(105,3,'1912185DF98259D1A67',1,1,1,1),
(106,3,'1912185DF982AF5AE59',1,1,1,1),
(107,3,'1912185DF982BF2E2B8',1,1,1,1),
(108,3,'1912185DF982C950CA0',1,1,1,1),
(109,3,'1912185DF982D2ECBD6',1,1,1,1),
(110,3,'1912185DF982DE2D5F3',1,1,1,1),
(111,3,'1912185DF982E6C0E2C',1,1,1,1),
(112,3,'1912185DF9831612747',1,1,1,1),
(113,3,'1912185DF9832DD1E4F',1,1,1,1),
(114,3,'1912185DF98347DBA6D',1,1,1,1),
(115,3,'1912185DF9835B603E4',1,1,1,1),
(116,3,'1912185DF98371E94DE',1,1,1,1),
(117,3,'1912185DF9838C62DC5',1,1,1,1),
(118,3,'1912185DF983BDF2925',1,1,1,1),
(119,3,'1912185DF983D281175',1,1,1,1),
(120,3,'1912185DF983F2AEEF4',1,1,1,1),
(121,3,'1912185DF9841ED34E4',1,1,1,1),
(122,3,'1912185DF9844950D3A',1,1,1,1),
(123,3,'1912185DF984FA7F533',1,1,1,1),
(124,3,'1912185DF98507BDD83',1,1,1,1),
(125,3,'1912185DF98585908AE',1,1,1,1),
(126,3,'1912185DF985A88D60A',1,1,1,1),
(127,3,'1912185DF985C490138',1,1,1,1),
(128,3,'1912185DF985D6F1F32',1,1,1,1),
(129,3,'1912185DF985F155869',1,1,1,1),
(130,3,'1912185DF985FDE5D9F',1,1,1,1),
(131,3,'1912185DF98618091AD',1,1,1,1),
(132,3,'1912185DF986304DDA1',1,1,1,1),
(133,3,'1912185DF98690C8FA5',1,1,1,1),
(134,3,'1912185DF986A6700B4',1,1,1,1),
(135,3,'1912185DF987023CC3A',1,1,1,1),
(136,3,'1912185DF987141323F',1,1,1,1),
(137,3,'1912185DF987262FE31',1,1,1,1),
(138,3,'1912185DF9876348180',1,1,1,1),
(139,3,'1912195DFAF689B8322',1,1,1,1),
(140,3,'1912195DFAF6BC991C7',1,1,1,1),
(141,3,'2001025E0D71F255709',1,1,1,1),
(142,3,'2001075E142E218A025',1,1,1,1),
(143,3,'2001085E152FE2811FB',1,1,1,1),
(144,3,'2001085E15302B7D589',1,1,1,1),
(145,3,'2001085E15306149976',1,1,1,1),
(146,3,'2001175E215F36889DE',1,1,1,1),
(147,3,'2001205E24993BBD939',1,1,1,1),
(148,3,'2001205E249BD4696A7',1,1,1,1),
(149,3,'2001205E249BEFC58CB',1,1,1,1),
(150,3,'2001225E27A75AECB24',1,1,1,1),
(151,3,'2001225E27FA3BD6214',1,1,0,1),
(152,3,'2001235E296617442DE',1,1,1,1),
(153,1,'1912185DF98252958B0',1,1,1,1),
(154,1,'1912185DF98256171B4',1,1,1,1),
(155,1,'1912185DF98259D1A67',1,1,1,1),
(156,1,'1912185DF982AF5AE59',1,1,1,1),
(157,1,'1912185DF982BF2E2B8',1,1,1,1),
(158,1,'1912185DF982C950CA0',1,1,1,1),
(159,1,'1912185DF982D2ECBD6',1,1,1,1),
(160,1,'1912185DF982DE2D5F3',1,1,1,1),
(161,1,'1912185DF982E6C0E2C',1,1,1,1),
(162,1,'1912185DF9831612747',1,1,1,1),
(163,1,'1912185DF9832DD1E4F',1,1,1,1),
(164,1,'1912185DF98347DBA6D',1,1,1,1),
(165,1,'1912185DF9835B603E4',1,1,1,1),
(166,1,'1912185DF98371E94DE',1,1,1,1),
(167,1,'1912185DF9838C62DC5',1,1,1,1),
(168,1,'1912185DF983BDF2925',1,1,1,1),
(169,1,'1912185DF983D281175',1,1,1,1),
(170,1,'1912185DF983F2AEEF4',1,1,1,1),
(171,1,'1912185DF9841ED34E4',1,1,1,1),
(172,1,'1912185DF9844950D3A',1,1,1,1),
(173,1,'1912185DF984FA7F533',1,1,1,1),
(174,1,'1912185DF98507BDD83',1,1,1,1),
(175,1,'1912185DF98585908AE',1,1,1,1),
(176,1,'1912185DF985A88D60A',1,1,1,1),
(177,1,'1912185DF985C490138',1,1,1,1),
(178,1,'1912185DF985D6F1F32',1,1,1,1),
(179,1,'1912185DF985F155869',1,1,1,1),
(180,1,'1912185DF985FDE5D9F',1,1,1,1),
(181,1,'1912185DF98618091AD',1,1,1,1),
(182,1,'1912185DF986304DDA1',1,1,1,1),
(183,1,'1912185DF98690C8FA5',1,1,1,1),
(184,1,'1912185DF986A6700B4',1,1,1,1),
(185,1,'1912185DF987023CC3A',1,1,1,1),
(186,1,'1912185DF987141323F',1,1,1,1),
(187,1,'1912185DF987262FE31',1,1,1,1),
(188,1,'1912185DF9876348180',1,1,1,1),
(189,1,'1912195DFAF689B8322',1,1,1,1),
(190,1,'1912195DFAF6BC991C7',1,1,1,1),
(191,1,'2001025E0D71F255709',1,1,1,1),
(192,1,'2001075E142E218A025',1,1,1,1),
(193,1,'2001085E152FE2811FB',1,1,1,1),
(194,1,'2001085E15302B7D589',1,1,1,1),
(195,1,'2001085E15306149976',1,1,1,1),
(196,1,'2001175E215F36889DE',1,1,1,1),
(197,1,'2001205E24993BBD939',1,1,1,1),
(198,1,'2001205E249BD4696A7',1,1,1,1),
(199,1,'2001205E249BEFC58CB',1,1,1,1),
(200,1,'2001225E27A75AECB24',1,1,1,1),
(201,1,'2001225E27FA3BD6214',1,1,1,1),
(202,1,'2001235E296617442DE',1,1,1,1),
(203,4,'1912185DF98252958B0',0,0,0,0),
(204,4,'1912185DF98256171B4',0,0,0,0),
(205,4,'1912185DF98259D1A67',0,0,0,0),
(206,4,'1912185DF982AF5AE59',0,0,0,0),
(207,4,'1912185DF982BF2E2B8',1,1,1,1),
(208,4,'1912185DF982C950CA0',0,0,0,0),
(209,4,'1912185DF982D2ECBD6',0,0,0,0),
(210,4,'1912185DF982DE2D5F3',0,0,0,0),
(211,4,'1912185DF982E6C0E2C',0,0,0,0),
(212,4,'1912185DF9831612747',0,0,0,0),
(213,4,'1912185DF9832DD1E4F',0,0,0,0),
(214,4,'1912185DF98347DBA6D',0,0,0,0),
(215,4,'1912185DF9835B603E4',0,0,0,0),
(216,4,'1912185DF98371E94DE',0,0,0,0),
(217,4,'1912185DF9838C62DC5',0,0,0,0),
(218,4,'1912185DF983BDF2925',0,0,0,0),
(219,4,'1912185DF983D281175',0,0,0,0),
(220,4,'1912185DF983F2AEEF4',0,0,0,0),
(221,4,'1912185DF9841ED34E4',1,1,1,1),
(222,4,'1912185DF9844950D3A',0,0,0,0),
(223,4,'1912185DF984FA7F533',0,0,0,0),
(224,4,'1912185DF98507BDD83',0,0,0,0),
(225,4,'1912185DF98585908AE',1,1,1,0),
(226,4,'1912185DF985A88D60A',0,0,0,0),
(227,4,'1912185DF985C490138',0,0,0,0),
(228,4,'1912185DF985D6F1F32',0,0,0,0),
(229,4,'1912185DF985F155869',1,1,1,0),
(230,4,'1912185DF985FDE5D9F',1,1,1,0),
(231,4,'1912185DF98618091AD',0,0,0,0),
(232,4,'1912185DF986304DDA1',0,0,0,0),
(233,4,'1912185DF98690C8FA5',0,0,0,0),
(234,4,'1912185DF986A6700B4',0,0,0,0),
(235,4,'1912185DF987023CC3A',0,0,0,0),
(236,4,'1912185DF987141323F',0,0,0,0),
(237,4,'1912185DF987262FE31',0,0,0,0),
(238,4,'1912195DFAF689B8322',0,0,0,0),
(239,4,'1912195DFAF6BC991C7',0,0,0,0),
(240,4,'2001025E0D71F255709',0,0,0,0),
(241,4,'2001075E142E218A025',0,0,0,0),
(242,4,'2001085E152FE2811FB',0,0,0,0),
(243,4,'2001085E15302B7D589',0,0,0,0),
(244,4,'2001085E15306149976',0,0,0,0),
(245,4,'2001175E215F36889DE',0,0,0,0),
(246,4,'2001205E24993BBD939',0,0,0,0),
(247,4,'2001205E249BD4696A7',0,0,0,0),
(248,4,'2001205E249BEFC58CB',0,0,0,0),
(249,4,'2001225E27A75AECB24',0,0,0,0),
(250,4,'2001225E27FA3BD6214',0,0,0,0),
(251,4,'2001235E296617442DE',0,0,0,0);

/*Table structure for table `acs_users` */

DROP TABLE IF EXISTS `acs_users`;

CREATE TABLE `acs_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `oauth_provider` enum('facebook','google','twitter','') NOT NULL,
  `oauth_uid` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT '1',
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `profile` text,
  `gender` varchar(100) DEFAULT NULL,
  `extra_payment` tinyint(1) DEFAULT NULL,
  `locale` varchar(10) DEFAULT NULL,
  `file_original` varchar(255) DEFAULT NULL,
  `file_thumb` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `address` text,
  `bank_number` varchar(50) DEFAULT NULL,
  `bank_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `acs_users` */

insert  into `acs_users`(`id`,`oauth_provider`,`oauth_uid`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_code`,`forgotten_password_code`,`forgotten_password_time`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`,`province`,`city`,`subdistrict`,`dob`,`zip`,`profile`,`gender`,`extra_payment`,`locale`,`file_original`,`file_thumb`,`file_name`,`picture`,`link`,`address`,`bank_number`,`bank_name`) values 
(6,'facebook',NULL,'182.1.79.6','user_5da904f1b954c','$2y$08$XIP.2oESbcSXYaV7TickXeVnTmWYMwStRa6oGm0Nw1x5nCjppyqsi',NULL,'admin@admin.com',NULL,NULL,NULL,NULL,1571357937,1580806367,1,'admin','admin',NULL,'0897379434',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `acs_users_groups` */

DROP TABLE IF EXISTS `acs_users_groups`;

CREATE TABLE `acs_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `acs_users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `acs_groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `acs_users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `acs_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `acs_users_groups` */

insert  into `acs_users_groups`(`id`,`user_id`,`group_id`) values 
(5,6,1);

/*Table structure for table `book_newsletter` */

DROP TABLE IF EXISTS `book_newsletter`;

CREATE TABLE `book_newsletter` (
  `id` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ip_address` varchar(30) DEFAULT NULL,
  `client_browser` varchar(100) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctd` datetime NOT NULL,
  `mdd` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `book_newsletter` */

/*Table structure for table `career_applicants` */

DROP TABLE IF EXISTS `career_applicants`;

CREATE TABLE `career_applicants` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `city` varchar(50) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `address` text NOT NULL,
  `curriculum_vitae` varchar(100) NOT NULL,
  `cv_link` varchar(100) DEFAULT NULL,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_career` (`post_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `career_applicants` */

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` varchar(25) NOT NULL,
  `subject_id` varchar(25) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `region` varchar(25) DEFAULT NULL,
  `message` text,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `contact` */

/*Table structure for table `contact_newsletter` */

DROP TABLE IF EXISTS `contact_newsletter`;

CREATE TABLE `contact_newsletter` (
  `id` varchar(25) NOT NULL,
  `email` varchar(150) NOT NULL,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `contact_newsletter` */

/*Table structure for table `contact_subject` */

DROP TABLE IF EXISTS `contact_subject`;

CREATE TABLE `contact_subject` (
  `id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `contact_subject` */

insert  into `contact_subject`(`id`,`title`,`slug`,`email`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1912195DFAF85EE9147','Product','product','adherifai6@gmail.com',1,'6','2019-12-19 11:11:10','6','2020-01-07 15:26:27');

/*Table structure for table `content_kt` */

DROP TABLE IF EXISTS `content_kt`;

CREATE TABLE `content_kt` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `content_name` varchar(255) NOT NULL,
  `content_type` enum('input','textarea','editor','tagging') NOT NULL,
  `content_format` text,
  `published` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt` */

/*Table structure for table `content_kt_image` */

DROP TABLE IF EXISTS `content_kt_image`;

CREATE TABLE `content_kt_image` (
  `id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `file_name` mediumtext,
  `file_location` mediumtext,
  `file_thumb` mediumtext,
  `file_original` mediumtext,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tags_kt_id` (`content_kt_id`) USING BTREE,
  CONSTRAINT `content_kt_image_ibfk_1` FOREIGN KEY (`content_kt_id`) REFERENCES `content_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt_image` */

/*Table structure for table `content_kt_lang` */

DROP TABLE IF EXISTS `content_kt_lang`;

CREATE TABLE `content_kt_lang` (
  `id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_acs_lang_id` (`acs_lang_id`) USING BTREE,
  KEY `fk_tags_kt_id` (`content_kt_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `content_kt_lang` */

/*Table structure for table `content_kt_menu` */

DROP TABLE IF EXISTS `content_kt_menu`;

CREATE TABLE `content_kt_menu` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `content_kt_id` varchar(25) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `content_kt_Id` (`content_kt_id`),
  KEY `menu_id` (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tabel ini digunakan untuk setting konten berdasarkan menu';

/*Data for the table `content_kt_menu` */

/*Table structure for table `coupon_category` */

DROP TABLE IF EXISTS `coupon_category`;

CREATE TABLE `coupon_category` (
  `id` varchar(25) NOT NULL,
  `coupon_name` tinytext,
  `coupon_postal_free` tinyint(1) DEFAULT '0',
  `coupon_unit` enum('percentage','nominal') DEFAULT NULL,
  `coupon_nominal` bigint(50) DEFAULT '0',
  `coupon_minimum_buy` bigint(50) DEFAULT '0',
  `coupon_maximum_nominal` bigint(50) DEFAULT '0',
  `status` tinyint(1) DEFAULT NULL,
  `ctd` date DEFAULT NULL,
  `ctb` varchar(25) DEFAULT NULL,
  `mdd` date DEFAULT NULL,
  `mdb` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `coupon_category` */

/*Table structure for table `coupon_code` */

DROP TABLE IF EXISTS `coupon_code`;

CREATE TABLE `coupon_code` (
  `id` varchar(25) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `coupon_category_id` varchar(25) NOT NULL,
  `coupon_code` varchar(25) NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `ctd` date DEFAULT NULL,
  `ctb` varchar(25) DEFAULT NULL,
  `mdd` date DEFAULT NULL,
  `mdb` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `coupon_code` */

/*Table structure for table `courier` */

DROP TABLE IF EXISTS `courier`;

CREATE TABLE `courier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '1',
  `ctb` tinyint(1) DEFAULT '1',
  `ctd` datetime NOT NULL,
  `mdb` tinyint(1) DEFAULT '1',
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Data for the table `courier` */

insert  into `courier`(`id`,`code`,`name`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
(1,'jne','Jalur Nugraha Ekakurir (JNE)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(2,'pos','POS Indonesia (POS)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(3,'tiki','Citra Van Titipan Kilat (TIKI)',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(4,'rpx','RPX Holding (RPX)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(5,'esl','Eka Sari Lorena (ESL)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(6,'pcp','Priority Cargo and Package (PCP)',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(7,'pandu','Pandu Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(8,'wahana','Wahana Prestasi Logistik',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(9,'sicepat','SiCepat Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(11,'pahala','Pahala Kencana Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(12,'cahaya','Cahaya Ekspress Logistik',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(13,'sap','SAP Express Courier',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(14,'jet','JET Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(15,'indah','Indah Logistic',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(16,'dse','21 Express',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(17,'slis','Synergy First Logistics',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(18,'ncs','Nusantara Card Semesta',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(19,'star','Star Cargo',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(20,'ninja','Ninja Xpress',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(21,'lion','Lion Parcel',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(22,'idl','Indotama Domestik Lestari',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(23,'rex','Royal Express Indonesia',0,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00'),
(24,'jnt','J&T Express',1,1,'0000-00-00 00:00:00',1,'0000-00-00 00:00:00');

/*Table structure for table `inquiry_form` */

DROP TABLE IF EXISTS `inquiry_form`;

CREATE TABLE `inquiry_form` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `message` text,
  `ip_address` varchar(30) NOT NULL,
  `client_browser` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `city` varchar(250) DEFAULT NULL,
  `address` text NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `inquiry_form` */

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `status_mode` enum('post','product','tags','other') NOT NULL,
  `status_single` tinyint(1) NOT NULL DEFAULT '0',
  `is_featured` int(1) DEFAULT '0',
  `filter_author` tinyint(1) DEFAULT '0',
  `filter_ordering` tinyint(1) NOT NULL DEFAULT '1',
  `filter_parent` tinyint(1) NOT NULL DEFAULT '1',
  `filter_title_sub` tinyint(1) DEFAULT '0',
  `filter_shortdesc` tinyint(1) NOT NULL DEFAULT '1',
  `filter_note` tinyint(1) DEFAULT '0',
  `filter_description` tinyint(1) NOT NULL DEFAULT '1',
  `filter_dashboard` int(11) NOT NULL DEFAULT '0',
  `filter_image` tinyint(1) NOT NULL DEFAULT '1',
  `filter_tags` tinyint(1) NOT NULL DEFAULT '1',
  `filter_featured` tinyint(1) DEFAULT '0',
  `filter_promo` tinyint(1) DEFAULT '0',
  `filter_icon` tinyint(1) DEFAULT '0',
  `filter_event_start` tinyint(1) DEFAULT '0',
  `filter_event_end` tinyint(1) DEFAULT '0',
  `filter_product_tags` tinyint(1) DEFAULT '0',
  `required_title` tinyint(1) DEFAULT '1',
  `required_title_sub` tinyint(1) DEFAULT '0',
  `required_shortdesc` tinyint(1) DEFAULT '0',
  `required_description` tinyint(1) DEFAULT '0',
  `required_note` tinyint(1) DEFAULT '0',
  `filter_brand` tinyint(1) DEFAULT '0',
  `recomended_image_size` varchar(50) DEFAULT NULL,
  `menu_cms_link` varchar(255) DEFAULT NULL,
  `menu_site_link` varchar(255) DEFAULT NULL,
  `published` datetime DEFAULT NULL,
  `text_product_tags` varchar(50) DEFAULT 'Product Tags',
  `text_brand` varchar(50) DEFAULT 'Brand',
  `text_ordering` varchar(50) DEFAULT 'Ordering',
  `text_parent` varchar(50) DEFAULT 'Parent',
  `text_title` varchar(50) DEFAULT 'Title',
  `text_title_sub` varchar(50) DEFAULT 'Subtitle',
  `text_shortdesc` varchar(50) DEFAULT 'Short Description',
  `text_note` varchar(50) DEFAULT 'Note',
  `text_description` varchar(50) DEFAULT 'Description',
  `text_image` varchar(50) DEFAULT 'Image',
  `text_promo` varchar(50) DEFAULT 'Promotion',
  `text_tags` varchar(50) DEFAULT 'Tags',
  `text_featured` varchar(50) DEFAULT 'Featured',
  `text_icon` varchar(50) DEFAULT 'Icon',
  `text_event_start` varchar(50) DEFAULT 'Event Start',
  `text_author` varchar(50) DEFAULT 'Author',
  `text_event_end` varchar(50) DEFAULT 'Event End',
  `disable_add` tinyint(1) DEFAULT '0',
  `disable_delete` tinyint(1) DEFAULT '0',
  `disable_status` tinyint(1) DEFAULT '0',
  `disabled_gallery` tinyint(1) DEFAULT '1',
  `mode_title_sub` enum('text','textarea limit','editor') DEFAULT 'text',
  `mode_shortdesc` enum('text','textarea limit','editor') DEFAULT 'textarea limit',
  `mode_description` enum('text','textarea limit','editor') DEFAULT 'editor',
  `mode_note` enum('text','textarea limit','editor') DEFAULT 'text',
  `mode_tags` enum('single','multiple') DEFAULT NULL,
  `mode_image` enum('single','multiple') DEFAULT 'single',
  `image_qty` int(11) DEFAULT '1',
  `tags_kt_id` varchar(25) DEFAULT NULL,
  `tags_brand_id` varchar(25) DEFAULT NULL,
  `tags_product_id` varchar(25) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `hit` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(25) DEFAULT NULL,
  `ctd` datetime NOT NULL,
  `ctb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu` */

insert  into `menu`(`id`,`parent`,`ordering`,`status`,`status_mode`,`status_single`,`is_featured`,`filter_author`,`filter_ordering`,`filter_parent`,`filter_title_sub`,`filter_shortdesc`,`filter_note`,`filter_description`,`filter_dashboard`,`filter_image`,`filter_tags`,`filter_featured`,`filter_promo`,`filter_icon`,`filter_event_start`,`filter_event_end`,`filter_product_tags`,`required_title`,`required_title_sub`,`required_shortdesc`,`required_description`,`required_note`,`filter_brand`,`recomended_image_size`,`menu_cms_link`,`menu_site_link`,`published`,`text_product_tags`,`text_brand`,`text_ordering`,`text_parent`,`text_title`,`text_title_sub`,`text_shortdesc`,`text_note`,`text_description`,`text_image`,`text_promo`,`text_tags`,`text_featured`,`text_icon`,`text_event_start`,`text_author`,`text_event_end`,`disable_add`,`disable_delete`,`disable_status`,`disabled_gallery`,`mode_title_sub`,`mode_shortdesc`,`mode_description`,`mode_note`,`mode_tags`,`mode_image`,`image_qty`,`tags_kt_id`,`tags_brand_id`,`tags_product_id`,`link`,`hit`,`icon`,`ctd`,`ctb`,`mdd`,`mdb`) values 
('1912185DF98252958B0','0',1,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:14',6,'2019-12-18 08:39:05',6),
('1912185DF98256171B4','0',2,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:18',6,'2019-12-18 08:49:10',6),
('1912185DF98259D1A67','0',3,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:35:21',6,'2019-12-18 08:36:40',6),
('1912185DF982AF5AE59','2001025E0D71F255709',4,1,'product',0,1,0,0,0,0,1,0,1,0,1,1,1,0,0,0,0,1,1,0,0,0,0,1,'890 x 784 pixel','mod_posted/index/product-item','','2020-01-30 00:00:00','Product Tags','Brand','Ordering','Parent','Product Name','Subtitle','Description','Note','Specification','Image','Promotion','Category','Recommended','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','multiple',12,'2001025E0D632F4F3B9','2001205E249B983B8DB','2001205E249BAB61E64',NULL,0,NULL,'2019-12-18 08:36:47',6,'2020-01-30 12:00:04',6),
('1912185DF982BF2E2B8','0',5,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:03',6,'2019-12-18 08:37:03',6),
('1912185DF982C950CA0','0',6,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:13',6,'2019-12-18 08:37:13',6),
('1912185DF982D2ECBD6','0',7,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:22',6,'2019-12-18 08:37:22',6),
('1912185DF982DE2D5F3','0',99,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_config','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:34',6,'2019-12-18 08:57:28',6),
('1912185DF982E6C0E2C','0',9,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:37:42',6,'2019-12-18 08:37:42',6),
('1912185DF9831612747','1912185DF98256171B4',99,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_services','contact/book-a-services','2019-12-19 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:38:30',6,'2019-12-19 13:31:21',6),
('1912185DF9832DD1E4F','1912185DF98252958B0',11,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_contact','contact-us','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:38:53',6,'2019-12-18 15:38:24',6),
('1912185DF98347DBA6D','1912185DF98256171B4',12,1,'other',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_members','','2020-01-21 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:39:19',6,'2020-01-21 09:36:43',6),
('1912185DF9835B603E4','1912185DF98256171B4',13,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_address','','2020-01-21 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:39:39',6,'2020-01-21 10:14:59',6),
('1912185DF98371E94DE','1912185DF98259D1A67',14,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_inquiry','','2020-01-21 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:40:01',6,'2020-01-21 10:26:49',6),
('1912185DF9838C62DC5','1912185DF98259D1A67',15,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_orders','','2020-01-22 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:40:28',6,'2020-01-22 08:38:09',6),
('1912185DF983BDF2925','1912185DF982C950CA0',16,1,'post',0,1,0,1,0,1,1,0,1,0,1,0,0,0,0,0,0,0,1,1,1,0,0,0,'1440 x 555','mod_posted/index/home-banner','','2020-01-24 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Button Text','Link ( must be include http/https )','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:41:17',6,'2020-01-24 14:07:12',6),
('1912185DF983D281175','1912185DF982C950CA0',17,1,'post',0,1,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'1000 x 260 px','mod_posted/index/page-banner','','2020-01-22 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','text','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:41:38',6,'2020-01-22 13:55:25',6),
('1912185DF983F2AEEF4','1912185DF982C950CA0',18,1,'post',0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,'','mod_posted/index/mastrada-on-youtube','','2020-01-17 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Link','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:42:10',6,'2020-01-17 14:03:58',6),
('1912185DF9841ED34E4','1912185DF982BF2E2B8',30,1,'post',0,1,0,1,0,0,1,0,1,0,1,0,1,1,0,0,0,0,1,0,0,0,0,0,'540 x 255 pixel','mod_posted/index/promotions','','2020-01-27 00:00:00','Product Tags','Brand','Ordering','Parent','Promo title','Subtitle','Description','Note','Terms & Condition','Image','Promotion','Tags','Pop Up Promotion','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:42:54',6,'2020-01-27 10:35:40',6),
('1912185DF9844950D3A','1912185DF982D2ECBD6',21,1,'post',0,1,0,1,0,1,1,1,1,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/branch-office','','2020-01-23 00:00:00','Product Tags','Brand','Ordering','Parent','Office','Address','Phone','EMAIL','FAX','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'editor','text','text','text','single','single',1,'1912315E0AB6C14C998','All','All',NULL,0,NULL,'2019-12-18 08:43:37',6,'2020-01-23 15:53:51',6),
('1912185DF984FA7F533','1912185DF982BF2E2B8',28,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:46:34',6,'2019-12-18 08:46:34',6),
('1912185DF98507BDD83','1912185DF982BF2E2B8',29,1,'post',0,1,0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'540 x 331','mod_posted/index/services','about/services','2020-01-22 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:46:47',6,'2020-01-22 13:47:16',6),
('1912185DF98585908AE','1912185DF982BF2E2B8',31,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:48:53',6,'2019-12-18 08:48:53',6),
('1912185DF985A88D60A','1912185DF982BF2E2B8',32,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','','','2019-12-18 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-18 08:49:28',6,'2019-12-18 08:49:28',6),
('1912185DF985C490138','1912185DF984FA7F533',33,1,'post',0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/about-mastrada','about/about-mastrada','2020-01-17 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:49:56',6,'2020-01-17 14:18:27',6),
('1912185DF985D6F1F32','1912185DF984FA7F533',34,1,'post',0,1,0,1,0,0,1,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/board-management','about/board-management','2020-01-17 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Position','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:50:14',6,'2020-01-17 14:32:42',6),
('1912185DF985F155869','1912185DF98585908AE',35,1,'post',0,1,0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'1063 x 580 pixel','mod_posted/index/news','news','2020-01-20 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Author',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:50:41',6,'2020-01-20 15:37:34',6),
('1912185DF985FDE5D9F','1912185DF98585908AE',36,1,'post',0,1,0,0,0,1,1,1,1,0,1,0,0,0,0,1,1,0,1,0,0,0,0,0,'708 x 465 pixel','mod_posted/index/events','events','2020-01-24 00:00:00','Product Tags','Brand','Ordering','Parent','Event Name','Description','Location','URL Map','Nominal (If free fill 0)','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,0,'editor','editor','text','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:50:53',6,'2020-01-24 15:21:32',6),
('1912185DF98618091AD','1912185DF985A88D60A',37,1,'post',0,1,0,0,0,0,1,0,0,0,1,0,1,0,0,0,0,0,1,0,0,0,0,0,'500 x 333 pixel','mod_posted/index/clients','about/customer-clients','2020-01-20 00:00:00','Product Tags','Brand','Ordering','Parent','Client Name','Subtitle','Location','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:51:20',6,'2020-01-20 15:39:33',6),
('1912185DF986304DDA1','1912185DF985A88D60A',38,1,'post',0,1,1,0,0,1,1,1,1,0,1,0,1,0,0,0,0,0,1,1,0,1,1,0,'','mod_posted/index/client-success-story','about/customer-testimonial','2020-01-24 00:00:00','Product Tags','Brand','Ordering','Client','Title','Short Story','Video Link','Company','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Author',0,0,0,1,'textarea limit','text','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:51:44',6,'2020-01-24 15:25:02',6),
('1912185DF98690C8FA5','1912185DF982BF2E2B8',39,1,'post',0,1,0,0,0,1,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/careers','careers','2020-01-06 00:00:00','Product Tags','Brand','Ordering','Parent','Career Name','Email Career PIC','Description','Note','Job Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','single',1,'All',NULL,NULL,NULL,0,NULL,'2019-12-18 08:53:20',6,'2020-01-06 14:11:09',6),
('1912185DF986A6700B4','1912185DF982BF2E2B8',40,1,'post',0,1,0,1,0,0,1,0,0,0,0,1,0,0,1,0,0,0,1,0,0,0,0,0,'','mod_posted/index/faq','faq','2020-01-22 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','editor','editor','text','single','single',1,'1912275E05CBB25B721','All','All',NULL,0,NULL,'2019-12-18 08:53:42',6,'2020-01-22 14:28:54',6),
('1912185DF987023CC3A','1912185DF982BF2E2B8',42,1,'post',0,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_posted/index/other-pages','','2020-01-23 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:14',6,'2020-01-23 14:45:15',6),
('1912185DF987141323F','1912185DF982E6C0E2C',43,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_users','','2020-01-29 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:32',6,'2020-01-29 11:55:02',6),
('1912185DF987262FE31','1912185DF982E6C0E2C',44,1,'post',0,1,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_acs_group','','2020-01-29 00:00:00','Product Tags','Brand','Ordering','Parent','Title','Subtitle','Short Description','Note','Description','Image','Promotion','Tags','Featured','Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,'All','All','All',NULL,0,NULL,'2019-12-18 08:55:50',6,'2020-01-29 11:55:17',6),
('1912195DFAF689B8322','1912185DF98252958B0',100,1,'post',0,0,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_contact_subject','','2019-12-19 00:00:00','Product Tags','Brand','','','Title','Subtitle','','Note','','','Promotion','',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-19 11:03:21',6,'2019-12-19 11:06:40',6),
('1912195DFAF6BC991C7','1912185DF98256171B4',101,1,'post',0,0,0,1,1,0,1,0,1,0,1,1,0,0,0,0,0,0,1,0,0,0,0,0,'','mod_service_type','','2019-12-19 00:00:00','Product Tags','Brand','','','Title','Subtitle','','Note','','','Promotion','',NULL,'Icon','Event Start','Author','Event End',0,0,0,1,'text','textarea limit','editor','text','single','single',1,NULL,NULL,NULL,NULL,0,NULL,'2019-12-19 11:04:12',6,'2019-12-19 13:23:20',6),
('2001025E0D71F255709','0',4,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,0,0,0,0,1,0,0,0,0,0,'','','','2020-01-02 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Icon','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-02 11:30:42',6,'2020-01-02 11:31:35',6),
('2001075E142E218A025','1912185DF98252958B0',103,1,'post',0,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'1000 x 505 pixel','mod_posted/index/contact-image','','2020-01-27 00:00:00','Product Tags','Brand','','','Title','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-07 14:07:13',6,'2020-01-27 16:32:47',6),
('2001085E152FE2811FB','0',4,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','','','2020-01-08 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-08 08:26:58',6,'2020-01-08 08:27:38',6),
('2001085E15302B7D589','2001085E152FE2811FB',104,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','mod_coupon_category','','2020-01-08 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-08 08:28:11',6,'2020-01-08 08:29:24',6),
('2001085E15306149976','2001085E152FE2811FB',105,1,'post',0,1,0,1,1,1,1,1,1,0,1,1,1,0,1,0,0,0,1,0,0,0,0,0,'','mod_coupon','','2020-01-08 00:00:00','Product Tags','Brand','','','','','','','','','Promotion','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All',NULL,NULL,NULL,0,NULL,'2020-01-08 08:29:05',6,'2020-01-08 08:29:05',6),
('2001175E215F36889DE','1912185DF984FA7F533',1,1,'post',0,1,0,0,1,0,1,0,1,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,'1110 x 192 px','mod_posted/index/history','','2020-01-17 00:00:00','Product Tags','Brand','','','Title','','Short Description','','Description','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','textarea limit','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-17 14:16:06',6,'2020-01-17 14:16:26',6),
('2001205E24993BBD939','1912185DF982D2ECBD6',106,1,'tags',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/branch-office-category','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-20 01:00:27',6,'2020-01-23 15:49:29',6),
('2001205E249BD4696A7','2001025E0D71F255709',107,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/brand','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-20 01:11:32',6,'2020-01-23 08:43:20',6),
('2001205E249BEFC58CB','2001025E0D71F255709',108,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/product-tags','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-20 01:11:59',6,'2020-01-23 08:43:57',6),
('2001225E27A75AECB24','1912185DF982D2ECBD6',109,1,'other',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_courier','','2020-01-22 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-22 08:37:30',6,'2020-01-22 08:37:30',6),
('2001225E27FA3BD6214','1912185DF982D2ECBD6',110,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags_single/index/faq','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-22 14:31:07',6,'2020-01-23 15:19:54',6),
('2001235E296617442DE','2001025E0D71F255709',111,1,'post',0,1,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,'','mod_tags/index/product','','2020-01-23 00:00:00','Product Tags','Brand','','','','','','','','','Featured','','Featured','Featured','Event Start','Author','Event End',1,1,1,1,'text','text','text','text','multiple','single',1,'All','All','All',NULL,0,NULL,'2020-01-23 16:23:35',6,'2020-01-23 16:24:20',6);

/*Table structure for table `menu_counter` */

DROP TABLE IF EXISTS `menu_counter`;

CREATE TABLE `menu_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `acs_users_id` int(11) NOT NULL,
  `client_ip` text NOT NULL,
  `client_browser` text NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_counter` */

/*Table structure for table `menu_image` */

DROP TABLE IF EXISTS `menu_image`;

CREATE TABLE `menu_image` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `file_name` varchar(50) NOT NULL,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `file_type` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`menu_id`),
  CONSTRAINT `menu_image_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_image` */

/*Table structure for table `menu_lang` */

DROP TABLE IF EXISTS `menu_lang`;

CREATE TABLE `menu_lang` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_aliases` varchar(255) NOT NULL DEFAULT '',
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`menu_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `menu_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `menu_lang_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `menu_lang` */

insert  into `menu_lang`(`id`,`menu_id`,`acs_lang_id`,`title`,`title_aliases`,`slug`,`description`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('1912185DF9825295B0D','1912185DF98252958B0','LGEN','Contact','','contact','',6,'2019-12-18 08:35:14',1,'2019-12-18 08:39:05'),
('1912185DF9825295BC1','1912185DF98252958B0','LGID','Contact Us','','contact-us','',6,'2019-12-18 08:35:14',1,'2019-12-18 08:39:05'),
('1912185DF98256173EE','1912185DF98256171B4','LGEN','Portal Customers','','portal-customers','',6,'2019-12-18 08:35:18',1,'2019-12-18 08:49:10'),
('1912185DF982561745C','1912185DF98256171B4','LGID','Customers','','customers','',6,'2019-12-18 08:35:18',1,'2019-12-18 08:49:10'),
('1912185DF98259D1C8C','1912185DF98259D1A67','LGEN','Orders','','orders','',6,'2019-12-18 08:35:21',1,'2019-12-18 08:36:40'),
('1912185DF98259D1CF3','1912185DF98259D1A67','LGID','Orders','','orders','',6,'2019-12-18 08:35:21',1,'2019-12-18 08:36:40'),
('1912185DF982AF5AFF4','1912185DF982AF5AE59','LGEN','Product Item','','product-item','',6,'2019-12-18 08:36:47',1,'2020-01-30 12:00:04'),
('1912185DF982AF5B040','1912185DF982AF5AE59','LGID','Product Item','','product-item','',6,'2019-12-18 08:36:47',1,'2020-01-30 12:00:04'),
('1912185DF982BF2E4C3','1912185DF982BF2E2B8','LGEN','Pages','','pages','',6,'2019-12-18 08:37:03',1,'2019-12-18 08:37:03'),
('1912185DF982BF2E511','1912185DF982BF2E2B8','LGID','Pages','','pages','',6,'2019-12-18 08:37:03',1,'2019-12-18 08:37:03'),
('1912185DF982C950E8B','1912185DF982C950CA0','LGEN','Media','','media','',6,'2019-12-18 08:37:13',1,'2019-12-18 08:37:13'),
('1912185DF982C950EDD','1912185DF982C950CA0','LGID','Media','','media','',6,'2019-12-18 08:37:13',1,'2019-12-18 08:37:13'),
('1912185DF982D2ECE91','1912185DF982D2ECBD6','LGEN','Master','','master','',6,'2019-12-18 08:37:22',1,'2019-12-18 08:37:22'),
('1912185DF982D2ECEFD','1912185DF982D2ECBD6','LGID','Master','','master','',6,'2019-12-18 08:37:22',1,'2019-12-18 08:37:22'),
('1912185DF982DE2DA30','1912185DF982DE2D5F3','LGEN','General Settings','','general-settings','',6,'2019-12-18 08:37:34',1,'2019-12-18 08:57:28'),
('1912185DF982DE2DAFC','1912185DF982DE2D5F3','LGID','General Settings','','general-settings','',6,'2019-12-18 08:37:34',1,'2019-12-18 08:57:28'),
('1912185DF982E6C0FDB','1912185DF982E6C0E2C','LGEN','Administrator','','administrator','',6,'2019-12-18 08:37:42',1,'2019-12-18 08:37:42'),
('1912185DF982E6C1075','1912185DF982E6C0E2C','LGID','Administrator','','administrator','',6,'2019-12-18 08:37:42',1,'2019-12-18 08:37:42'),
('1912185DF9831612BCD','1912185DF9831612747','LGEN','Book a Services','','book-a-services','',6,'2019-12-18 08:38:30',1,'2019-12-19 13:31:22'),
('1912185DF9831612C86','1912185DF9831612747','LGID','','','','',6,'2019-12-18 08:38:30',1,'2019-12-19 13:31:22'),
('1912185DF9832DD2007','1912185DF9832DD1E4F','LGEN','Contact Us','','contact-us','',6,'2019-12-18 08:38:53',1,'2019-12-18 15:38:24'),
('1912185DF9832DD2059','1912185DF9832DD1E4F','LGID','Contact Us','','contact-us','',6,'2019-12-18 08:38:53',1,'2019-12-18 15:38:24'),
('1912185DF98347DBE3F','1912185DF98347DBA6D','LGEN','Customer List','','customer-list','',6,'2019-12-18 08:39:19',1,'2020-01-21 09:36:43'),
('1912185DF98347DBEFD','1912185DF98347DBA6D','LGID','Customer List','','customer-list','',6,'2019-12-18 08:39:19',1,'2020-01-21 09:36:43'),
('1912185DF9835B605A6','1912185DF9835B603E4','LGEN','Customer Address','','customer-address','',6,'2019-12-18 08:39:39',1,'2020-01-21 10:14:59'),
('1912185DF9835B60604','1912185DF9835B603E4','LGID','Customer Address','','customer-address','',6,'2019-12-18 08:39:39',1,'2020-01-21 10:14:59'),
('1912185DF98371E96B5','1912185DF98371E94DE','LGEN','Inquiry Product','','inquiry-product','',6,'2019-12-18 08:40:01',1,'2020-01-21 10:26:49'),
('1912185DF98371E9704','1912185DF98371E94DE','LGID','Inquiry Product','','inquiry-product','',6,'2019-12-18 08:40:01',1,'2020-01-21 10:26:49'),
('1912185DF9838C62FC0','1912185DF9838C62DC5','LGEN','Orders Product','','orders-product','',6,'2019-12-18 08:40:28',1,'2020-01-22 08:38:09'),
('1912185DF9838C6301B','1912185DF9838C62DC5','LGID','Orders Product','','orders-product','',6,'2019-12-18 08:40:28',1,'2020-01-22 08:38:09'),
('1912185DF983BDF2B07','1912185DF983BDF2925','LGEN','Home Banner','','home-banner','',6,'2019-12-18 08:41:17',1,'2020-01-24 14:07:12'),
('1912185DF983BDF2B5D','1912185DF983BDF2925','LGID','Home Banner','','home-banner','',6,'2019-12-18 08:41:17',1,'2020-01-24 14:07:12'),
('1912185DF983D281328','1912185DF983D281175','LGEN','Page Banner','','page-banner','',6,'2019-12-18 08:41:38',1,'2020-01-22 13:55:25'),
('1912185DF983D281376','1912185DF983D281175','LGID','Page Banner','','page-banner','',6,'2019-12-18 08:41:38',1,'2020-01-22 13:55:25'),
('1912185DF983F2AF0BC','1912185DF983F2AEEF4','LGEN','Mastrada on Youtube','','mastrada-on-youtube','',6,'2019-12-18 08:42:10',1,'2020-01-17 14:03:58'),
('1912185DF983F2AF173','1912185DF983F2AEEF4','LGID','Mastrada on Youtube','','mastrada-on-youtube','',6,'2019-12-18 08:42:10',1,'2020-01-17 14:03:58'),
('1912185DF9841ED36AC','1912185DF9841ED34E4','LGEN','Promotions','','promotions','',6,'2019-12-18 08:42:54',1,'2020-01-27 10:35:40'),
('1912185DF9841ED36F9','1912185DF9841ED34E4','LGID','Promotions','','promotions','',6,'2019-12-18 08:42:54',1,'2020-01-27 10:35:40'),
('1912185DF9844950ECD','1912185DF9844950D3A','LGEN','Branch Office','','branch-office','',6,'2019-12-18 08:43:37',1,'2020-01-23 15:53:51'),
('1912185DF9844950F1A','1912185DF9844950D3A','LGID','Branch Office','','branch-office','',6,'2019-12-18 08:43:37',1,'2020-01-23 15:53:51'),
('1912185DF984FA7F76D','1912185DF984FA7F533','LGEN','About','','about','',6,'2019-12-18 08:46:34',1,'2019-12-18 08:46:34'),
('1912185DF984FA7F7BC','1912185DF984FA7F533','LGID','About','','about','',6,'2019-12-18 08:46:34',1,'2019-12-18 08:46:34'),
('1912185DF98507BDF4C','1912185DF98507BDD83','LGEN','Services','','services','',6,'2019-12-18 08:46:47',1,'2020-01-22 13:47:16'),
('1912185DF98507BDFA0','1912185DF98507BDD83','LGID','Services','','services','',6,'2019-12-18 08:46:47',1,'2020-01-22 13:47:16'),
('1912185DF9858590BBE','1912185DF98585908AE','LGEN','News & Events','','news-events','',6,'2019-12-18 08:48:53',1,'2019-12-18 08:48:53'),
('1912185DF9858590C59','1912185DF98585908AE','LGID','News & Events','','news-events','',6,'2019-12-18 08:48:53',1,'2019-12-18 08:48:53'),
('1912185DF985A88D7FA','1912185DF985A88D60A','LGEN','Customer','','customer','',6,'2019-12-18 08:49:28',1,'2019-12-18 08:49:28'),
('1912185DF985A88D853','1912185DF985A88D60A','LGID','Customer','','customer','',6,'2019-12-18 08:49:28',1,'2019-12-18 08:49:28'),
('1912185DF985C4903AE','1912185DF985C490138','LGEN','About Mastrada','','about-mastrada','',6,'2019-12-18 08:49:56',1,'2020-01-17 14:18:27'),
('1912185DF985C49044D','1912185DF985C490138','LGID','About Mastrada','','about-mastrada','',6,'2019-12-18 08:49:56',1,'2020-01-17 14:18:27'),
('1912185DF985D6F2129','1912185DF985D6F1F32','LGEN','Board Management','','board-management','',6,'2019-12-18 08:50:14',1,'2020-01-17 14:32:42'),
('1912185DF985D6F2187','1912185DF985D6F1F32','LGID','Board Management','','board-management','',6,'2019-12-18 08:50:14',1,'2020-01-17 14:32:42'),
('1912185DF985F155AC0','1912185DF985F155869','LGEN','News','','news','',6,'2019-12-18 08:50:41',1,'2020-01-20 15:37:34'),
('1912185DF985F155B14','1912185DF985F155869','LGID','News','','news','',6,'2019-12-18 08:50:41',1,'2020-01-20 15:37:34'),
('1912185DF985FDE5F96','1912185DF985FDE5D9F','LGEN','Events','','events','',6,'2019-12-18 08:50:53',1,'2020-01-24 15:21:32'),
('1912185DF985FDE5FFC','1912185DF985FDE5D9F','LGID','Events','','events','',6,'2019-12-18 08:50:53',1,'2020-01-24 15:21:32'),
('1912185DF986180936C','1912185DF98618091AD','LGEN','Clients','','clients','',6,'2019-12-18 08:51:20',1,'2020-01-20 15:39:33'),
('1912185DF98618093BF','1912185DF98618091AD','LGID','Clients','','clients','',6,'2019-12-18 08:51:20',1,'2020-01-20 15:39:33'),
('1912185DF986304E0B4','1912185DF986304DDA1','LGEN','Client Success Story','','client-success-story','',6,'2019-12-18 08:51:44',1,'2020-01-24 15:25:02'),
('1912185DF986304E15D','1912185DF986304DDA1','LGID','Client Success Story','','client-success-story','',6,'2019-12-18 08:51:44',1,'2020-01-24 15:25:02'),
('1912185DF98690C961C','1912185DF98690C8FA5','LGEN','Careers','','careers','',6,'2019-12-18 08:53:20',1,'2020-01-06 14:11:09'),
('1912185DF98690C96AE','1912185DF98690C8FA5','LGID','Careers','','careers','',6,'2019-12-18 08:53:20',1,'2020-01-06 14:11:09'),
('1912185DF986A67046D','1912185DF986A6700B4','LGEN','FAQ','','faq','',6,'2019-12-18 08:53:42',1,'2020-01-22 14:28:54'),
('1912185DF986A6704D0','1912185DF986A6700B4','LGID','FAQ','','faq','',6,'2019-12-18 08:53:42',1,'2020-01-22 14:28:54'),
('1912185DF987023D078','1912185DF987023CC3A','LGEN','Other Pages','','other-pages','',6,'2019-12-18 08:55:14',1,'2020-01-23 14:45:15'),
('1912185DF987023D108','1912185DF987023CC3A','LGID','Other Pages','','other-pages','',6,'2019-12-18 08:55:14',1,'2020-01-23 14:45:15'),
('1912185DF98714134B2','1912185DF987141323F','LGEN','User Management','','user-management','',6,'2019-12-18 08:55:32',1,'2020-01-29 11:55:02'),
('1912185DF987141350D','1912185DF987141323F','LGID','User Management','','user-management','',6,'2019-12-18 08:55:32',1,'2020-01-29 11:55:02'),
('1912185DF987263000D','1912185DF987262FE31','LGEN','Access Level','','access-level','',6,'2019-12-18 08:55:50',1,'2020-01-29 11:55:17'),
('1912185DF9872630062','1912185DF987262FE31','LGID','Access Level','','access-level','',6,'2019-12-18 08:55:50',1,'2020-01-29 11:55:17'),
('1912195DFAF689BB4E8','1912195DFAF689B8322','LGEN','Contact Subject','','contact-subject','',6,'2019-12-19 11:03:21',1,'2019-12-19 11:06:40'),
('1912195DFAF689BB70A','1912195DFAF689B8322','LGID','Contact Subject','','contact-subject','',6,'2019-12-19 11:03:21',1,'2019-12-19 11:06:40'),
('1912195DFAF6BC9BB07','1912195DFAF6BC991C7','LGEN','Service Type','','service-type','',6,'2019-12-19 11:04:12',1,'2019-12-19 13:23:20'),
('1912195DFAF6BC9BB84','1912195DFAF6BC991C7','LGID','Service Type','','service-type','',6,'2019-12-19 11:04:12',1,'2019-12-19 13:23:20'),
('2001025E0D71F257F29','2001025E0D71F255709','LGEN','Product','','product','',6,'2020-01-02 11:30:42',1,'2020-01-02 11:31:35'),
('2001025E0D71F258018','2001025E0D71F255709','LGID','Product','','product','',6,'2020-01-02 11:30:42',1,'2020-01-02 11:31:35'),
('2001075E142E218D797','2001075E142E218A025','LGEN','Contact Image','','contact-image','',6,'2020-01-07 14:07:13',1,'2020-01-27 16:32:47'),
('2001075E142E218D824','2001075E142E218A025','LGID','Contact Image','','contact-image','',6,'2020-01-07 14:07:13',1,'2020-01-27 16:32:47'),
('2001085E152FE28496A','2001085E152FE2811FB','LGEN','Promotion Management','','promotion-management','',6,'2020-01-08 08:26:58',1,'2020-01-08 08:27:38'),
('2001085E152FE284A6B','2001085E152FE2811FB','LGID','Promotion Management','','promotion-management','',6,'2020-01-08 08:26:58',1,'2020-01-08 08:27:38'),
('2001085E15302B80C96','2001085E15302B7D589','LGEN','Promo Category','','promo-category','',6,'2020-01-08 08:28:11',1,'2020-01-08 08:29:24'),
('2001085E15302B80D54','2001085E15302B7D589','LGID','Promo Category','','promo-category','',6,'2020-01-08 08:28:11',1,'2020-01-08 08:29:24'),
('2001085E1530614D18A','2001085E15306149976','LGEN','Promo Code','','promo-code','',6,'2020-01-08 08:29:05',1,'2020-01-08 08:29:05'),
('2001085E1530614D274','2001085E15306149976','LGID','Promo Code','','promo-code','',6,'2020-01-08 08:29:05',1,'2020-01-08 08:29:05'),
('2001175E215F3688CEA','2001175E215F36889DE','LGEN','History','','history','',6,'2020-01-17 14:16:06',1,'2020-01-17 14:16:26'),
('2001175E215F3688DF0','2001175E215F36889DE','LGID','Sejarah','','sejarah','',6,'2020-01-17 14:16:06',1,'2020-01-17 14:16:26'),
('2001205E24993BC0541','2001205E24993BBD939','LGEN','Branch Category','','branch-category','',6,'2020-01-20 01:00:27',1,'2020-01-23 15:49:29'),
('2001205E24993BC0606','2001205E24993BBD939','LGID','Branch Category','','branch-category','',6,'2020-01-20 01:00:27',1,'2020-01-23 15:49:29'),
('2001205E249BD46C9BD','2001205E249BD4696A7','LGEN','Product Brand','','product-brand','',6,'2020-01-20 01:11:32',1,'2020-01-23 08:43:20'),
('2001205E249BD46CA81','2001205E249BD4696A7','LGID','Product Brand','','product-brand','',6,'2020-01-20 01:11:32',1,'2020-01-23 08:43:20'),
('2001205E249BEFC93FA','2001205E249BEFC58CB','LGEN','Product Tags','','product-tags','',6,'2020-01-20 01:11:59',1,'2020-01-23 08:43:57'),
('2001205E249BEFC953C','2001205E249BEFC58CB','LGID','Product Tags','','product-tags','',6,'2020-01-20 01:11:59',1,'2020-01-23 08:43:57'),
('2001225E27A75AED016','2001225E27A75AECB24','LGEN','Courier','','courier','',6,'2020-01-22 08:37:30',1,'2020-01-22 08:37:30'),
('2001225E27A75AED1A0','2001225E27A75AECB24','LGID','Courier','','courier','',6,'2020-01-22 08:37:30',1,'2020-01-22 08:37:30'),
('2001225E27FA3BD6724','2001225E27FA3BD6214','LGEN','Tags Faq','','tags-faq','',6,'2020-01-22 14:31:07',1,'2020-01-23 15:19:54'),
('2001225E27FA3BD68C8','2001225E27FA3BD6214','LGID','Tags Faq','','tags-faq','',6,'2020-01-22 14:31:07',1,'2020-01-23 15:19:54'),
('2001235E29661744699','2001235E296617442DE','LGEN','Product Category','','product-category','',6,'2020-01-23 16:23:35',1,'2020-01-23 16:24:20'),
('2001235E296617447E7','2001235E296617442DE','LGID','Product Category','','product-category','',6,'2020-01-23 16:23:35',1,'2020-01-23 16:24:20');

/*Table structure for table `notification_log` */

DROP TABLE IF EXISTS `notification_log`;

CREATE TABLE `notification_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_order_id` varchar(40) NOT NULL,
  `transaction_status` varchar(50) NOT NULL,
  `fraud_status` varchar(50) NOT NULL,
  `payment_type` varchar(50) NOT NULL,
  `transaction_time` varchar(50) DEFAULT NULL,
  `notif` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

/*Data for the table `notification_log` */

/*Table structure for table `order_billing` */

DROP TABLE IF EXISTS `order_billing`;

CREATE TABLE `order_billing` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_billing` */

/*Table structure for table `order_customer` */

DROP TABLE IF EXISTS `order_customer`;

CREATE TABLE `order_customer` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_customer` */

/*Table structure for table `order_shiping` */

DROP TABLE IF EXISTS `order_shiping`;

CREATE TABLE `order_shiping` (
  `id` varchar(40) NOT NULL,
  `product_order_id` varchar(40) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `country_code` varchar(10) DEFAULT NULL,
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `order_shiping` */

/*Table structure for table `post` */

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` varchar(25) NOT NULL,
  `menu_id` varchar(25) DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '1',
  `parent` varchar(25) DEFAULT '0',
  `parent_branch` varchar(25) DEFAULT '0',
  `ordering` smallint(6) DEFAULT NULL,
  `published` datetime NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `flag` int(11) DEFAULT '0',
  `is_featured` int(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  `file_location` varchar(100) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `city` int(11) DEFAULT NULL,
  `subdistrict` int(11) DEFAULT NULL,
  `coupon_id` varchar(25) DEFAULT NULL,
  `event_start` date DEFAULT NULL,
  `event_end` date DEFAULT NULL,
  `author` varchar(100) DEFAULT NULL,
  `can_buy` tinyint(1) DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`menu_id`),
  KEY `post_city_fk` (`city`),
  CONSTRAINT `post_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post` */

/*Table structure for table `post_counter` */

DROP TABLE IF EXISTS `post_counter`;

CREATE TABLE `post_counter` (
  `post_id` varchar(25) NOT NULL,
  `tanggal` datetime NOT NULL,
  `counter` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `post_id` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_counter` */

/*Table structure for table `post_image` */

DROP TABLE IF EXISTS `post_image`;

CREATE TABLE `post_image` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `file_name` text,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_caption` varchar(255) DEFAULT NULL,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `file_type` varchar(50) NOT NULL,
  `status_cover` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_image_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_image` */

/*Table structure for table `post_lang` */

DROP TABLE IF EXISTS `post_lang`;

CREATE TABLE `post_lang` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title_sub` text,
  `short_description` text,
  `description` text,
  `note` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`post_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `post_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_lang_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_lang` */

/*Table structure for table `post_menu` */

DROP TABLE IF EXISTS `post_menu`;

CREATE TABLE `post_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` varchar(25) NOT NULL,
  `menu_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produk_id` (`post_id`),
  KEY `post_kt_id` (`menu_id`),
  CONSTRAINT `post_menu_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_menu_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_menu` */

/*Table structure for table `post_product` */

DROP TABLE IF EXISTS `post_product`;

CREATE TABLE `post_product` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) DEFAULT NULL,
  `sku` varchar(25) DEFAULT NULL,
  `price` bigint(25) DEFAULT '0',
  `price_promo` bigint(25) DEFAULT '0',
  `weight` int(11) DEFAULT '1',
  `stock` bigint(25) DEFAULT '0',
  `sold` int(11) DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  CONSTRAINT `post_product_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_product` */

/*Table structure for table `post_product_variant` */

DROP TABLE IF EXISTS `post_product_variant`;

CREATE TABLE `post_product_variant` (
  `id` varchar(25) NOT NULL,
  `post_product_id` varchar(25) DEFAULT NULL,
  `variant_type_id` varchar(25) DEFAULT NULL,
  `variant` varchar(50) DEFAULT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `variant_type_id` (`variant_type_id`),
  KEY `post_product_id` (`post_product_id`),
  CONSTRAINT `post_product_variant_ibfk_1` FOREIGN KEY (`variant_type_id`) REFERENCES `variant_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_product_variant_ibfk_2` FOREIGN KEY (`post_product_id`) REFERENCES `post_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_product_variant` */

/*Table structure for table `post_room` */

DROP TABLE IF EXISTS `post_room`;

CREATE TABLE `post_room` (
  `id` varchar(50) NOT NULL,
  `post_id` varchar(50) DEFAULT NULL,
  `metres` varchar(25) DEFAULT NULL,
  `feet` varchar(25) DEFAULT NULL,
  `banquet` varchar(25) DEFAULT NULL,
  `classroom` varchar(25) DEFAULT NULL,
  `theatre` varchar(25) DEFAULT NULL,
  `reception` varchar(25) DEFAULT NULL,
  `ctd` datetime DEFAULT NULL,
  `ctb` datetime DEFAULT NULL,
  `mdd` datetime DEFAULT NULL,
  `mdb` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_room` */

/*Table structure for table `post_tags` */

DROP TABLE IF EXISTS `post_tags`;

CREATE TABLE `post_tags` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `post_tags_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `post_tags_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `post_tags` */

/*Table structure for table `post_type` */

DROP TABLE IF EXISTS `post_type`;

CREATE TABLE `post_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `form` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Data for the table `post_type` */

/*Table structure for table `product_brand` */

DROP TABLE IF EXISTS `product_brand`;

CREATE TABLE `product_brand` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `product_brand_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_brand_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_brand` */

/*Table structure for table `product_cart` */

DROP TABLE IF EXISTS `product_cart`;

CREATE TABLE `product_cart` (
  `id` varchar(25) NOT NULL,
  `post_product_id` varchar(25) NOT NULL,
  `user_id` varchar(25) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '1',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `product_order_id` varchar(40) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `product_cart` */

/*Table structure for table `product_order` */

DROP TABLE IF EXISTS `product_order`;

CREATE TABLE `product_order` (
  `id` varchar(40) NOT NULL,
  `order_number` varchar(40) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `address` text,
  `subdistrict_id` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `note` text,
  `courier_code` varchar(25) NOT NULL,
  `courier_package` varchar(25) DEFAULT NULL,
  `coupon_code` varchar(25) DEFAULT NULL,
  `coupon_nominal` bigint(20) DEFAULT NULL,
  `postal_fee` bigint(20) DEFAULT NULL,
  `cart_total` bigint(20) DEFAULT NULL,
  `price_total` bigint(20) DEFAULT NULL,
  `payment_method` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `snap` varchar(50) DEFAULT NULL,
  `status_dropship` tinyint(1) DEFAULT '0',
  `registration_number` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `product_order` */

/*Table structure for table `product_tags` */

DROP TABLE IF EXISTS `product_tags`;

CREATE TABLE `product_tags` (
  `id` varchar(25) NOT NULL,
  `post_id` varchar(25) NOT NULL,
  `tags_id` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `tags_id` (`tags_id`),
  CONSTRAINT `product_tags_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `product_tags_ibfk_2` FOREIGN KEY (`tags_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `product_tags` */

/*Table structure for table `service_type` */

DROP TABLE IF EXISTS `service_type`;

CREATE TABLE `service_type` (
  `id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  `price` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `service_type` */

/*Table structure for table `services` */

DROP TABLE IF EXISTS `services`;

CREATE TABLE `services` (
  `id` varchar(25) NOT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `name` varchar(150) NOT NULL,
  `company` varchar(50) DEFAULT NULL,
  `company_address` text,
  `city` int(11) DEFAULT NULL,
  `province` int(11) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `brand_id` varchar(25) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `product_serial_number` varchar(50) DEFAULT NULL,
  `product_invoice_number` varchar(50) DEFAULT NULL,
  `product_store_name` varchar(50) DEFAULT NULL,
  `product_store_address` text,
  `product_city` int(11) DEFAULT NULL,
  `product_province` int(11) DEFAULT NULL,
  `pic_name` varchar(50) DEFAULT NULL,
  `pic_phone` varchar(50) DEFAULT NULL,
  `pic_email` varchar(50) DEFAULT NULL,
  `file_location` varchar(100) DEFAULT NULL,
  `file_thumb` varchar(100) DEFAULT NULL,
  `file_original` varchar(100) DEFAULT NULL,
  `service_type_id` varchar(25) DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `service_problem` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` varchar(25) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` varchar(25) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `services` */

/*Table structure for table `tags` */

DROP TABLE IF EXISTS `tags`;

CREATE TABLE `tags` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) NOT NULL DEFAULT '0',
  `tags_kt_id` varchar(25) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `file_location` varchar(50) DEFAULT NULL,
  `file_original` varchar(50) DEFAULT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_tags_kt_id` (`tags_kt_id`),
  CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags` */

/*Table structure for table `tags_kt` */

DROP TABLE IF EXISTS `tags_kt`;

CREATE TABLE `tags_kt` (
  `id` varchar(25) NOT NULL,
  `parent` varchar(25) DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `status_mode` enum('post','product','tags','other') NOT NULL,
  `status_single` tinyint(1) NOT NULL DEFAULT '0',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt` */

/*Table structure for table `tags_kt_image` */

DROP TABLE IF EXISTS `tags_kt_image`;

CREATE TABLE `tags_kt_image` (
  `id` varchar(25) NOT NULL,
  `tags_kt_id` varchar(25) NOT NULL,
  `file_name` text,
  `file_location` text,
  `file_thumb` text,
  `file_original` text,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`tags_kt_id`),
  CONSTRAINT `tags_kt_image_ibfk_1` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt_image` */

/*Table structure for table `tags_kt_lang` */

DROP TABLE IF EXISTS `tags_kt_lang`;

CREATE TABLE `tags_kt_lang` (
  `id` varchar(25) NOT NULL,
  `tags_kt_id` varchar(25) NOT NULL,
  `acs_lang_id` varchar(25) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `post_kt_id` (`tags_kt_id`),
  KEY `sys_lang_id` (`acs_lang_id`),
  CONSTRAINT `tags_kt_lang_ibfk_1` FOREIGN KEY (`acs_lang_id`) REFERENCES `acs_lang` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tags_kt_lang_ibfk_2` FOREIGN KEY (`tags_kt_id`) REFERENCES `tags_kt` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tags_kt_lang` */

/*Table structure for table `variant_type` */

DROP TABLE IF EXISTS `variant_type`;

CREATE TABLE `variant_type` (
  `id` varchar(25) NOT NULL,
  `name` text,
  `status` tinyint(1) DEFAULT '1',
  `ctb` int(11) NOT NULL,
  `ctd` datetime NOT NULL,
  `mdb` int(11) NOT NULL,
  `mdd` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `variant_type` */

insert  into `variant_type`(`id`,`name`,`status`,`ctb`,`ctd`,`mdb`,`mdd`) values 
('2001025E0DBFD296981','Size',1,6,'2020-01-02 17:02:58',6,'2020-01-02 17:02:58');

/* Function  structure for function  `strip_tags` */

/*!50003 DROP FUNCTION IF EXISTS `strip_tags` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `strip_tags`($str TEXT) RETURNS text CHARSET latin1
BEGIN

	    DECLARE $START, $END INT DEFAULT 1;
	    LOOP
		SET $START = LOCATE("<", $str, $START);
		IF (!$START) THEN RETURN $str; END IF;
		SET $END = LOCATE(">", $str, $START);
		IF (!$END) THEN SET $END = $START; END IF;
		SET $str = INSERT($str, $START, $END - $START + 1, "");
	    END LOOP;
    END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
